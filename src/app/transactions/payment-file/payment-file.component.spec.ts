import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PaymentFileGenerationComponent } from './payment-file.component';

describe('PaymentFileGenerationComponent', () => {
  let component: PaymentFileGenerationComponent;
  let fixture: ComponentFixture<PaymentFileGenerationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PaymentFileGenerationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PaymentFileGenerationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
