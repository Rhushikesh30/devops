import { Component, OnInit,ViewChild   } from '@angular/core';
import Swal from 'sweetalert2';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { ManageSecurityService } from 'src/app/core/service/manage-security.service';
import { PaymentFileMasterElement } from 'src/app/shared/models/payment-file'
import { PaymentFileServiceService } from 'src/app/shared/services/payment-file-service.service';
import { ErrorCodes } from 'src/app/shared/error-codes';

@Component({
  selector: 'app-payment-file',
  templateUrl: './payment-file.component.html',
  styleUrls: ['./payment-file.component.sass'],
  providers: [ErrorCodes]

})
export class PaymentFileGenerationComponent implements OnInit {
  @ViewChild('dataSourcePaginator',{read: MatPaginator}) dataSourcePaginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  dataSource :MatTableDataSource<PaymentFileMasterElement>;
  resultData: PaymentFileMasterElement[];

  displayColumns = [
    'actions',
    'biller_name',
    'from_date',
    'to_date',
    'total_transaction_amount',
    'transaction_date',
    'File_Status'
  ];
  renderedData: PaymentFileMasterElement[];
  screenName = 'Bulk Payment File';
  submitBtn = true;
  showLoader = false;
  rowData = [];
  listDiv:boolean = false;
  showList:boolean = true;
  sidebarData: any;
  paymentFileData =[];
  editFlag = '';
  Company_type: string;
  newSourceData: any = [];

  constructor(  private manageSecurity:ManageSecurityService, 
                private PaymentFileServiceService:PaymentFileServiceService,
                private errorCodes: ErrorCodes) { }

  ngOnInit(): void {
    this.Company_type = localStorage.getItem('COMPANY_TYPE')
    this.getAllData();
    let userId = localStorage.getItem('USER_ID');

    this.manageSecurity.getAccessLeftPanel(userId, this.screenName).subscribe({
      next:data =>{
        this.sidebarData =data;
      }
    });
  }

  editViewRecord(row,flag){
    this.rowData = row;
    this.showList = false;
    this.submitBtn = flag;
    this.listDiv = true;
    this.showLoader = false;
    this.editFlag = flag;
  }

  showFormList(item: boolean) {
    if(item ===false){
      this.listDiv = true;
      this.showList = false;
    }
    else{
      this.listDiv = false;
      this.showList = true;
    }
  }

  onCancel(item: boolean){
    this.listDiv = item;
    this.showList = true;
    this.submitBtn = true;
    this.rowData = [];
  }

  getAllData(){
    this.PaymentFileServiceService.getAll().subscribe({
      next:(data: PaymentFileMasterElement[])=>{
        this.paymentFileData = data;
        for (let indexValue of this.paymentFileData) {
            const element = indexValue;
            if (element.status == 'Permanent Save') {
              this.newSourceData.push(element.status);
            }
        }
        this.dataSource = new MatTableDataSource(data);
        this.dataSource.paginator = this.dataSourcePaginator;
        this.dataSource.sort = this.sort;
        this.dataSource.connect().subscribe(d => this.renderedData = d);
      }
    });
  }
  
  tbl_FilterDatatable(value:string) {
    this.dataSource.filter = value.trim().toLocaleLowerCase();
  }

showSwalmessage(message, text, icon, confirmButton): void {
  if (confirmButton === false) {
      Swal.fire({ title: message, text: text, icon: icon, timer: 2000, showConfirmButton: false });
  } else {
      Swal.fire({ title: message, text: text, icon: icon, timer: 2000, showConfirmButton: true, confirmButtonText: "Yes", cancelButtonText: "No" });
  }
}

onSave(formValue){
  this.showLoader = true;

  this.PaymentFileServiceService.create(formValue).subscribe({
    next: (data:any) => {
      if (data['status'] == 2) {
        this.showSwalmessage('Your record has been added successfully!','','success',false)
      }
      else if (data['status'] == 1) {
        this.showSwalmessage('Your record has been updated successfully!','','success',false)
      }
      this.getAllData();
      this.showList = true;
      this.listDiv = false;
      this.showLoader = false;      
    },
    error:(error) => {
      this.showLoader = false;
      this.showSwalmessage(this.errorCodes.getErrorMessage(error.status), error.error, 'error', true)
    }
  })
}

}
