import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PaymentFileDetailsComponent } from './payment-file-details.component';

describe('PaymentFileDetailsComponent', () => {
  let component: PaymentFileDetailsComponent;
  let fixture: ComponentFixture<PaymentFileDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PaymentFileDetailsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PaymentFileDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
