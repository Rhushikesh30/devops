import { Component, ViewChild, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormArray, FormControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DatePipe } from '@angular/common'
import { PaymentFileMasterElement } from 'src/app/shared/models/payment-file';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import Swal from 'sweetalert2';
import { PaymentFileServiceService } from 'src/app/shared/services/payment-file-service.service';
import { ErrorCodes } from 'src/app/shared/error-codes';

@Component({
  selector: 'app-payment-file-details',
  templateUrl: './payment-file-details.component.html',
  styleUrls: ['./payment-file-details.component.sass'],
  providers: [DatePipe, ErrorCodes]
})
export class PaymentFileDetailsComponent implements OnInit {

  @Input() rowData: [];
  @Input() editFlag:string;
  @Input() submitBtn:boolean;
  @Input() showLoader:boolean;
  @Input() billerData: any;
  @Output() onSave = new EventEmitter<any>();
  @Output() onCancel = new EventEmitter<any>();

  @ViewChild('dataSourcePaginator',{read: MatPaginator}) dataSourcePaginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  dataSource :MatTableDataSource<PaymentFileMasterElement>;
  resultData: PaymentFileMasterElement[];

  btnVal = 'Submit';
  cancelFlag=true 
  paymentFileForm: FormGroup;
  newPayload = [];  
  todayDate : string;
  todays_date = new Date().toJSON().split('T')[0];
  fromMinDate : string;
  BillerNameValue:string;
  companyData: any = [];
  company_type:string;
  btnValue =  'Temporary Save';
  billerName:any;
  DebitAccNo:any;
  TotalTransaction:any;
  Permanent_Save_total_total_payment_amount:any
  renderedData: PaymentFileMasterElement[];
  disabledBillerDropdown:boolean = false;
  disabledFrom_date:boolean = true;
  CustomerRefNumber:any;
  paymentFilterData: any = [];
  tbl_FilteredData = [];
  billerdata=[]
  newInit: any = [];
  TransactionData: any = [];
  BankDetailsaData: any = [];
  details_table_data: any = [];
  percentage_share:number;
  debit_acc: any = [];
  isReadOnly = true;
  total_transaction_amount:boolean = true;
  Permanent_Save_total_transaction_amount:any;
  public getBillertoSearch: FormControl = new FormControl();

  constructor(
    private formBuilder: FormBuilder,
    private datePipe: DatePipe,
    private PaymentFileServiceService: PaymentFileServiceService,
    private errorCodes: ErrorCodes) { }

  ngOnInit(): void {
      this.company_type = localStorage.getItem('COMPANY_TYPE');
      let prev_date = new Date();
      prev_date.setDate(prev_date.getDate() - 1);
      this.todayDate=prev_date.toJSON().split('T')[0]
      let form_date = new Date();
      form_date.setDate(form_date.getDate() - 5);
      this.fromMinDate=form_date.toJSON().split('T')[0]      

      this.paymentFileForm = this.formBuilder.group({
      id: [''],
      biller_ref_id:['',Validators.required],
      payment_ref_no:['', Validators.maxLength(30)],
      total_transaction_amount: ['', [Validators.required, Validators.min(1)]],
      transaction_date: [this.todays_date],
      from_date:  ['', Validators.required],
      to_date:  [this.todayDate],
      remark:['', Validators.maxLength(30)],
      status: [''],
      initialItemRow: this.formBuilder.array([this.initialitemRow()])
    });

    if(!Array.isArray(this.rowData)){
      this.editViewRecord(this.rowData, this.editFlag);
    }

    this.getBillertoSearch.valueChanges.subscribe({
      next:val=>{
        this.BillerNameValue=val;
      }
    })

    this.PaymentFileServiceService.getBillersAgg(this.company_type).subscribe({
      next:data => {
        this.companyData = data; 
      }    
    });

    this.paymentFileForm.get('biller_ref_id').valueChanges.subscribe(val =>{
      this.billerChangFunction(val)
    })

    this.paymentFileForm.get('from_date').valueChanges.subscribe(val =>{
      this.fromDateChangeFunction(val)
    })

    this.paymentFileForm.get('total_transaction_amount').valueChanges.subscribe(val =>{  
      this.totalTransactionAmountFunction(val)
    })
  }

  fromDateChangeFunction(val){
    if(this.btnValue == 'Temporary Save'){
      if (val!==''){
        let biller_ref_id = this.paymentFileForm.get("biller_ref_id").value
        let formDate_pass = this.datePipe.transform(val, "yyyy-MM-dd")
        let to_date_pass = this.paymentFileForm.get("to_date").value
        this.PaymentFileServiceService.getBillerPaymentDetails(biller_ref_id, formDate_pass, to_date_pass).subscribe({
          next:(data:any) => {
            this.CustomerRefNumber = data['customer_ref']
            this.billerName = data['biller_name']              
            this.DebitAccNo = data['account_number']   
            this.TotalTransaction = data['payment_amount']
            this.paymentFileForm.get("total_transaction_amount").setValue(this.TotalTransaction);
          }
        });
      }        
    }
    if(this.btnValue == 'Permanent Save'){        
      if (val!==''){
        let biller_ref_id = this.paymentFileForm.get("biller_ref_id").value
        let formDate_pass = this.datePipe.transform(val, "yyyy-MM-dd")
        let to_date_pass = this.paymentFileForm.get("to_date").value
        this.PaymentFileServiceService.getBillerPaymentDetails(biller_ref_id, formDate_pass, to_date_pass).subscribe({
          next:(data:any) => {
            this.CustomerRefNumber = data['customer_ref']
            this.billerName = data['biller_name']  
            this.DebitAccNo = data['account_number']   
          }
        });
      } 
    }
  }

  billerChangFunction(val){
    if(this.btnValue == 'Temporary Save'){
      this.paymentFileForm.get('from_date').setValue('')
      this.paymentFileForm.get("total_transaction_amount").setValue('');
    }      
    if(val!==''){
      this.disabledFrom_date = false;
    }
  }

  totalTransactionAmountFunction(val){
    if(this.btnValue == 'Temporary Save'){
      this.totalTransactionTemporarySFunction(val)
    }

    if(this.btnValue == 'Permanent Save'){
      this.totalTransactionPermanentSFunction(val)
    }
  }

  totalTransactionTemporarySFunction(val){
    if (val!=='' && val!==0){
      this.PaymentFileServiceService.getPaymentBankPercentage(this.paymentFileForm.get("biller_ref_id").value).subscribe({
        next: (data:any) => {
          if(data.length!==0 ){
            let details_schema = [];
            for(let indexValue of data){
              let percentage_share = indexValue['percentage_share'];
              let transacti_amt = (percentage_share * val)/100;
              details_schema.push({
                "transaction_amount": transacti_amt,
                "beneficiary_name": this.billerName,
                "value_date": this.todays_date,
                "ifsc_code": indexValue['ifsc_code'],
                "bene_bank_name": indexValue['bank_name'],
                "beneficiary_accno": indexValue['account_number'],
                "debit_acc_number": this.DebitAccNo,
                "customer_ref_no": this.CustomerRefNumber,
                "zipcode" : indexValue['zipcode'],
                "bene_branch_name" : indexValue['bene_branch_name'],
                "beneficiary_code" : indexValue['beneficiary_code'],
                "remark" : this.paymentFileForm.get("remark").value
              });
            }
            this.paymentFileForm.setControl('initialItemRow', this.setExistingArray(details_schema));
          }
        },
        error:(error) => {
          this.showLoader = false;
          this.showSwalmessage(this.errorCodes.getErrorMessage(error.status), error.error, 'warning', true)
        }
      })
    }
    else{
      while (this.formArray.length) {
        this.formArray.removeAt(-1)
      }
    }
  }

  totalTransactionPermanentSFunction(val){
    if (parseFloat(this.Permanent_Save_total_total_payment_amount)==parseFloat(val)){
      while (this.formArray.length) {
        this.formArray.removeAt(-1)
      }
      this.paymentFileForm.setControl('initialItemRow', this.setExistingArray(this.newPayload));
    }
    else{
      while (this.formArray.length) {
        this.formArray.removeAt(-1)
      }

      this.PaymentFileServiceService.getPaymentBankPercentage(this.paymentFileForm.get("biller_ref_id").value).subscribe({
        next:(data:any) => {
          if(data.length!==0 ){
            let details_schema = [];
            for(let indexValue of data){
              let percentage_share = indexValue['percentage_share'];
              let transacti_amt = (percentage_share * val)/100;
              details_schema.push({
                "transaction_amount": transacti_amt,
                "beneficiary_name": this.billerName,
                "value_date": this.todays_date,
                "ifsc_code": indexValue['ifsc_code'],
                "bene_bank_name": indexValue['bank_name'],
                "beneficiary_accno": indexValue['account_number'],
                "debit_acc_number": this.DebitAccNo,
                "customer_ref_no": this.CustomerRefNumber,
                "zipcode" : indexValue['zipcode'],
                "bene_branch_name" : indexValue['bene_branch_name'],
                "beneficiary_code" : indexValue['beneficiary_code'],
                "remark" : this.paymentFileForm.get("remark").value
              });
            }
            this.paymentFileForm.setControl('initialItemRow', this.setExistingArray(details_schema));
          }
        },error:(error) => {
          this.showLoader = false;
          this.showSwalmessage(this.errorCodes.getErrorMessage(error.status), error.error, 'warning', true)
        }
      });
    }
  }

  showSwalmessage(message, text, icon, confirmButton): void {
    if (! confirmButton) {
        Swal.fire({ title: message, text: text, icon: icon, timer: 2000, showConfirmButton: false });
    } else {
        Swal.fire({ title: message, text: text, icon: icon, timer: 2000, showConfirmButton: true, confirmButtonText: "Yes", cancelButtonText: "No" });
    }
  }

  editViewRecord(payment, editFlag) {
    if(editFlag=='edit'){
      this.btnValue = 'Permanent Save';
    }else{
      this.submitBtn = false;
    }
    this.disabledBillerDropdown = true;
    this.isReadOnly = false; 
    this.total_transaction_amount = false;
    this.newPayload = payment.initialItemRow;  
    this.Permanent_Save_total_transaction_amount = parseFloat(payment.total_transaction_amount)

    this.paymentFileForm.patchValue({
      id: payment.id,
      biller_ref_id: payment.biller_ref_id,
      payment_ref_no:payment.payment_ref_no,
      biller_name: payment.biller_name,
      to_date: payment.to_date,
      from_date: payment.from_date,
      total_transaction_amount: parseFloat(payment.total_transaction_amount),
      transaction_date: payment.transaction_date,    
      remark:payment.remark
    });

    this.paymentFileForm.setControl('initialItemRow', this.setExistingArray(this.newPayload));
    this.disabledFrom_date = true;   
    this.paymentFileForm.get('from_date').clearValidators();   
    this.paymentFileForm.get('from_date').updateValueAndValidity();
  }

  initialitemRow() {
    return this.formBuilder.group({
      id: [''],
      transaction_amount: [''],
      beneficiary_name: [''],
      value_date: [''],
      ifsc_code: [''],
      bene_bank_name: [''],
      beneficiary_accno: [''],
      debit_acc_number: [''],
      customer_ref_no:[''],
      beneficiary_code:[''],
      zipcode:[''],
    });
  }

  get formArray() {
  return this.paymentFileForm.get('initialItemRow') as FormArray;
}

onBillerChange(event){
  this.PaymentFileServiceService.getPaymentBankPercentage(this.paymentFileForm.get("biller_ref_id").value).subscribe((data:any) => {
    this.paymentFileForm.setControl("initialItemRow",this.setExistingArray(data))    
  });
}

deleteRow(index) {
  if (this.formArray.length == 1) {
    return false;
  } else {
    this.formArray.removeAt(index);
    return true;
  }
}  

setExistingArray(initialArray = []): FormArray {
  const formArray = new FormArray([]);
  initialArray.forEach(element => {
    formArray.push(this.formBuilder.group({
      id: element.id,
      transaction_amount: element.transaction_amount,
      beneficiary_name: element. beneficiary_name,
      value_date:element.value_date,
      ifsc_code:element.ifsc_code,
      bene_bank_name:element.bene_bank_name,
      beneficiary_accno:element.beneficiary_accno,
      debit_acc_number:element.debit_acc_number,
      customer_ref_no:element.customer_ref_no,
      beneficiary_code:element.beneficiary_code,
      zipcode:element.zipcode,
    }));
  });
  return formArray;
}

onSubmit() {
  let initialItem = this.paymentFileForm.value.initialItemRow
  let transaction_amount_count:any = 0;
  initialItem.forEach(element => {
    transaction_amount_count += parseFloat(element.transaction_amount);
  });

  if (parseFloat(transaction_amount_count) !== parseFloat(this.paymentFileForm.value.total_transaction_amount)){
    Swal.fire({
      title: 'Bank Transaction Amount Not Match!',
      icon: 'warning',
      timer: 2500,
      showConfirmButton: false
    });
    return false
  }
  if (this.btnValue== 'Temporary Save'){
    let from_date = this.datePipe.transform(this.paymentFileForm.get('from_date').value, "yyyy-MM-dd")
    this.paymentFileForm.value.from_date = from_date
    this.paymentFileForm.value.status = 'Temporary Save';
  } else {
    this.paymentFileForm.value.status = 'Permanent Save';
  }

  if (this.paymentFileForm.invalid) {
    return false; 
  } 
  this.onSave.emit(this.paymentFileForm.value);
}

onCancelForm(){
  this.cancelFlag=false
  this.paymentFileForm.reset();
  this.onCancel.emit(false);
}

}
