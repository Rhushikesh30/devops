import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BillerInvoiceDetailsComponent } from './biller-invoice-details.component';

describe('BillerInvoiceDetailsComponent', () => {
  let component: BillerInvoiceDetailsComponent;
  let fixture: ComponentFixture<BillerInvoiceDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BillerInvoiceDetailsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BillerInvoiceDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
