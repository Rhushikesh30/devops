import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormArray, FormControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BillerService } from 'src/app/core/service/biller.service';
import { DatePipe } from '@angular/common'
import { BillerInvoiceMasterElement } from 'src/app/shared/models/biller-invoice'
import { BillerbankChargesService } from 'src/app/shared/services/billerbank-charges.service';
@Component({
  selector: 'app-biller-invoice-details',
  templateUrl: './biller-invoice-details.component.html',
  styleUrls: ['./biller-invoice-details.component.sass'],
  providers: [DatePipe]
})

export class BillerInvoiceDetailsComponent implements OnInit {
  @Input() rowData: [];
  @Input() editFlag:string;
  @Input() submitBtn:boolean;
  @Input() showLoader:boolean;
  @Input() billerData: any;
  @Output() onSave = new EventEmitter<any>();
  @Output() onCancel = new EventEmitter<any>();

  btnVal = 'Submit';
  cancelFlag=true 
  billerInvoiceForm: FormGroup;
  submitted = false;
  invoiceData = [];
  allBillerData: any = [];
  aggregatorData: any = [];

  OnboardData: any =[];
  currencyData = [];
  transactionData: any = [];
  currentbillerData: any = [];
  adminCompanybbpouBankData: any = [];
  fromDateControl: FormControl;
  myDate = new Date();
  COMPANY_TYPE_REF_ID: string;
  COMPANY_TYPE:any;
  BillerDataForInvoice = [];
  renderedData: BillerInvoiceMasterElement[];
  tbl_FilteredData:any = [];
  onboarding_type: any = [];
  disable = false;
  master_key : any;
  newPayload = [];
  newPayload1 = [];
  platformchargesdata: any;
  BillerNameValue:string
  currentbillerElement:any= ''

  public getBillertoSearch: FormControl = new FormControl();
  constructor(private formBuilder: FormBuilder,
              private BillerService: BillerService, 
              private datePipe: DatePipe,
              private billerbankChargesService: BillerbankChargesService){ }

  ngOnInit(): void {
    this.COMPANY_TYPE_REF_ID = localStorage.getItem('COMPANY_TYPE_REF_ID');
    this.COMPANY_TYPE = localStorage.getItem('COMPANY_TYPE');

    this.billerInvoiceForm = this.formBuilder.group({
      id: [''],
      invoice_ref_no: [''],
      parent_tenant_id: ['', Validators.required],
      invoice_tenant_id: ['', Validators.required],
      transaction_date: [''],      
      fiscal_year: [''],
      period: [''],
      billing_address: ['', [Validators.required, Validators.pattern('^[A-Za-z ]{3,300}$')]],
      currency_ref_id: ['', Validators.required],
      total_amount: [''],
      remark: ['', Validators.required],  
      initialItemRow: this.formBuilder.array([this.initialItemRow()]),
    });

    if(!Array.isArray(this.rowData)){
      this.editViewRecord(this.rowData,this.editFlag);
    }

    this.getBillertoSearch.valueChanges.subscribe({
      next:val=>{
        this.BillerNameValue=val;
      }
    })  

    this.BillerService.getCurrencyData().subscribe({
      next:data => {
        this.currencyData = data;      
      }
    })
    
    this.billerbankChargesService.getAllBillersDataBYID(this.COMPANY_TYPE).subscribe({
      next:(data: any) => {
        this.allBillerData = data;
      }
    });

    this.billerbankChargesService.getAllAggregatorsData('Aggregator', this.COMPANY_TYPE).subscribe({
      next:(data: any) => {
        this.aggregatorData = data;
      }
    });
  }

  changedBiller(value){
    this.BillerService.getCurrentBillerTransactionData(value).subscribe({
      next:data => {
        while (this.formArray.length) {
          this.formArray.removeAt(-1)
        }
        this.formArray.push(this.initialItemRow());
        if(data.length !=0){
          this.billerInvoiceForm.get('invoice_ref_no').setValue(data[0]['invoice_ref_no'])
          this.billerInvoiceForm.get('fiscal_year').setValue(data[0]['fiscal_year'])
          this.billerInvoiceForm.get('period').setValue(data[0]['period'])
          this.billerInvoiceForm.get('total_amount').setValue(data[0]['total_amount'])
          this.billerInvoiceForm.setControl("initialItemRow",this.setExistingArray(data))
        }else{
          this.billerInvoiceForm.get('invoice_ref_no').setValue('')
          this.billerInvoiceForm.get('fiscal_year').setValue('')
          this.billerInvoiceForm.get('period').setValue('')
          this.billerInvoiceForm.get('total_amount').setValue('')
        }
      }
    });
  }

  initialItemRow(){
      return this.formBuilder.group({
        total_no_transaction: ['', Validators.required],
        total_basic_amount:['', Validators.required], 
        cgst_amount:['', Validators.required], 
        igst_amount:['', Validators.required], 
        sgst_amount:['', Validators.required], 
        total_tax_amount:['',Validators.required]
    });
  }

  get formArray() {
    return this.billerInvoiceForm.get('initialItemRow') as FormArray;
  }

  addNewRow() {
    (document.getElementById('add_button') as HTMLInputElement).disabled = true;
    this.formArray.push(this.initialItemRow());
  }

  deleteRow(index) {
    if (this.formArray.length == 1) {
      return false;
    } else {
      this.formArray.removeAt(index);
      return true;
    }
  }

  setExistingArray(initialArray = []): FormArray {
    const formArray = new FormArray([]);
    initialArray.forEach(element => {
      formArray.push(this.formBuilder.group({
        total_no_transaction: element.total_no_transaction,
        total_basic_amount:element.total_basic_amount,
        cgst_amount:element.cgst_amount,
        igst_amount:element.igst_amount,
        sgst_amount:element.sgst_amount,
        total_tax_amount: element.total_tax_amount
      }));
    });
    return formArray;
  }

  editViewRecord(row, editFlag) {
    this.BillerService.getOnboardType(row.id).subscribe({
      next:(data: any) => {
        this.submitBtn = false;
        this.billerInvoiceForm.patchValue({
          id: data.id,
          invoice_ref_no: data.invoice_ref_no,
          parent_tenant_id: data.parent_tenant_id,          
          invoice_tenant_id: data.invoice_tenant_id,
          fiscal_year: data.fiscal_year,
          period: data.period,
          billing_address: data.billing_address,     
          currency_ref_id: data.currency_ref_id,
          total_amount: data.total_amount,
          remark: data.remark          
        });
        this.billerInvoiceForm.setControl('initialItemRow', this.setExistingArray(data.initialItemRow));     
      }
    });
    this.showLoader = false;
  }

  onSubmit() {   
    this.billerInvoiceForm.get('transaction_date').setValue(this.datePipe.transform(this.myDate, 'YYYY-MM-dd'));
    this.onSave.emit(this.billerInvoiceForm.value);
  } 

  onCancelForm(){
    this.cancelFlag=false
    this.billerInvoiceForm.reset();
    this.onCancel.emit(false);
  }
}
