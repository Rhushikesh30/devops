import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BillerInvoiceComponent } from './biller-invoice.component';

describe('BillerInvoiceComponent', () => {
  let component: BillerInvoiceComponent;
  let fixture: ComponentFixture<BillerInvoiceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BillerInvoiceComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BillerInvoiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
