import { Component, OnInit, ViewChild } from '@angular/core';
import { BillerService } from 'src/app/core/service/biller.service';
import Swal from 'sweetalert2';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import * as XLSX from 'xlsx';
import { ManageSecurityService } from 'src/app/core/service/manage-security.service';
import { BillerInvoiceMasterElement } from 'src/app/shared/models/biller-invoice'
import { ErrorCodes } from 'src/app/shared/error-codes';

@Component({
  selector: 'app-biller-invoice',
  templateUrl: './biller-invoice.component.html',
  styleUrls: ['./biller-invoice.component.sass'],
  providers: [ErrorCodes]
})

export class BillerInvoiceComponent implements OnInit {
  @ViewChild('dataSourcePaginator',{read: MatPaginator}) dataSourcePaginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  dataSource :MatTableDataSource<BillerInvoiceMasterElement>;
  resultData: BillerInvoiceMasterElement[];
  displayColumns = [
    'actions',
    'invoice_tenant_id',
    'transaction_date',
    'period',
    'fiscal_year',
    'invoice_ref_no',
    'currency_ref_id',
    'remark'    
  ]
  renderedData: BillerInvoiceMasterElement[];
  submitBtn = true;
  showLoader = false;
  rowData = [];
  listDiv:boolean = false;
  showList:boolean = true;
  sidebarData: any;
  billerData =[];
  editFlag = '';
  Screen_Type: string;
  screenName = 'Biller Invoice'
  listingDataCheck:boolean = true;

  constructor( 
              private BillerService: BillerService, 
              private manageSecurity:ManageSecurityService,
              private errorCodes: ErrorCodes) { }

  ngOnInit(): void {    
    this.getAllData();
    this.manageSecurity.getAccessLeftPanel(localStorage.getItem('USER_ID'), this.screenName).subscribe({
      next:data =>{
        this.sidebarData = data;
      }
    });
  }

  editViewRecord(row,flag){
    this.rowData = row;
    this.showList = false;
    this.submitBtn = flag;
    this.listDiv = true;
    this.showLoader = false;
    this.editFlag = flag
  }

  showFormList(item: boolean) {
    if(item ===false){
      this.listDiv = true;
      this.showList = false;
    }
    else{
      this.listDiv = false;
      this.showList = true;
    }
  }

  onCancel(item: boolean){
    this.listDiv = item;
    this.showList = true;
    this.submitBtn = true;
    this.rowData = [];
  }

  getAllData(){
    this.BillerService.getAll(localStorage.getItem('COMPANY_TYPE')).subscribe({
      next:(data: BillerInvoiceMasterElement[])=>{
        if(data.length==0){
          this.listingDataCheck=false;
        }
        this.dataSource = new MatTableDataSource(data);
        this.dataSource.paginator = this.dataSourcePaginator;
        this.dataSource.sort = this.sort;
        this.dataSource.connect().subscribe(d => this.renderedData = d);
        this.billerData = data;
      }
    });
  }

  tbl_FilterDatatable(value:string) {
    this.dataSource.filter = value.trim().toLocaleLowerCase();
  }

  showSwalmessage(message, text, icon, confirmButton): void {
    if (confirmButton === false) {
        Swal.fire({ title: message, text: text, icon: icon, timer: 2000, showConfirmButton: false });
    } else {
        Swal.fire({ title: message, text: text, icon: icon, timer: 2000, showConfirmButton: true, confirmButtonText: "Yes", cancelButtonText: "No" });
    }
  }

  onSave(formValue){    
    this.showLoader = true;
    this.BillerService.saveBillerInvoiceData(formValue).subscribe({
      next: (data:any) => {
        this.showSwalmessage('Your record has been added successfully!','','success',false)
        this.getAllData();
        this.showList = true;
        this.listDiv = false;
        this.showLoader = false;
      },
      error:(error) => {
        this.showLoader = false;
        this.showSwalmessage(this.errorCodes.getErrorMessage(error.status), error.error, 'error', true)
      }
    })
  }

  export_to_excel() {
    let XlsMasterData = this.dataSource.data
    const workSheet = XLSX.utils.json_to_sheet(XlsMasterData);
    const workBook: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(workBook, workSheet, 'SheetName');
    XLSX.writeFile(workBook, 'Biller-Invoice.xlsx');
  }

}
