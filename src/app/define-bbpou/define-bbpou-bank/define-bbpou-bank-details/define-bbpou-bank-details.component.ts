import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AllmasterService } from 'src/app/shared/services/allmaster.service';
import { BbpouBankService } from 'src/app/shared/services/bbpou-bank.service';
import { CurrencyService } from 'src/app/shared/services/currency.service';
import { ErrorCodes } from 'src/app/shared/error-codes';
import Swal from 'sweetalert2';
@Component({
  selector: 'app-define-bbpou-bank-details',
  templateUrl: './define-bbpou-bank-details.component.html',
  styleUrls: ['./define-bbpou-bank-details.component.sass'],
  providers: [ErrorCodes]
})
export class DefineBbpouBankDetailsComponent implements OnInit {
  @Input() rowData: [];
  @Input() editFlag:string;
  @Input() submitBtn:boolean;
  @Input() showLoader:boolean;
  @Input() aggregatorChargesData: any;
  @Output() onSave = new EventEmitter<any>();
  @Output() onCancel = new EventEmitter<any>();

  bbpouBankForm: FormGroup;
  btnVal = 'Submit';
  cancelFlag=true 
  COMPANY_ID: string;
  COMPANY_TYPE:string;
  onboardingstatusData=[];
  countryData=[];
  stateData=[];
  cityData=[];
  rate_percent_row = {};
  minv_maxv_dict = {};
  submitted = false;

  todayDate = new Date().toJSON().split('T')[0];
 
  constructor(private formBuilder: FormBuilder, 
              private currencyService: CurrencyService, 
              private allmasterService: AllmasterService, 
              private bbpouBankService: BbpouBankService,
              private errorCodes: ErrorCodes) { }

  ngOnInit(): void {
    this.COMPANY_ID = localStorage.getItem('COMPANY_ID');
    this.COMPANY_TYPE  = localStorage.getItem('COMPANY_TYPE');


    this.bbpouBankForm = this.formBuilder.group({
      id: [''],
      membership_id: ['', Validators.required],
      npci_biller_bbpou_id: ['', Validators.required],
      onboarding_status_ref_id:  ['', Validators.required],
      account_number: ['', [Validators.required, Validators.minLength(9), Validators.maxLength(26),Validators.pattern("^[0-9]*$")]],
      bbpou_bank_name: ['', Validators.required],
      branch_name: ['', Validators.required],
      bank_code: ['', Validators.required],
      country_ref_id: ['', Validators.required],
      state_ref_id: ['', Validators.required],
      city_ref_id:  ['', Validators.required],
      address1: ['', Validators.required],
      address2: ['', Validators.required],
      pincode: ['', [Validators.required, Validators.maxLength(6), Validators.pattern('[0-9]{6}')]],
      contact_person_name: ['', Validators.required],
      contact_person_mobile_number: ['', [Validators.required, Validators.pattern('[1-9]{1}[0-9]{9}')]],
      email: ['',[Validators.required,Validators.email]],
      gst: ['', [Validators.required, Validators.pattern('[0-3]{1}[0-9]{1}[A-Z]{5}[0-9]{4}[A-Z]{1}[1-9]{1}Z[0-9A-Z]{1}')]],
      pan: ['', [Validators.required, Validators.pattern('[A-Z]{5}[0-9]{4}[A-Z]{1}')]],
      tan: ['', [Validators.required, Validators.pattern('[A-Z]{4}[0-9]{5}[A-Z]{1}')]],
      ifsc_code: ['', [Validators.required, Validators.pattern('[A-Z]{4}[0-9A-Z]{7}')]],
      swift_code: [''],
      iban_code: [''],
      initialItemRow: this.formBuilder.array([this.initialitemRow()])
    });

    if(!Array.isArray(this.rowData)){
      this.editViewRecord(this.rowData,this.editFlag);
     }

    this.allmasterService.getMasterDataByType('Onboarding Status').subscribe({
      next: (data:any) => {
        this.onboardingstatusData=data;  
      },
      error:(error) => {
        this.showSwalmessage(this.errorCodes.getErrorMessage(error.status), error.error, 'error', true)
      }
    })

    this.currencyService.getAll().subscribe({
      next: (data:any) => {
        this.countryData=data;  
      },
      error:(error) => {
        this.showSwalmessage(this.errorCodes.getErrorMessage(error.status), error.error, 'error', true)
      }
    })

    this.bbpouBankForm.get('country_ref_id').valueChanges.subscribe({
      next: (val:any) => {
        if(val!=undefined){
          this.currencyService.getState(val).subscribe({
            next: (data:any) => {
              this.stateData=data;  
            },
            error:(error) => {
              this.showSwalmessage(this.errorCodes.getErrorMessage(error.status), error.error, 'error', true)
            }
          })
        }  
      },
      error:(error) => {
        this.stateData = []
      }
    })

    this.bbpouBankForm.get('state_ref_id').valueChanges.subscribe({
      next: (val:any) => {
        if(val!=undefined){
          this.currencyService.getCity(val).subscribe({
            next: (data:any) => {
              this.cityData=data;  
            },
            error:(error) => {
              this.showSwalmessage(this.errorCodes.getErrorMessage(error.status), error.error, 'error', true)
            }
          })
        }  
      },
      error:(error) => {
        this.cityData = []
      }
    })

  }


  initialitemRow() {
    return this.formBuilder.group({
      id:[''],
      contact_person_name: ['', Validators.required],
      designation: [''],
      contact_person_mobile_number: ['', Validators.required],
      email: ['', Validators.required],
    });
  }


  get formArray() {
    return this.bbpouBankForm.get('initialItemRow') as FormArray;
  }

  addNewRow() {
    this.formArray.push(this.initialitemRow());
  }

  deleteRow(index) {
    if (this.formArray.length == 1) {
      return false;
    } else {
      this.formArray.removeAt(index);
      return true;
    }
  }

  setExistingArray(initialArray = []): FormArray {
    const formArray = new FormArray([]);
    initialArray.forEach(element => {
      formArray.push(this.formBuilder.group({
        id: element.id,
        contact_person_name: element.contact_person_name,
        designation: element.designation,
        contact_person_mobile_number: element.contact_person_mobile_number,
        email: element.email,
      }));
    });
    return formArray;
  }


  editViewRecord(data,editFlag) {
    this.bbpouBankService.getBBPOUBankById(data.id).subscribe({
      next: (row:any) => {
        if(editFlag == 'view'){
          this.submitBtn = false;
        }
  
        if(editFlag == 'edit'){
          this.submitBtn = true;
          this.btnVal = 'Update'
        }
  
        this.bbpouBankForm.patchValue({
          id: row.id,
          membership_id: row.membership_id,
          npci_biller_bbpou_id: row.npci_biller_bbpou_id,
          onboarding_status_ref_id:  row.onboarding_status_ref_id,
          bbpou_bank_name: row.bbpou_bank_name,
          branch_name: row.branch_name,
          account_number:row.account_number,
          bank_code: row.bank_code,
          country_ref_id: row.country_ref_id,
          state_ref_id: row.state_ref_id,
          city_ref_id: row.city_ref_id,
          address1: row.address1,
          address2: row.address2,
          pincode: row.pincode,
          contact_person_name: row.contact_person_name,
          contact_person_mobile_number: row.contact_person_mobile_number,
          email: row.email,
          gst: row.gst,
          pan: row.pan,
          tan: row.tan,
          ifsc_code: row.ifsc_code,
          swift_code: row.swift_code,
          iban_code: row.iban_code,
        });     
        this.bbpouBankForm.setControl('initialItemRow', this.setExistingArray(row.initialItemRow));
      },
      error:(error) => {
        this.showSwalmessage(this.errorCodes.getErrorMessage(error.status), error.error, 'error', true)
      }
    })
    this.showLoader = false;    
  }

  onCancelForm() {
    this.bbpouBankForm.reset();
    this.onCancel.emit(false);
  }

  showSwalmessage(message, text, icon, confirmButton): void {
    if (! confirmButton) {
        Swal.fire({ title: message, text: text, icon: icon, timer: 2000, showConfirmButton: false });
    } else {
        Swal.fire({ title: message, text: text, icon: icon, timer: 2000, showConfirmButton: true, confirmButtonText: "Yes", cancelButtonText: "No" });
    }
  }

  onSubmit(){
    if (this.bbpouBankForm.invalid) {
      return
    } 
    else {
      this.onSave.emit(this.bbpouBankForm.value);
     }

  }
}
