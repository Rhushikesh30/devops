import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DefineBbpouBankDetailsComponent } from './define-bbpou-bank-details.component';

describe('DefineBbpouBankDetailsComponent', () => {
  let component: DefineBbpouBankDetailsComponent;
  let fixture: ComponentFixture<DefineBbpouBankDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DefineBbpouBankDetailsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DefineBbpouBankDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
