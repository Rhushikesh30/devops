import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DefineBbpouBankComponent } from './define-bbpou-bank.component';

describe('DefineBbpouBankComponent', () => {
  let component: DefineBbpouBankComponent;
  let fixture: ComponentFixture<DefineBbpouBankComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DefineBbpouBankComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DefineBbpouBankComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
