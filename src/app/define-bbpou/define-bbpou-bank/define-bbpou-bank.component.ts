import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ManageSecurityService } from 'src/app/core/service/manage-security.service';
import { BBPOUBank } from 'src/app/shared/models/bbpou-bank'
import { BbpouBankService } from 'src/app/shared/services/bbpou-bank.service';
import * as XLSX from 'xlsx';
import Swal from 'sweetalert2';
import { ErrorCodes } from 'src/app/shared/error-codes';

@Component({
  selector: 'app-define-bbpou-bank',
  templateUrl: './define-bbpou-bank.component.html',
  styleUrls: ['./define-bbpou-bank.component.sass'],
  providers: [ErrorCodes]
})
export class DefineBbpouBankComponent implements OnInit {
  @ViewChild('dataSourcePaginator',{read: MatPaginator}) dataSourcePaginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  dataSource :MatTableDataSource<BBPOUBank>;
  resultData: BBPOUBank[];
  
  displayColumns = [
    'actions',
    'bbpou_bank_name',
    'branch_name',
    'country_name',   
    'state_name',
    'city_name'
  ];
  renderedData: BBPOUBank[];

  screenName = 'BBPOU Bank';
  submitBtn = true;
  showLoader = false;
  rowData = [];
  listDiv:boolean = false;
  showList:boolean = true;
  sidebarData: any;
  bbpouBankData =[];
  editFlag = '';

  constructor(private manageSecurityService :ManageSecurityService, 
              private bbpouBankService :BbpouBankService,
              private errorCodes: ErrorCodes) { }

  ngOnInit(): void {
    this.getAllData();
    let userId = localStorage.getItem('USER_ID');
    this.manageSecurityService.getAccessLeftPanel(userId,this.screenName).subscribe({
      next: (data:any) => {
        this.sidebarData = data;  
      },
      error:(error) => {
        this.showSwalmessage(this.errorCodes.getErrorMessage(error.status), error.error, 'error', true)
      }
    })
  }
  editViewRecord(row,flag){
    this.rowData = row;
    this.showList = false;
    this.submitBtn = flag;
    this.listDiv = true;
    this.showLoader = false;
    this.editFlag = flag;
  }

  showFormList(item: boolean) {
    if(item ===false){
      this.listDiv = true;
      this.showList = false;
    }
    else{
      this.listDiv = false;
      this.showList = true;
    }
  }

  onCancel(item: boolean){
    this.listDiv = item;
    this.showList = true;
    this.submitBtn = true;
    this.rowData = [];
  }

  getAllData(){
    this.bbpouBankService.getAll().subscribe({
      next: (data:BBPOUBank[]) => {
        this.dataSource = new MatTableDataSource(data);
        this.dataSource.paginator = this.dataSourcePaginator;
        this.dataSource.sort = this.sort;
        this.dataSource.connect().subscribe(d => this.renderedData = d);
        this.bbpouBankData = data;
      },
      error:(error) => {
        this.showSwalmessage(this.errorCodes.getErrorMessage(error.status), error.error, 'error', true)
      }
    })

  }

  tbl_FilterDatatable(value:string) {
    this.dataSource.filter = value.trim().toLocaleLowerCase();
  }

  showSwalmessage(message, text, icon, confirmButton): void {
    if (! confirmButton) {
        Swal.fire({ title: message, text: text, icon: icon, timer: 2000, showConfirmButton: false });
    } else {
        Swal.fire({ title: message, text: text, icon: icon, timer: 2000, showConfirmButton: true, confirmButtonText: "Yes", cancelButtonText: "No" });
    }
  }

  onSave(formValue){  
    this.showLoader = true
    this.bbpouBankService.create(formValue).subscribe({
      next: (data:any) => {
        if (data['status'] == 2) {
          this.showSwalmessage('Your record has been added successfully!','','success',false)
        }
        else if (data['status'] == 1) {
          this.showSwalmessage('Your record has been updated successfully!','','success',false)
        }
        this.getAllData();
        this.showList = true;
        this.listDiv = false;
        this.showLoader = false;
      },
      error:(error) => {
        this.showLoader = false;
        this.showSwalmessage(this.errorCodes.getErrorMessage(error.status), error.error, 'error', true)
      }
    })
  }

  export_to_excel() {
    let XlsMasterData = this.dataSource.data.map(({bbpou_bank_name,branch_name,city_name,state_name,country_name}) => ({bbpou_bank_name,branch_name,city_name,state_name,country_name}));
    const workSheet = XLSX.utils.json_to_sheet(XlsMasterData);
    const workBook: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(workBook, workSheet, 'SheetName');
    XLSX.writeFile(workBook, 'bbpou Bank.xlsx');
  }

}

