import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DefineBbpouCompanyComponent } from './define-bbpou-company.component';

describe('DefineBbpouCompanyComponent', () => {
  let component: DefineBbpouCompanyComponent;
  let fixture: ComponentFixture<DefineBbpouCompanyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DefineBbpouCompanyComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DefineBbpouCompanyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
