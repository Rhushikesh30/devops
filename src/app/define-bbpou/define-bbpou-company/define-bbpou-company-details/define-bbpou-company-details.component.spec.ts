import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DefineBbpouCompanyDetailsComponent } from './define-bbpou-company-details.component';

describe('DefineBbpouCompanyDetailsComponent', () => {
  let component: DefineBbpouCompanyDetailsComponent;
  let fixture: ComponentFixture<DefineBbpouCompanyDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DefineBbpouCompanyDetailsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DefineBbpouCompanyDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
