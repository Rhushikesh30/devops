import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BbpouCompanyService } from 'src/app/shared/services/bbpou-company.service'
import { AllmasterService } from 'src/app/shared/services/allmaster.service';
import {CurrencyService} from 'src/app/shared/services/currency.service'
import Swal from 'sweetalert2';
import { ErrorCodes } from 'src/app/shared/error-codes';

@Component({
  selector: 'app-define-bbpou-company-details',
  templateUrl: './define-bbpou-company-details.component.html',
  styleUrls: ['./define-bbpou-company-details.component.sass'],
  providers: [ErrorCodes]
})
export class DefineBbpouCompanyDetailsComponent implements OnInit {
  @Input() rowData: [];
  @Input() editFlag:string;
  @Input() submitBtn:boolean;
  @Input() showLoader:boolean;
  @Input() aggregatorChargesData: any;
  @Output() onSave = new EventEmitter<any>();
  @Output() onCancel = new EventEmitter<any>();

  BBPOUCompanyForm: FormGroup;
  btnVal = 'Submit';
  cancelFlag=true 
  COMPANY_ID: string;
  COMPANY_TYPE:string;
  companydata: any = [];
  service_model: any = [];
  billing_mode: any = [];
  onboarding_mode: [];
  settlement_mode: [];
  service_mode: [];
  biller_mode: [];
  bbpou_bank_name:any=[];
  settlement_frequency: any = [];
  settlement_type: any = [];
  billing_frequency:any=[];
  transactionmode:any=[];
  transaction_type = [];
  company_name= [];
  billerdata =[];
  taxdata=[];
  companies=[];
  locationData=[];
  ownershipStatusData=[];
  countrydata=[];
  statedata=[];
  citydata=[];
  BBPOUBankData=[];
  rate_percent_row = {};
  minv_maxv_dict = {};
  submitted = false;
  locationSelected: boolean = false;
  shouldisable=false;
  disableflag = true;
  isBranch = false;
  isManagedBy = false;
  state:any;
  city:any;
  currency:any;

  todayDate = new Date().toJSON().split('T')[0];

  constructor(private formBuilder: FormBuilder,
              private currencyService:CurrencyService,
              private allmasterService: AllmasterService, 
              private bbpouBankService: BbpouCompanyService,
              private errorCodes: ErrorCodes) { }

  ngOnInit(): void {
    this.COMPANY_ID = localStorage.getItem('COMPANY_ID');
    this.COMPANY_TYPE  = localStorage.getItem('COMPANY_TYPE');

    this.BBPOUCompanyForm = this.formBuilder.group({
      id: [''],
      company_name: ['', [Validators.required, Validators.pattern("^[a-zA-Z_ ]*$")]],
      company_shortname: ['', [Validators.required, Validators.pattern("^[a-zA-Z_ ]*$")]],
      address1:['',[Validators.required,Validators.maxLength(30)]],
      address2:[''],
      country_ref_id: [''],
      state_ref_id: [''],
      city_ref_id: [''],
      settlement_type_ref_id:['',Validators.required],
      ownership_status_ref_id: ['', Validators.required],
      cin: ['', [Validators.pattern('[L,U]{1}[0-9]{5}[A-Z]{2}[1-9]{1}[0-9]{3}[A-Z]{3}[0-9]{6}')]],
      pan: ['', [Validators.required, Validators.pattern('[A-Z]{5}[0-9]{4}[A-Z]{1}')]],
      tan: ['', [Validators.required, Validators.pattern('[A-Z]{4}[0-9]{5}[A-Z]{1}')]],
      gst: ['', [Validators.required, Validators.pattern('[0-3]{1}[0-9]{1}[A-Z]{5}[0-9]{4}[A-Z]{1}[1-9]{1}Z[0-9A-Z]{1}')]],
      email: ['',[Validators.required,Validators.email]],
      contact_person_name: ['', Validators.required],
      contact_person_mobile_number: ['', [Validators.required, Validators.pattern('[1-9]{1}[0-9]{9}')]],
      bbpou_bank_ref_id: [''],
      company_type_ref_id: [''],
      created_by: [''],
      pincode: ['', [Validators.required, Validators.maxLength(6), Validators.pattern('[0-9]{6}')]],
      entity_share_id:[0],
      initialItemRow: this.formBuilder.array([this.initialItemRow()])
    });

    
    if(!Array.isArray(this.rowData)){
      this.editViewRecord(this.rowData,this.editFlag);
     }

    this.allmasterService.getMasterDataByType('Ownership').subscribe({
      next: (data:any) => {
        this.ownershipStatusData = data;  
      },
      error:(error) => {
        this.showSwalmessage(this.errorCodes.getErrorMessage(error.status), error.error, 'error', true)
      }
    })

    this.allmasterService.getMasterDataByType('Settlement Type').subscribe({
      next: (data:any) => {
        this.settlement_type = data.filter(function(item){
          return item.master_key=='Net';
      });

      },
      error:(error) => {
        this.showSwalmessage(this.errorCodes.getErrorMessage(error.status), error.error, 'error', true)
      }
    })

    this.currencyService.getAll().subscribe({
      next: (data:any) => {
        this.countrydata =data;  
      },
      error:(error) => {
        this.showSwalmessage(this.errorCodes.getErrorMessage(error.status), error.error, 'error', true)
      }
    })

    this.BBPOUCompanyForm.get('country_ref_id').valueChanges.subscribe({
      next: (val:any) => {
        if(val!=null){
          this.currencyService.getState(val).subscribe({
            next: (data:any) => {
              this.statedata = data;
            },
            error:(error) => {
              this.showSwalmessage(this.errorCodes.getErrorMessage(error.status), error.error, 'error', true)
            }
          })
        }
      }
    })

    this.BBPOUCompanyForm.get('state_ref_id').valueChanges.subscribe({
      next: (val:any) => {
        if(val!=null){
          this.currencyService.getCity(val).subscribe({
            next: (data:any) => {
              this.citydata = data;
            },
            error:(error) => {
              this.showSwalmessage(this.errorCodes.getErrorMessage(error.status), error.error, 'error', true)
            }
          })
        }
      }
    })
  }

  get f() { return this.BBPOUCompanyForm.controls; }

  initialItemRow() {
    return this.formBuilder.group({
      contact_person_name: ['', Validators.required],
      designation: [''],
      contact_person_mobile_number: ['', Validators.required],
      email: ['', Validators.required],
    });
  }

  setExistingArray(initialArray = []): FormArray {
    const formArray = new FormArray([]);
    initialArray.forEach(element => {
      formArray.push(this.formBuilder.group({
        id: element.id,
        contact_person_name: element.contact_person_name,
        designation: element.designation,
        contact_person_mobile_number: element.contact_person_mobile_number,
        email: element.email,
      }));
    });
    return formArray;
  }

  get formArray() {
    return this.BBPOUCompanyForm.get('initialItemRow') as FormArray;
  }

  addNewRow() {
    this.formArray.push(this.initialItemRow());
  }

  deleteRow(index) {
    if (this.formArray.length == 1) {
      return false;
    } else {
      this.formArray.removeAt(index);
      return true;
    }
  }
  showSwalmessage(message, text, icon, confirmButton): void {
    if (! confirmButton) {
        Swal.fire({ title: message, text: text, icon: icon, timer: 2000, showConfirmButton: false });
    } else {
        Swal.fire({ title: message, text: text, icon: icon, timer: 2000, showConfirmButton: true, confirmButtonText: "Yes", cancelButtonText: "No" });
    }
  }
  onCancelForm(){
    this.cancelFlag=false
    this.BBPOUCompanyForm.reset();
    this.onCancel.emit(false);
  }

  editViewRecord(data,editFlag) {
    this.bbpouBankService.getBBPOUBankById(data.id).subscribe({
      next: (company:any) => {
        if(editFlag == 'view'){
          this.submitBtn = false;
        }
  
        if(editFlag == 'edit'){
          this.submitBtn = true;
          this.btnVal = 'Update'
        }
        this.BBPOUCompanyForm.patchValue({
          id: company.id,
          company_id:company.company_id,
          company_name: company.company_name,
          bbpou_bank_ref_id: company.bbpou_bank_ref_id,
          company_type_ref_id: company.company_type_ref_id,
          entity_share_id: company.entity_share_id,
          share_id:company.share_id,
          company_shortname: company.company_shortname,
          address1: company.address1,
          address2: company.address2,
          settlement_type_ref_id:company.settlement_type_ref_id,
          country_id: company.country_id,
          state_id: company.state_id,
          city_id: company.city_id,
          country_ref_id: company.country_ref_id,
          state_ref_id: company.state_ref_id,
          city_ref_id: company.city_ref_id,
          location_ref_id: company.location_ref_id,
          ownership_status_ref_id: company.ownership_status_ref_id,
          cin: company.cin,
          pan: company.pan,
          tan: company.tan,
          gst: company.gst,
          email: company.email,
          contact_person_name: company.contact_person_name,
          currency_ref_id: company.currency_ref_id,
          contact_person_mobile_number: company.contact_person_mobile_number,
          pincode: company.pincode,
          created_by: company.created_by
        });  
        this.BBPOUCompanyForm.setControl('initialItemRow', this.setExistingArray(company.initialItemRow));        
      },
      error:(error) => {
        this.showSwalmessage(this.errorCodes.getErrorMessage(error.status), error.error, 'error', true)
      }
    })
    this.showLoader = false;   
  }
  
  onSubmit(){
    if(this.BBPOUCompanyForm.invalid){
      return;
    }
    else{   
    this.onSave.emit(this.BBPOUCompanyForm.value);     
    }
  }
}
