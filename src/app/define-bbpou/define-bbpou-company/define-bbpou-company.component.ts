import { Component, OnInit, ViewChild } from '@angular/core';
import Swal from 'sweetalert2';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import * as XLSX from 'xlsx';
import { ManageSecurityService } from 'src/app/core/service/manage-security.service';
import { BbpouCompanyService } from 'src/app/shared/services/bbpou-company.service';
import { BBPOUCompany } from 'src/app/shared/models/bbpou-company';
import { ErrorCodes } from 'src/app/shared/error-codes';

@Component({
  selector: 'app-define-bbpou-company',
  templateUrl: './define-bbpou-company.component.html',
  styleUrls: ['./define-bbpou-company.component.sass'],
  providers: [ErrorCodes]
})
export class DefineBbpouCompanyComponent implements OnInit {
  @ViewChild('dataSourcePaginator',{read: MatPaginator}) dataSourcePaginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  dataSource :MatTableDataSource<BBPOUCompany>;
  resultData: BBPOUCompany[];
  displayColumns = [
    'actions',
    'company_name',
    'company_type',
    'city',
    'address1',
    'email'
  ];  
  renderedData: BBPOUCompany[];
  screenName = '';
  submitBtn = true;
  showLoader = false;
  rowData = [];
  listDiv:boolean = false;
  showList:boolean = true;
  sidebarData: any;
  bbpouCompanyData =[];
  BBPOUBankData=[];
  editFlag = '';
  flag = false;
  COMPANY_TYPE:any;
  listingDataCheck:boolean = true;

  constructor(private manageSecurityService :ManageSecurityService, 
              private bbpouCompanyService :BbpouCompanyService,
              private errorCodes: ErrorCodes) { }

  ngOnInit(): void {
    this.COMPANY_TYPE  = localStorage.getItem('COMPANY_TYPE');
    this.getAllData();

    this.manageSecurityService.getAccessLeftPanel(localStorage.getItem('USER_ID'), this.screenName).subscribe({
      next: (data:any) => {
        this.sidebarData =data;
      }
    })
  }

  editViewRecord(row,flag){
    this.rowData = row;
    this.showList = false;
    this.submitBtn = flag;
    this.listDiv = true;
    this.showLoader = false;
    this.editFlag = flag    
  }

  showFormList(item: boolean){
    if(item ===false){
      this.listDiv = true;
      this.showList = false;
    }
    else{
      this.listDiv = false;
      this.showList = true;
    }
  }

  onCancel(item: boolean){
    this.listDiv = item;
    this.showList = true;
    this.submitBtn = true;
    this.rowData = [];
  }

  getAllData(){
    this.screenName = 'BBPOU-Bank';
    this.bbpouCompanyService.getCompanyByType(this.COMPANY_TYPE).subscribe({
      next: (data:any) => {
        if(data.length==0){
          this.listingDataCheck=false;
        }
        this.dataSource = new MatTableDataSource(data);
        this.dataSource.paginator = this.dataSourcePaginator;
        this.dataSource.sort = this.sort;
        this.dataSource.connect().subscribe(d => this.renderedData = d);
        this.bbpouCompanyData = data;
      }
    })
  }
  
  showSwalmessage(message, text, icon, confirmButton): void {
    if (! confirmButton) {
      Swal.fire({ title: message, text: text, icon: icon, timer: 2000, showConfirmButton: false });
    } else {
      Swal.fire({ title: message, text: text, icon: icon, timer: 2000, showConfirmButton: true, confirmButtonText: "Yes", cancelButtonText: "No" });
    }
  }

  onSave(formValue){
    this.showLoader = true
    this.bbpouCompanyService.create(formValue).subscribe({
      next: (data:any) => {
        if (data['status'] == 2) {
          this.showSwalmessage('Your record has been added successfully!','','success',false)
        }
        else if (data['status'] == 1) {
          this.showSwalmessage('Your Record has been updated successfully!','','success',false)
        }
        this.getAllData();
        this.showList = true;
        this.listDiv = false;
        this.showLoader = false
      },
      error:(error) => {
        this.showLoader = false;
        this.showSwalmessage(this.errorCodes.getErrorMessage(error.status), error.error, 'error', true)
      }
    })
  }
  
  tbl_FilterDatatable(value:string) {
    this.dataSource.filter = value.trim().toLocaleLowerCase();
  }

  export_to_excel() {
    let XlsMasterData = this.dataSource.data.map(({company_name,pincode,contact_person_mobile_number,email,pan,tan,gst,cin}) => ({company_name,pincode,contact_person_mobile_number,email,pan,tan,gst,cin}));    
    const workSheet = XLSX.utils.json_to_sheet(XlsMasterData);
    const workBook: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(workBook, workSheet, 'SheetName');
    XLSX.writeFile(workBook, 'Aggregator Company.xlsx');
  }
}
