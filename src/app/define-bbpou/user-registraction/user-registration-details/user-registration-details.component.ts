import { Component, OnInit, Input, Output, EventEmitter, } from '@angular/core';
import { FormBuilder, FormGroup , Validators } from '@angular/forms';

@Component({
  selector: 'app-user-registration-details',
  templateUrl: './user-registration-details.component.html',
  styleUrls: ['./user-registration-details.component.sass']
})

export class UserRegistrationDetailsComponent implements OnInit {
  @Input() rowData: [];
  @Input() editFlag:string;
  @Input() submitBtn: boolean;
  @Input() showLoader: boolean;
  @Output() onSave = new EventEmitter<any>();
  @Output() onCancel = new EventEmitter<any>();

 
  employeeForm: FormGroup;
  empRegistrationFormData: any = [];
  btnVal = 'Submit';
  COMPANY_ID: string;
  COMPANY_TYPE: string;
  sidebarData:any;
  lastItemOfUrl:any = null;
  submitted = false;
  
  constructor(
    public formbuilder: FormBuilder,
  ) { }

  ngOnInit(): void {
    this.COMPANY_ID = localStorage.getItem('COMPANY_ID');
    this.COMPANY_TYPE = localStorage.getItem('COMPANY_TYPE');
    
    this.employeeForm = this.formbuilder.group({
      id: [''],
      emp_name: ['', [Validators.required, Validators.pattern("^[a-zA-Z0-9_ ]*$")]],
      email: ['', [Validators.required, Validators.email, Validators.minLength(5)]],      
      phone: [''],
      company_role: [this.COMPANY_TYPE]
    });

    if (!Array.isArray(this.rowData)) {
      this.editViewRecord(this.rowData,this.editFlag);
    }
  }

  editViewRecord(row,editFlag) {
    if(editFlag == 'view')
    {
      this.submitBtn = false;
      this.employeeForm.get('email').disable({ onlySelf: true});
    }
    this.employeeForm.patchValue({
    id: row.id,
    emp_name: row.emp_name,
    email: row.email,
    phone: row.phone,
  });
    if(editFlag == 'edit'){
       this.btnVal = 'Update'; 
       this.employeeForm.get('email').disable({ onlySelf: true});
    }
    this.showLoader = false;
  }

  onCancelForm() {
    this.employeeForm.reset();
    this.onCancel.emit(false);
  }
 
  onSubmit() {
    if (this.employeeForm.invalid) {
      return;
    }
    else {
      this.onSave.emit(this.employeeForm.value);
    }
  }
}



