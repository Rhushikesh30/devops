import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DefineBbpouCompanyComponent } from './define-bbpou-company/define-bbpou-company.component';
import { UserRegistractionComponent } from './user-registraction/user-registraction.component';
import { DefineBbpouBankComponent } from './define-bbpou-bank/define-bbpou-bank.component';
import { NpciChargesComponent } from '../define-charges/npci-bbpou-charges/npci-charges.component';
import { AggregatorBankChargesComponent } from '../define-charges/bbpou-aggregator-charges/aggregator-bank-charges.component';

const routes: Routes = [
    {
      path: 'bbpou',
      component: DefineBbpouCompanyComponent
    },
    {
      path:'bbpou_bank',
      component:DefineBbpouBankComponent
    },
    {
      path: 'users',
      component: UserRegistractionComponent
    },
    {
      path: 'npci-charges',
      component: NpciChargesComponent
    },
    {
      path: 'agg-charges',
      component:AggregatorBankChargesComponent
    },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DefineBBPOURoutingModule { }
