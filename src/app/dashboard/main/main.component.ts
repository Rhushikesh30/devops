import { Component, ViewChild, OnInit } from '@angular/core';
import { DashboardService } from 'src/app/core/service/dashboard.service';
import { CompanySetupService } from 'src/app/core/service/company-setup.service';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import {DatePipe} from '@angular/common';

import {
  ChartComponent,
  ApexAxisChartSeries,
  ApexChart,
  ApexXAxis,
  ApexDataLabels,
  ApexStroke,
  ApexMarkers,
  ApexYAxis,
  ApexGrid,
  ApexTitleSubtitle,
  ApexPlotOptions,
  ApexTooltip,
  ApexLegend
} from 'ng-apexcharts';

export type ChartOptions = {
  series: ApexAxisChartSeries;
  chart: ApexChart;
  xaxis: ApexXAxis;
  stroke: ApexStroke;
  dataLabels: ApexDataLabels;
  markers: ApexMarkers;
  colors: string[];
  yaxis: ApexYAxis;
  grid: ApexGrid;
  legend: ApexLegend;
  title: ApexTitleSubtitle;
  tooltip: ApexTooltip;
  plotOptions: ApexPlotOptions;
};

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss'],
  providers: [DatePipe]
})
export class MainComponent implements OnInit {
  COMPANY_ID: string;
  COMPANY_TYPE_REF_ID: string;
  COMPANY_TYPE: string;
  ROLE_NAME: string;
  not_reconciled:string;
  cumulative_collection:string;
  SettlementAmount:string;
  total_transaction:number;
  total_value:number;
  total_transaction_0_30:number;
  total_value_0_30:number;
  total_transaction_31_60:number;
  total_value_31_60:number;
  total_transaction_61_above:number;
  total_value_61_above:number;
  totalInvoices:number;
  Total_transaction_data = [];
  companyData: any = [];
  cumulative_data=[]
  Bank_Balance_As_Per_Payment=[]
  no_of_transaction_for_outstanding=[]
  value_for_outstanding=[]
  no_of_transaction=[0,0,0,0,0,0,0,0,0,0,0,0]
  Amount=[0,0,0,0,0,0,0,0,0,0,0,0]
  Aggregator_Login:boolean = false;
  OU_Login:boolean = false;
  Aggregator_biller_data:any;
  billerData:any
  BillerNameValue:string
  public getBillertoSearch: FormControl = new FormControl();
  dashboardForm: FormGroup;
  selected = 'Collection';
  billerSelected = ''
  billerSelect
  SetMaxMonth:any;
  collectionOutstanding:any = []

  @ViewChild('chart', { static: false }) chart: ChartComponent;
  public chartOptions: Partial<ChartOptions>;
  public chartOptions2: Partial<ChartOptions>;
  constructor( private DashboardService: DashboardService, 
               private CompanySetupService: CompanySetupService, 
               private formBuilder: FormBuilder, private datePipe: DatePipe) {}

  public areaChartOptions = {
    responsive: true,
    legend: {
      display: false,
      labels: {
        usePointStyle: true,
        fontFamily: 'Poppins'
      }
    },
    scales: {
      xAxes: [
        {
          display: true,
          gridLines: {
            display: false,
            drawBorder: false
          },
          scaleLabel: {
            display: false,
            labelString: 'Month'
          },
          ticks: {
            fontFamily: 'Poppins',
            fontColor: '#9aa0ac' // Font Color
          }
        }
      ],
      yAxes: [
        {
          display: true,
          gridLines: {
            display: true,
            draw1Border: !1,
            lineWidth: 0.5,
            zeroLineColor: 'transparent',
            drawBorder: false
          },
          scaleLabel: {
            display: true,
            labelString: 'Value',
            fontFamily: 'Poppins'
          },
          ticks: {
            fontFamily: 'Poppins',
            fontColor: '#9aa0ac' // Font Color
          }
        }
      ]
    },
    title: {
      display: false,
      text: 'Normal Legend'
    }
  };
  areaChartData = [
    {
      label: 'Foods',
      data: [0, 105, 190, 140, 270],
      borderWidth: 4,
      pointStyle: 'circle',
      pointRadius: 5,
      borderColor: 'rgba(154, 156, 157, 1)',
      backgroundColor: 'rgba(154, 156, 157, 0.4)',
      pointBackgroundColor: 'rgba(154, 156, 157, 1)',
      pointBorderColor: 'transparent',
      pointHoverBackgroundColor: 'transparent',
      pointHoverBorderColor: 'rgba(154, 156, 157,0.8)'
    },
    {
      label: 'Electronics',
      data: [0, 152, 80, 250, 190],
      borderWidth: 4,
      pointStyle: 'circle',
      pointRadius: 5,
      borderColor: 'rgba(76, 194, 176, 1)',
      backgroundColor: 'rgba(76, 194, 176, 0.4)',
      pointBackgroundColor: 'rgba(76, 194, 176, 1)',
      pointBorderColor: 'transparent',
      pointHoverBackgroundColor: 'transparent',
      pointHoverBorderColor: 'rgba(76, 194, 176,0.8)'
    }
  ];

  areaChartLabels = ['January', 'February', 'March', 'April', 'May'];
  // area chart end

  // barChart
  public barChartOptions: any = {
    scaleShowVerticalLines: false,
    responsive: true,
    legend: {
      display: true,
      labels: {
        fontColor: '#9aa0ac'
      }
    },
    scales: {
      xAxes: [
        {
          gridLines: {
            display: false
          },
          ticks: {
            fontFamily: 'Poppins',
            fontColor: '#9aa0ac' // Font Color
          }
        }
      ],
      yAxes: [
        {
          ticks: {
            beginAtZero: true,
            fontFamily: 'Poppins',
            fontColor: '#9aa0ac' // Font Color
          }
        }
      ]
    }
  };
  public barChartLabels: string[] = [
    'Jan',
    'Feb',
    'March',
    'April',
    'May',
    'June',
    'July',
    'Aug',
    'Sep',
    'Oct',
    'Nov',
    'Dec'
  ];
  public barChartType = 'bar';
  public barChartLegend = true;

  public barChartData :any[]= [

  ];

  public barChartColors: Array<any> = [
    {
      backgroundColor: 'rgba(90, 155, 246, 0.8)',
      borderColor: 'rgba(90, 155, 246, 1)',
      pointBackgroundColor: 'rgba(90, 155, 246, 1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(90, 155, 246, 0.8)'
    },
    {
      backgroundColor: 'rgba(174, 174, 174, 0.8)',
      borderColor: 'rgba(174, 174, 174, 1)',
      pointBackgroundColor: 'rgba(174, 174, 174, 1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(174, 174, 174, 0.8)'
    }
  ];

  ngOnInit() {
    this.chart1();
    this.chart2();
    this.COMPANY_TYPE_REF_ID = localStorage.getItem('COMPANY_TYPE_REF_ID')
    this.COMPANY_TYPE = localStorage.getItem('COMPANY_TYPE')
    this.COMPANY_ID = localStorage.getItem('COMPANY_ID')
    this.ROLE_NAME= localStorage.getItem('ROLE_NAME')

    this.dashboardForm = this.formBuilder.group({
      month: ['']
    });

    let form_date = new Date();
    this.SetMaxMonth = this.datePipe.transform(form_date, "yyyy-MM")
    this.dashboardForm.get('month').setValue(this.datePipe.transform(form_date, "yyyy-MM"))

    if(this.COMPANY_TYPE=="Aggregator" ||this.COMPANY_TYPE=="Aggregator Partner" ){
      this.Aggregator_Login = true;
      this.CompanySetupService.getBillersAgg(this.COMPANY_TYPE).subscribe({
        next:data => {
          this.Aggregator_biller_data = data
          this.Aggregator_biller_data.sort(this.sortByName);
          this.intialsApis(this.Aggregator_biller_data[0]['id'])
          this.billerSelected = this.Aggregator_biller_data[0]['id'];
        }
      });


    }
    else if(this.COMPANY_TYPE=='Biller'){
      this.intialsApis(localStorage.getItem('COMPANY_ID'))
      this.Aggregator_Login = false;
      this.CompanySetupService.getBillersAgg(this.COMPANY_TYPE).subscribe({
        next:data => {
          this.Aggregator_biller_data = data;
          this.billerSelected = data[0]['id'];
        }
      });
    }

    else if(this.COMPANY_TYPE == 'Biller-OU'){
      this.Aggregator_Login = true;
      this.OU_Login = true;
      this.CompanySetupService.getBillersAgg(this.COMPANY_TYPE).subscribe({
        next:data => {
          this.Aggregator_biller_data = data
          this.Aggregator_biller_data.sort(this.sortByName);
          if (this.billerSelected == ''){
            this.intialsApis(localStorage.getItem('COMPANY_ID'))
          }else{
            this.intialsApis(this.billerSelected)     
          }
        }
      });
    }

    this.getBillertoSearch.valueChanges.subscribe({
      next:val=>{
        this.BillerNameValue = val;
      }
    })

    this.dashboardForm.get('month').valueChanges.subscribe({
      next:val =>{
        if (this.billerSelected !== undefined){
          this.intialsApis(this.billerSelected)
        }else if(this.COMPANY_TYPE=='Biller'){
          this.intialsApis(localStorage.getItem('COMPANY_ID'))        
        }  
      }
    })

  }

 sortByName(a: any, b: any) {
    let nameA = a.company_name
    let nameB = b.company_name
    if (nameA < nameB) {
      return -1;
    }
    if (nameA > nameB) {
      return 1;
    }
    return 0;
  }

  intialsApis(Company_id){
    this.COMPANY_ID = Company_id
    let getMonth = this.dashboardForm.get('month').value
    getMonth = getMonth.replace("-",",");
    this.changeBar('Collection');

    this.DashboardService.GetdashboardCount(Company_id, getMonth).subscribe({
      next:(data:any) => {
          // Total Collection
          if (data['total_coll'] >= 10000000) {
            this.cumulative_collection = (data['total_coll'] / 10000000).toFixed(2) + ' Cr';
          }
          if (data['total_coll'] >= 100000) {
            this.cumulative_collection = (data['total_coll'] / 100000).toFixed(2) + ' L';
          }
          else{
            this.cumulative_collection = data['total_coll'];
          }
  
          // SettlementAmount
          if (data['remitted_amount'] >= 10000000) {
            this.SettlementAmount = (data['remitted_amount'] / 10000000).toFixed(2) + ' Cr';
          }
          if (data['remitted_amount'] >= 100000) {
          this.SettlementAmount = (data['remitted_amount'] / 100000).toFixed(2) + ' L';
          }
          else{
              this.SettlementAmount = data['remitted_amount'];
          }
  
        // Not Reconciled
        if(data['not_recon'] >= 10000000) {
          this.not_reconciled = (data['not_recon'] / 10000000).toFixed(2) + ' Cr';
        }
        if(data['not_recon'] >= 100000) {
          this.not_reconciled = (data['not_recon'] / 100000).toFixed(2) + ' L';
        }
        else{
          this.not_reconciled = data['not_recon'];
        }
        // Total Invoices
        this.totalInvoices = data['total_invoices'];
      }
    })
  }

  public changeBar(type){ 
    this.DashboardService.getCollectionOut(type).subscribe({
      next:(data:any) => {
        this.collectionOutstanding = data;
      }
    });
  }

  private chart1() {
    this.chartOptions = {
      series: [
        {
          name: 'High - 2013',
          data: [15, 13, 30, 23, 13, 32, 27]
        },
        {
          name: 'Low - 2013',
          data: [12, 25, 14, 18, 27, 13, 21]
        }
      ],
      chart: {
        height: 250,
        type: 'line',
        foreColor: '#9aa0ac',
        dropShadow: {
          enabled: true,
          color: '#000',
          top: 18,
          left: 7,
          blur: 10,
          opacity: 0.2
        },
        toolbar: {
          show: false
        }
      },
      colors: ['#9F78FF', '#858585'],
      stroke: {
        curve: 'smooth'
      },
      grid: {
        borderColor: 'rgba(216, 216, 216, 0.30)',
        row: {
          colors: ['transparent', 'transparent'],
          opacity: 0.5
        }
      },
      markers: {
        size: 3
      },
      xaxis: {
        categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul'],
        title: {
          text: 'Month'
        }
      },
      yaxis: {
        min: 5,
        max: 40
      },
      legend: {
        position: 'top',
        horizontalAlign: 'right',
        floating: true,
        offsetY: -25,
        offsetX: -5
      },
      tooltip: {
        theme: 'dark',
        marker: {
          show: true
        },
        x: {
          show: true
        }
      }
    };
  }

  private chart2() {
    this.chartOptions2 = {
      series: [
        {
          name: 'blue',
          data: [
            {
              x: 'Team A',
              y: [1, 5]
            },
            {
              x: 'Team B',
              y: [4, 6]
            },
            {
              x: 'Team C',
              y: [5, 8]
            }
          ]
        },
        {
          name: 'green',
          data: [
            {
              x: 'Team A',
              y: [2, 6]
            },
            {
              x: 'Team B',
              y: [1, 3]
            },
            {
              x: 'Team C',
              y: [7, 8]
            }
          ]
        }
      ],
      chart: {
        type: 'rangeBar',
        height: 250,
        foreColor: '#9aa0ac'
      },
      plotOptions: {
        bar: {
          horizontal: false
        }
      },
      dataLabels: {
        enabled: true
      },
      tooltip: {
        theme: 'dark',
        marker: {
          show: true
        },
        x: {
          show: true
        }
      }
    };
  }
}
