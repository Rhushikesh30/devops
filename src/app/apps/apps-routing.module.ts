import { Routes } from '@angular/router';
import { ChatComponent } from './chat/chat.component';
import { SupportComponent } from './support/support.component';

const routes: Routes = [
  {
    path: 'chat',
    component: ChatComponent
  },
  {
    path: 'support',
    component: SupportComponent
  }
];
export class AppsRoutingModule {}
