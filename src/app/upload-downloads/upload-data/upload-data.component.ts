import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators} from '@angular/forms';
import Swal from 'sweetalert2';
import { environment } from 'src/environments/environment'
import { DownloadbillstructureService } from 'src/app/shared/services/downloadbillstructure.service'
import { DefineBillerStructureService } from 'src/app/shared/services/define-biller-structure.service';
import { Router } from '@angular/router';
declare const $: any;
@Component({
  selector: 'app-upload-data',
  templateUrl: './upload-data.component.html',
  styleUrls: ['./upload-data.component.scss']
})
export class UploadDataComponent implements OnInit {
  frontEndUrl = environment.apiUrl;
  application_id: string;
  user_id: string;
  company_id: string;
  sub_application_id: string;
  CREATED_BY: string;
  upload_dataForm: FormGroup;
  billerName: string;
  file_type: any;
  COMPANY_TYPE: any;
  getBillerName: any = [];

  uploadData: FormData;
  format: any;

  constructor(private formBuilder: FormBuilder,
              private billStructureService : DefineBillerStructureService,
              private downloadbillstructureService: DownloadbillstructureService,
              private router: Router) { }

  ngOnInit(): void {
    this.company_id = localStorage.getItem('COMPANY_ID');
    this.COMPANY_TYPE = localStorage.getItem('COMPANY_TYPE');

    this.upload_dataForm = this.formBuilder.group({
      id: [''],
      company_name: [''],
      file_type_ref_id: [null, Validators.required],      
      upload_file: [''],
      server_name: ['SFTP']
    });

    this.billStructureService.getAll().subscribe({
      next:data => {
        this.billerName = data[0]['biller_name']
        this.file_type = data[0]['file_type']
        this.upload_dataForm.get('company_name').setValue(this.billerName)   
        this.upload_dataForm.get('file_type_ref_id').setValue(this.file_type)
      }
    });
    (document.getElementById('submit') as HTMLInputElement).disabled = true;    
  }

  reloadFunction(){
    this.router.navigate(['/onboard-biller/upload_data']).then(() => {
      setTimeout(function() {window.location.reload();} , 3000);
    });
  }

  uploadFile(event) {
    let file = event.target.files[0];
    let formData = new FormData();
    this.format = file['name'].split('.')[1].toLowerCase();
    formData.append('uploadedFile', file );
    formData.append('company_id', this.company_id);
    formData.append('source_name', this.upload_dataForm.get('company_name').value.toLowerCase());
    formData.append('format', this.format);
    formData.append('cloud_name', this.upload_dataForm.get('server_name').value);
    this.uploadData = formData;
    
    let compare_file_type = this.upload_dataForm.getRawValue().file_type_ref_id.toLowerCase();
    if(compare_file_type=='excel'){
      compare_file_type='xlsx';
    }
    if (this.upload_dataForm.invalid){
      return;
    }else{      
      if(compare_file_type==this.format || compare_file_type=='xlsx' && this.format=='xls'){
        document.getElementById("overlay").style.display = "block";
        (document.getElementById('submit') as HTMLInputElement).disabled = true;
        this.UploadFileServer()
      }else if(compare_file_type==null){
        Swal.fire({
          icon: 'error',
          title: 'Select Source First !!',
          timer: 2000,
          showConfirmButton: false
        });
      }else{
        Swal.fire({
          icon: 'error',
          title: 'Wrong File Format !!',
          timer: 2000,
          showConfirmButton: false
        });
        this.reloadFunction()
      }
    }
  }

  UploadFileServer(){
    this.downloadbillstructureService.uploadFileData(this.uploadData).subscribe({
      next: data => {
        document.getElementById("overlay").style.display = "none";
        if (data['Status'] == 'Success') {
          (document.getElementById('submit') as HTMLInputElement).disabled = false;
          $('#chooseFile').hide();
          Swal.fire({
            title: 'File Loaded Successfully!',
            icon: 'success',
            timer: 2000,
            showConfirmButton: false
          });
        }
        if (data['Status'] == 'Credential Not Found'){
          Swal.fire({
            title: 'SFTP Credential Not Found!',
            icon: 'error',
            timer: 2000,
            showConfirmButton: false
          });
          this.reloadFunction()
        }
        if (data['Status'] == 'Failed'){
          Swal.fire({
            title: 'Something went Wrong',
            icon: 'error',
            timer: 2000,
            showConfirmButton: false
          });
          this.reloadFunction()
        }
      },
      error:(error) => {
        document.getElementById("overlay").style.display = "none";    
        (document.getElementById('submit') as HTMLInputElement).disabled = true;      
        Swal.fire({
          title: 'Something went Wrong !!',
          text: error,
          icon: 'error',
          timer: 2000,
          showConfirmButton: false
        });
        this.reloadFunction()
      }
    })
  }

  onSubmit() {
    $('#chooseFile').hide();
    document.getElementById("overlay").style.display = "block";
    this.downloadbillstructureService.insertData(
      {'filetype': this.upload_dataForm.value.file_type_ref_id, 'company_id': this.company_id, 'master_key': this.upload_dataForm.get('server_name').value})
      .subscribe({
        next:(data: any) => {
          document.getElementById("overlay").style.display = "none";
          if (data == 'Data Upload Success') {
            Swal.fire({
              title: 'Your data has been uploaded Successfully!',
              icon: 'success',
              timer: 3500,
              showConfirmButton: false
            });
            $('#chooseFile').show();
            (document.getElementById('submit') as HTMLInputElement).disabled = true;
            this.reloadFunction()
          }
          else if (data == 'Update Success') {
            Swal.fire({
              title: 'Your data has been Updated Successfully!',
              icon: 'success',
              timer: 3500,
              showConfirmButton: false
            });
            $('#chooseFile').show();
            (document.getElementById('submit') as HTMLInputElement).disabled = true;
            this.reloadFunction()
          }
    
          else if (data == 'Data Upload Failed') {
            Swal.fire({
              title: 'Data upload failed!',
              text: 'Data upload failed Plz Try Again!',
              icon: 'warning',
              timer: 3500,
              showConfirmButton: false
            });
            this.reloadFunction()
          }
    
          else if (data == 'Bill Structure Not Permanently Save') {
            Swal.fire({
              title: 'Data upload failed !',
              text: 'Bill Structure Not Permanently Save !',
              icon: 'warning',
              timer: 3500,
              showConfirmButton: false
            });
            this.reloadFunction()
          }
    
          else if (data == 'Biller Table Does Not Exist') {
            Swal.fire({
              title: 'Data upload failed !',
              text: 'Table Does Not Exist !',
              icon: 'warning',
              timer: 3500,
              showConfirmButton: false
            });
            this.reloadFunction()
          }
    
          else if (data == 'File Not Found SFTP') {
            Swal.fire({
              title: 'Data upload failed !',
              text: 'File Not Found SFTP !',
              icon: 'warning',
              timer: 3500,
              showConfirmButton: false
            });
            this.reloadFunction()
          }
          else if (data == 'SFTP Credentials Not Found') {
            Swal.fire({
              title: 'Data upload failed !',
              text: 'SFTP Credentials Not Found!',
              icon: 'warning',
              timer: 3500,
              showConfirmButton: false
            });
            this.reloadFunction()
          }
          else if (data == 'Duplicate Primary Keys') {
            Swal.fire({
              title: 'Data upload failed !',
              text: 'Duplicate Primary Keys !',
              icon: 'warning',
              timer: 3500,
              showConfirmButton: false
            });
            this.reloadFunction()
          }      
          else if (data == 'Column Not Match!') {
            Swal.fire({
              title: 'Columns Missing!',
              text: 'Some Column are Extra or Unnecessary',
              icon: 'warning',
              timer: 3500,
              showConfirmButton: false
            });
            this.reloadFunction()
          }
          else if (data == 'Data Missmatch') {
            Swal.fire({
              title: 'Data Formate!',
              text: 'Data Missmatch',
              icon: 'warning',
              timer: 3500,
              showConfirmButton: false
            });
            this.reloadFunction()
          }
        },
        error:(error) => {
          Swal.fire({
            title: error,
            icon: 'error',
            showConfirmButton: false
          })
          document.getElementById("overlay").style.display = "none";
          this.reloadFunction()
        }
      })
    }
}

