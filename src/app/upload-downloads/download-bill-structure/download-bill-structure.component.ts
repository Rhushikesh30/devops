import { Component, OnInit} from '@angular/core';
import { FormBuilder, FormGroup, Validators} from '@angular/forms';
import * as FileSaver from 'file-saver';
import { DefineBillerStructureService } from 'src/app/shared/services/define-biller-structure.service';
import { DownloadbillstructureService } from 'src/app/shared/services/downloadbillstructure.service'

declare const $: any;

@Component({
  selector: 'app-download-bill-structure',
  templateUrl: './download-bill-structure.component.html',
  styleUrls: ['./download-bill-structure.component.sass']
})
export class DownloadBillStructureComponent implements OnInit {

  screenName = 'Download Bill Structure'
  download_structureForm: FormGroup;
  blr_name: string;
  file_type: any;
  company_id: string;
  getBillerName: any = [];
  submitBtn = true;
  showLoader = false;
  rowData = [];
  listDiv:boolean = false;
  showList:boolean = true;
  sidebarData: any;
  billerData =[];
  editFlag = '';

  constructor(private formBuilder: FormBuilder, 
    private billStructureService : DefineBillerStructureService,
    private downloadStructureService: DownloadbillstructureService,) { }

  ngOnInit(): void {
    this.company_id = localStorage.getItem('COMPANY_ID')
    this.download_structureForm = this.formBuilder.group({
      company_name: [''],
      file_type_ref_id: [null, Validators.required],
      id: ['']
    });

    this.billStructureService.getAll().subscribe({
      next:data => {
        this.blr_name = data[0]['biller_name']
        this.file_type = data[0]['file_type']
        this.download_structureForm.get('company_name').setValue(this.blr_name)   
        this.download_structureForm.get('file_type_ref_id').setValue(this.file_type)
      }
    });
  }

  onSubmit(){
    let formatted_source_name = this.blr_name;
    formatted_source_name = formatted_source_name.replace(/ +/g, "_");
    let source_name = formatted_source_name.toLowerCase()
		this.downloadStructureService.downloadbillstructure(
      {'filetype':this.download_structureForm.value.file_type_ref_id,'company_id':this.company_id})
      .subscribe({
        next:(response: any) => {
          let blob:any = new Blob([response], { type: 'csv/json; charset=utf-8' });
          window.URL.createObjectURL(blob);      
          FileSaver.saveAs(blob, source_name+'.'+this.file_type.toLowerCase());	
        } 
    })
  }

}
