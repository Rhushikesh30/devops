import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DownloadBillStructureComponent } from './download-bill-structure.component';

describe('DownloadBillStructureComponent', () => {
  let component: DownloadBillStructureComponent;
  let fixture: ComponentFixture<DownloadBillStructureComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DownloadBillStructureComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DownloadBillStructureComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
