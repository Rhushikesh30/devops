import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TaxRateDetailsComponent } from './tax-rate-details.component';

describe('TaxRateDetailsComponent', () => {
  let component: TaxRateDetailsComponent;
  let fixture: ComponentFixture<TaxRateDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TaxRateDetailsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TaxRateDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
