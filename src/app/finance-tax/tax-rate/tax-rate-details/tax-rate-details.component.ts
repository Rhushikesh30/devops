import { Component, OnInit, Input, Output, EventEmitter, } from '@angular/core';
import { FormBuilder, FormGroup , Validators ,FormControl , FormArray} from '@angular/forms';
import { TaxRateService } from 'src/app/shared/services/tax-rate.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-tax-rate-details',
  templateUrl: './tax-rate-details.component.html',
  styleUrls: ['./tax-rate-details.component.sass']
})
export class TaxRateDetailsComponent implements OnInit {
  @Input() rowData: [];
  @Input() editFlag:string;
  @Input() submitBtn: boolean;
  @Input() showLoader: boolean;
  @Input() taxRateData: any;
  @Output() onSave = new EventEmitter<any>();
  @Output() onCancel = new EventEmitter<any>();

  taxRateForm: FormGroup;
  fromDateControl: FormControl;
  taxRateMasterData: any = [];
  taxAuthorityData: any = [];
  rcmFlagData: any = [];
  taxTypeData: any = [];

  btnVal = "Submit";
  todayDate = new Date().toJSON().split('T')[0];
  submitted = false;
  

  constructor(private formBuilder : FormBuilder,
    private taxRateService : TaxRateService) { }

  ngOnInit(): void { 
    this.taxRateForm = this.formBuilder.group({
      id: [''],
      tax_rate_name: ['', [Validators.required,Validators.pattern("^[a-zA-Z_ ]*$")]],
      tax_authority_ref_id: ['', Validators.required],
      tax_type_ref_id: ['', Validators.required],          
      initialItemRow: this.formBuilder.array([this.initialItemRow()])      
    });

    if (!Array.isArray(this.rowData)) {
      this.editViewRecord(this.rowData,this.editFlag);
    }

    this.taxRateService.getTaxAuthorityData().subscribe({
      next:data => {
        this.taxAuthorityData = data;
      }
    });

    this.taxRateService.getMasterDataAsPerType('Tax Type').subscribe({
      next:(data: []) => {
        this.taxTypeData = data;
      }
    });

    this.taxRateService.getMasterDataAsPerType('RCM Flag').subscribe({
      next:data => {
        this.rcmFlagData = data;
      }
    });  
  }

  initialItemRow() {
    return this.formBuilder.group({
      hsn_sac_no: ['', Validators.required],
      description: ['', Validators.required],
      from_date: ['', Validators.required],
      to_date: ['', Validators.required],
      tax_rate: ['', Validators.required],
      rcm_flag: ['', Validators.required],
      cess: ['0', Validators.required],      
    });
  }

  get formArr() {
    return this.taxRateForm.get('initialItemRow') as FormArray;
  }

  addNewRow() {
    this.formArr.push(this.initialItemRow());
  }

  deleteRow(index) {
    if (this.formArr.length == 1) {
      return false;
    } else {
      this.formArr.removeAt(index);
      return true;
    }
  }    
  
  get f() {
    return this.taxRateForm.controls;
  }
                
   
  setExistingArray(initialArray = []): FormArray {     
    const formArray = new FormArray([]);
    initialArray.forEach(element => {
    formArray.push(this.formBuilder.group({
      id: element.id,
      hsn_sac_no: element.hsn_sac_no,       
      description: element.description,
      from_date: element.from_date,
      to_date: element.to_date,
      tax_rate: element.tax_rate,
      rcm_flag: element.rcm_flag,
      cess: element.cess       
    }));
  });
  return formArray;
}

editViewRecord(data,editFlag) {
  this.taxRateService.getTaxRateById(data.id).subscribe({
    next:(row: any) => {
      if(editFlag == 'view'){
        this.submitBtn = false;
      }
      if(editFlag == 'edit'){
        this.btnVal = 'Update'; 
      }
      this.taxRateForm.patchValue({
        id: row.id,
        tax_rate_name: row.tax_rate_name,
        tax_authority_ref_id: row.tax_authority_ref_id,
        tax_type_ref_id: row.tax_type_ref_id,
      });
      this.taxRateForm.setControl('initialItemRow', this.setExistingArray(row.initialItemRow));
    }
  });
  this.showLoader = false;
}

showSwalmessage(message, text, icon, confirmButton): void {
    if (! confirmButton) {
        Swal.fire({ title: message, text: text, icon: icon, timer: 2000, showConfirmButton: false });
    } else {
        Swal.fire({ title: message, text: text, icon: icon, timer: 2000, showConfirmButton: true, confirmButtonText: "Yes", cancelButtonText: "No" });
    }
}
      
onCancelForm() {
  this.taxRateForm.reset();
  this.onCancel.emit(false);
}
  
onSubmit() {
  if (this.taxRateForm.invalid) {
    return;
  } 
  else {
    this.onSave.emit(this.taxRateForm.value);
  }
}
}
