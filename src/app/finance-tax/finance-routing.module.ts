import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TaxAuthorityComponent } from './tax-authority/tax-authority.component';
import { TaxRateComponent } from './tax-rate/tax-rate.component';
const routes: Routes = [
  {
    path: 'tax_authority',
    component: TaxAuthorityComponent
  },
  {
    path: 'tax_rate',
    component: TaxRateComponent
  }
 
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FinanceRoutingModule { }
