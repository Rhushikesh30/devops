import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TaxAuthorityComponent } from './tax-authority.component';

describe('TaxAuthorityComponent', () => {
  let component: TaxAuthorityComponent;
  let fixture: ComponentFixture<TaxAuthorityComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TaxAuthorityComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TaxAuthorityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
