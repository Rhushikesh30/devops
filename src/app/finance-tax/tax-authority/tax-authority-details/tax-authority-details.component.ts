import { Component, OnInit, Input, Output, EventEmitter, } from '@angular/core';
import { FormBuilder, FormGroup , Validators } from '@angular/forms';
import { TaxAuthorityService } from 'src/app/shared/services/tax-authority.service';

@Component({
  selector: 'app-tax-authority-details',
  templateUrl: './tax-authority-details.component.html',
  styleUrls: ['./tax-authority-details.component.sass']
})
export class TaxAuthorityDetailsComponent implements OnInit {
  @Input() rowData: [];
  @Input() editFlag:string;
  @Input() submitBtn: boolean;
  @Input() showLoader: boolean;
  @Input() taxAuthorityData: any;
  @Output() onSave = new EventEmitter<any>();
  @Output() onCancel = new EventEmitter<any>();

  taxAuthorityForm: FormGroup;
  btnVal = 'Submit'
  tbl_FilteredData = [];
  zoneData = [];
  taxTypeData = [];
  countrydata:any = [];  

  constructor(private formBuilder: FormBuilder, 
              private taxAuthorityService: TaxAuthorityService) { }

  ngOnInit(): void {
    this.taxAuthorityForm = this.formBuilder.group({
    id: [''],
    tax_name: ['', [Validators.required,Validators.pattern("^[a-zA-Z_ ]*$")]],
    country_ref_id: ['',Validators.required],
    ward: ['', Validators.required],
    zone_ref_id: ['',Validators.required],
    tax_type_ref_id: ['',Validators.required],
  });

  if (!Array.isArray(this.rowData)) {
    this.editViewRecord(this.rowData,this.editFlag);
  }

  this.taxAuthorityService.getCountryData().subscribe({
    next:(data: []) => {
      this.countrydata = data;
    }
  });

  this.taxAuthorityService.getMasterDataAsPerType('Tax Type').subscribe({
    next:(data: []) => {
      this.taxTypeData = data;
    }
  });

}

editViewRecord(row,editFlag) {
  if(editFlag == 'view')
    {
      this.submitBtn = false;
    }
  this.taxAuthorityForm.patchValue({
    id: row.id,
    tax_name: row.tax_name,
    country_ref_id: row.country_ref_id,
    ward: row.ward,
    zone_ref_id: row.zone_ref_id,
    tax_type_ref_id: row.tax_type_ref_id,
  });
  if(editFlag == 'edit'){
    this.btnVal = 'Update'; 
 }
 this.showLoader = false;
}
 
  onCancelForm() {
    this.taxAuthorityForm.reset();
    this.onCancel.emit(false);
  }
 
  onSubmit() {
    if (this.taxAuthorityForm.invalid) {
      return;
    }
    else {
      this.onSave.emit(this.taxAuthorityForm.value);
    }
  }
}