import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TaxAuthorityDetailsComponent } from './tax-authority-details.component';

describe('TaxAuthorityDetailsComponent', () => {
  let component: TaxAuthorityDetailsComponent;
  let fixture: ComponentFixture<TaxAuthorityDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TaxAuthorityDetailsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TaxAuthorityDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
