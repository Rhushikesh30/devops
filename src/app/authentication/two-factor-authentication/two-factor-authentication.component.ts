import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { AuthService, DataService } from 'src/app/core/service/auth.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-two-factor-authentication',
  templateUrl: './two-factor-authentication.component.html',
  styleUrls: ['./two-factor-authentication.component.sass']
})

export class TwoFactorAuthenticationComponent implements OnInit {
  loginForm: FormGroup;
  submitted = false;
  returnUrl: string;
  hide = true;
  error = '';
  errorAttempts = '';
  code_token : string;
  userData:any
  errorAttemptsCount = 4;
  showLoader = false;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private authService: AuthService,
    private dataService: DataService
  ) { }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      OTP: ['',[Validators.required]],
    });

    this.dataService.data.subscribe({
      next: (response:any) => {
        this.userData = response
        if(response == 'Initial Value'){
          this.router.navigate([''])
        }  
      }
    })
  }

  get f() {
    return this.loginForm.controls;
  }

  resendCode(){
    this.showLoader = true;
    this.authService.resendCode().subscribe({
      next: (data:any) => {
        this.showLoader = false;
        if (data['status']==='Success'){
          this.userData['profile'] = data['profile']
        }else{
          this.error=data['detail']
        }
      }
    })
  }

  login(otp) {
    this.error = ''
    this.errorAttempts = ''
    this.submitted = true;
    if (this.loginForm.invalid){
      this.error = 'Unable to login! Please Enter OTP'
    } else {
      this.showLoader = true;
      this.userData['otp'] = this.authService.encryptedData(otp)
      this.authService.login(this.userData).subscribe({
        next: (success:any) => {
          this.showLoader = false;
          if (success['status']==='Success'){
            this.router.navigate(['/dashboard/main']).then(() => {window.location.reload();});
          }else{
            this.error = success['detail'];
            if(success['error_code']==449){
              this.errorAttemptsCount = this.errorAttemptsCount -1;
              this.errorAttempts = 'Your Remaining Attempts are '+ this.errorAttemptsCount
            }else{
              this.dataService.sendData(null); 
              this.router.navigate([''])
            }      
          }
        },
        error:(error) => {
          this.showLoader = false;
          this.error = error.error;
          this.error='Unable to login! Incorrect OTP provided! Please Try again!' 
        }
      })
    }
  }
}
