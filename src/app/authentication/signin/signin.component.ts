import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService, DataService } from 'src/app/core/service/auth.service';
import { UnsubscribeOnDestroyAdapter } from 'src/app/shared/UnsubscribeOnDestroyAdapter';
@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.scss']
})
export class SigninComponent
  extends UnsubscribeOnDestroyAdapter
  implements OnInit{

  loginForm: FormGroup;
  submitted = false;
  error = '';
  hide = true;
  chide = true;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private authService: AuthService,
    private dataService: DataService) {super();}

  ngOnInit() {
    this.authService.clearSessions();
    this.loginForm = this.formBuilder.group({
      username: ['',[Validators.required]],
      password: ['', [Validators.required, Validators.pattern(/^(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})/)]]
    });
  }

  get f() {
    return this.loginForm.controls;
  }
 onSubmit() {
    this.submitted = true;
    this.error = '';
    if (this.loginForm.invalid) {
      this.error = 'Username and Password not valid !';
    } else {
      this.authService.code(this.authService.encryptedData(this.f.username.value), this.authService.encryptedData(this.f.password.value)).subscribe({
        next: (response:any) => {
          if (response['status']=='Success') {
            let res = this.loginForm.value
            res['profile'] = response['profile']
              let userData:object = {}
              userData['username'] = this.authService.encryptedData(this.f.username.value)
              userData['password'] = this.authService.encryptedData(this.f.password.value)
              userData['profile'] = response['profile']
              this.dataService.sendData(userData);       
              this.router.navigate(['authentication/2fa']);
            } else {
              this.error = response['detail'];
            }
        },
        error:(error) => {
          this.error = error.error;
          this.submitted = false;
        }
      })
    }    
  }
}
