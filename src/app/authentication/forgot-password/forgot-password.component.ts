import { Component, OnInit } from '@angular/core';
import { Router} from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from 'src/app/core/service/auth.service';
import { UnsubscribeOnDestroyAdapter } from 'src/app/shared/UnsubscribeOnDestroyAdapter';
import Swal from "sweetalert2";

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordComponent 
  extends UnsubscribeOnDestroyAdapter
  implements OnInit 
  {
  loginForm: FormGroup;
  isForgotScreen:boolean = true;
  error = '';
  auth_token = '';
  showLoader = false;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private authService: AuthService
  ) {super();}

  ngOnInit() {
    this.defaultForm()
  }

  defaultForm(){
    this.loginForm = this.formBuilder.group({
      username: ['', [Validators.required]],
      otp: ['null', [Validators.required]],
      profile: ['null', [Validators.required]],
      password: ['null', [Validators.required, Validators.pattern(/^(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})/)]],
      confirmpassword: ['null', [Validators.required, Validators.pattern(/^(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})/)]]
    });
  }

  resendCode(){
    this.onSubmit()
  }

  get f() {
    return this.loginForm.controls;
  }

  onSubmit() {
    this.error = '';
    if (this.f.username.value==''){
      this.error = 'Username not valid!';
    } else {
      this.showLoader = true;
      this.subs.sink = this.authService.ResetPassword(this.authService.encryptedData(this.f.username.value)).subscribe({
        next: (res:any) => {
          this.showLoader = false;
          if (res['status']=='Success'){
            this.isForgotScreen = false; 
            this.loginForm.get('profile').setValue(res['profile'])
            this.loginForm.get('otp').setValue('')    
            this.loginForm.get('password').setValue('')    
            this.loginForm.get('confirmpassword').setValue('')    
          } else {
            this.error = res['detail']
          } 
        },
        error:(error)=>{
          this.showLoader = false;
          this.error = error.error;
        }
      })
    }
  }

  updatePassword(){
    let otp = this.authService.encryptedData(this.loginForm.get('otp').value)
    let password = this.loginForm.get('password').value
    let profile = this.loginForm.get('profile').value
    let username = this.authService.encryptedData(this.loginForm.get('username').value)    

    let confirmpassword = this.loginForm.get('confirmpassword').value
    if (otp==''&& password==''&&confirmpassword==''){
      this.error = 'Please Provide details!';
      return      
    }
    if(password === confirmpassword){
      let userData = {
        'password': this.authService.encryptedData(this.loginForm.get('password').value),
        'otp': otp,
        'profile':profile,
        'username': username
      }
      this.showLoader = true;
      this.subs.sink = this.authService.UpdatePassword(userData).subscribe({
        next: (res:any) => {
          this.showLoader = false;
          if (res['status']=="Success"){
            Swal.fire({
              title: 'Password Updated Successfully',
              icon: 'success',
              timer: 2000,
              showConfirmButton: false
            });          
            this.router.navigate(['']).then(() => {
              setTimeout(function() {window.location.reload();} , 2000);
            });  
          }else if(res['status']=="Expired" || res['error_code']==401){
              this.error = res['detail']
              this.isForgotScreen = true;
              this.defaultForm()
            }else{
             this.error = res['detail']
          }
        },
        error:(error) => {
          this.showLoader = false;
          this.error = error.error;
        }
      })
    }else{
      this.error = 'Password Not Match!';     
    }
  }
}
