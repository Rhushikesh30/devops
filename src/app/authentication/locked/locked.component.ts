import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from 'src/app/core/service/auth.service';

@Component({
  selector: 'app-locked',
  templateUrl: './locked.component.html',
  styleUrls: ['./locked.component.scss']
})
export class LockedComponent implements OnInit {
  licenseForm: FormGroup;
  submitted = false;
  hide = true;
  error = '';
  constructor(private formBuilder: FormBuilder, 
              private router: Router, 
              private authService: AuthService) {}

  ngOnInit() {
    this.licenseForm = this.formBuilder.group({
      licensekey: ['', Validators.required]
    });
  }

  get f() {
    return this.licenseForm.controls;
  }
  onSubmit() {
    this.submitted = true;
    this.error = '';
    if (this.licenseForm.invalid) {
      this.error = 'Enter valid License Key!!!';
    } else {
      this.authService.saveLicenseKey(this.licenseForm.value).subscribe({
        next: (res:any) => {
          if (res.status == 'ok') {
            this.router.navigate(['']);
          } else {
            this.error = res.status;
          } 
        },
        error:(error) => {
          this.error = error.error;
          this.submitted = false;
        }
      })
    }
  }
}
