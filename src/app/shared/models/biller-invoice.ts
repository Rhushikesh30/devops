export interface BillerInvoiceMasterElement {
    id:any;
    biller_ref_id:any;
    transaction_date:any;
    period:any;
    fiscal_year:any;
    invoice_ref_no:any;
    transaction_currency_ref_id:any;
    conversion_rate:any;
    billing_address:any;
    remark:any;
  }