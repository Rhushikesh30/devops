export interface BankMasterElement {
    id:any;
    bank_name:any;
    branch_name:any;
    bank_code:any;
    account_number:any;
    account_type:any;
    ifsc_code:any;
    iban_number:any;
    cash_credit_limit:any;
    address1:any;
    address2:any;
    country_ref_id:any;
    state_ref_id:any;
    city_ref_id:any;
    pincode:any;
    nodal_acc_flag:any;
  }
  