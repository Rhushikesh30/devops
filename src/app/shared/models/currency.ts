export interface Currency {
    id:any;
    iso_codes:any;
    currency_code:any;
    name:any;
    currency_symbol:any;
    country_code:any;
  }