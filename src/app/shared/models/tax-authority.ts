export interface TaxAuthorityMasterElement {
    id:any;
    tax_name:any;
    country_ref_id:any;
    ward:any;
    zone_ref_id:any;
    tax_type_ref_id:any;
  }