export interface PaymentFileMasterElement {
    id:any;
    biller_name:any;
    transaction_date:any;
    from_date:any;
    to_date:any;
    total_transaction_amount:any;
    remark:any;
  }