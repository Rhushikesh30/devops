export interface DefineAggregatorElement {
    id:any;
    company_name:any;
    pincode:any;
    contact_person_mobile_number:any;
    email:any;
    pan:any;
    tan:any;
    gst:any;
    cin:any;
  }