export interface BillerElement {
    id:any;
    biller_name;
    bbpoubank_name;
    settlementfrequency_name;
    npcisettlementtype_name;
    billingfrequency_name
    start_date;
    end_date;
    revision_status;
  }