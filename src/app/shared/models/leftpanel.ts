export interface LeftPanelMasterElement {
    id:any;
    form_name:string;
    form_link:any;
    module_path:any;
    is_parent:any
    parent_code:any;
    is_child:any
    child_code:any;
    is_sub_child:any;
    sub_child_code:any;
    icon_class:any;
    sequence_id:any;
  }