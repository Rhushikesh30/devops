export interface BillerStructureElement {
    id:any;
    biller_name:any;
    biller_category:any;
    source_name:any;
    file_type:any;
    from_date:any;
    to_date:any;
    revision_status:any;
    version_number:any;
  }