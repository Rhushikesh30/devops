
export interface BillerElement {
    id:any;
    company_name:any;
    country:any;
    city: any;
    location:any;
    pincode:any;
    ownership_status:any;
    contact_person_name:any;
    email:any;
    bank_name:any;
  }