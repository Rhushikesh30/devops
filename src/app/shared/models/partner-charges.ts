export interface PartnerCharges{
    id:any;
    biller_name;
    bbpoubank_name;
    settlementtype_name;
    billingfrequency_name
    start_date;
    end_date;
    revision_status;
  }