export class ErrorCodes {
    getErrorMessage(code) {
        if (code == 400) {
            return "Bad request";
        }
        else if (code == 401) {
            return "Authentication credentials were not provided";
        }
        else if (code == 403) {
            return "You do not have permission";
        }
        else if (code == 404) {
            return "Records not found";
        }
        else if (code == 405) {
            return "Requested method not allowed";
        }
        else if (code == 406) {
            return "Not acceptable";
        }
        else if (code == 429) {
            return "Exceeded the Attempts";
        }
        else if (code == 500) {
            return "Internal Server error";
        }
    }
}