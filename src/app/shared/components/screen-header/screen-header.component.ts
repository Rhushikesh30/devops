import { Component, OnInit,  Input, Output, EventEmitter  } from '@angular/core';

@Component({
  selector: 'app-screen-header',
  templateUrl: './screen-header.component.html',
  styleUrls: ['./screen-header.component.sass']
})
export class ScreenHeaderComponent implements OnInit {
  @Input() screenName: string;
  @Input() showList: boolean;
  @Input() sidebarData: any;
  @Output() showFormList = new EventEmitter<boolean>();

  ngOnInit(): void {
    if(this.showList === true){
      this.showListTable();
    }
    else{
      this.showFrom();
    }
  }

  showFrom()
  {
    this.showList = false;
    this.showFormList.emit(this.showList);
  }

	showListTable()
  {
    this.showList = true;
    this.showFormList.emit(this.showList);
  }


}
