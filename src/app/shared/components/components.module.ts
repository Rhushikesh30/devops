import { NgModule } from '@angular/core';
import { FileUploadComponent } from './file-upload/file-upload.component';
import { SharedModule } from '../shared.module';
import { ScreenHeaderComponent } from './screen-header/screen-header.component';
import { DataNotFoundComponent } from './data-not-found/data-not-found.component';

@NgModule({
  declarations: [FileUploadComponent, ScreenHeaderComponent, DataNotFoundComponent],
  imports: [SharedModule],
  exports: [FileUploadComponent,ScreenHeaderComponent, DataNotFoundComponent]
})
export class ComponentsModule {}
