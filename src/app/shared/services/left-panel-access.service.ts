import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Observable } from 'rxjs'
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { LeftPanelMasterElement } from 'src/app/shared/models/leftpanel';

@Injectable({
  providedIn: 'root'
})
export class LeftPanelAccessService {
  constructor(private httpClient: HttpClient) { }

  getAllDataById(COMPANY_ID): Observable<LeftPanelMasterElement[]> {
    return this.httpClient.get<LeftPanelMasterElement[]>(`${environment.apiUrl}/left-panel/`,{params:{COMPANY_ID}})
  }
  
  getAll(): Observable<LeftPanelMasterElement[]> {
    return this.httpClient.get<LeftPanelMasterElement[]>(`${environment.apiUrl}/left-panel/`)
  }

  create(formValue): Observable<LeftPanelMasterElement[]> {
      const id = formValue.id
      if (id) {
        return this.httpClient.put<LeftPanelMasterElement[]>(`${environment.apiUrl}/left-panel/${id}/`, formValue)
      .pipe(
        map(data => {
          let dataObject:any = {}
          dataObject = data;
          dataObject["status"] = 1;
          return dataObject;
        })
      )
    }
    else{
      delete formValue['id']
      return this.httpClient.post<LeftPanelMasterElement[]>(`${environment.apiUrl}/left-panel/`,formValue)
      .pipe(
        map(data => {
          let dataObject:any = {}
          dataObject = data;
          dataObject["status"] = 2;
          return dataObject;
        })
        )
      }
  }
    
    delete(id) {
      return this.httpClient.delete<LeftPanelMasterElement[]>(`${environment.apiUrl}/left-panel/${id}/`,id);
    }
    
    getTableNameData(){
      return this.httpClient.get(`${environment.apiUrl}`.concat('/table-names/'));
    }

}


