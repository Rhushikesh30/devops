import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Observable } from 'rxjs'
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { BillerStructureElement } from '../models/biller-structure';

@Injectable({
  providedIn: 'root'
})
export class DefineBillerStructureService {
  constructor(private httpClient: HttpClient) { }

  getAll(): Observable<BillerStructureElement[]> {
    return this.httpClient.get<BillerStructureElement[]>(`${environment.apiUrl}/define-bill-structure/`)
  }

  create(formValue): Observable<BillerStructureElement[]> {
    const id = formValue['id']
    if (id) {
      for (let indexValues of formValue.initialItemRow) {
        if (indexValues.id == '') {
          delete indexValues.id;
        }
      }
      return this.httpClient.put<BillerStructureElement[]>(`${environment.apiUrl}/define-bill-structure/${id}/`, formValue)
      .pipe(
        map(data => {
          let dataObject:any = {}
          dataObject = data;
          dataObject['status'] = 1;
          return dataObject;
        })
      )
    }
    else{
      delete formValue['id']
      formValue.initialItemRow.filter(function (props) {
        delete props.id;
        return true;
      });
      return this.httpClient.post<BillerStructureElement[]>(`${environment.apiUrl}/define-bill-structure/`,formValue)
      .pipe(
        map(data => {
          let dataObject:any = {}
          dataObject = data;
          dataObject['status'] = 2;
          return dataObject;
        })
      )
    }
  }

  getbillerStructureById(id) {
    return this.httpClient.get<any>(`${environment.apiUrl}/define-bill-structure/${id}/`)
  }

}
