import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs'
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DownloadbillstructureService {
  constructor(private httpClient: HttpClient) { }
  downloadbillstructure(value){
    return this.httpClient.post<any[]>(`${environment.apiUrl}/download-structure/`, value);
  }  

  getAllBillersData(COMPANY_ID,COMPANY_TYPE): Observable<any[]> {
    return this.httpClient.get<any[]>(`${environment.apiUrl}/company/`, { params: {COMPANY_ID,COMPANY_TYPE } })
  }

  getfileDataType(value){
    return this.httpClient.post<any[]>(`${environment.apiUrl}/filetype/`, value);
  }


  uploadFileData(fileData) {
    return this.httpClient.post(`${environment.apiUrl}`.concat('/upload/'), fileData)
      .pipe(map(res => {
        return res;
      })
    );
  }

  insertData(val) {
    return this.httpClient.post<any[]>(`${environment.apiUrl}/insert-biller-file/`, val);
  }

}
