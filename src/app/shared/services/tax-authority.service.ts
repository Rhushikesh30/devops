import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Observable } from 'rxjs'
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { TaxAuthorityMasterElement } from 'src/app/shared/models/tax-authority';

@Injectable({
  providedIn: 'root'
})
export class TaxAuthorityService {
  constructor(private httpClient: HttpClient) { }

  getById(id) {
    return this.httpClient.get<any>(`${environment.apiUrl}/taxAuthority/${id}/`)
  }

  getAll(): Observable<TaxAuthorityMasterElement[]> {
    return this.httpClient.get<TaxAuthorityMasterElement[]>(`${environment.apiUrl}/taxAuthority/`)
  }

  create(formValue): Observable<TaxAuthorityMasterElement[]> {
    const id = formValue.id
    if (id) {
      return this.httpClient.put<TaxAuthorityMasterElement[]>(`${environment.apiUrl}/taxAuthority/${id}/`, formValue)
      .pipe(
        map(data => {
          let dataObject:any = {}
          dataObject = data;
          dataObject["status"] = 1;
          return dataObject;
        })
      )
    }
    else{
      delete formValue['id']
      return this.httpClient.post<TaxAuthorityMasterElement[]>(`${environment.apiUrl}/taxAuthority/`,formValue)
      .pipe(
        map(data => {
          let dataObject:any = {}
          dataObject = data;
          dataObject["status"] = 2;
          return dataObject;
        })
      )
    }
  }

  delete(id) {
    return this.httpClient.delete<TaxAuthorityMasterElement[]>(`${environment.apiUrl}/taxAuthority/${id}/`,id)
  }

  getCountryData() {
    return this.httpClient.get<any>(`${environment.apiUrl}/country/`)
  }

  getMasterDataAsPerType(tax_type) {
    return this.httpClient.get<any[]>(`${environment.apiUrl}/master/`,{params:{tax_type}})
  }

}
