import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs'
import { map } from 'rxjs/operators';
import { NpciCharges } from '../models/npci-charges';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class NpciChargesServiceService {
  constructor(private httpClient: HttpClient) { }

  getAll(): Observable<NpciCharges[]> {
    return this.httpClient.get<NpciCharges[]>(`${environment.apiUrl}/npci-charges/`)
  }

  getBBPOUCompanyData(tenant_type): Observable<any[]> {
    return this.httpClient.get<any[]>(`${environment.apiUrl}/define-bbpou/`,{params:{tenant_type}})
  }

  getNPCIChargesById(id) {
    return this.httpClient.get<any>(`${environment.apiUrl}/npci-charges/${id}/`)
  }

  getBillerCategory(){
    return this.httpClient.get<any[]>(`${environment.apiUrl}/billerCategory/`)
  }


  create(formValue): Observable<NpciCharges[]> {
    const id = formValue['id']
    if (id) {
      for (let indexValue of formValue.initialItemRow) {
        if (indexValue.id == '') {
          delete indexValue.id;
        }
      }
      return this.httpClient.put<NpciCharges[]>(`${environment.apiUrl}/npci-charges/${id}/`, formValue)
      .pipe(
        map(data => {
          let dataObject:any = {}
          dataObject = data;
          dataObject["status"] = 1;
          return dataObject;
        })
      )
    }
    else{
      delete formValue['id']
      formValue.initialItemRow.filter(function (props) {
        delete props.id;
        return true;
      });
      return this.httpClient.post<NpciCharges[]>(`${environment.apiUrl}/npci-charges/`,formValue)
      .pipe(
        map(data => {
          let dataObject:any = {}
          dataObject = data;
          dataObject["status"] = 2;
          return dataObject;
        })
      )
    }
  }

  getAllBillersData(COMPANY_TYPE): Observable<any[]> {
    return this.httpClient.get<any[]>(`${environment.apiUrl}/company/`, { params: {COMPANY_TYPE } })
  }
  getnpciChargesById(id) {
    return this.httpClient.get<any>(`${environment.apiUrl}/npci-charges/${id}/`)
  }

  getSheduleProcessById(id) {
    return this.httpClient.get<NpciCharges[]>(`${environment.apiUrl}`.concat(`/scheduleBatchMst/${id}/`))
  }

  gettaxdata(tax_rate_name){
    return this.httpClient.get<any[]>(`${environment.apiUrl}/tax/`, { params: {tax_rate_name } })
  }

  getFeesCode(){
    return this.httpClient.get<any[]>(`${environment.apiUrl}/feescode/`)
  }

}
