import { Injectable } from '@angular/core';
import { Observable } from 'rxjs'
import { map } from 'rxjs/operators';
import { BBPOUBank } from '../models/bbpou-bank';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class BbpouBankService {
  constructor(private httpClient: HttpClient) { }

  getAll(): Observable<BBPOUBank[]> {
    return this.httpClient.get<BBPOUBank[]>(`${environment.apiUrl}/definebppouBank/`)
  }

  getBBPOUBankById(id) {
    return this.httpClient.get<BBPOUBank[]>(`${environment.apiUrl}/definebppouBank/${id}/`)
  }

  create(formValue): Observable<BBPOUBank[]> {
    const id = formValue['id']
    if (id) {
      for (let indexValues of formValue.initialItemRow) {
        if (indexValues.id == '') {
          delete indexValues.id;
        }
      }
      return this.httpClient.put<BBPOUBank[]>(`${environment.apiUrl}/definebppouBank/${id}/`, formValue)
      .pipe(
        map(data => {
          let dataObject:any = {}
          dataObject = data;
          dataObject['status'] = 1;
          return dataObject;
        })
      )
    }
    else{
      delete formValue['id']
      formValue.initialItemRow.filter(function (props) {
        delete props.id;
        return true;
      });
      return this.httpClient.post<BBPOUBank[]>(`${environment.apiUrl}/definebppouBank/`,formValue)
      .pipe(
        map(data => {
          let dataObject:any = {}
          dataObject = data;
          dataObject['status'] = 2;
          return dataObject;
        })
      )
    }
  }
}
