import { TestBed } from '@angular/core/testing';

import { AggregatorTspService } from './aggregator-tsp.service';

describe('AggregatorTspService', () => {
  let service: AggregatorTspService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AggregatorTspService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
