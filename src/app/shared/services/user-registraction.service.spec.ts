import { TestBed } from '@angular/core/testing';

import { UserRegistractionService } from './user-registraction.service';

describe('UserRegistractionService', () => {
  let service: UserRegistractionService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(UserRegistractionService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
