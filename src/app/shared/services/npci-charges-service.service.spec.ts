import { TestBed } from '@angular/core/testing';

import { NpciChargesServiceService } from './npci-charges-service.service';

describe('NpciChargesServiceService', () => {
  let service: NpciChargesServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(NpciChargesServiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
