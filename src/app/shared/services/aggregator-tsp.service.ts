import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs'
import { map } from 'rxjs/operators';
import { AggregatorCompany } from 'src/app/shared/models/aggregator-company'
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AggregatorTspService {
  constructor(private httpClient: HttpClient) { }

  getCompanyByType(COMPANY_TYPE, SCREEN_NANE){
    return this.httpClient.get<any[]>(`${environment.apiUrl}/define-aggregator/`,{params:{COMPANY_TYPE, SCREEN_NANE}})
  }
  
  getMasterData(master_type) {
    return this.httpClient.get<any[]>(`${environment.apiUrl}/master/oftype/`,{params:{master_type}})
  }

  getBBPOUBankById(id) {
    return this.httpClient.get<any>(`${environment.apiUrl}/define-aggregator/${id}/`)
  }

  create(formValue): Observable<AggregatorCompany[]> {
    const id = formValue['id']
    if (id) {
      for (let indexValue of formValue.initialItemRow) {
        if (indexValue.id == '') {
          delete indexValue.id;
        }
      }
      return this.httpClient.put<AggregatorCompany[]>(`${environment.apiUrl}/define-aggregator/${id}/`, formValue)
      .pipe(
        map(data => {
          let dataObject:any = {}
          dataObject = data;
          dataObject['status'] = 1;
          return dataObject;
        })
      )
    }
    else{
      delete formValue['id']
      formValue.initialItemRow.filter(function (props) {
        delete props.id;
        return true;
      });
      return this.httpClient.post<AggregatorCompany[]>(`${environment.apiUrl}/define-aggregator/`,formValue)
      .pipe(
        map(data => {
          let dataObject:any = {}
          dataObject = data;
          dataObject['status'] = 2;
          return dataObject;
        })
      )
    }
  }
}
