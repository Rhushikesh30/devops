import { TestBed } from '@angular/core/testing';

import { AggregatorChargesService } from './aggregator-charges.service';

describe('AggregatorChargesService', () => {
  let service: AggregatorChargesService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AggregatorChargesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
