import { TestBed } from '@angular/core/testing';

import { LeftPanelAccessService } from './left-panel-access.service';

describe('LeftPanelAccessService', () => {
  let service: LeftPanelAccessService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(LeftPanelAccessService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
