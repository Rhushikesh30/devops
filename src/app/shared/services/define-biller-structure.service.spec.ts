import { TestBed } from '@angular/core/testing';

import { DefineBillerStructureService } from './define-biller-structure.service';

describe('DefineBillerStructureService', () => {
  let service: DefineBillerStructureService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DefineBillerStructureService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
