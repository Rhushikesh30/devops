import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class MisReportService {
  private baseUrl = environment.apiUrl;
  constructor(private http: HttpClient) { }
  
  get_biller_report_data(value){
    return this.http.post<any[]>(`${this.baseUrl}/get-reports`, value);
  }

  getCommissionReport(value){
    return this.http.post<any[]>(`${this.baseUrl}/commision-report`, value);
  }

  getMasterData(master_type) {
    return this.http.get(this.baseUrl.concat(`/master/oftype/${master_type}/`))
  }
}
