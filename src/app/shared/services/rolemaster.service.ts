import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Observable } from 'rxjs'
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { ROLLMasterElement } from 'src/app/shared/models/role-master';
@Injectable({
  providedIn: 'root'
})
export class RolemasterService {
  constructor(private httpClient: HttpClient) { }

  getAll(): Observable<ROLLMasterElement[]> {
    return this.httpClient.get<ROLLMasterElement[]>(`${environment.apiUrl}/role-master/`)
  }

  create(formValue): Observable<ROLLMasterElement[]> {
    const id = formValue.id
    if (id) {
      return this.httpClient.put<ROLLMasterElement[]>(`${environment.apiUrl}/role-master/${id}/`, formValue)
      .pipe(
        map(data => {
          let dataObject:any = {}
          dataObject = data;
          dataObject["status"] = 1;
          return dataObject;
        })
      )
    }
    else{
      return this.httpClient.post<ROLLMasterElement[]>(`${environment.apiUrl}/role-master/`,formValue)
      .pipe(
        map(data => {
          let dataObject:any = {}
          dataObject = data;
          dataObject["status"] = 2;
          return dataObject;
        })
      )
    }
  }

  delete(id) {
    return this.httpClient.delete<ROLLMasterElement[]>(`${environment.apiUrl}/role-master/${id}/`,id)
  }

}

