import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Observable } from 'rxjs'
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { BankMasterElement } from 'src/app/shared/models/bank-master';

@Injectable({
  providedIn: 'root'
})
export class BankMasterService {
  constructor(private httpClient: HttpClient) { }

  getAll(): Observable<BankMasterElement[]> {
    return this.httpClient.get<BankMasterElement[]>(`${environment.apiUrl}/bank/`)
  }

  create(formValue): Observable<BankMasterElement[]> {
    const id = formValue.id
    if (id) {
      return this.httpClient.put<BankMasterElement[]>(`${environment.apiUrl}/bank/${id}/`, formValue)
      .pipe(
        map(data => {
          let dataObject:any = {}
          dataObject = data;
          dataObject['status'] = 1;
          return dataObject;
        })
      )
    }
    else{
      delete formValue['id']
      return this.httpClient.post<BankMasterElement[]>(`${environment.apiUrl}/bank/`,formValue)
      .pipe(
        map(data => {
          let dataObject:any = {}
          dataObject = data;
          dataObject['status'] = 2;
          return dataObject;
        })
      )
    }
  }

  delete(id) {
    return this.httpClient.delete<BankMasterElement[]>(`${environment.apiUrl}/bank/${id}/`,id)
  }

  getBankMasterById(tenant_id) {
    return this.httpClient.get<any>(`${environment.apiUrl}/bank/${tenant_id}/`)
  }
  
  getAccountTypeData(master_type) {
    return this.httpClient.get<any[]>(`${environment.apiUrl}/master/`, { params: {master_type} })
  }
  
  getAllCountryData() {
    return this.httpClient.get<any[]>(`${environment.apiUrl}/country/`)
  }
  
  getAllStateData(Country) {
    return this.httpClient.get<any[]>(`${environment.apiUrl}/state/`, { params: {Country} })
  }
    
  getAllCityData(State) {
    return this.httpClient.get<any[]>(`${environment.apiUrl}/cities/`, { params: {State} })
  }

}
  
