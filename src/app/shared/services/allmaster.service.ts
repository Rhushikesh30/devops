import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Observable } from 'rxjs'
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { Allmaster } from 'src/app/shared/models/allmaster';

@Injectable({
  providedIn: 'root'
})
export class AllmasterService {
  constructor(private httpClient: HttpClient) { }

  getAll(): Observable<Allmaster[]> {
    return this.httpClient.get<Allmaster[]>(`${environment.apiUrl}/master/`)
  }

  create(formValue): Observable<Allmaster[]> {
    const id = formValue.id
    if (id) {
      return this.httpClient.put<Allmaster[]>(`${environment.apiUrl}/master/${id}/`, formValue)
      .pipe(
        map(data => {
          let dataObject:any = {}
          dataObject = data;
          dataObject['status'] = 1;
          return dataObject;
        })
      )
    }
    else{
      return this.httpClient.post<Allmaster[]>(`${environment.apiUrl}/master/`,formValue)
      .pipe(
        map(data => {
          let dataObject:any = {}
          dataObject = data;
          dataObject['status'] = 2;
          return dataObject;
        })
      )
    }
  }

  delete(id) {
    return this.httpClient.delete<Allmaster[]>(`${environment.apiUrl}/master/${id}/`,id);
  }

  getMasterDataByType(master_type): Observable<Allmaster[]> {
    return this.httpClient.get<Allmaster[]>(`${environment.apiUrl}/master/`,{params: {master_type}})
  }
}
