import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Observable } from 'rxjs'
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { UserMasterElement } from 'src/app/shared/models/user-registraction';

@Injectable({
  providedIn: 'root'
})
export class UserRegistractionService {
  constructor(private httpClient: HttpClient) { }

  getUsersById(id) {
    return this.httpClient.get<any>(`${environment.apiUrl}/employee/${id}/`)
  }

  getAll(): Observable<UserMasterElement[]> {
    return this.httpClient.get<UserMasterElement[]>(`${environment.apiUrl}/employee/`)
  }

  create(formValue): Observable<UserMasterElement[]> {
    const id = formValue.id
    if (id) {
      return this.httpClient.put<UserMasterElement[]>(`${environment.apiUrl}/employee/${id}/`, formValue)
      .pipe(
        map(data => {
          let dataObject:any = {}
          dataObject = data;
          dataObject["status"] = 1;
          return dataObject;
        }))
    }
    else{
      delete formValue['id']
      return this.httpClient.post<UserMasterElement[]>(`${environment.apiUrl}/employee/`,formValue)
      .pipe(
        map(data => {
          let dataObject:any = {}
          dataObject = data;
          dataObject["status"] = 2;
          return dataObject;
        })
      )
    }
  }

  delete(id) {
    return this.httpClient.delete<UserMasterElement[]>(`${environment.apiUrl}/employee/${id}/`,id);
  }

}

