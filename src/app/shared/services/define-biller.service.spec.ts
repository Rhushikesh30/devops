import { TestBed } from '@angular/core/testing';

import { DefineBillerService } from './define-biller.service';

describe('DefineBillerService', () => {
  let service: DefineBillerService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DefineBillerService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
