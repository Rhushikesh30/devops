import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs'
import { map } from 'rxjs/operators';
import { AggregatorCharges } from '../models/aggregatorCharges';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AggregatorChargesService {
  constructor(private httpClient: HttpClient) { }

  getAll(): Observable<AggregatorCharges[]> {
    return this.httpClient.get<AggregatorCharges[]>(`${environment.apiUrl}/aggregator-charges/`)
  }

  create(formValue): Observable<AggregatorCharges[]> {
    const id = formValue['id']
    if (id) {
      for (let indexValue of formValue.initialItemRow){
        if (indexValue.id == '') {
          delete indexValue.id;
        }
      }
      return this.httpClient.put<AggregatorCharges[]>(`${environment.apiUrl}/aggregator-charges/${id}/`, formValue)
      .pipe(
        map(data => {
          let dataObject:any = {}
          dataObject = data;
          dataObject["status"] = "1";
          return dataObject;
        })
      )
    }
    else{
      delete formValue['id']
      formValue.initialItemRow.filter(function (props) {
        delete props.id;
        return true;
      });
      return this.httpClient.post<AggregatorCharges[]>(`${environment.apiUrl}/aggregator-charges/`,formValue)
      .pipe(
        map(data => {
          let dataObject:any = {}
          dataObject = data;
          dataObject["status"] = 1;
          return dataObject;
        })
      )
    }
  }

  getAllBillersData(tenant_type): Observable<any[]> {
    return this.httpClient.get<any[]>(`${environment.apiUrl}/define-biller/`,{params:{tenant_type}})
  }

  getBBPOUCompanyData(tenant_type): Observable<any[]> {
    return this.httpClient.get<any[]>(`${environment.apiUrl}/define-bbpou/`,{params:{tenant_type}})
  }

  getAggregatorData(tenant_type){
    return this.httpClient.get<any[]>(`${environment.apiUrl}/define-aggregator/`,{params:{tenant_type}})
  }

  getBillerCategory(){
    return this.httpClient.get<any[]>(`${environment.apiUrl}/billerCategory/`)
  }

  getAggregatorChargesById(id) {
    return this.httpClient.get<any>(`${environment.apiUrl}/aggregator-charges/${id}/`)
  }

  getSheduleProcessById(id) {
    return this.httpClient.get<AggregatorCharges[]>(`${environment.apiUrl}`.concat(`/scheduleBatchMst/${id}/`))
  }

  gettaxdata(tax_rate_name){
    return this.httpClient.get<any[]>(`${environment.apiUrl}/tax/`, { params: {tax_rate_name } })
  }

}
