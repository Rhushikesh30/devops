import { TestBed } from '@angular/core/testing';

import { AllmasterService } from './allmaster.service';

describe('AllmasterService', () => {
  let service: AllmasterService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AllmasterService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
