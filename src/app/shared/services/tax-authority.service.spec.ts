import { TestBed } from '@angular/core/testing';

import { TaxAuthorityService } from './tax-authority.service';

describe('TaxAuthorityService', () => {
  let service: TaxAuthorityService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TaxAuthorityService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
