import { TestBed } from '@angular/core/testing';

import { BillerbankChargesService } from './billerbank-charges.service';

describe('BillerbankChargesService', () => {
  let service: BillerbankChargesService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(BillerbankChargesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
