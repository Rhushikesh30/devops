import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Observable } from 'rxjs'
import { map } from 'rxjs/operators';
import { PartnerElement } from 'src/app/shared/models/partner'
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DefinePartnerService {
  constructor(private httpClient: HttpClient) { }

  getAll(): Observable<PartnerElement[]> {
    return this.httpClient.get<PartnerElement[]>(`${environment.apiUrl}/definePartner/`)
  }

  getAllByType(COMPANY_TYPE): Observable<PartnerElement[]> {
    return this.httpClient.get<PartnerElement[]>(`${environment.apiUrl}/definePartner/`,{params:{COMPANY_TYPE}})
  }

  getPartnerById(id) {
    return this.httpClient.get<any>(`${environment.apiUrl}/definePartner/${id}/`)
  }

  create(formValue): Observable<PartnerElement[]> {
    const id = formValue['id']
    if (id) {
      for (let indexValues of formValue.initialItemRow) {
        if (indexValues.id == '') {
          delete indexValues.id;
        }
      }
      return this.httpClient.put<PartnerElement[]>(`${environment.apiUrl}/definePartner/${id}/`, formValue)
      .pipe(
        map(data => {
          let dataObject:any = {}
          dataObject = data;
          dataObject['status'] = 1;
          return dataObject;
        })
      )
    }
    else{
      delete formValue['id']
      formValue.initialItemRow.filter(function (props) {
        delete props.id;
        return true;
      });
      return this.httpClient.post<PartnerElement[]>(`${environment.apiUrl}/definePartner/`,formValue)
      .pipe(
        map(data => {
          let dataObject:any = {}
          dataObject = data;
          dataObject['status'] = 2;
          return dataObject;
        })
      )
    }
  }

  delete(id) {
    return this.httpClient.delete<PartnerElement[]>(`${environment.apiUrl}/definePartner/${id}/`,id);
  }

}
