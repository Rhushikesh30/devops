import { TestBed } from '@angular/core/testing';

import { DownloadbillstructureService } from './downloadbillstructure.service';

describe('DownloadbillstructureService', () => {
  let service: DownloadbillstructureService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DownloadbillstructureService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
