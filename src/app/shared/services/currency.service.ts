import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Observable } from 'rxjs'
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { Currency } from 'src/app/shared/models/currency';

@Injectable({
  providedIn: 'root'
})
export class CurrencyService {
  constructor(private httpClient: HttpClient) { }

  getAll(): Observable<Currency[]> {
    return this.httpClient.get<Currency[]>(`${environment.apiUrl}/country/`)
  }

  create(formValue): Observable<Currency[]> {
    const id = formValue.id
    if (id) {
      return this.httpClient.put<Currency[]>(`${environment.apiUrl}/country/${id}/`, formValue)
      .pipe(
        map(data => {
          let dataObject:any = {}
          dataObject = data;
          dataObject['status'] = 1;
          return dataObject;
        })
      )
    }
    else{
      return this.httpClient.post<Currency[]>(`${environment.apiUrl}/country/`,formValue)
      .pipe(
        map(data => {
          let dataObject:any = {}
          dataObject = data;
          dataObject['status'] = 2;
          return dataObject;
        })
      )
    }
  }

  delete(id) {
    return this.httpClient.delete<Currency[]>(`${environment.apiUrl}/country/${id}/`,id);
  }

  getState(Country) {
    return this.httpClient.get<any[]>(`${environment.apiUrl}/state/`, { params:{Country}})
  }

  getCity(State) {
    return this.httpClient.get<any[]>(`${environment.apiUrl}/cities/`, { params: { State } })
  }

}
