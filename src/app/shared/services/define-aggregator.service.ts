import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs'
import { map } from 'rxjs/operators';
import { DefineAggregatorElement } from '../models/define-aggregator';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DefineAggregatorService {
  constructor(private httpClient: HttpClient) { }

  getAll(): Observable<DefineAggregatorElement[]> {
    return this.httpClient.get<DefineAggregatorElement[]>(`${environment.apiUrl}/aggregator-charges/`)
  }

  create(formValue): Observable<DefineAggregatorElement[]> {
    const id = formValue['id']
    if (id) {
      for (let indexValues of formValue.initialItemRow) {
        if (indexValues.id == '') {
          delete indexValues.id;
        }
      }
      return this.httpClient.put<DefineAggregatorElement[]>(`${environment.apiUrl}/aggregator-charges/${id}/`, formValue)
      .pipe(
        map(data => {
          let dataObject:any = {}
          dataObject = data;
          dataObject['status'] = 1;
          return dataObject;
        })
      )
    }
    else{
      delete formValue['id']
      formValue.initialItemRow.filter(function (props) {
        delete props.id;
        return true;
      });
      return this.httpClient.post<DefineAggregatorElement[]>(`${environment.apiUrl}/aggregator-charges/`,formValue)
      .pipe(
        map(data => {
          let dataObject:any = {}
          dataObject = data;
          dataObject['status'] = 2;
          return dataObject
        })
      )
    }
  }

  getAllBillersData(COMPANY_TYPE): Observable<any[]> {
    return this.httpClient.get<any[]>(`${environment.apiUrl}/company/`, { params: {COMPANY_TYPE } })
  }
}
