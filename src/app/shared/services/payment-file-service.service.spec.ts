import { TestBed } from '@angular/core/testing';

import { PaymentFileServiceService } from './payment-file-service.service';

describe('PaymentFileServiceService', () => {
  let service: PaymentFileServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PaymentFileServiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
