import { TestBed } from '@angular/core/testing';

import { PartnerChargesService } from './partner-charges.service';

describe('PartnerChargesService', () => {
  let service: PartnerChargesService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PartnerChargesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
