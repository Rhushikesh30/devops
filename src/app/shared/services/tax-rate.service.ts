import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Observable } from 'rxjs'
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { TaxrateMasterElement } from 'src/app/shared/models/tax-rate';
@Injectable({
  providedIn: 'root'
})
export class TaxRateService {
  constructor(private httpClient: HttpClient) { }

  getTaxRateById(id) {
    return this.httpClient.get<any>(`${environment.apiUrl}/taxRate/${id}/`)
  }

  getAll(): Observable<TaxrateMasterElement[]> {
    return this.httpClient.get<TaxrateMasterElement[]>(`${environment.apiUrl}/taxRate/`)
  }

  create(formValue): Observable<TaxrateMasterElement[]> {
    const id = formValue.id
    if (id) {
      return this.httpClient.put<TaxrateMasterElement[]>(`${environment.apiUrl}/taxRate/${id}/`, formValue)
      .pipe(
        map(data => {
          let dataObject:any = {}
          dataObject = data;
          dataObject["status"] = 1;
          return dataObject;
        })
      )
    }
    else{
      delete formValue['id']
      return this.httpClient.post<TaxrateMasterElement[]>(`${environment.apiUrl}/taxRate/`,formValue)
      .pipe(
        map(data => {
          let dataObject:any = {}
          dataObject = data;
          dataObject["status"] = 2;
          return dataObject
        })
      )
    }
  }

  delete(id) {
    return this.httpClient.delete<TaxrateMasterElement[]>(`${environment.apiUrl}/taxRate/${id}/`,id)
  }

  getTaxAuthorityData() {
    return this.httpClient.get<any[]>(`${environment.apiUrl}/taxAuthority/`)
  }

  getMasterDataAsPerType(master_type){
    return this.httpClient.get<any[]>(`${environment.apiUrl}/master/`,{params:{master_type}})
  }

}

