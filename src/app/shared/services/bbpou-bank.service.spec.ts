import { TestBed } from '@angular/core/testing';

import { BbpouBankService } from './bbpou-bank.service';

describe('BbpouBankService', () => {
  let service: BbpouBankService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(BbpouBankService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
