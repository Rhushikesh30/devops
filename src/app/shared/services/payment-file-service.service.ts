
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { PaymentFileMasterElement } from 'src/app/shared/models/payment-file';

@Injectable({
  providedIn: 'root'
})
export class PaymentFileServiceService {
  private baseUrl = environment.apiUrl;
  constructor(private http: HttpClient) { }

  getBillersAgg(tenant_type): Observable<any[]> {
    return this.http.get<any[]>(`${environment.apiUrl}/define-biller/`,{params:{tenant_type}})
  }

  getAll(): Observable<PaymentFileMasterElement[]> {
    return this.http.get<PaymentFileMasterElement[]>(`${environment.apiUrl}/payment-file/`)
  }

  getPaymentBankPercentage(biller_id){
    return this.http.get(`${environment.apiUrl}`.concat('/percentage-blr-banklink/'),{params: {biller_id}})
  }

  getBillerPaymentDetails(biller_id,from_date,to_date){
    return this.http.get(`${environment.apiUrl}`.concat('/biller-payment-details/'),{params: {biller_id,from_date,to_date}})
  }


  create(formValue){
    if (formValue.id) {      
       for (let value of formValue.initialItemRow) {
        if (value.id == '' || value.id == null) {
          delete value.id;
        }
      }
      return this.http.put(this.baseUrl.concat('/payment-file/' + formValue.id + '/'), formValue)
        .pipe(map(data => {
          let dataObject:any = {}
          dataObject = data;
          dataObject['status'] = 2;
          return dataObject;
        })
      );
    }
     else {
       delete formValue['id']
        formValue.initialItemRow.filter(function (props) {
         delete props.id;
         return true;
       });
        return this.http.post(this.baseUrl.concat('/payment-file/'), formValue)
         .pipe(map(data => {
           let dataObject:any = {}
           dataObject = data;
           dataObject['status'] = 1;
           return dataObject;
         })
       );
     }    
   }

}
