import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Observable } from 'rxjs'
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { PartnerCharges } from 'src/app/shared/models/partner-charges';


@Injectable({
  providedIn: 'root'
})
export class PartnerChargesService {
  constructor(private httpClient: HttpClient) { }

  getAll(): Observable<PartnerCharges[]> {
    return this.httpClient.get<PartnerCharges[]>(`${environment.apiUrl}/partnerCharges/`)
  }
  
  create(formValue): Observable<PartnerCharges[]> {
    const id = formValue['id']
    if (id) {
      for (let indexValue of formValue.initialItemRow) {
        if (indexValue.id == '') {
          delete indexValue.id;
        }
      }
      return this.httpClient.put<PartnerCharges[]>(`${environment.apiUrl}/partnerCharges/${id}/`, formValue)
      .pipe(
        map(data => {
          let dataObject:any = {}
          dataObject = data;
          dataObject["status"] = 1;
          return dataObject;
        })
      )
    }
    else{
      delete formValue['id']
      formValue.initialItemRow.filter(function (props) {
        delete props.id;
        return true;
      });
      return this.httpClient.post<PartnerCharges[]>(`${environment.apiUrl}/partnerCharges/`,formValue)
      .pipe(
        map(data => {
          let dataObject:any = {}
          dataObject = data;
          dataObject["status"] = 2;
          return dataObject;
        })
      )
    }
  }

  getAllBillersData(COMPANY_TYPE): Observable<any[]> {
    return this.httpClient.get<any[]>(`${environment.apiUrl}/company/`, { params: {COMPANY_TYPE } })
  }

  getBillerCategory(All){
    return this.httpClient.get<any[]>(`${environment.apiUrl}/billerCategory/`,{params:{All}})
  }

  getPartnerChargesById(id) {
    return this.httpClient.get<any>(`${environment.apiUrl}/partnerCharges/${id}/`)
  }

  getpartnerchargesdata(role,cid) {
    return this.httpClient.get<any[]>(`${environment.apiUrl}/partnerCharges/`,{params:{role,cid}})
  }
  
  gettaxdata(tax_rate_name){
    return this.httpClient.get<any[]>(`${environment.apiUrl}/tax/`, { params: {tax_rate_name } })

  }

  delete(id) {
    return this.httpClient.delete<PartnerCharges[]>(`${environment.apiUrl}/partnerCharges/${id}/`,id);
  }

}



