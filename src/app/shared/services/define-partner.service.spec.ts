import { TestBed } from '@angular/core/testing';

import { DefinePartnerService } from './define-partner.service';

describe('DefinePartnerService', () => {
  let service: DefinePartnerService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DefinePartnerService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
