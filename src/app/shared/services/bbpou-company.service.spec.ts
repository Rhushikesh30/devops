import { TestBed } from '@angular/core/testing';

import { BbpouCompanyService } from './bbpou-company.service';

describe('BbpouCompanyService', () => {
  let service: BbpouCompanyService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(BbpouCompanyService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
