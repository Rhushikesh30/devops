import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs'
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { BillerBankLink } from '../models/biller-bank-link';

@Injectable({
  providedIn: 'root'
})
export class BillerBankLinkService {
  constructor(private httpClient: HttpClient) { }

  getBillersAllBanks(tenant_id){
    return this.httpClient.get<any[]>(`${environment.apiUrl}/bank/`,{params:{tenant_id}})
  }

  getAll(COMPANY_TYPE): Observable<BillerBankLink[]> {
    return this.httpClient.get<BillerBankLink[]>(`${environment.apiUrl}/biller-bank-link/`,{ params: {COMPANY_TYPE} })
  }

  getbillerbankbyDataId(id) {
    return this.httpClient.get<BillerBankLink[]>(`${environment.apiUrl}/biller-bank-link/${id}/`)
  }
  
  create(formValue): Observable<BillerBankLink[]> {
    const id = formValue['id']
    if (id) {
      for (let indexValue of formValue.initialItemRow) {
        if (indexValue.id == '') {
          delete indexValue.id;
        }
      }
      return this.httpClient.put<BillerBankLink[]>(`${environment.apiUrl}/biller-bank-link/${id}/`, formValue)
      .pipe(
        map(data => {
          let dataObject:any = {}
          dataObject = data;
          dataObject['status'] = 1;
          return dataObject;
        })
      )
    }
    else{
      delete formValue['id']
      formValue.initialItemRow.filter(function (props) {
        delete props.id;
        return true;
      });
      return this.httpClient.post<BillerBankLink[]>(`${environment.apiUrl}/biller-bank-link/`,formValue)
      .pipe(
        map(data => {
          let dataObject:any = {}
          dataObject = data;
          dataObject['status'] = 2;
          return dataObject;
        })
      )
    }
  }

}