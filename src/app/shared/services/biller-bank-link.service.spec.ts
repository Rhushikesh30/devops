import { TestBed } from '@angular/core/testing';

import { BillerBankLinkService } from './biller-bank-link.service';

describe('BillerBankLinkService', () => {
  let service: BillerBankLinkService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(BillerBankLinkService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
