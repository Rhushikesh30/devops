import { TestBed } from '@angular/core/testing';

import { DefineAggregatorService } from './define-aggregator.service';

describe('DefineAggregatorService', () => {
  let service: DefineAggregatorService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DefineAggregatorService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
