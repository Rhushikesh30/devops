import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { BBPOUCompany } from 'src/app/shared/models/bbpou-company'
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class BbpouCompanyService {
  constructor(private httpClient: HttpClient) { }

  getAll(): Observable<BBPOUCompany[]> {
    return this.httpClient.get<BBPOUCompany[]>(`${environment.apiUrl}/define-bbpou/`)
  }

  getCompanyByType(tenant_type){
    return this.httpClient.get<BBPOUCompany[]>(`${environment.apiUrl}/define-bbpou/`,{params:{tenant_type}})
  }

  getBBPOUBankById(id) {
    return this.httpClient.get<BBPOUCompany>(`${environment.apiUrl}/define-bbpou/${id}/`)
  }

  create(formValue): Observable<BBPOUCompany[]> {
    const id = formValue['id']
    if (id) {
      for (let indexValues of formValue.initialItemRow) {
        if (indexValues.id == '') {
          delete indexValues.id;
        }
      }
      return this.httpClient.put<BBPOUCompany[]>(`${environment.apiUrl}/define-bbpou/${id}/`, formValue)
      .pipe(
        map(data => {
          let dataObject:any = {}
          dataObject = data;
          dataObject['status'] = 1;
          return dataObject;
        })
      )
    }
    else{
      delete formValue['id']
      formValue.initialItemRow.filter(function (props) {
        delete props.id;
        return true;
      });
      return this.httpClient.post<BBPOUCompany[]>(`${environment.apiUrl}/define-bbpou/`,formValue)
      .pipe(
        map(data => {
          let dataObject:any = {}
          dataObject = data;
          dataObject['status'] = 2;
          return dataObject;
        })
      )
    }
  }

  getroleMst(){
    return this.httpClient.get<any[]>(`${environment.apiUrl}/role-master/`)
  }

  getAllBillersData(COMPANY_TYPE, ROLE_TYPE): Observable<any[]> {
    return this.httpClient.get<any[]>(`${environment.apiUrl}/define-biller/`,{ params:{COMPANY_TYPE,ROLE_TYPE}})
  }

  getAllBillers(tenant_type): Observable<any[]> {
    return this.httpClient.get<any[]>(`${environment.apiUrl}/define-biller/`,{ params:{tenant_type}})
  }

  getAllPBillersData(COMPANY_ID,company_type){
    let res = COMPANY_ID.split(" ")
    if (res[1]!=undefined)
      return this.httpClient.get<any[]>(`${environment.apiUrl}/company/${res[0]}/${company_type}/${res[1]}/`)
    else
      return this.httpClient.get<any[]>(`${environment.apiUrl}/company/${COMPANY_ID}/${company_type}/`)
  }

  getPartnerData(COMPANY_TYPE): Observable<any[]> {
    return this.httpClient.get<any[]>(`${environment.apiUrl}/company/`, { params: {COMPANY_TYPE } })
  }

}
