import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { BillerElement } from '../models/define-biller';
import { environment } from 'src/environments/environment';
import { Allmaster } from 'src/app/shared/models/allmaster';
import { PartnerElement } from 'src/app/shared/models/partner'
@Injectable({
  providedIn: 'root'
})

export class DefineBillerService {
  constructor(private httpClient: HttpClient) { }

  getAll(tenant_type): Observable<BillerElement[]> {
    return this.httpClient.get<BillerElement[]>(`${environment.apiUrl}/define-biller/`,{params:{tenant_type}})
  }

  getMasterDataByType(master_type): Observable<Allmaster[]> {
    return this.httpClient.get<Allmaster[]>(`${environment.apiUrl}/master/`,{params:{master_type}})
  }
  
  getBillerCategoryData(){
    return this.httpClient.get<any[]>(`${environment.apiUrl}/billerCategory/`);
  }

  getAllCountryData(){
    return this.httpClient.get<any[]>(`${environment.apiUrl}/country/`);
  }

  getStateData(Country): Observable<any[]> {
    return this.httpClient.get<any[]>(`${environment.apiUrl}/state/`,{params:{Country}})
  }

  getCityData(State): Observable<any[]> {
    return this.httpClient.get<any[]>(`${environment.apiUrl}/cities/`,{params:{State}})
  }

  getCateOwnershipData(Ownership): Observable<any[]> {
    return this.httpClient.get<any[]>(`${environment.apiUrl}/category-ownership/`,{params:{Ownership}})
  }
  
  getBBPOUCompanyData(tenant_type): Observable<any[]> {
    return this.httpClient.get<any[]>(`${environment.apiUrl}/define-bbpou/`,{params:{tenant_type}})
  }

  getAggregatorPartner(COMPANY_TYPE): Observable<PartnerElement[]> {
    return this.httpClient.get<PartnerElement[]>(`${environment.apiUrl}/definePartner/`,{params:{COMPANY_TYPE}})
  }

  getKycFileData(id) {
    return this.httpClient.get<any>(`${environment.apiUrl}/getKycFilePath/${id}/`)
  }

  uploadFileData(fileData) {
    return this.httpClient.post(`${environment.apiUrl}`.concat('/upload-kyc-file/'), fileData)
      .pipe(map(res => {
        let dataObject:any = {}
        dataObject = res;
        dataObject['status'] = 1;
        return dataObject;
      })
      );
  }

  getBillerById(ID): Observable<BillerElement[]> {
    return this.httpClient.get<BillerElement[]>(`${environment.apiUrl}/define-biller/`,{params:{ID}})
  }

  create(formValue): Observable<BillerElement[]> {
    const id = formValue['id']
    if (id) {
      for (let indexValue of formValue.KYC_initialItemRow) {
        if (indexValue.id == '') {
          delete indexValue.id;
        }
      }
      return this.httpClient.put<BillerElement[]>(`${environment.apiUrl}/define-biller/${id}/`, formValue)
      .pipe(
        map(data => {
          let dataObject:any = {}
          dataObject = data;
          dataObject['status'] = 1;
          return dataObject;
        })
      )
    }
    else{
      delete formValue['id']
      formValue.KYC_initialItemRow.filter(function (props) {
        delete props.id;
        return true;
      });
      return this.httpClient.post<BillerElement[]>(`${environment.apiUrl}/define-biller/`,formValue)
      .pipe(
        map(data => {
          let dataObject:any = {}
          dataObject = data;
          dataObject['status'] = 2;
          return dataObject;
        })
      )
    }
  }

  delete(id) {
    return this.httpClient.delete<BillerElement[]>(`${environment.apiUrl}/define-biller/${id}/`,id);
  }

  getAllBillersData(COMPANY_TYPE): Observable<any[]> {
    return this.httpClient.get<any[]>(`${environment.apiUrl}/company/`,{params:{COMPANY_TYPE } })
  }
}

