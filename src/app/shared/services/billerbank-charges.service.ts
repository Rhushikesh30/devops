import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Observable } from 'rxjs'
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { BillerElement } from 'src/app/shared/models/biller-bank-charges';

@Injectable({
  providedIn: 'root'
})
export class BillerbankChargesService {
  private baseUrl = environment.apiUrl;
  constructor(private httpClient: HttpClient) { }

  getAll(screen_type, tenant_type): Observable<BillerElement[]> {
    return this.httpClient.get<BillerElement[]>(`${environment.apiUrl}/biller-bank-charges/`,{params:{screen_type, tenant_type}})
  }  

  getBBPOUCompanyData(tenant_type): Observable<any[]> {
    return this.httpClient.get<any[]>(`${environment.apiUrl}/define-bbpou/`,{params:{tenant_type}})
  }

  getBillerCategory(){
    return this.httpClient.get<any[]>(`${environment.apiUrl}/billerCategory/`)
  }

  getBillerStandardChargesByID(id){
    return this.httpClient.get<any[]>(`${environment.apiUrl}/billerChargesMst/`,{params:{id}})
  }

  getAggregatorStandardCharges(){
    return this.httpClient.get<any[]>(`${environment.apiUrl}/aggregator_charges_ratecard/`)
  }

  getbillerBankChargesById(id) {
    return this.httpClient.get<any>(`${environment.apiUrl}/biller-bank-charges/${id}/`)
  }
 getbillerbankchargesdata(){
   return this.httpClient.get(this.baseUrl.concat('/biller-bank-charges/'));
  }
  
create(formValue): Observable<BillerElement[]> {
  const id = formValue['id']
  if (id) {
    for (let indexValues of formValue.initialItemRow) {
      if (indexValues.id == '') {
        delete indexValues.id;
      }
    }
    return this.httpClient.put<BillerElement[]>(`${environment.apiUrl}/biller-bank-charges/${id}/`, formValue)
    .pipe(
      map(data => {
        let dataObject:any = {}
        dataObject = data;
        dataObject['status'] = 1;
        return dataObject;
      })
    )
  }
  else{
    delete formValue['id']
    formValue.initialItemRow.filter(function (props) {
      delete props.id;
      return true;
    });
    return this.httpClient.post<BillerElement[]>(`${environment.apiUrl}/biller-bank-charges/`,formValue)
    .pipe(
      map(data => {
        let dataObject:any = {}
        dataObject = data;
        dataObject['status'] = 2;
        return dataObject;
      })
    )
  }
}

gettaxdata(tax_rate_name){
  return this.httpClient.get<any[]>(`${environment.apiUrl}/tax/`, { params: {tax_rate_name } })
}

// Revised
getAllBillersData(COMPANY_TYPE, ROLE_TYPE): Observable<any[]> {
  return this.httpClient.get<any[]>(`${environment.apiUrl}/company/`, { params:{COMPANY_TYPE, ROLE_TYPE}})
}

getAllAggregatorsData(tenant_type, screen_type): Observable<any[]> {
  return this.httpClient.get<any[]>(`${environment.apiUrl}/company/`,{params:{tenant_type, screen_type}})
}

getAllBillersDataBYID(tenant_type): Observable<any[]> {
  return this.httpClient.get<any[]>(`${environment.apiUrl}/define-biller/`,{params: {tenant_type}})
}

}