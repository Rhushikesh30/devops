import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DefinePartnerComponent } from './define-partner.component';

describe('DefinePartnerComponent', () => {
  let component: DefinePartnerComponent;
  let fixture: ComponentFixture<DefinePartnerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DefinePartnerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DefinePartnerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
