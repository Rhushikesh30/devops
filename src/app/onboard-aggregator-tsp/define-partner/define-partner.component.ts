import { Component, OnInit, ViewChild } from '@angular/core';
import Swal from 'sweetalert2';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import * as XLSX from 'xlsx';
import { ManageSecurityService } from 'src/app/core/service/manage-security.service';
import { PartnerElement } from 'src/app/shared/models/partner'
import { DefinePartnerService } from 'src/app/shared/services/define-partner.service';
import { ErrorCodes } from 'src/app/shared/error-codes';

@Component({
  selector: 'app-define-partner',
  templateUrl: './define-partner.component.html',
  styleUrls: ['./define-partner.component.sass'],
  providers: [ErrorCodes]
})
export class DefinePartnerComponent implements OnInit {
  @ViewChild('dataSourcePaginator',{read: MatPaginator}) dataSourcePaginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  dataSource :MatTableDataSource<PartnerElement>;
  resultData: PartnerElement[];
  displayColumns = [
    'actions', 
    'company_name',
    'pincode',
    'city',
    'state',
    'country'];

  renderedData: PartnerElement[];
  screenName:any;
  submitBtn = true;
  showLoader = false;
  rowData = [];
  listDiv:boolean = false;
  showList:boolean = true;
  sidebarData: any;
  bbpouCompanyData =[];
  partnerData=[];
  editFlag = '';
  COMPANY_TYPE:any;
  listingDataCheck:boolean = true;

  constructor(private manageSecurityService :ManageSecurityService, 
              private definePartnerService :DefinePartnerService,
              private errorCodes: ErrorCodes) { }

  ngOnInit(): void {
    this.COMPANY_TYPE  = localStorage.getItem('COMPANY_TYPE');
    this.getAllData();
    this.manageSecurityService.getAccessLeftPanel(localStorage.getItem('USER_ID'), this.screenName).subscribe({
      next:data =>{
        this.sidebarData = data;
      }
    });
  }

  editViewRecord(row,flag){
    this.rowData = row;
    this.showList = false;
    this.submitBtn = flag;
    this.listDiv = true;
    this.showLoader = false;
    this.editFlag = flag

  }

  showFormList(item: boolean) {
    if(item ===false){
      this.listDiv = true;
      this.showList = false;
    }
    else{
      this.listDiv = false;
      this.showList = true;
    }
  }

  getAllData(){
    if(localStorage.getItem('COMPANY_TYPE')=='Aggregator Partner'){
      this.screenName = 'Partner Company';

      this.definePartnerService.getAllByType(localStorage.getItem('COMPANY_TYPE')).subscribe({
        next:(data: PartnerElement[])=>{
          if(data.length==0){
            this.listingDataCheck=false;
          }
          this.dataSource = new MatTableDataSource(data);
          this.dataSource.paginator = this.dataSourcePaginator;
          this.dataSource.sort = this.sort;
          this.dataSource.connect().subscribe(d => this.renderedData = d);
          this.partnerData = data;
        }
      });
    }

    if(localStorage.getItem('COMPANY_TYPE')=='Aggregator'){
      this.screenName = 'Define Agg Partner';
      this.definePartnerService.getAllByType(localStorage.getItem('COMPANY_TYPE')).subscribe({
        next:(data: PartnerElement[])=>{
          if(data.length==0){
            this.listingDataCheck=false;
          }
          this.dataSource = new MatTableDataSource(data);
          this.dataSource.paginator = this.dataSourcePaginator;
          this.dataSource.sort = this.sort;
          this.dataSource.connect().subscribe(d => this.renderedData = d);
          this.partnerData = data;  
        }
      });
    }
  }

  onCancel(item: boolean){
    this.listDiv = item;
    this.showList = true;
    this.submitBtn = true;
    this.rowData = [];
  }

  tbl_FilterDatatable(value:string) {
    this.dataSource.filter = value.trim().toLocaleLowerCase();
  }

  showSwalmessage(message, text, icon, confirmButton): void {
    if (! confirmButton) {
        Swal.fire({ title: message, text: text, icon: icon, timer: 2000, showConfirmButton: false });
    } else {
        Swal.fire({ title: message, text: text, icon: icon, timer: 2000, showConfirmButton: true, confirmButtonText: "Yes", cancelButtonText: "No" });
    }
  }


  onSave(formValue){
    this.showLoader = true;

    this.definePartnerService.create(formValue).subscribe({
      next: (data:any) => {
        if (data['status'] == 2) {
          this.showSwalmessage('Your record has been added successfully!','','success',false)
        }
        else if (data['status'] == 1) {
          this.showSwalmessage('Your Record has been updated successfully!','','success',false)
        }
        this.getAllData();
        this.showList = true;
        this.listDiv = false;
        this.showLoader = false;      
      },
      error:(error) => {
        this.showLoader = false;
        this.showSwalmessage(this.errorCodes.getErrorMessage(error.status), error.error, 'error', true)
      }
    })    
  }

  export_to_excel() {
    let XlsMasterData = this.dataSource.data.map(({company_name,pincode,contact_person_mobile_number,city,country}) =>
    ({company_name,pincode,contact_person_mobile_number,city,country}));
    const workSheet = XLSX.utils.json_to_sheet(XlsMasterData);
    const workBook: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(workBook, workSheet, 'SheetName');
    XLSX.writeFile(workBook, 'Partner.xlsx');
  }

}


