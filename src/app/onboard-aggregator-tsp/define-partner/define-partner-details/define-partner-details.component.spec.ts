import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DefinePartnerDetailsComponent } from './define-partner-details.component';

describe('DefinePartnerDetailsComponent', () => {
  let component: DefinePartnerDetailsComponent;
  let fixture: ComponentFixture<DefinePartnerDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DefinePartnerDetailsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DefinePartnerDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
