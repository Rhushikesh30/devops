import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AllmasterService } from 'src/app/shared/services/allmaster.service';
import { CurrencyService } from 'src/app/shared/services/currency.service';
import { DefineBillerService } from 'src/app/core/service/bbpou.service';
import { DefinePartnerService } from 'src/app/shared/services/define-partner.service';

@Component({
  selector: 'app-define-partner-details',
  templateUrl: './define-partner-details.component.html',
  styleUrls: ['./define-partner-details.component.sass']
})
export class DefinePartnerDetailsComponent implements OnInit {
  @Input() rowData: [];
  @Input() editFlag:string;
  @Input() submitBtn:boolean;
  @Input() showLoader:boolean;
  @Input() billerBankChargesData : any;
  @Output() onSave = new EventEmitter<any>();
  @Output() onCancel = new EventEmitter<any>();
  
  cancelFlag=true
  definePartnerForm: FormGroup;
  btnVal = 'Submit';
  companydata: any = [];
  statedata:any = [];
  citydata:any = [];
  countrydata:any =[];
  ownershipStatusData:any =[];
  BBPOUBankData:any=[];
  shouldisable = false
  submitted = false;
  isAdharExist = false;
  todayDate = new Date().toJSON().split('T')[0];

  constructor(
    private formBuilder: FormBuilder, 
    private allmasterService: AllmasterService,
    private currencyService : CurrencyService,
    private billerService :DefineBillerService,
    private definePartnerService : DefinePartnerService
  ) { }

  ngOnInit(): void {
    this.definePartnerForm = this.formBuilder.group({
      id:[''],
      parent_company_id: localStorage.getItem('COMPANY_ID'),
      company_role: ['Aggregator Partner'],
      company_name: ['', [Validators.required, Validators.pattern("^[a-zA-Z_ ]*$")]],
      company_shortname: ['', [Validators.required, Validators.pattern("^[a-zA-Z_ ]*$")]],
      contact_person_name:['',Validators.required],
      contact_person_mobile_number:['',[Validators.required,Validators.maxLength(10),Validators.minLength(10), Validators.pattern("^[0-9]{9,16}$")]],
      bbpou_bank_ref_id:['',Validators.required],
      address1:['',[Validators.required,Validators.maxLength(30)]],
      address2:[''],
      country_id: [''],
      state_id: [''],
      city_id: [''],
      country_ref_id: ['', Validators.required],
      state_ref_id: ['', Validators.required],
      city_ref_id: ['', Validators.required],
      ownership_status_ref_id: ['', Validators.required],
      cin: ['', [Validators.pattern('[L,U]{1}[0-9]{5}[A-Z]{2}[1-9]{1}[0-9]{3}[A-Z]{3}[0-9]{6}')]],
      pan: ['', [Validators.required, Validators.pattern('[A-Z]{5}[0-9]{4}[A-Z]{1}')]],
      tan: ['', [Validators.pattern('[A-Z]{4}[0-9]{5}[A-Z]{1}')]],
      adhar: ['', [Validators.pattern('[0-9]{12}')]],
      gst: ['', [Validators.pattern('[0-3]{1}[0-9]{1}[A-Z]{5}[0-9]{4}[A-Z]{1}[1-9]{1}Z[0-9A-Z]{1}')]],
      currency_ref_id: [''],
      is_holding_company: [false],
      is_group_company: [false],
      is_this_branch: [false],
      belongs_to_company_id:[''],
      is_this_under_same_management: [false],
      management_belongs_to_company_id:[''],
      pincode: ['', [Validators.required, Validators.maxLength(8), Validators.pattern('[0-9]*')]],
      entity_share_id: [''],
      share_id:[0],
      updated_by: [0],
      company_id: [''],
      email:['',Validators.required],
      initialItemRow: this.formBuilder.array([this.initialItemRow()])
    })

    if(!Array.isArray(this.rowData)){
      this.editViewRecord(this.rowData,this.editFlag);
     }

    this.billerService.getBBPOUBankData().subscribe({
      next:data => {
        this.BBPOUBankData = data;
      }
    });

    this.allmasterService.getMasterDataByType('Ownership').subscribe({
      next:data =>{
        this.ownershipStatusData = data;  
      } 
    });

    this.currencyService.getAll().subscribe({
      next:data=>{
        this.countrydata =data;
      }
    })

    this.definePartnerForm.get('country_ref_id').valueChanges.subscribe({
      next:(val) => {
        if(val!=undefined){
          this.currencyService.getState(val).subscribe({
            next:(data: []) => {
              this.statedata = data;
            }
          });
        }
      }
    });

    this.definePartnerForm.get('state_ref_id').valueChanges.subscribe({
      next:(val) => {
        if(val!=undefined){
          this.currencyService.getCity(val).subscribe({
            next:data=>{
              this.citydata = data;
            }
          });
        }
      }
    });

    this.definePartnerForm.get('adhar').valueChanges.subscribe({
      next:val => {
        if (this.definePartnerForm.get('adhar').valid === true){
          this.isAdharExist = true;
          this.definePartnerForm.get('pan').clearValidators()
          this.definePartnerForm.get('pan').updateValueAndValidity()
        }
        if (this.definePartnerForm.get('adhar').value.length== 0){
          this.isAdharExist = false;
          this.definePartnerForm.get('pan').setValidators([Validators.required]);
          this.definePartnerForm.get('pan').updateValueAndValidity()
        }
      }
    })  

  }//end of ngonint


  initialItemRow() {
    return this.formBuilder.group({
      contact_person_name: ['', Validators.required],
      designation: [''],
      contact_person_mobile_number: ['', Validators.required],
      email: ['', Validators.required],
    });
  }

  setExistingArray(initialArray = []): FormArray {
    const formArray = new FormArray([]);
    initialArray.forEach(element => {
      formArray.push(this.formBuilder.group({
        id: element.id,
        contact_person_name: element.contact_person_name,
        designation: element.designation,
        contact_person_mobile_number: element.contact_person_mobile_number,
        email: element.email,
      }));
    });
    return formArray;
  }

  get formArray() {
    return this.definePartnerForm.get('initialItemRow') as FormArray;
  }

  addNewRow() {   
    this.formArray.push(this.initialItemRow());
  }

  deleteRow(index) {
    if (this.formArray.length == 1) {
      return false;
    } else {
      this.formArray.removeAt(index);
      return true;
    }
  }

  editViewRecord(row,editFlag){
    this.definePartnerService.getPartnerById(row.id).subscribe({
      next:(company: any) => {
        if(editFlag == 'view'){
          this.submitBtn = false;
        }
        if(editFlag == 'edit'){        
          this.submitBtn = true;
          this.btnVal = "Update";
        }
        this.definePartnerForm.patchValue({
          id: company.id,
          company_id:company.company_id,
          company_name: company.company_name,
          bbpou_bank_ref_id:company.bbpou_bank_ref_id,
          address1:company.address1,
          address2:company.address2,
          entity_share_id: company.entity_share_id,
          share_id:company.share_id,
          company_shortname: company.company_shortname,
          country_id: company.country_id,
          state_id: company.state_id,
          city_id: company.city_id,
          country_ref_id: company.country_ref_id,
          state_ref_id: company.state_ref_id,
          city_ref_id: company.city_ref_id,
          location_ref_id: company.location_ref_id,
          ownership_status_ref_id: company.ownership_status_ref_id,
          cin: company.cin,
          pan: company.pan,
          tan: company.tan,
          gst: company.gst,
          email: company.email,
          adhar: company.adhar,
          contact_person_name: company.contact_person_name,
          currency_ref_id: company.currency_ref_id,
          is_holding_company: company.is_holding_company,
          belongs_to_company_id: company.belongs_to_company_id,
          is_group_company: company.is_group_company,
          contact_person_mobile_number: company.contact_person_mobile_number,
          is_this_branch: company.is_this_branch,
          is_this_under_same_management: company.is_this_under_same_management,
          management_belongs_to_company_id: company.management_belongs_to_company_id,
          pincode: company.pincode,
        });
        this.definePartnerForm.setControl('initialItemRow', this.setExistingArray(company.initialItemRow));  
      }
    });

  }

  onCancelForm(){
    this.cancelFlag=false
    this.definePartnerForm.reset();
    this.onCancel.emit(false);
  }

  onSubmit(){
    if(this.definePartnerForm.invalid){
      return
    }
    else{
    this.onSave.emit(this.definePartnerForm.value);
    }

  }


}
