import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DefinePartnerComponent } from './define-partner/define-partner.component';
import { DefineAggregatorTspComponent } from './define-aggregator-tsp/define-aggregator-tsp.component';
import { PartnerChargesComponent } from '../define-charges/agg-partner-charges/partner-charges.component';
import { UserRegistractionComponent } from '../define-bbpou/user-registraction/user-registraction.component';
import { BankMasterComponent } from '../banking/bank-master/bank-master.component';
import { PaymentFileGenerationComponent } from '../transactions/payment-file/payment-file.component'
import { BillerInvoiceComponent } from '../transactions/biller-invoice/biller-invoice.component';

const routes: Routes = [
  {
    path: 'define-aggregator',
    component:DefineAggregatorTspComponent
  },
  {
    path: 'define-partner',
    component:DefinePartnerComponent
  },
  {
    path: 'partner-charges',
    component:PartnerChargesComponent
  },
  {
    path: 'aggregator-users',
    component: UserRegistractionComponent
  },    
  {
    path: 'aggregator-bank-account',
    component: BankMasterComponent
  },
  {
    path: 'payment-file',
    component: PaymentFileGenerationComponent
  },
  {
    path: 'agg-invoice',
    component: BillerInvoiceComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OnboardAggregatorRoutingModule { }
