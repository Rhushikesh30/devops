import { Component, OnInit, ViewChild } from '@angular/core';
import Swal from 'sweetalert2';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import * as XLSX from 'xlsx';
import { ManageSecurityService } from 'src/app/core/service/manage-security.service';
import { AggregatorTspService } from 'src/app/shared/services/aggregator-tsp.service'
import { AggregatorCompany } from 'src/app/shared/models/aggregator-company'
import { ErrorCodes } from 'src/app/shared/error-codes';

@Component({
  selector: 'app-define-aggregator-tsp',
  templateUrl: './define-aggregator-tsp.component.html',
  styleUrls: ['./define-aggregator-tsp.component.sass'],
  providers: [ErrorCodes]
})
export class DefineAggregatorTspComponent implements OnInit {
  @ViewChild('dataSourcePaginator',{read: MatPaginator}) dataSourcePaginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  dataSource :MatTableDataSource<AggregatorCompany>;
  resultData: AggregatorCompany[];
  displayColumns = [
    'actions',
    'company_name',
    'company_type',
    'city',
    'address1',
    'email'
  ];
  
  renderedData: AggregatorCompany[];
  screenName = '';
  submitBtn = true;
  showLoader = false;
  rowData = [];
  listDiv:boolean = false;
  showList:boolean = true;
  sidebarData: any;
  AggregatorCompanyData =[];
  BBPOUBankData=[];
  editFlag = '';
  flag = false;
  COMPANY_TYPE:any;
  listingDataCheck:boolean = true;

  constructor(private manageSecurityService :ManageSecurityService, 
    private AggregatorCompanyService :AggregatorTspService,
    private errorCodes: ErrorCodes) { }

  ngOnInit(): void {
    this.COMPANY_TYPE  = localStorage.getItem('COMPANY_TYPE');
    this.getAllData();
    this.manageSecurityService.getAccessLeftPanel(localStorage.getItem('USER_ID'), this.screenName).subscribe({
      next:data =>{
        this.sidebarData = data; 
      }
    });
  }

  editViewRecord(row,flag){
    this.rowData = row;
    this.showList = false;
    this.submitBtn = flag;
    this.listDiv = true;
    this.showLoader = false;
    this.editFlag = flag    
  }

  showFormList(item: boolean) {
    if(item ===false){
      this.listDiv = true;
      this.showList = false;
    }
    else{
      this.listDiv = false;
      this.showList = true;
    }
  }

  onCancel(item: boolean){
    this.listDiv = item;
    this.showList = true;
    this.submitBtn = true;
    this.rowData = [];
  }

  getAllData(){
    if (this.COMPANY_TYPE == 'Biller-OU'){
    this.screenName = 'Define Aggregator';

      this.AggregatorCompanyService.getCompanyByType(this.COMPANY_TYPE,'Aggregator-List').subscribe({
        next:(data:any) => {
          if(data.length==0){
            this.listingDataCheck=false;
          }
          this.dataSource = new MatTableDataSource(data);
          this.dataSource.paginator = this.dataSourcePaginator;
          this.dataSource.sort = this.sort;
          this.dataSource.connect().subscribe(d => this.renderedData = d);
          this.AggregatorCompanyData = data;  
        }       
      });
    }

    if(this.COMPANY_TYPE == 'Aggregator'){ 
    this.screenName = 'Aggregator TSP';

      this.AggregatorCompanyService.getCompanyByType(this.COMPANY_TYPE,'Aggregator').subscribe({
        next:(data:any) => {
          if(data.length==0){
            this.listingDataCheck=false;
          }
          this.dataSource = new MatTableDataSource(data);
          this.dataSource.paginator = this.dataSourcePaginator;
          this.dataSource.sort = this.sort;
          this.dataSource.connect().subscribe(d => this.renderedData = d);
          this.AggregatorCompanyData = data;        
        }
      });
    }


    if(this.COMPANY_TYPE == 'Aggregator Partner'){ 
    this.screenName = 'Aggregator TSP';
      this.AggregatorCompanyService.getCompanyByType(this.COMPANY_TYPE,'Aggregator').subscribe({
        next:(data:any) => {
          if(data.length==0){
            this.listingDataCheck=false;
          }
          this.dataSource = new MatTableDataSource(data);
          this.dataSource.paginator = this.dataSourcePaginator;
          this.dataSource.sort = this.sort;
          this.dataSource.connect().subscribe(d => this.renderedData = d);
          this.AggregatorCompanyData = data;
        }
      });
    }
  }

  
  showSwalmessage(message, text, icon, confirmButton): void {
    if (! confirmButton) {
      Swal.fire({ title: message, text: text, icon: icon, timer: 2000, showConfirmButton: false });
    } else {
      Swal.fire({ title: message, text: text, icon: icon, timer: 2000, showConfirmButton: true, confirmButtonText: "Yes", cancelButtonText: "No" });
    }
  }

  onSave(formValue){
    this.showLoader = true
    this.AggregatorCompanyService.create(formValue).subscribe({
      next: (data:any) => {
        if (data['status'] == 2) {
          this.showSwalmessage('Your record has been added successfully!','','success',false)
        }
        else if (data['status'] == 1) {
          this.showSwalmessage('Your Record has been updated successfully!','','success',false)
        }
        this.getAllData();
        this.showList = true;
        this.listDiv = false;
        this.showLoader = false      
      },
      error:(error) => {
        this.showLoader = false;
        this.showSwalmessage(this.errorCodes.getErrorMessage(error.status), error.error, 'error', true)
      }
    })
  }
  
  tbl_FilterDatatable(value:string) {
    this.dataSource.filter = value.trim().toLocaleLowerCase();
  }

  export_to_excel() {
    let XlsMasterData = this.dataSource.data.map(({company_name,pincode,contact_person_mobile_number,email,pan,tan,gst,cin}) => ({company_name,pincode,contact_person_mobile_number,email,pan,tan,gst,cin}));    
    const workSheet = XLSX.utils.json_to_sheet(XlsMasterData);
    const workBook: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(workBook, workSheet, 'SheetName');
    XLSX.writeFile(workBook, 'Aggregator Company.xlsx');
  }
}
