import { ComponentFixture, TestBed } from '@angular/core/testing';


import { DefineAggregatorTspComponentDetailsComponent } from './define-aggregator-tsp-details.component';

describe('DefineAggregatorTspComponentDetailsComponent', () => {
  let component: DefineAggregatorTspComponentDetailsComponent;
  let fixture: ComponentFixture<DefineAggregatorTspComponentDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DefineAggregatorTspComponentDetailsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DefineAggregatorTspComponentDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
