import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UserRegistractionComponent } from '../define-bbpou/user-registraction/user-registraction.component';
import { BankMasterComponent} from '../banking/bank-master/bank-master.component';
import { DownloadBillStructureComponent } from '../upload-downloads/download-bill-structure/download-bill-structure.component';
import { UploadDataComponent } from '../upload-downloads/upload-data/upload-data.component';
import { BillerInvoiceComponent } from '../transactions/biller-invoice/biller-invoice.component';
import { DefineBillerComponent } from './define-biller/define-biller.component';
import { DefineBillerStructureComponent } from './define-biller-structure/define-biller-structure.component'
import { BillerBankChargesComponent } from '../define-charges/bbpou-biller-charges/biller-bank-charges.component';
import { AggregatorChargesComponent } from '../define-charges/agg-biller-charges/aggregator-charges.component';
import { BillerBankLinkComponent } from '../banking/biller-bank-link/biller-bank-link.component';
import { PaymentFileGenerationComponent } from '../transactions/payment-file/payment-file.component';
 
const routes: Routes = [
  {
    path: 'biller-bank-link',    
    component: BillerBankLinkComponent
  },
  {
    path: 'employee-registration',
    component: UserRegistractionComponent
  },
  {
    path: 'biller',
    component: DefineBillerComponent
  },
  {
    
    path: 'bank-account',
    component: BankMasterComponent
  },
  {
    path:'difine-bill-structure',
    component:DefineBillerStructureComponent
  },
  {
    path:'dwnbill_structure',
    component:DownloadBillStructureComponent
  },
  {
    path:'upload_data',
    component:UploadDataComponent
  },
  {
    path: 'biller-invoice',
    component: BillerInvoiceComponent
  },
  {
    path: 'bank-charges',
    component: BillerBankChargesComponent
  },
  {
    path: 'plt-charges',
    component: AggregatorChargesComponent
  },
  {
    path: 'payment-file',
    component: PaymentFileGenerationComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DefineBillerRoutingModule { }
