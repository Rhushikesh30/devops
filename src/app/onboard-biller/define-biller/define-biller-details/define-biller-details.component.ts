import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DefineBillerService } from 'src/app/shared/services/define-biller.service';
import { environment } from 'src/environments/environment';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-define-biller-details',
  templateUrl: './define-biller-details.component.html',
  styleUrls: ['./define-biller-details.component.sass']
})
export class DefineBillerDetailsComponent implements OnInit {
  @Input() rowData: [];
  @Input() editFlag:string;
  @Input() submitBtn:boolean;
  @Input() showLoader:boolean;
  @Input() billerData: any;
  @Output() onSave = new EventEmitter<any>();
  @Output() onCancel = new EventEmitter<any>();

  billerForm: FormGroup;
  btnVal = 'Submit';
  cancelFlag=true 

  COMPANY_ID: string;
  COMPANY_TYPE:string;
  tbl_FilteredData = [];
  billersData: any = [];
  statusData: any = [];
  companyType: any = [];
  countryData: any = [];
  BBPOUBankData: any = [];
  BbpouData :any
  PartnerData:any = [];
  uploadData:any
  getuploadData:any
  stateData: any = [];
  cityData: any = [];
  commsStateData: any = [];
  commsCityData: any = [];
  ownershipStatusData: any = [];
  service_model: any = [];
  billing_mode: any = [];
  bill_amount: any = [];
  onboarding_type: any = [];
  settlement_type: any = [];
  biller_category: any = [];
  kycCategoryData: any = [];
  locationData: any = [];
  biller_name:any;
  disablebbpoubank_name:boolean=false;
  shouldisable:boolean=false;
  getdata:any;
  to_set_file_name:any
  disabledsubmit= false;

  constructor(private formBuilder: FormBuilder, private billerService: DefineBillerService) { }

  ngOnInit(): void {
    this.COMPANY_ID = localStorage.getItem('COMPANY_ID');
    this.COMPANY_TYPE  = localStorage.getItem('COMPANY_TYPE');
  
    this.billerForm = this.formBuilder.group({
      id: [''],
      company_name: ['', [Validators.required, Validators.pattern('^[A-Za-z ]{3,30}$')]],
      company_shortname: ['', Validators.required],
      npci_assigned_id: ['', Validators.required],
      bbpou_company_ref_id: ['', Validators.required],
      address1: ['', Validators.required],
      address2: [''],
      country_ref_id: ['', Validators.required],
      state_ref_id: ['', Validators.required],
      city_ref_id: ['', Validators.required],
      bbpou_bank_ref_id:[''],
      ownership_status_ref_id: ['', Validators.required],
      cin: [''],
      pan: ['', [Validators.required, Validators.pattern('[A-Z]{5}[0-9]{4}[A-Z]{1}')]],
      tan: ['', [Validators.required, Validators.pattern('[A-Z]{4}[0-9]{5}[A-Z]{1}')]],
      gst: ['', [Validators.pattern('[0-3]{1}[0-9]{1}[A-Z]{5}[0-9]{4}[A-Z]{1}[1-9]{1}Z[0-9A-Z]{1}')]],
      email: ['', [Validators.required, Validators.email, Validators.minLength(5)]],
      contact_person_name: ['', [Validators.required, Validators.maxLength(30)]],
      contact_person_mobile_number: ['', [Validators.required, Validators.maxLength(10), Validators.pattern('[1-9]{1}[0-9]{9}')]],
      pincode: ['', [Validators.required, Validators.maxLength(6), Validators.pattern('[0-9]{6}')]],
      revision_status: ['Effective'],
      service_model_ref_id: ['', Validators.required],
      billing_mode_ref_id: ['', Validators.required],
      bill_amount_option_ref_id: ['', Validators.required],
      onboarding_ref_id: ['', Validators.required],
      settlement_type_ref_id: ['', Validators.required],
      biller_category_ref_id: ['', Validators.required],
      verification_comments: ['', Validators.required],
      partner_company_ref_id:[],
      KYC_initialItemRow: this.formBuilder.array([this.KYC_initialItemRow()])
    });

    if(!Array.isArray(this.rowData)){
      this.editViewRecord(this.rowData, this.editFlag);
    }

    this.callAllMasterServices()

    this.billerService.getBBPOUCompanyData(this.COMPANY_TYPE).subscribe({
      next:data => {
        this.BBPOUBankData = data;
      }
    });

    if(this.COMPANY_TYPE == 'Aggregator'){
      this.billerService.getAggregatorPartner( this.COMPANY_TYPE).subscribe({
        next:data => {
          this.PartnerData = data;
        }
      });
    }
    
    this.billerService.getBillerCategoryData().subscribe({
      next:(data: []) => {
        this.biller_category = data;
      }
    });

    this.billerService.getAllCountryData().subscribe({
      next:data => {
        this.countryData = data;
      }
    });

    this.billerForm.get('country_ref_id').valueChanges.subscribe({
      next:val => {
        this.stateData = [];
        if (val != null) {
          this.billerService.getStateData(val).subscribe({
            next:(data:[]) => {
              this.stateData = data;
            }
          })
        }
      }
    })

    this.billerForm.get('state_ref_id').valueChanges.subscribe({
      next:val => {
        this.cityData = [];
        if (val != null) {
          this.billerService.getCityData(val).subscribe({
            next:(data: []) => {
              this.cityData = data;
            }
          })
        }
      }
    })

    this.billerForm.get('ownership_status_ref_id').valueChanges.subscribe({
      next:val =>{
        if (val == 5 || val == 6 || val == 7) {
          this.billerForm.controls['cin'].setValidators([Validators.required, Validators.pattern('[A-Z]{1}[0-9]{5}[A-Z]{2}[0-9]{4}[A-Z]{3}[0-9]{6}')]);
          this.billerForm.controls['cin'].updateValueAndValidity();
        } else {
          this.billerForm.controls['cin'].setValidators([Validators.pattern('[A-Z]{1}[0-9]{5}[A-Z]{2}[0-9]{4}[A-Z]{3}[0-9]{6}')]);
          this.billerForm.controls['cin'].updateValueAndValidity();
        }
        if (val !=null){
          this.billerService.getCateOwnershipData(val).subscribe({
            next:(data: any) => {
              this.kycCategoryData = data;
            }
          });
        }
      }
    });  
  }

  callAllMasterServices(){
    this.billerService.getMasterDataByType('Ownership').subscribe({
      next:data =>{
        this.ownershipStatusData = data;
      } 
    });

    this.billerService.getMasterDataByType('Service Model').subscribe({
      next:(data: any) => {
        if (this.COMPANY_TYPE == 'Biller-OU'){
          this.service_model = data.filter(data =>(data.master_key == "TSP"))
        }
        if(this.COMPANY_TYPE == 'Aggregator'){
          this.service_model = data.filter(data =>(data.master_key == "Aggregator"))
        }
      }
    });

    this.billerService.getMasterDataByType('Billing Mode').subscribe({
      next:(data: []) => {
        this.billing_mode = data;
      }
    });

    this.billerService.getMasterDataByType('Bill Amount Option').subscribe({
      next:(data: []) => {
        this.bill_amount = data;
      }
    });

    this.billerService.getMasterDataByType('Onboarding Type').subscribe({
      next:(data:any) => {
        if (this.COMPANY_TYPE == 'Biller-OU'){
          this.onboarding_type = data.filter(data =>(data.master_key == "Direct"))
        }
        if(this.COMPANY_TYPE == 'Aggregator'){
          this.onboarding_type = data.filter(data =>(data.master_key == "Indirect"))
        }
      }
    });

    this.billerService.getMasterDataByType('Settlement Type').subscribe({
      next:(data: []) => {
        this.settlement_type = data;
      }
    });
  }

  KYC_initialItemRow() {
    return this.formBuilder.group({
      file:[''],
      kyc_doc_ref_id: ['', Validators.required],
      kyc_document_path: ['', Validators.required]
    });
  }

  get formArray() {
    return this.billerForm.get('KYC_initialItemRow') as FormArray;
  }

  addNewRow() {
    (document.getElementById('add_button') as HTMLInputElement).disabled = true;
    this.formArray.push(this.KYC_initialItemRow());
  }

  deleteRow(index) {
    if (this.formArray.length == 1) {
      return false;
    } else {
      this.formArray.removeAt(index);
      return true;
    }
  }  

  setExistingArray(initialArray = []): FormArray {
    const formArray = new FormArray([]);
    initialArray.forEach(element => {
      formArray.push(this.formBuilder.group({
        id: element.id,
        file:element.file,
        kyc_doc_ref_id:element.kyc_doc_ref_id,
        kyc_document_path:element.kyc_document_path,
      }));
    });
    return formArray;
  }

  editViewRecord(row, editFlag) {
    this.billerService.getBillerById(row.id).subscribe({
      next:(getData: any) => {
        let data = getData[0]
        if(editFlag == 'view'){
          this.submitBtn = false;
        }
        this.btnVal = 'Update'
        this.billerForm.patchValue({
          id: data.id,
          data_type_ref_id: data.data_type_ref_id,
          company_name: data.company_name,
          company_shortname: data.company_shortname,
          npci_assigned_id: data.npci_assigned_id,
          bbpou_company_ref_id: data.bbpou_company_ref_id, 
          country_ref_id: data.country_ref_id,
          state_ref_id: data.state_ref_id,
          city_ref_id: data.city_ref_id,
          partner_company_ref_id : data.partner_company_ref_id,
          address1: data.address1,
          address2: data.address2,
          company_id:data.company_id,
          bbpou_bank_ref_id:data.bbpou_bank_ref_id,
          pan: data.pan,
          gst: data.gst,
          cin: data.cin,
          email: data.email,
          contact_person_name: data.contact_person_name,
          ownership_status_ref_id: data.ownership_status_ref_id,
          contact_person_mobile_number: data.contact_person_mobile_number,
          pincode: data.pincode,
          tan: data.tan,
          belongs_to_company_id: data.belongs_to_company_id,
          revision_status: data.revision_status,
          service_model_ref_id: data.service_model_ref_id,
          billing_mode_ref_id: data.billing_mode_ref_id,
          bill_amount_option_ref_id: data.bill_amount_option_ref_id,
          onboarding_ref_id: data.onboarding_ref_id,
          settlement_type_ref_id: data.settlement_type_ref_id,
          biller_category_ref_id: data.biller_category_ref_id,
          verification_comments: data.verification_comments,
        });      
        this.billerForm.setControl('KYC_initialItemRow', this.setExistingArray(data.KYC_initialItemRow));
      }
    });
    this.showLoader = false;
  }

  openToViewFile(gridRowId){
    let kyc_path=this.billerForm.get('KYC_initialItemRow').value[gridRowId].kyc_document_path
    let biller_name=this.billerForm.get('company_name').value
    let formData = new FormData();
    formData.append('kyc_path', kyc_path);
    formData.append('biller_name', biller_name);
    this.getuploadData = formData;
    this.billerService.getKycFileData(this.getuploadData).subscribe({
      next:data => {
        this.getdata=data;
        let dataurl=environment.apiUrl+ this.getdata
        window.open(dataurl, 'winname', 'directories=no,titlebar=no,toolbar=no,location=no,status=no,menubar=no,scrollbars=no,resizable=no,width=400,height=350');
      }
    })
  }

  setFileName(event, i) {
    let biller_name=this.billerForm.get('company_name').value
    let file = event.target.files[0];
    let num = 'upload_key_button'+(i+1);
    if(file.size <= 256000){
    if(file.type == "image/png" || file.type == "image/jpg" || file.type == "image/jpeg" || file.type == "application/pdf" ){
      let formData = new FormData();
      formData.append('uploadedFile', file, file.name);
      formData.append('biller_name', biller_name);
      this.uploadData = formData;
      this.to_set_file_name = file.name;
      (document.getElementById(num) as HTMLButtonElement).disabled = false;
    }else
    {
      (document.getElementById(num) as HTMLButtonElement).disabled = true; 
      this.showSwalmessage("File Type Not Supported!",'File type must be png , jpg , jpeg or pdf ',"warning",false);
    }
  }
  else
    {
      (document.getElementById(num) as HTMLButtonElement).disabled = true;    
      this.showSwalmessage("fFile Size Not Supported!",'File size is greater then 256 Kb',"warning",false);
    }
  }

  uploadFile(i){
    let biller_name=this.billerForm.get('company_name').value
    let gridrow = (<FormArray>(this.billerForm.get('KYC_initialItemRow'))).at(i);
    gridrow.value.file_key_path = '/Biller-Documents/'+biller_name+'/' + this.to_set_file_name;
    (<FormArray>(this.billerForm.get('KYC_initialItemRow'))).at(i).get("kyc_document_path").setValue('/uploads/Biller-Documents/'+biller_name+'/' + this.to_set_file_name);//setting the path value to a variable i.e "kyc_document_path"
    let num = 'upload_key_button'+(i+1);
      this.billerService.uploadFileData(this.uploadData).subscribe({
        next:data => {
          if (data['res'] == 'Success') {
            this.showSwalmessage("file is Uploaded successfully!",'',"success",false);
            (document.getElementById('add_button') as HTMLInputElement).disabled = false; //add button
            (document.getElementById(num) as HTMLButtonElement).disabled = true; //upload button
          }
          else if (data['res'] == 'Already Exists') {
            this.showSwalmessage("File Already Exist!",'',"warning",false);
            (document.getElementById('add_button') as HTMLInputElement).disabled = false;
            (document.getElementById(num) as HTMLButtonElement).disabled = false;
          }
          else{
            alert('File Uploading Failed!');
            this.showSwalmessage("File Uploading Failed!",'',"warning",false);
            (document.getElementById('add_button') as HTMLInputElement).disabled = true;
          }
        }
      });
    }


  showSwalmessage(message, text, icon, confirmButton): void {
    if (! confirmButton) {
        Swal.fire({ title: message, text: text, icon: icon, timer: 2000, showConfirmButton: false });
    } else {
        Swal.fire({ title: message, text: text, icon: icon, timer: 2000, showConfirmButton: true, confirmButtonText: "Yes", cancelButtonText: "No" });
    }
  }
  onCancelForm(){
    this.cancelFlag=false
    this.billerForm.reset();
    this.onCancel.emit(false);
  }

  onSubmit(){
    if (this.billerForm.invalid){    
      return;
    }
    else{
      for(let index1=0; index1<this.formArray.length; index1++) { 
        let currentgridRow = this.billerForm.get("KYC_initialItemRow").value
        let selectedfile = currentgridRow[index1].file
        if(this.btnVal == 'Submit'){
          if(selectedfile == null || selectedfile == ''){
            this.showSwalmessage("Please Attach the Doccument!",'',"warning",false);
            return;
          }
        }
    }
    this.onSave.emit(this.billerForm.value);
    }
  }
}
