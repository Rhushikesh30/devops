import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DefineBillerDetailsComponent } from './define-biller-details.component';

describe('DefineBillerDetailsComponent', () => {
  let component: DefineBillerDetailsComponent;
  let fixture: ComponentFixture<DefineBillerDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DefineBillerDetailsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DefineBillerDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
