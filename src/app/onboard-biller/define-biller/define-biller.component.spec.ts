import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DefineBillerComponent } from './define-biller.component';

describe('DefineBillerComponent', () => {
  let component: DefineBillerComponent;
  let fixture: ComponentFixture<DefineBillerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DefineBillerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DefineBillerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
