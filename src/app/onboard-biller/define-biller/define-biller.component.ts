import { Component, OnInit, ViewChild } from '@angular/core';
import Swal from 'sweetalert2';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { BillerElement } from 'src/app/shared/models/define-biller'
import * as XLSX from 'xlsx';
import { ManageSecurityService } from 'src/app/core/service/manage-security.service';
import { DefineBillerService } from 'src/app/shared/services/define-biller.service';  
import { ErrorCodes } from 'src/app/shared/error-codes';

@Component({
  selector: 'app-define-biller',
  templateUrl: './define-biller.component.html',
  styleUrls: ['./define-biller.component.sass'],
  providers: [ErrorCodes]
})
export class DefineBillerComponent implements OnInit {
  @ViewChild('dataSourcePaginator',{read: MatPaginator}) dataSourcePaginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  dataSource :MatTableDataSource<BillerElement>;
  resultData: BillerElement[];

  displayColumns  = [
    'actions',
    'company_name',
    'service_model',
    'address1',
    'email',
    'bbpou_company',
    'biller_category'
  ]
  renderedData: BillerElement[];
  submitBtn = true;
  showLoader = false;
  rowData = [];
  listDiv:boolean = false;
  showList:boolean = true;
  sidebarData: any;
  billerData =[];
  editFlag = '';
  screenName = 'Biller'
  listingDataCheck:boolean = true;

  constructor(private manageSecurityService :ManageSecurityService,
              private defineBillerService :DefineBillerService,
              private errorCodes: ErrorCodes) { }

  ngOnInit(): void {
    this.getAllData();
    this.manageSecurityService.getAccessLeftPanel(localStorage.getItem('USER_ID'), this.screenName).subscribe({
      next:data =>{
        this.sidebarData = data;
      }
    });
  }

  editViewRecord(row,flag){
    this.rowData = row;
    this.showList = false;
    this.submitBtn = flag;
    this.listDiv = true;
    this.showLoader = false;
    this.editFlag = flag
  }

  showFormList(item: boolean) {
    if(item ===false){
      this.listDiv = true;
      this.showList = false;
    }
    else{
      this.listDiv = false;
      this.showList = true;
    }
  }

  onCancel(item: boolean){
    this.listDiv = item;
    this.showList = true;
    this.submitBtn = true;
    this.rowData = [];
  }

  getAllData(){
    this.defineBillerService.getAll(localStorage.getItem('COMPANY_TYPE')).subscribe({
      next:(data: BillerElement[])=>{
        if(data.length==0){
          this.listingDataCheck=false;
        }
        this.dataSource = new MatTableDataSource(data);
        this.dataSource.paginator = this.dataSourcePaginator;
        this.dataSource.sort = this.sort;
        this.dataSource.connect().subscribe(d => this.renderedData = d);
        this.billerData = data;
      }
    });
  }

  tbl_FilterDatatable(value:string) {
    this.dataSource.filter = value.trim().toLocaleLowerCase();
  }

  showSwalmessage(message, text, icon, confirmButton): void {
    if (! confirmButton) {
        Swal.fire({ title: message, text: text, icon: icon, timer: 2000, showConfirmButton: false });
    } else {
        Swal.fire({ title: message, text: text, icon: icon, timer: 2000, showConfirmButton: true, confirmButtonText: "Yes", cancelButtonText: "No" });
    }
  }
  deleteRecord(rowId) {
    Swal.fire({ title: "Are you sure you want to delete?", text: "", icon: "warning", showCancelButton: true, confirmButtonText: "Yes", cancelButtonText: "No" }).then((result) => {
        if (result.value) {
          this.defineBillerService.delete(rowId).subscribe({
            next: (data:any) => {
              this.showSwalmessage("Your record has been deleted successfully!", "", "success", false);
              this.getAllData();
              this.rowData = [];              
            },
            error:(error) => {              
              this.showSwalmessage(this.errorCodes.getErrorMessage(error.status), error.error, 'error', true)
            }
          })
        }
    });
  }

  onSave(formValue){    
    this.showLoader = true;
    this.defineBillerService.create(formValue).subscribe({
      next: (data:any) => {
        if (data['status'] == 2) {
          this.showSwalmessage('Your record has been added successfully!','','success',false)
        }
        else if (data['status'] == 1) {
          this.showSwalmessage('Your record has been updated successfully!','','success',false)
        }
        this.getAllData();
        this.showList = true;
        this.listDiv = false;
        this.showLoader = false;      
      },
      error:(error) => {
        this.showLoader = false;
        this.showSwalmessage(this.errorCodes.getErrorMessage(error.status), error.error, 'error', true)
      }
    })
  }

  export_to_excel() {
    let XlsMasterData = this.dataSource.data.map(({company_name,country,location,city,pincode,ownership_status,contact_person_name, email,bank_name}) => ({company_name,country,location,city,pincode,ownership_status,contact_person_name, email,bank_name}));
    const workSheet = XLSX.utils.json_to_sheet(XlsMasterData);
    const workBook: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(workBook, workSheet, 'SheetName');
    XLSX.writeFile(workBook, 'Biller.xlsx');
  }

}
