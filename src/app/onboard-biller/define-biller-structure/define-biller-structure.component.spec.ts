import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DefineBillerStructureComponent } from './define-biller-structure.component';

describe('DefineBillerStructureComponent', () => {
  let component: DefineBillerStructureComponent;
  let fixture: ComponentFixture<DefineBillerStructureComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DefineBillerStructureComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DefineBillerStructureComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
