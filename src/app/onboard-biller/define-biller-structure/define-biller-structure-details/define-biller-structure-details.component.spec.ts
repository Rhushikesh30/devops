import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DefineBillerStructureDetailsComponent } from './define-biller-structure-details.component';

describe('DefineBillerStructureDetailsComponent', () => {
  let component: DefineBillerStructureDetailsComponent;
  let fixture: ComponentFixture<DefineBillerStructureDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DefineBillerStructureDetailsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DefineBillerStructureDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
