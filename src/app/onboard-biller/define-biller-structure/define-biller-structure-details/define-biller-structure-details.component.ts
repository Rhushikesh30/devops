import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AllmasterService } from 'src/app/shared/services/allmaster.service';
import { DefineBillerStructureService } from 'src/app/shared/services/define-biller-structure.service';
import { SelectionModel } from '@angular/cdk/collections';
import Swal from 'sweetalert2';
import { DefineBillerService } from 'src/app/shared/services/define-biller.service';

@Component({
  selector: 'app-define-biller-structure-details',
  templateUrl: './define-biller-structure-details.component.html',
  styleUrls: ['./define-biller-structure-details.component.sass']
})
export class DefineBillerStructureDetailsComponent implements OnInit {
  @Input() rowData: [];
  @Input() editFlag:string;
  @Input() submitBtn:boolean;
  @Input() showLoader:boolean;
  @Input() billerStructureData : any;
  @Output() onSave = new EventEmitter<any>();
  @Output() onCancel = new EventEmitter<any>();

  selection = new SelectionModel<any>(false, []);
  cancelFlag=true
  billerStructureForm: FormGroup;
  getBillerName=[];
  filetypesData=[];
  fieldData=[];
  npciparameterData=[];
  billersData=[];
  billermodeData=[];
  dateFormats=[];
  showDiv=[];
  allDataTypes=[];
  dyanmicPlaceHolder: any = ["Length"];
  master_key: "Length";

  COMPANY_TYPE:any;
  COMPANY_ID:string;
  blr_cat_id:any;
  blr_name: string;
  blr_cat:any;
  isdisabledflag:boolean = false;
  primaryKeySelection:boolean =false;
  disabled = true;
  submitted = false;
  btnVal ='Temporary Save'
  disabledtemporary_flag:boolean =false;
  todayDate = new Date().toJSON().split('T')[0];

  constructor(private formBuilder: FormBuilder, 
              private allmasterService : AllmasterService,
              private billerStructureService : DefineBillerStructureService, 
              private billerService:DefineBillerService) { }

  ngOnInit(): void {
    this.COMPANY_TYPE = localStorage.getItem('COMPANY_TYPE');
    this.COMPANY_ID = localStorage.getItem('COMPANY_ID');
    this.billerStructureForm = this.formBuilder.group({
      id: [''],
      biller_name: ['',[Validators.required]],
      biller_category: ['', Validators.required],
      source_name: ['', [Validators.required]],
      file_type_ref_id: ['', Validators.required],
      from_date: ['', Validators.required],
      to_date: ['2099-12-31'],
      revision_status: [''],
      version_number: ['1'], 
      is_temporary_flag:[false],
      status: [],
      initialItemRow: this.formBuilder.array([this.initialitemRow()])
    });       

    if(!Array.isArray(this.rowData)){
      this.editViewRecord(this.rowData,this.editFlag);
     }

    this.allmasterService.getMasterDataByType('Type of File').subscribe({
      next:data =>{
        this.filetypesData = data;
      } 
    });

    this.allmasterService.getMasterDataByType('Type of Field').subscribe({
      next:data =>{
        this.fieldData = data;
      } 
    });

    this.allmasterService.getMasterDataByType('NPCI Parameter').subscribe({
      next:data =>{
        this.npciparameterData = data;  
      } 
    });

    this.allmasterService.getMasterDataByType('DateFormats').subscribe({
      next:data =>{
        this.dateFormats = data;  
      } 
    });

    this.billerService.getBillerById(this.COMPANY_ID).subscribe({
      next:(data: any) => {
        this.billersData = data;
        for (let indexValues of this.billersData) {
          let element = indexValues;
          this.blr_name = element.company_name 
          let formated_source_name = element.company_shortname.replace(/ +/g, "_").toLowerCase()
          this.blr_cat = element.biller_category
          this.blr_cat_id = element.biller_category_ref_id
          this.billerStructureForm.get('biller_category').setValue(this.blr_cat_id)
          this.billerStructureForm.get('source_name').setValue(formated_source_name)
          this.billerStructureForm.get('biller_name').setValue(this.blr_name);      
        }
      }
    });

    this.billerStructureForm.get("from_date").valueChanges.subscribe({
      next:val => {
        if (val != null) {
          if (val === this.todayDate) {
            this.billerStructureForm.get("revision_status").setValue("Effective");
          }
          else {
            this.billerStructureForm.get("revision_status").setValue("Future");
          }
        }
      }
    });
  }

  isItTemporary(e){
    if(e.checked===true){
      this.btnVal = 'Permanent Save';
    }
    else{
      this.btnVal = 'Temporary Save';
    }
  }

  initialitemRow() {
    return this.formBuilder.group({
      id: [''],
      sr_no: [''],
      npci_parameter_ref_id: ['', [Validators.required]],
      customer_parameter_field:[false],
      field_data_type_ref_id: ['', [Validators.required]],
      field_name: ['', [Validators.required]],
      field_length: ['', [Validators.required]],
      actual_field_name: ['', [Validators.required]],
      is_primary_field: [false],
    });
  }

  get formArr() {
    return this.billerStructureForm.get('initialItemRow') as FormArray;
  }


  versionCreateFunction(data, row){
    this.btnVal = "Create Version";
    let currtDate = new Date().toJSON().split("T")[0];

    if(row.from_date == currtDate) {
      this.showSwalmessage("Cannot create version on the same day!",'',"warning",false);
      this.onCancelForm();
    }

    let futureCheckData = [];
    futureCheckData = this.billerStructureData.find((x)=>{
    if(x.revision_status === 'Future' && x.source_name === row.source_name ){
        return x
      }
    })

    if(futureCheckData != undefined){
      this.showSwalmessage('Cannot create Bill version as its already created!','','warning',false)
      this.onCancelForm();
    }

    this.disabledtemporary_flag=true;
    for (let indexValues of data.initialItemRow) {
      if(indexValues.is_primary_field===true){
        this.primaryKeySelection=true;
      }
    }  
    
    let newdate = new Date(this.todayDate);
    let new_to_date=newdate.setDate(newdate.getDate()+1);
    let new_to_date_1 =this.format_date(new_to_date)
    this.todayDate = new_to_date_1
  }

  editViewRecord(row,editFlag) {
    this.billerStructureService.getbillerStructureById(row.id).subscribe({
      next: (data: any) => {
        if(editFlag == 'view'){
          this.submitBtn = false;
          let a = row.from_date
          this.billerStructureForm.get("from_date").setValue(a)
        }
        if (editFlag !="veredit"){      
          if(data.is_temporary_flag===false){
            this.btnVal = 'Temporary Save'
          }
          else{
            this.btnVal = 'Permanent Save';
          }
        }
        if(editFlag== 'versionCreate'){
          this.versionCreateFunction(data, row)  
        }
  
        if(editFlag == 'veredit'){
          this.disabledtemporary_flag=true;
          this.btnVal = 'Update Version';
          this.billerStructureForm.get("from_date").setValue(data.from_date)
        }  
        this.billerStructureForm.patchValue({
          biller_name: data.biller_name,
          biller_category: data.biller_category,
          source_name: data.source_name,
          id: data.id,
          file_type_ref_id: data.file_type_ref_id,
          no_of_header_rows: String(data.no_of_header_rows),
          to_date: data.to_date,
          status: data.status,
          is_temporary_flag:data.is_temporary_flag,
          revision_status: data.revision_status,
          version_number: data.version_number,
        });        
        this.billerStructureForm.setControl('initialItemRow', this.setExistingArray(data.initialItemRow));
      }
    });
    this.showLoader = false;    
  }

  setExistingArray(initialArray = []): FormArray {
    const formArray = new FormArray([]);
    initialArray.forEach(element => {
      formArray.push(this.formBuilder.group({
        id: element.id,
        npci_parameter_ref_id: element.npci_parameter_ref_id,
        table_name: element.table_name,
        field_name: element.field_name,
        actual_field_name: element.actual_field_name,
        field_data_type_ref_id: element.field_data_type_ref_id,
        field_length: element.field_length,
        is_key_field: element.is_key_field,
        is_primary_field: element.is_primary_field,
        customer_parameter_field:element.customer_parameter_field,
        sr_no: element.sr_no,
      }));
    });
    return formArray;
  }

  showSwalmessage(message, text, icon, confirmButton): void {
    if (! confirmButton) {
        Swal.fire({ title: message, text: text, icon: icon, timer: 2000, showConfirmButton: false });
    } else {
        Swal.fire({ title: message, text: text, icon: icon, timer: 2000, showConfirmButton: true, confirmButtonText: "Yes", cancelButtonText: "No" });
    }
  }

  addNewRow() {
    this.formArr.push(this.initialitemRow());
    if (this.isdisabledflag===true){
      this.isdisabledflag=false;
    }
  }  

  deleteRow(index) {
    if (this.formArr.length == 1) {
      return false;
    } else {
      this.formArr.removeAt(index);
      return true;
    }
  }
  selectDataFormat(value , i){
    this.showDiv[i] = false;

    let dyanmicPlaceHolder = {
          'Date': " Select Date",
          'Decimal': "eg.10,2",
          'Integer': "Enter valid integer",
          'VarChar': "Enter valid integer",
          'Boolean': "eg.True/False "
        };

    let masterKeyObject = this.fieldData.find((x) => x.id == value.target.value);
    let masterKey = masterKeyObject['master_key']
    this.dyanmicPlaceHolder[i] = dyanmicPlaceHolder[masterKey]

    if(masterKey == 'Date'){
      this.showDiv[i] = true 
      this.allDataTypes[i] = this.dateFormats
    }
  }


  reset_value_details(index_val1){
    let gridRowevent1 = (<FormArray>(this.billerStructureForm.get('initialItemRow'))).at(index_val1);
    gridRowevent1.get('field_length').reset();
  }

  format_date(date){
    let d = new Date(date),
    month = '' + (d.getMonth() + 1),
    day = '' + d.getDate(),
    year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;
    return [year, month,day ].join('-');
  }


  changeFieldName(gridRowID) {
    let gridRow = (<FormArray>(this.billerStructureForm.get('initialItemRow'))).at(gridRowID);
    let actualFieldName = gridRow.get('actual_field_name').value;
    let finalFieldName = actualFieldName.replace(/[&/\\#,+()$~%. '":_*?<>{}]/g, '');
    let field_name = this.billerStructureForm.get('initialItemRow').value

    for (let indexValues of field_name){
      if(indexValues['field_name']==finalFieldName.toLowerCase()){
        this.showSwalmessage("Biller Parameter already taken used another parameters name!",'',"warning",false);
          gridRow.get('field_name').setValue('');
          gridRow.get('actual_field_name').setValue('');
        return
      }
    }
    gridRow.get('field_name').setValue((finalFieldName).toLowerCase());
  }

  CharacterValueAlertFunction(fieldLength, gridRow){
    let newNumber = Number.isInteger(parseInt(fieldLength))
    if (newNumber===true){
      if (parseInt(fieldLength) > 40) {
        gridRow.get('field_length').setValue('');
        this.showSwalmessage("Character Value Length less than 40!",'',"warning",false);
      }else{
        gridRow.get('field_length').setValue(parseInt(fieldLength));          
      }
    }else{
        gridRow.get('field_length').setValue('');
        this.showSwalmessage("Character Value Length Invalid Should be integer!",'',"warning",false);
    }
  }

  DecimalValueAlertFunction(fieldLength, gridRow){
    let fisrtValue = fieldLength.split(',')[0];      
    let newNumber = Number.isInteger(parseInt(fisrtValue))
    if (newNumber===true){
      if (parseInt(fisrtValue) > 10) {
        gridRow.get('field_length').setValue('');
        this.showSwalmessage("Decimal Value Length less than 10!",'',"warning",false);
      }else{
        let NewfisrtValue = parseInt(fisrtValue)
        gridRow.get('field_length').setValue(NewfisrtValue+',2');          
      }
    }
    else{
        gridRow.get('field_length').setValue('');
        this.showSwalmessage("Decimal Value Length Invalid Should be integer with Coma and 2nd Value!",'',"warning",false);
    }
  }

  checkLengthnShowAlerts(gridRow, fieldLength, field_data_type, decimalFormateId, integerFormateId, varCharFormateId){
    if (field_data_type==integerFormateId[0]['id']){      
      let newNumber = Number.isInteger(parseInt(fieldLength))
      if (newNumber===true){
        if (parseInt(fieldLength) > 10) {
          gridRow.get('field_length').setValue('');
          this.showSwalmessage("Integer Value Length less than 10!",'',"warning",false);
        }else{
          gridRow.get('field_length').setValue(parseInt(fieldLength));          
        }
      }else{
          gridRow.get('field_length').setValue('');
          this.showSwalmessage("Integer Value Length Invalid Should be integer!",'',"warning",false);
      }
    }
    else if(field_data_type==varCharFormateId[0]['id']){
      this.CharacterValueAlertFunction(fieldLength, gridRow)
    }
    else if(field_data_type==decimalFormateId[0]['id']){
      this.DecimalValueAlertFunction(fieldLength, gridRow)
    }
  }

  changeFieldNameAccFieldLength(gridRowID) {
    let gridRow = (<FormArray>(this.billerStructureForm.get('initialItemRow'))).at(gridRowID);
    let fieldLength = gridRow.get('field_length').value;
    let field_data_type = gridRow.get('field_data_type_ref_id').value;
    let decimalFormateId = this.fieldData.filter(data =>(data.master_key=='Decimal'))
    let integerFormateId = this.fieldData.filter(data =>(data.master_key=='Integer'))
    let varCharFormateId = this.fieldData.filter(data =>(data.master_key=='VarChar'))
    this.checkLengthnShowAlerts(gridRow, fieldLength, field_data_type, decimalFormateId, integerFormateId, varCharFormateId)
  }

  changeNPCIParameter(gridRowID) {
    let getAdditionalParameter = this.npciparameterData.filter(data =>(data.master_key=='Additional Parameter'))
    let gridRow = (<FormArray>(this.billerStructureForm.get('initialItemRow'))).at(gridRowID);    
    let fieldLength = gridRow.get('npci_parameter_ref_id').value;
    if(getAdditionalParameter[0]['id'] != fieldLength){
      let npci_parameter = (<FormArray>(this.billerStructureForm.get('initialItemRow'))).value
      if(npci_parameter.filter(data=>(data.npci_parameter_ref_id==fieldLength)).length>1){
        let npci = this.npciparameterData.filter(data=>(data.id==fieldLength))
        this.showSwalmessage(npci[0]['master_key']+ "Allready Used!,Please Select another Parameter!",'',"warning",false);
        fieldLength = gridRow.get('npci_parameter_ref_id').setValue('')
        return
      }
    }
  }

  toggleValue(indexKey){
    for(let j=0;j<this.billerStructureForm.get('initialItemRow').value.length;j++){
      if(j!=indexKey){
        let gridRow2 = (<FormArray>(this.billerStructureForm.get('initialItemRow'))).at(j);   
        gridRow2.get("is_primary_field").setValue(false);
      }
    }
  }

  onCancelForm(){
    this.cancelFlag=false
    this.billerStructureForm.reset();
    this.onCancel.emit(false);
  }

  onSubmit(){
    this.submitted = true;
    let npci_bill_number = this.npciparameterData.filter(data =>(data.master_key == 'Bill Number'))
    let chaeckPrimaryKey = this.billerStructureForm.get('initialItemRow').value
    let findPrimaryKey = chaeckPrimaryKey.filter(data =>(data.is_primary_field===true))
    
    if(findPrimaryKey.length==0){
      this.showSwalmessage("Please Select Primary Key as Unique parameters on biller side!",'',"warning",false);
      return      
    }
    if(npci_bill_number[0]['id']!=findPrimaryKey[0]['npci_parameter_ref_id']){
      this.showSwalmessage('Please Select Primary Key as "Bill Number" as NPCI Parameters!','',"warning",false);
      return      
    }
    if (this.billerStructureForm.value.id == 'INVALID' || this.billerStructureForm.value.id == "" || this.billerStructureForm.value.is_temporary_flag===false) {
      this.billerStructureForm.value.status = 'Temporary Save';
    } else {
      this.billerStructureForm.value.status = 'Permanent Save';
    }
    if (this.billerStructureForm.invalid) {
      return;
    } 
    else{
      this.onSave.emit(this.billerStructureForm.value);
    }
  }

}
