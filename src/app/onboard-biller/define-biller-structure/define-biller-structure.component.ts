import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { ManageSecurityService } from 'src/app/core/service/manage-security.service';
import { BillerStructureElement } from 'src/app/shared/models/biller-structure';
import { DefineBillerStructureService } from 'src/app/shared/services/define-biller-structure.service';
import * as XLSX from 'xlsx';
import Swal from 'sweetalert2';
import { ErrorCodes } from 'src/app/shared/error-codes';

@Component({
  selector: 'app-define-biller-structure',
  templateUrl: './define-biller-structure.component.html',
  styleUrls: ['./define-biller-structure.component.sass'],
  providers: [ErrorCodes]

})
export class DefineBillerStructureComponent implements OnInit {
  @ViewChild(DatatableComponent, { static: false }) table: DatatableComponent;
  @ViewChild('dataSourcePaginator',{read: MatPaginator}) dataSourcePaginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  dataSource :MatTableDataSource<BillerStructureElement>;

  resultData: BillerStructureElement[];
  displayColumns  = [
    'actions',
    'biller_name',
    'biller_category',
    'source_name',
    'file_type',
    'from_date',
    'to_date',
    'revision_status',
    'version_number'
  ];
  renderedData: BillerStructureElement[];
  sidebarData: any;
  screenName = 'Define Bill Structure';
  submitBtn = true;
  showLoader = false;
  rowData = [];
  listDiv:boolean = false;
  showList:boolean = true;
  billerStructureData=[];
  editFlag = '';
  
  constructor( private billerStructureService: DefineBillerStructureService, 
               private manageSecurity:ManageSecurityService,
               private errorCodes: ErrorCodes) { }

  ngOnInit(): void {
    this.getAllData();
    let userId = localStorage.getItem('USER_ID');
    this.manageSecurity.getAccessLeftPanel(userId,this.screenName).subscribe({
      next:data =>{
        this.sidebarData = data;
      }
    })
  }

  editViewRecord(row,flag){
    this.rowData = row;
    this.showList = false;
    this.submitBtn = flag;
    this.listDiv = true;
    this.showLoader = false;
    this.editFlag = flag;
  }

  showFormList(item: boolean) {
    if(item ===false){
      this.listDiv = true;
      this.showList = false;
    }
    else{
      this.listDiv = false;
      this.showList = true;
    }
  }

  onCancel(item: boolean){
    this.listDiv = item;
    this.showList = true;
    this.submitBtn = true;
    this.rowData = [];
  }

  showSwalmessage(message, text, icon, confirmButton): void {
    if (! confirmButton) {
        Swal.fire({ title: message, text: text, icon: icon, timer: 2000, showConfirmButton: false });
    } else {
        Swal.fire({ title: message, text: text, icon: icon, timer: 2000, showConfirmButton: true, confirmButtonText: "Yes", cancelButtonText: "No" });
    }
  }

  getAllData(){
    this.billerStructureService.getAll().subscribe({
      next:(data: BillerStructureElement[])=>{
        this.dataSource = new MatTableDataSource(data);
        this.dataSource.paginator = this.dataSourcePaginator;
        this.dataSource.sort = this.sort;
        this.dataSource.connect().subscribe(d => this.renderedData = d);
        this.billerStructureData = data;  
      }
    });
  }

  tbl_FilterDatatable(value:string) {
    this.dataSource.filter = value.trim().toLocaleLowerCase();
  }

  onSave(formValue){
    this.showLoader = true;

    this.billerStructureService.create(formValue).subscribe({
      next: (data:any) => {
        if (data['status'] == 1 && formValue.revision_status == 'Future' && this.editFlag == 'versionCreate' ) {
          this.showSwalmessage('Version Created Successfully!','','success',false)
        }
        else if (data['status'] == 2) {
          this.showSwalmessage('Your record has been added successfully!','','success',false)
        }
        else if (data['status'] == 1  && formValue.is_temporary_flag === false) {
          this.showSwalmessage('Record has been updated successfully!','','success',false)
        }
        else if(data['status'] == 1 && formValue.is_temporary_flag === true && Number(formValue.version_number) == 1) {
          this.showSwalmessage('Your record has been saved Permanently!','','success',false)
        }
        this.getAllData();
        this.showList = true;
        this.listDiv = false;
        this.showLoader = false;     
      },
      error:(error) => {
        this.showLoader = false;
        this.showSwalmessage(this.errorCodes.getErrorMessage(error.status), error.error, 'error', true)
      }
    })
  }

  export_to_excel() {
    let XlsMasterData = this.dataSource.data.map(({biller_name,biller_category,source_name,file_type,from_date,to_date,revision_status,version_number}) => ({biller_name,biller_category,source_name,file_type,from_date,to_date,revision_status,version_number}));
    const workSheet = XLSX.utils.json_to_sheet(XlsMasterData);
    const workBook: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(workBook, workSheet, 'SheetName');
    XLSX.writeFile(workBook, 'Biller Structure.xlsx');
  }

}
