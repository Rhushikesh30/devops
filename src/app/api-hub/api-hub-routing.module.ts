import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DefineApiComponent} from './define-api/define-api.component'
const routes: Routes = [
  {
    path: 'define_api',
    component: DefineApiComponent 
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ApiHubRoutingModule { } 
