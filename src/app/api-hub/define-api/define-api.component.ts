import { Component, OnInit, ViewChild } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ApiHubService } from 'src/app/core/service/api-hub.service';
import Swal from "sweetalert2";
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { Router } from '@angular/router';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { ManageSecurityService } from 'src/app/core/service/manage-security.service';
import { ErrorCodes } from 'src/app/shared/error-codes';

declare const $: any;
@Component({
  selector: 'app-define-api',
  templateUrl: './define-api.component.html',
  styleUrls: ['./define-api.component.sass'],
  providers: [ErrorCodes]
})

export class DefineApiComponent implements OnInit {
  @ViewChild(DatatableComponent, { static: false }) table: DatatableComponent;
  @ViewChild('ApiHubMasterPaginator',{read: MatPaginator}) ApiHubMasterPaginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  ApiHubMasterData :MatTableDataSource<ApiHubMasterElement>;

  displayColumns = [
    'actions',
    'name',    
    'company_name',
    'from_date',
    'to_date',
    'revision_status'
  ];

  renderedData: ApiHubMasterElement[];
  tbl_FilteredData = [];
  sidebardata: any;
  defineApiHubForm: FormGroup;
  apiMasterData = [];
  companyData:any = [];
  typeOfRequests = [];
  apiRequests = [];
  typeOfFields = [];
  btnValue = "Submit";
  todayDate = new Date().toJSON().split('T')[0];
  submitted = false;
  disableBiller = false;
  showLoader = false;
  versionCreationFlag: any = [];
  editAppData: any=[];

  constructor(private formBuilder: FormBuilder, 
              private apiHubService: ApiHubService,
              private router: Router, 
              private manageSecurity:ManageSecurityService,
              private errorCodes: ErrorCodes){ }

  ngOnInit(): void {
    this.showList();
    this.defineApiHubForm = this.formBuilder.group({
      id: [''],
      name: ['', [Validators.required, Validators.pattern('^[A-Za-z ]{3,200}$')]],
      tenant_id: ['', Validators.required],      
      request_type_ref_id: ['', Validators.required],
      from_date: [this.todayDate,Validators.required],
      to_date: ['2099-12-31'],
      revision_status: ['Effective'],
      token_request_ref_id: ['', Validators.required],
      access_token_url: ['', Validators.required],
      agent_id: ['', [Validators.required, Validators.maxLength(700)]],
      agent_keyword: ['', [Validators.required, Validators.maxLength(700)]],
      time_out: [0, [Validators.required, Validators.maxLength(6), Validators.pattern('[0-9]')]],
      end_point_request_ref_id: ['', Validators.required],
      end_point_url: ['', Validators.required],
      api_definition_details: this.formBuilder.array([this.definitionDetailsGrid()]),
      api_definition_sub_details: this.formBuilder.array([this.definitionSubDetailsGrid()])
    });
    
    $(document).ready(function () {
      $('#new_entry_form').hide();
      $('#new_entry_title').hide();
      $('#btn_list').hide();
    });

    this.apiHubService.getApiMasterData().subscribe({
      next: (data:any) => {
        this.apiMasterData = data;
        this.ApiHubMasterData = new MatTableDataSource(data);
        this.ApiHubMasterData.paginator = this.ApiHubMasterPaginator;
        this.ApiHubMasterData.sort = this.sort;
        this.ApiHubMasterData.connect().subscribe(d => this.renderedData = d);
        this.tbl_FilteredData =data;  
      }
    })

    this.manageSecurity.getAccessLeftPanel(localStorage.getItem('USER_ID'),'Define API').subscribe({
      next: (data:any) => {
        this.sidebardata=data; 
      }
    })

    this.apiHubService.getCompanyData(localStorage.getItem('COMPANY_TYPE')).subscribe({
      next: (data:any) => {
        this.companyData=data; 
      }
    })

    this.apiHubService.getMasterData().subscribe({
      next: (data:any) => {
        this.typeOfRequests = data.filter(function (data: any) {
          return data.master_type == 'Type of Request for API';
        });
  
        this.typeOfFields = data.filter(function (data: any) {
          return data.master_type == 'Type of Field';
        });
      }
    })

    this.apiHubService.getMasterData().subscribe({
      next: (data:any) => {
        this.apiRequests = data.filter(function (data: any) {
          return data.master_type == 'Type of Request';
        });
        this.typeOfFields = data.filter(function (data: any) {
          return data.master_type == 'Type of Field';
        });
      }
    })

    this.defineApiHubForm.get("from_date").valueChanges.subscribe(val => {
      if (val != null) {
        if (val === this.todayDate) {
          this.defineApiHubForm.get("revision_status").setValue("Effective");
        } else {
          this.defineApiHubForm.get("revision_status").setValue("Future");
        }
      }
    });

    this.defineApiHubForm.get("from_date").valueChanges.subscribe({
      next: (val:any) => {
        if (val != null) {
          if (val === this.todayDate) {
            this.defineApiHubForm.get("revision_status").setValue("Effective");
          } else {
            this.defineApiHubForm.get("revision_status").setValue("Future");
          }
        }
      }
    })

  }

  showSwalmessage(message, text, icon, confirmButton): void {
    if (! confirmButton) {
      Swal.fire({ title: message, text: text, icon: icon, timer: 2000, showConfirmButton: false });
    } else {
      Swal.fire({ title: message, text: text, icon: icon, timer: 2000, showConfirmButton: true, confirmButtonText: "Yes", cancelButtonText: "No" });
    }
  }

  tbl_FilterDatatable(value:string) {
    this.ApiHubMasterData.filter = value.trim().toLocaleLowerCase();
  }   

  showList() {
    $('#list_form').show();
    $('#list_title').show();
    $('#btn_new_entry').show();
    $('#btn_list').hide();
    $('#new_entry_form').hide();
    $('#new_entry_title').hide();
  }

  showNewEntry() {
    $('#new_entry_form').show();
    $('#new_entry_title').show();
    $('#btn_list').show();
    $('#list_form').hide();
    $('#list_title').hide();
    $('#btn_new_entry').hide();
    this.submitted = false;
  }

  definitionDetailsGrid() {
    return this.formBuilder.group({
      id: [''],
      api_field_name: ['', Validators.required],
      table_name: ['', Validators.required],
      field_name: ['', Validators.required],
      api_field_length: ['', Validators.required],
      field_length: ['', Validators.required],
      api_field_data_type_ref_id: ['', Validators.required],
      field_data_type_ref_id: ['', Validators.required],
    });
  }

  get formArrdDefinitionDetails() {
    return this.defineApiHubForm.get('api_definition_details') as FormArray;
  }

  addNewformArrdDefinitionDetails() {
    this.formArrdDefinitionDetails.push(this.definitionDetailsGrid());
  }

  deleteDefinitionDetails(index) {
    if (this.formArrdDefinitionDetails.length == 1) {
      return false;
    } else {
      this.formArrdDefinitionDetails.removeAt(index);
      return true;
    }
  }

  setExistingArrayDefinitionDetails(initialArray = []): FormArray {
    const formArray = new FormArray([]);
    initialArray.forEach(element => {
      formArray.push(this.formBuilder.group({
        id: element.id,
        api_field_name: element.api_field_name,
        api_field_data_type_ref_id: element.api_field_data_type_ref_id,
        api_field_length: element.api_field_length,
        table_name: element.table_name,
        field_name: element.field_name,
        field_data_type_ref_id: element.field_data_type_ref_id,
        field_length: element.field_length
      }));
    });
    return formArray;
  }

  definitionSubDetailsGrid() {
    return this.formBuilder.group({
      id: [''],
      parameter_name: ['', Validators.required],
      parameter_data_type_ref_id: ['', Validators.required],
      parameter_length: ['', Validators.required]
    });
  }

  get formDefinitionSubDetails(){
    return this.defineApiHubForm.get('api_definition_sub_details') as FormArray;
  }

  addDefinitionSubDetails() {
    this.formDefinitionSubDetails.push(this.definitionSubDetailsGrid());
  }

  deleteDefinitionSubDetails(index) {
    if (this.formDefinitionSubDetails.length == 1) {
      return false;
    } else {
      this.formDefinitionSubDetails.removeAt(index);
      return true;
    }
  }

  setExistingArrayDefinitionSubDetails(initialArray = []): FormArray {
    const formArray = new FormArray([]);
    initialArray.forEach(element => {
      formArray.push(this.formBuilder.group({
        id: element.id,
        parameter_name: element.parameter_name,
        parameter_data_type_ref_id: element.parameter_data_type_ref_id,
        parameter_length: element.parameter_length
      }));
    });
    return formArray;
  }

  get f() {
    return this.defineApiHubForm.controls;
  }  

  onSubmit() {    
    this.showLoader = true;
    this.apiHubService.saveApiHubData(this.defineApiHubForm.value).subscribe({
      next: (data:any) => {
        this.showLoader = false;
        if (data.status === 1) {       
          Swal.fire({
            title: 'Your Record has been added successfully!',
            icon: 'success',
            timer: 2000,
            showConfirmButton: false
          });
        }
        else if (data.status === 2) {
          Swal.fire({
            title: 'Version has been Created successfully!',
            icon: 'success',
            timer: 2000,
            showConfirmButton: false
          });
        }
        this.router.navigate(['/api_hub/define_api']).then(() => {
          setTimeout(function() {window.location.reload();} , 2000);
        });  
      },
      error:(error) => {
        this.showLoader = false;
        this.showSwalmessage(this.errorCodes.getErrorMessage(error.status), error.error, 'error', true)
      }
    })
  }
  
  viewAllMasterData(payload) {
    this.disableBiller = true;
    this.defineApiHubForm.patchValue({
      id: payload.id,
      name: payload.name,
      tenant_id: payload.tenant_id,
      request_type_ref_id: payload.request_type_ref_id,
      from_date: payload.from_date,
      to_date: payload.to_date,
      revision_status: payload.revision_status,
      token_request_ref_id: payload.token_request_ref_id,
      access_token_url: payload.access_token_url,
      agent_id: payload.agent_id,
      agent_keyword: payload.agent_keyword,      
      time_out: payload.time_out,
      end_point_request_ref_id: payload.end_point_request_ref_id,
      end_point_url: payload.end_point_url
    });
    this.defineApiHubForm.setControl('api_definition_details', this.setExistingArrayDefinitionDetails(payload.api_definition_details))
    this.defineApiHubForm.setControl('api_definition_sub_details', this.setExistingArrayDefinitionSubDetails(payload.api_definition_sub_details))

    const id = $('#id').val();
    if (id != '') {
      $('#new_entry_form').show();
      $('#new_entry_title').show();
      $('#btn_list').show();
      $('#list_form').hide();
      $('#list_title').hide();
      $('#btn_new_entry').hide();
      $("#submit_btn").hide();
    }
  }

  createVersion(payload) {
    if(payload.from_date==this.todayDate){
      Swal.fire({
        title: 'Can Not Create Version in Same Day!',
        icon: 'warning',
        timer: 2000,
        showConfirmButton: false
      });
    }else{
      this.disableBiller = true;
      this.defineApiHubForm.patchValue({
        id: payload.id,
        name: payload.name,
        tenant_id: payload.tenant_id,
        request_type_ref_id: payload.request_type_ref_id,
        from_date: this.todayDate,
        to_date: payload.to_date,
        revision_status: payload.revision_status,
        token_request_ref_id: payload.token_request_ref_id,
        access_token_url: payload.access_token_url,
        agent_id: payload.agent_id,
        agent_keyword: payload.agent_keyword,      
        time_out: payload.time_out,
        end_point_request_ref_id: payload.end_point_request_ref_id,
        end_point_url: payload.end_point_url
      });  
      this.defineApiHubForm.setControl('api_definition_details', this.setExistingArrayDefinitionDetails(payload.api_definition_details))
      this.defineApiHubForm.setControl('api_definition_sub_details', this.setExistingArrayDefinitionSubDetails(payload.api_definition_sub_details))
      this.btnValue = 'Create Version';
      const id = $('#id').val();
      if (id != '') {
        $('#new_entry_form').show();
        $('#new_entry_title').show();
        $('#btn_list').show();
        $('#list_form').hide();
        $('#list_title').hide();
        $('#btn_new_entry').hide();
      }
    }
  }
}

export interface ApiHubMasterElement {
  id:any;
  name:any;
  table_name:any;
  company_ref_id:any;
  admin_company_ref_id:any;
  is_it_company_flag:any;
  bbpou_bank_ref_id:any;
  request_type_ref_id:any;
  token_request_ref_id:any;
  from_date:any;
  to_date:any;
  revision_status:any;
  end_point_url:any;
  time_out:any;
  agent_id:any;
  agent_keyword:any;
}