import { Injectable } from '@angular/core';
import { Buffer } from 'buffer';
import { cipher, util, random,pki, md } from "node-forge";
import { globalEnvironment } from 'src/global_module/global_env';
import * as CryptoJS from 'crypto-js';

@Injectable()
export class EncryptionDecryption{

    generateRandomString():string{
        const bytes = random.getBytesSync(16);
        return util.bytesToHex(bytes);
    }

    encryption3Tire(data){
        let dynamicKey = this.generateRandomString()
        let iv = dynamicKey.slice(0,12)
        let cipherObject = cipher.createCipher('AES-GCM', dynamicKey);
        cipherObject.start({
          iv:iv,
          tagLength: 128
        })
        cipherObject.update(util.createBuffer(data));
        let checked = cipherObject.finish();    
        if(checked){
          const encrypted = cipherObject.output;
          let tag = cipherObject.mode.tag;
          let encryptionRequestBody=util.encode64(encrypted.getBytes())
          let encryptedTag=util.encode64(tag.getBytes())
          const rsa_key = pki.privateKeyFromPem(globalEnvironment.privateKeyString);
          const publicKey=pki.publicKeyFromPem(globalEnvironment.publicKeyString)
          const hash = md.sha256.create();
          hash.update(encryptionRequestBody,'utf8')
          let signature = rsa_key.sign(hash);
          let digitalSignature = util.encode64(signature)
          const Header=publicKey.encrypt(dynamicKey,'RSA-OAEP')
          const encryptedHeader = util.encode64(Header)
          const encryptedString = encryptedHeader+globalEnvironment.separator+encryptionRequestBody+globalEnvironment.separator+digitalSignature+globalEnvironment.separator+encryptedTag
          const base64EncryptedString = util.encode64(encryptedString)
          return base64EncryptedString
        }
        return false
    }  
    
    decryption3Tire(encString):string{
        let descryptedBase64EncString = Buffer.from(encString, 'base64').toString('ascii')
        let splitStringArray = descryptedBase64EncString.split(globalEnvironment.separator)
        let headerKey = splitStringArray[0]
        let encryptionRequestBody = splitStringArray[1]
        let digitalSignatureString = splitStringArray[2]
        let tag=splitStringArray[3]
        const encryptedData = util.decode64(headerKey);
        const rsa_key = pki.privateKeyFromPem(globalEnvironment.privateKeyString);
        const decryptedBytes = rsa_key.decrypt(encryptedData, 'RSA-OAEP');
        const decryptedHeader = util.decodeUtf8(decryptedBytes);
        const publicKey = pki.publicKeyFromPem(globalEnvironment.publicKeyString);
        const hash = md.sha256.create();
        hash.update(encryptionRequestBody, 'utf8');
        const hashBytes = util.hexToBytes(hash.digest().toHex());
        const signatureBytes = util.decode64(digitalSignatureString);
        const verified = publicKey.verify(hashBytes, signatureBytes);
        if(!verified){
            throw new Error('Signature verification failed');
        }
        let IV=decryptedHeader.slice(0,12)
        let keyBuffer = util.createBuffer(decryptedHeader);
        let ciphertextBytes = util.decode64(encryptionRequestBody);
        const tag_buffer=Buffer.from(tag,'base64')
        const buffer_array=Array.from(tag_buffer)
        const byteStringBuffer = util.createBuffer();
        for(let item of buffer_array){
            byteStringBuffer.putByte(item);
        }
        // Create AES-GCM cipher object
        let decipher = cipher.createDecipher('AES-GCM', keyBuffer);
        decipher.start({
            iv: IV,
            tagLength:128, 
            tag:byteStringBuffer 
        });
        decipher.update(util.createBuffer(ciphertextBytes));
        let check=decipher.finish();
        if(!check){
            throw new Error("Decryption Algorithm failed");
        }
        // Get the decrypted plaintext
        let plaintext = decipher.output;
        return plaintext.toString()
    }

    decryption1Tire(Base64CBC){
        let iv = CryptoJS.enc.Utf8.parse(`${globalEnvironment.EncryptionIV}`);
        let key = CryptoJS.enc.Utf8.parse(`${globalEnvironment.EncryptionKey}`);
        let decrypted =  CryptoJS.AES.decrypt(Base64CBC, key, { iv: iv, mode: CryptoJS.mode.CFB});
        decrypted = decrypted.toString(CryptoJS.enc.Utf8);
        return decrypted;
    }
    
    encryption1Tire(Base64CBC) {
        let iv = CryptoJS.enc.Utf8.parse(`${globalEnvironment.EncryptionIV}`);
        let key = CryptoJS.enc.Utf8.parse(`${globalEnvironment.EncryptionKey}`);
        let encrypted =  CryptoJS.AES.encrypt(JSON.stringify(Base64CBC), key, { iv: iv, mode: CryptoJS.mode.CFB}).toString();
        return encrypted;
    }

}