import { Injectable } from '@angular/core';
import {HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HttpResponse, HttpErrorResponse} from '@angular/common/http';
import { Observable, map, throwError } from 'rxjs';
import {EncryptionDecryption} from 'src/app/core/encryption-decreption/encrypdecpt';
import { catchError } from 'rxjs/operators';
import { environment } from 'src/environments/environment.prod';
import { globalEnvironment } from 'src/global_module/global_env';

@Injectable()
export class JwtInterceptor implements HttpInterceptor{
  constructor(private EncryptionDecryption: EncryptionDecryption) {}
  handleError(error: HttpErrorResponse){
    let errorMessage = {}
    try {
      errorMessage = {status : error.status, error:error.error.detail}
    }
    catch(err){
      errorMessage = {status : error.status, error:err}
    }
    return throwError(()=> errorMessage);
  }

  #headerName = 'X-XSRF-TOKEN';
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>>{
    let excludedUrls = req.url.split(environment.apiUrl)[1];
    let encryptData:any
    if(!globalEnvironment.excludedUrlList.includes(excludedUrls)){
      encryptData = this.EncryptionDecryption.encryption3Tire(JSON.stringify(req.body))
      encryptData = {'data':encryptData}
    }else{
      encryptData = req.body
    }
    const token:any = localStorage.getItem('token');
    const cookie:any = localStorage.getItem('session_key');
    const csrftoken:any = localStorage.getItem('csrftoken');
    if(token){
      const cloned = req.clone({
            headers: req.headers.set('Authorization', 'Bearer '.concat(token)).set 
            (this.#headerName, csrftoken),
            setHeaders: {'Session-Key': cookie},                       
            body:encryptData
          });
          return next.handle(cloned).pipe(
            map((event: HttpEvent<any>) => {
              if (event instanceof HttpResponse){
                if(!globalEnvironment.excludedUrlList.includes(excludedUrls)){
                  const decryptedData = this.EncryptionDecryption.decryption3Tire(event.body.data) 
                  const decryptedResponse = JSON.parse(decryptedData);
                  return event.clone({ body: decryptedResponse });
                }else{
                  return event
                }
              }
              return event;
            }),
            catchError(this.handleError)
          );
    }else{
      const cloned = req.clone({
        body:encryptData
      });
      return next.handle(cloned).pipe(
        map((event: HttpEvent<any>) => {
          if (event instanceof HttpResponse){
            const decryptedData = this.EncryptionDecryption.decryption3Tire(event.body.data) 
            const decryptedResponse = JSON.parse(decryptedData);
            return event.clone({ body: decryptedResponse });
          }
          return event;
        }),
        catchError(this.handleError)
      )
    }
  }
}
