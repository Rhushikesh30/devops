import { map} from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs'

@Injectable({
  providedIn: 'root'
})
export class ManageSecurityService {
  constructor(private http:HttpClient) { }

  getLeftPanelAccess(user_id,role_id):Observable<any>  {
    return this.http.get<any>(`${environment.apiUrl}/left-panel-user/`,{params: {user_id, role_id}})
  }

  getRoleRef_By_id() {
    return this.http.get(`${environment.apiUrl}`.concat('/role-master/'))
  }
  
  getAccessLeftPanel(username,form_name){
    return this.http.get(`${environment.apiUrl}`.concat('/left-panel-access/'),{params: {username, form_name}})
  }

  get_screen_to_role() {
    return this.http.get(`${environment.apiUrl}`.concat('/screen-roles/'))
  }

  getLeftPanelData() {
    return this.http.get(`${environment.apiUrl}`.concat('/define-left-panel/'))
  }

  saveScreenToRoleData(form) {
    if (form[0].id) {
      for (let value of form){
        if (value.id == ''){
          delete value.id;
        }
      }
      return this.http.put(`${environment.apiUrl}`.concat('/assign-screen-roles/'), form).pipe(map(data => {
        data['status'] = 1;
        return data;
      })
      );
    }
    else {
      delete form['id'];
      form.filter(function (props) {
        delete props.id;
        return true;
      });
      return this.http.post(`${environment.apiUrl}`.concat('/assign-screen-roles/'), form).pipe(map(data => {
        data['status'] = 2;
        return data;
      })
      );
    }
  }

  getAllData(role_id):Observable<any>  {
    return this.http.get<any>(`${environment.apiUrl}/assign-screen-roles/`,{params: {role_id}})
  }

  deleteScreenToRoleData(ID) {
    return this.http.delete(`${environment.apiUrl}`.concat(`/assign-screen-roles/${ID}/`))
      .pipe(map(data=>{
        return data
      }))
  }
}