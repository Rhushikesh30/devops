import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CompanySetupService {
  private baseUrl = environment.apiUrl;
  constructor(private http: HttpClient) { }
  
  getBillersAgg(tenant_type) {
    return this.http.get<any[]>(`${this.baseUrl}/define-biller/`,{params:{tenant_type}})
  }
}

