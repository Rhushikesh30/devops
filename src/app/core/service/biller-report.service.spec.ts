import { TestBed } from '@angular/core/testing';

import { BillerReportService } from './biller-report.service';

describe('BillerReportService', () => {
  let service: BillerReportService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(BillerReportService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
