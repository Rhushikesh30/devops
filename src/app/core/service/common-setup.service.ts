import { map } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { environment } from 'src/environments/environment';
@Injectable({
  providedIn: 'root'
})
export class CommonSetupService {
  private baseUrl = environment.apiUrl;
  constructor(private http: HttpClient) { }

  getUomData() {
    return this.http.get(this.baseUrl.concat('/uom/'));
  }

  saveUomData(value) { 
  if (value.ID) {
    return this.http.put(this.baseUrl.concat('/uom/' + value.ID + '/'), value)
      .pipe(map(data => {
        data['status'] = 1;
        return data;
      })
    );
  }
  else {
    
    return this.http.post(this.baseUrl.concat('/uom/'), value)
      .pipe(map(data => {
        data['status'] = 2;
        return data;
      })
    );
  }
} 

  deleteUomData(ID) {
    return this.http.delete(this.baseUrl.concat(`/uom/${ID}/`))
      .pipe(map(data => {
        return data;
      }));
  }
}


  
