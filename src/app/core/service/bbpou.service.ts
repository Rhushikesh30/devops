import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DefineBillerService {
  private baseUrl = environment.apiUrl;
  constructor(private http: HttpClient) { }

  getBBPOUBankData() {
    return this.http.get<any[]>(`${this.baseUrl}/bbpou-bank/`)
  }
}
