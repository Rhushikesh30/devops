import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { tap, shareReplay } from 'rxjs/operators';
import { User } from '../models/user';
import { environment } from 'src/environments/environment';
import * as moment from 'moment';
import * as CryptoJS from 'crypto-js';
import {globalEnvironment} from 'src/global_module/global_env'

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  public currentUser: Observable<User>;
  code_token = null;
  username: string;
  password: string;
  EncryptionIV: string;
  EncryptionKey: string;

  constructor(private http: HttpClient) {
    this.currentUser=new Observable<User>((subscriber)=>{
      subscriber.next(JSON.parse(JSON.stringify(localStorage)))
      subscriber.complete()
    })
  }

  public get currentUserValue(): User {
    let return_value=null
    this.currentUser.subscribe({
     next(value) {
       return_value=value;
     },
    }).unsubscribe()
     return return_value;
   } 

  getCodeToken(): string {
    return this.code_token; 
  }

  get token(): string {
    return localStorage.getItem('token');
  }


  get refeshToken(): string {
    return localStorage.getItem('refresh_token');
  }
  
  resendCode(){
    return this.http.post(
      `${environment.apiUrl}/auth/code/`,
      {username:this.username, password:this.password}
    )
  }

  setCodeToken(token){
    this.code_token = token;
  }

  code(username: string, password: string){
    this.username = username;
    this.password = password;
    return this.http.post(
      `${environment.apiUrl}/auth/code/`,
      { username, password}
    )
  }

  ResetPassword(username: string){
    this.username = username;
    return this.http.post(
      `${environment.apiUrl}/reset-password/`,
      { username}
    )
  }

  UpdatePassword(user){
    return this.http.post(
      `${environment.apiUrl}/update-password/`,
      user)
  }

  decryptedData(Base64CBC){
    let iv = CryptoJS.enc.Utf8.parse(`${globalEnvironment.EncryptionIV}`);
    let key = CryptoJS.enc.Utf8.parse(`${globalEnvironment.EncryptionKey}`);
    let decrypted =  CryptoJS.AES.decrypt(Base64CBC, key, { iv: iv, mode: CryptoJS.mode.CFB});
    decrypted = decrypted.toString(CryptoJS.enc.Utf8);
    return decrypted;
  }

  encryptedData(Base64CBC) {
    let iv = CryptoJS.enc.Utf8.parse(`${globalEnvironment.EncryptionIV}`);
    let key = CryptoJS.enc.Utf8.parse(`${globalEnvironment.EncryptionKey}`);
    let encrypted =  CryptoJS.AES.encrypt(JSON.stringify(Base64CBC), key, { iv: iv, mode: CryptoJS.mode.CFB}).toString();
    return encrypted;
  }

  private setSession(authResult) {
    this.clearSessions()
    const expiresAt = moment().add(Number(this.decryptedData(authResult.expires_in)),'s');
    localStorage.setItem('token', this.decryptedData(authResult.access_token).toString());
    localStorage.setItem('refresh_token',this.decryptedData(authResult.refresh_token).toString());
    localStorage.setItem('expires_at', expiresAt.valueOf().toString());
    localStorage.setItem('currentUser', this.decryptedData(authResult.id).toString());
    localStorage.setItem('currentUserValue', this.decryptedData(authResult.id).toString());
    localStorage.setItem('USER_ID', this.decryptedData(authResult.id).toString());
    localStorage.setItem('USER_NAME', this.decryptedData(authResult.username).toString());
    localStorage.setItem('EMAIL_ID', this.decryptedData(authResult.email).toString());
    localStorage.setItem('ROLE_ID', this.decryptedData(authResult.role_id).toString());
    localStorage.setItem('ROLE_NAME', this.decryptedData(authResult.role).toString());
    localStorage.setItem('COMPANY_ID', this.decryptedData(authResult.company_id.toString()));
    localStorage.setItem('COMPANY_NAME', this.decryptedData(authResult.company_name.toString()));
    localStorage.setItem('COMPANY_TYPE_REF_ID', this.decryptedData(authResult.company_type_ref_id.toString()));
    localStorage.setItem('COMPANY_TYPE', this.decryptedData(authResult.company_type.toString()));
    localStorage.setItem('session_key', this.decryptedData(authResult.session_key.toString()));
    localStorage.setItem('csrftoken', this.decryptedData(authResult.csrftoken.toString()));
  }

 login(payload) {
    return this.http.post(      
      `${environment.apiUrl}/auth/login/`,payload).pipe( tap(response => {
        if (response['status']==='Success'){
           this.setSession(response);
          this.username = null;
          this.password = null;
        }
      }),
      shareReplay(),
    );
  }

  serverLogOut(){
   this.http.post(`${environment.apiUrl}/auth/logout/`,{ token: this.token }).subscribe();
  }

  clearSessions() {
    localStorage.clear();
    return of({ success: false });
  }

  logoutSessions() {    
    this.startInterval(false)
    if(this.token!==null){
      this.serverLogOut()
      localStorage.clear(); 
    }
    return of({ success: false });
  }

  private UpdateSession(response){
    if(response['status'] =='Success'){
      const expiresAt = moment().add(Number(this.decryptedData(response.expires_in)),'s');
      localStorage.setItem('token', this.decryptedData(response.access_token).toString());
      localStorage.setItem('refresh_token',this.decryptedData(response.refresh_token).toString());
      localStorage.setItem('expires_at', expiresAt.valueOf().toString());
    }
    return true;
  }

  intervalRefreshToken:any
  startInterval(check){
    clearInterval(this.intervalRefreshToken)
    if (check===true){
      this.intervalRefreshToken = setInterval(() =>
       this.refreshToken()
      ,20000)
    }else{
      clearInterval(this.intervalRefreshToken)
    }
  }

  refreshToken() {
    if (moment().isBetween(this.getExpiration().subtract(60,'s'), this.getExpiration())){
      return this.http.post(
        `${environment.apiUrl}/auth/refresh-token/`,{ token: localStorage.getItem('token'), refresh_token : localStorage.getItem('refresh_token')}).pipe(
        tap(response => this.UpdateSession(response)),
        shareReplay(),
      ).subscribe();
    }
  }

  getExpiration() {
    const expiration = localStorage.getItem('expires_at');
    const expiresAt = JSON.parse(expiration);
    return moment(expiresAt);
  }

  isLoggedIn() {
    this.startInterval(true)    
    return moment().isBefore(this.getExpiration());
  }

  isLoggedOut() {
    return !this.isLoggedIn();
  }

  getLicenseKey(){
    return this.http.get(`${environment.apiUrl}`.concat('/licensekey/'));
  }

  saveLicenseKey(form){
    return this.http.post(`${environment.apiUrl}`.concat('/licensewrite_key/'), form)
  }
}

export interface JWTPayload {
  currentUserValue: number;
  currentUser: number;
  exp: number;
  orig_lat: Date;
  ID: number;
  username: string;
  email: string;
  ROLE: string;
  ROLE_ID: string;
  COMPANY_ID: string;
  COMPANY_NAME: string;
  COMPANY_TYPE_REF_ID: string;
  COMPANY_TYPE: string;
}

@Injectable({
  providedIn: 'root'
})

export class DataService {
  private dataSource: BehaviorSubject<string> = new BehaviorSubject<string>('Initial Value');
  data: Observable<string> = this.dataSource.asObservable(); 
  sendData(data) {
    this.dataSource.next(data);
  }
}
 