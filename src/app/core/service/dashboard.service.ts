import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})

export class DashboardService {
  constructor(private http: HttpClient) { }

  GetdashboardCount(tenent, month){
    return this.http.get<any[]>(`${environment.apiUrl}/dashboard-count/`,{params:{tenent, month}})
  }

  getCollectionOut(type){
    return this.http.get<any[]>(`${environment.apiUrl}/collection-count/`,{params:{type}})
  }
}
