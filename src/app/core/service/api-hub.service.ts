import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ApiHubService {
  private baseUrl = environment.apiUrl;
  constructor(private http: HttpClient) { }

  getCompanyData(tenant_type) {
    return this.http.get<any[]>(`${this.baseUrl}/define-biller/`, { params: {tenant_type}})
  }
  
  getMasterData() {
    return this.http.get<any[]>(`${this.baseUrl}/master/`)
  }

  getApiMasterData() {
    return this.http.get<any[]>(`${this.baseUrl}/define-api/`);
  }

  saveApiHubData(data) {
    if (data.id) {
      for (let value of data.api_definition_details){
        if (value.id == '') {
          delete value.id;
        }
      }

      for (let value of data.api_definition_sub_details){
        if (value.id == '') {
          delete value.id;
        }
      }
      return this.http.put(this.baseUrl.concat('/define-api/' + data.id + '/'), data)
        .pipe(map(data => {
          data['status'] = 2;
          return data;
        }));
    }
    else {
      delete data['id']
      for (let indexValue of data.api_definition_details){
        if (indexValue.id == '') {
          delete indexValue.id;
        }
      }
      for (let indexValue of data.api_definition_sub_details){
        if (indexValue.id == '') {
          delete indexValue.id;
        }
      }
      return this.http.post(this.baseUrl.concat('/define-api/'), data)
        .pipe(map(data => {
          data['status'] = 1;
          return data;
        }));
    }
  }
}
