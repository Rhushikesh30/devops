import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { BillerInvoiceMasterElement } from 'src/app/shared/models/biller-invoice';

@Injectable({
  providedIn: 'root'
})
export class BillerService {
  private baseUrl = environment.apiUrl;
  constructor(private http: HttpClient) { }

  getCurrencyData() {
    return this.http.get<any[]>(`${this.baseUrl}/country/`);
  }

  getAll(tenant_type): Observable<BillerInvoiceMasterElement[]> {
    return this.http.get<BillerInvoiceMasterElement[]>(`${environment.apiUrl}/billerinvoice/`,{params:{tenant_type}})
  }

  getCurrentBillerTransactionData(tenant_id){
    return this.http.get<BillerInvoiceMasterElement[]>(`${environment.apiUrl}/biller-invoice-data/`,{params:{tenant_id}})
  }

  getOnboardType(id){
    return this.http.get<BillerInvoiceMasterElement[]>(`${environment.apiUrl}/billerinvoice/${id}/`)  
  }

  saveBillerInvoiceData(data){
    delete data['id']
    return this.http.post(this.baseUrl.concat('/billerinvoice/'),data)
      .pipe(map(data => {
        return data;
      })
    );
  }

}
