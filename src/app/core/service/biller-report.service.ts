import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
@Injectable({
  providedIn: 'root'
})
export class BillerReportService {
  private baseUrl = environment.apiUrl;
  constructor(private http: HttpClient) { }

  getBillerReportData(value){
    return this.http.post<any[]>(`${this.baseUrl}/get-reports`, value);
  }
}
