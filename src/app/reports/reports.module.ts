import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { NgxEchartsModule } from 'ngx-echarts';
import { MatMenuModule } from '@angular/material/menu';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { FullCalendarModule } from '@fullcalendar/angular';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { NgApexchartsModule } from 'ng-apexcharts';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatSelectModule } from '@angular/material/select';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatCardModule } from '@angular/material/card';
import { MatDatepickerModule } from '@angular/material/datepicker'; 
import { MatInputModule } from '@angular/material/input';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { NgxMaskModule } from 'ngx-mask';
import { BillerReportsRoutingModule } from './reports-routing.module';
import { BillerTransactionReportComponent } from './biller-transaction-report/biller-transaction-report.component';
import {PipesModule} from '../pipes/pipes.module';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';
import { MisReportComponent } from './mis-report/mis-report.component';
import { CommissionReportComponent } from './commission-report/commission-report.component';

@NgModule({
  declarations: [BillerTransactionReportComponent, MisReportComponent, CommissionReportComponent],
  imports: [
    CommonModule,
    BillerReportsRoutingModule,
    ReactiveFormsModule,
     NgxEchartsModule,
     MatMenuModule ,
     MatFormFieldModule,
     MatIconModule,
     MatButtonModule,
     FullCalendarModule ,
     PerfectScrollbarModule,
     NgApexchartsModule,
     MatExpansionModule,
     MatSelectModule,
     MatCardModule,
     MatDatepickerModule,
     MatInputModule,
     NgxDatatableModule ,
     NgxMaskModule,
     PipesModule,
     NgxMatSelectSearchModule
  ]
})
export class BillerReportsModule { }
