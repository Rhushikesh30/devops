import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BillerTransactionReportComponent } from './biller-transaction-report/biller-transaction-report.component';
import { MisReportComponent } from './mis-report/mis-report.component';
import { CommissionReportComponent } from './commission-report/commission-report.component';

const routes: Routes = [
  {
    path: 'blrtranc_report',
    component: BillerTransactionReportComponent ,
  } ,

  {
    path: 'mis_report',
    component: MisReportComponent ,
  },

  {
    path: 'commission-report',
    component: CommissionReportComponent,
  }
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BillerReportsRoutingModule { }
