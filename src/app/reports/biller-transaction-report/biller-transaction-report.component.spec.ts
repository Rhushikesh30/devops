import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BillerTransactionReportComponent } from './biller-transaction-report.component';

describe('BillerTransactionReportComponent', () => {
  let component: BillerTransactionReportComponent;
  let fixture: ComponentFixture<BillerTransactionReportComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BillerTransactionReportComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BillerTransactionReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
