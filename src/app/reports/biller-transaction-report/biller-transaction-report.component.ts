import { Component, OnInit ,ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { BillerReportService } from 'src/app/core/service/biller-report.service';
import { BbpouCompanyService } from 'src/app/shared/services/bbpou-company.service';
import { AllmasterService } from 'src/app/shared/services/allmaster.service';
import Swal from 'sweetalert2';
import * as XLSX from 'xlsx';

@Component({
  selector: 'app-biller-transaction-report',
  templateUrl: './biller-transaction-report.component.html',
  styleUrls: ['./biller-transaction-report.component.sass']
})
export class BillerTransactionReportComponent implements OnInit {
  @ViewChild(DatatableComponent, { static: false }) table: DatatableComponent;
  reportForm: FormGroup;
  btnValue =  'Proceed';
  billerData = [];
  statusData=[]
  billerreportData=[]
  key:any
  todayDate = new Date().toJSON().split('T')[0];
  todayDate1 = new Date().toJSON().split('T')[0];
  todayDate2 = new Date().toJSON().split('T')[0];
  company_id:number;
  biller_id:number;
  isBillerLogin = false;
  loginuserdata:any;
  company_type: any;
  BillerNameValue:string
  disabledTo_date:boolean=true
  public getCompanytoSearch: FormControl = new FormControl();

  constructor(private formBuilder: FormBuilder,
              private  BillerReportService: BillerReportService,
              private BbpouCompanyService: BbpouCompanyService,
              private allmasterService: AllmasterService ) { }

  ngOnInit(): void {
    this.company_type = localStorage.getItem('COMPANY_TYPE');
    this.reportForm = this.formBuilder.group({
      biller_name: ['', Validators.required],
      company_name: [''],
      from_date:  ['', Validators.required],
      to_date: ['', Validators.required] ,
      status: ['', Validators.required],
      tenant_type: [this.company_type],
    });

    this.BbpouCompanyService.getAllBillers(this.company_type).subscribe({
      next:(data:any)=>{
        this.billerData = data.sort(this.sortByName)
        if(this.company_type=='Biller'){
          this.isBillerLogin = true;
          this.reportForm.get('biller_name').setValue(localStorage.getItem('COMPANY_ID'))
        }
      }
    });

    this.getCompanytoSearch.valueChanges.subscribe({
      next:val=>{
        this.BillerNameValue=val;
      }
    })

    this.allmasterService.getMasterDataByType('Transaction Report Type').subscribe({
      next:data =>{
        this.statusData = data;
      } 
    });
  }

  sortByName(a: any, b: any) {
    let nameA = a.company_name
    let nameB = b.company_name
    if (nameA < nameB) {
      return -1;
    }
    if (nameA > nameB) {
      return 1;
    }
    return 0;
  }

  showSwalmessage(message, text, icon, confirmButton): void {
    if (! confirmButton) {
        Swal.fire({ title: message, text: text, icon: icon, timer: 2000, showConfirmButton: false });
    } else {
        Swal.fire({ title: message, text: text, icon: icon, timer: 2000, showConfirmButton: true, confirmButtonText: "Yes", cancelButtonText: "No" });
    }
  }

  onSubmit() {
    this.BillerReportService.getBillerReportData(this.reportForm.value)
      .subscribe({
        next:data => {
          if(data.length!=0){
            let XlsMasterData = data
            const workSheet = XLSX.utils.json_to_sheet(XlsMasterData);
            const workBook: XLSX.WorkBook = XLSX.utils.book_new();
            XLSX.utils.book_append_sheet(workBook, workSheet, 'SheetName');
            XLSX.writeFile(workBook, 'TransactionReport.csv');
          }else{
            this.showSwalmessage("No record Found!", "", "warning", true);
          } 
        },
        error:(error) => {              
          this.showSwalmessage("Something Went wrong", error, "warning", true);
        }
      }
    );
  }
}











