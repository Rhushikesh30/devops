import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { DatatableComponent} from '@swimlane/ngx-datatable';
import { MisReportService } from 'src/app/shared/services/mis-report.service'
import { BbpouCompanyService } from 'src/app/shared/services/bbpou-company.service'
import { Router } from '@angular/router';
import { AllmasterService } from 'src/app/shared/services/allmaster.service';
import Swal from 'sweetalert2';
import * as FileSaver from 'file-saver';

@Component({
  selector: 'app-mis-report',
  templateUrl: './mis-report.component.html',
  styleUrls: ['./mis-report.component.sass']
})
export class MisReportComponent implements OnInit {

  @ViewChild(DatatableComponent, { static: false }) table: DatatableComponent;

  reportForm: FormGroup;
  btnValue =  'Proceed';
  billerData = [];
  statusData=[]
  billerreportData=[]
  public filterBillers = [];

  key:any
  todayDate = new Date().toJSON().split('T')[0];
  todayDate1 = new Date().toJSON().split('T')[0];
  todayDate2 = new Date().toJSON().split('T')[0];
  company_id:number;
  biller_id:number
  is_it_company_checked = false;
  loginuserdata:any;
  shouldDisable: boolean = false;
  company_type: any;
  BillerNameValue:string;
  blr_name:string;
  hide:boolean = false;
  show:boolean = true
  disabledTo_date:boolean=true;
  public getCompanytoSearch: FormControl = new FormControl();

  constructor(private formBuilder: FormBuilder,private BbpouCompanyService: BbpouCompanyService,
    private  MisReportService: MisReportService,private router: Router , private allmasterService: AllmasterService) { }

  ngOnInit(): void {
    this.reportForm = this.formBuilder.group({
      id: [''],
      biller_name: ['', Validators.required],
      from_date:  ['', Validators.required],
      to_date: ['', Validators.required] ,
      status: ['', Validators.required],
    });

    this.BbpouCompanyService.getAllBillersData(this.company_type,'All-Billers').subscribe({
      next:(data:any)=>{
        this.billerData = data
        this.filterBillers = this.billerData.slice();
      }
    });
   
    this.allmasterService.getMasterDataByType(' Transaction Report Type').subscribe({
      next:data =>{
        this.statusData = data;  
      } 
    });
  }
  
  cancelForm(){
    this.reportForm.reset({
      id: [''],
      biller_name: [''],
      from_date:  [''],
      to_date: [''] ,
      status: [''],
    })
}

  onSubmit() {
    let biller_name=this.reportForm.value.biller_name;
    for(let indexValue of this.billerData){
      if(indexValue['company_name']== biller_name){
          this.company_id=indexValue['id']
      }
    }

    this.MisReportService.get_biller_report_data({'biller_id':this.company_id,'biller_name':biller_name,'from_date':this.reportForm.value.from_date,
                                                  'to_date':this.reportForm.value.to_date,'status':this.reportForm.value.status})
        .subscribe({
          next:data => {
            this.billerreportData=data;           
            if(this.billerreportData.length==0){
              Swal.fire('No data Found',);
              this.router.navigate(['/reports/mis_report']).then(() => {
                  setTimeout(function() {window.location.reload();} , 2000);
              });
            }

            else if(this.reportForm.value.status['master_key']=='Unpaid')
            {
              this.key =Object.keys(data)
              let data1=Object.keys(data[0])
              let source_name=biller_name+'_report'
              const replacer = (key, value) => value === null ? '' : value;
              let header1=data1
              let csv =this.billerreportData.map(row => header1.map(fieldName => JSON.stringify(row[fieldName], replacer)).join(','));
                csv.unshift(header1.join(','));
                let csvArray = csv.join('\r\n');
                let blob = new Blob([csvArray],{ type: 'text/csv' });
                window.URL.createObjectURL(blob);
                FileSaver.saveAs(blob, source_name+'.'+'csv');
                this.router.navigate(['/reports/mis_report']).then(() => {
                  setTimeout(function() {window.location.reload();} , 2000);
                });
            }
            else{
                let source_name='Biller_report_data'
                const replacer = (key, value) => value === null ? '' : value;
                let header=['biller_name','biller_id','transaction_date','utr_number','fetch_unique_id','bill_amount','payment_amount','bill_number',
                'customer_name','payment_date','transaction_mode','due_date','bill_period','biller_bank_fee','biller_bank_sgst_rate','biller_bank_sgst_amount','biller_bank_igst_rate','biller_bank_igst_amount','biller_bank_cgst_rate',
                'biller_bank_cgst_amount','platform_fee','platform_sgst_rate','platform_sgst_amount','platform_igst_rate','platform_igst_amount',
                'platform_cgst_rate','platform_cgst_amount','partner_fee','partner_igst_rate','partner_igst_amount','partner_cgst_rate',
                'partner_cgst_amount','partner_sgst_rate','partner_sgst_amount','currency_code','bbpou_bank_name','income_amount','income_tax_amount','expense_amount','expense_tax_amount']
                let csv =this.billerreportData.map(row => header.map(fieldName => JSON.stringify(row[fieldName], replacer)).join(','));
                csv.unshift(header.join(','));
                let csvArray = csv.join('\r\n');
                let blob = new Blob([csvArray],{ type: 'text/csv' });
                window.URL.createObjectURL(blob);
                FileSaver.saveAs(blob, source_name+'.'+'csv');
                this.router.navigate(['/reports/blrtranc_report']).then(() => {
                  setTimeout(function() {window.location.reload();} , 2000);
              });
            }
          }
      });
    }
}
