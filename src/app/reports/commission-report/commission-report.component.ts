import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MisReportService } from 'src/app/shared/services/mis-report.service'
import Swal from 'sweetalert2';
import * as XLSX from 'xlsx';

@Component({
  selector: 'app-commission-report',
  templateUrl: './commission-report.component.html',
  styleUrls: ['./commission-report.component.sass']
})
export class CommissionReportComponent implements OnInit {
  CommissionreportForm:FormGroup;
  btnValue = 'Download'
  todayDate = new Date().toJSON().split('T')[0];
  todayDate1 = new Date().toJSON().split('T')[0];
  todayDate2 = new Date().toJSON().split('T')[0];

  constructor(private formBuilder: FormBuilder, 
      private  MisReportService: MisReportService) { }

  ngOnInit():void{

    this.CommissionreportForm = this.formBuilder.group({
      tenant_type: [localStorage.getItem('COMPANY_TYPE')],
      from_date:  ['', Validators.required],
      to_date: ['', Validators.required]
    });
  }

  showSwalmessage(message, text, icon, confirmButton): void {
    if (! confirmButton) {
        Swal.fire({ title: message, text: text, icon: icon, timer: 2000, showConfirmButton: false });
    } else {
        Swal.fire({ title: message, text: text, icon: icon, timer: 2000, showConfirmButton: true, confirmButtonText: "Yes", cancelButtonText: "No" });
    }
  }

  onSubmit(){
    this.MisReportService.getCommissionReport(this.CommissionreportForm.value)
    .subscribe({
      next:data => {
        if(data.length!=0){
          let XlsMasterData = data
          const workSheet = XLSX.utils.json_to_sheet(XlsMasterData);
          const workBook: XLSX.WorkBook = XLSX.utils.book_new();
          XLSX.utils.book_append_sheet(workBook, workSheet, 'SheetName');
          XLSX.writeFile(workBook, 'CommissionReport.csv');
        }else{
          this.showSwalmessage("No record Found!", "", "warning", true);
        } 
      },
      error:(error) => {              
        this.showSwalmessage("Something Went wrong", error, "warning", true);
      }
    }
  );
 }
}


