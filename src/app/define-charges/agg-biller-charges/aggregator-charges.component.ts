import { Component, OnInit, ViewChild } from '@angular/core';
import Swal from 'sweetalert2';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { AggregatorCharges } from 'src/app/shared/models/aggregatorCharges'
import * as XLSX from 'xlsx';
import { ManageSecurityService } from 'src/app/core/service/manage-security.service';
import { AggregatorChargesService } from 'src/app/shared/services/aggregator-charges.service'
import { ErrorCodes } from 'src/app/shared/error-codes';

@Component({
  selector: 'app-aggregator-charges',
  templateUrl: './aggregator-charges.component.html',
  styleUrls: ['./aggregator-charges.component.sass'],
  providers: [ErrorCodes]
})
export class AggregatorChargesComponent implements OnInit {
  @ViewChild('dataSourcePaginator',{read: MatPaginator}) dataSourcePaginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  dataSource :MatTableDataSource<AggregatorCharges>;
  resultData: AggregatorCharges[];
 
  displayColumns  = [
    'actions',
    'biller_name',
    'bbpoubank_name',
    'start_date',
    'end_date',
    'revision_status'
  ]
  renderedData: AggregatorCharges[];
  screenName = 'Aggregator Biller Charges';
  submitBtn = true;
  showLoader = false;
  rowData = [];
  listDiv:boolean = false;
  showList:boolean = true;
  sidebarData: any;
  aggregatorChargesData =[];
  editFlag = '';
  
  constructor(private manageSecurityService :ManageSecurityService, 
                      private aggregatorChargesService :AggregatorChargesService,
                      private errorCodes: ErrorCodes) { }

  ngOnInit(): void {
    this.getAllData();
    let userId = localStorage.getItem('USER_ID');
    this.manageSecurityService.getAccessLeftPanel(userId,this.screenName).subscribe({
      next:data =>{
        this.sidebarData = data
      }
    });
  }

  editViewRecord(row,flag){
    this.rowData = row;
    this.showList = false;
    this.submitBtn = flag;
    this.listDiv = true;
    this.showLoader = false;
    this.editFlag = flag

  }

  showFormList(item: boolean) {
    if(item ===false){
      this.listDiv = true;
      this.showList = false;
    }
    else{
      this.listDiv = false;
      this.showList = true;
    }
  }

  onCancel(item: boolean){
    this.listDiv = item;
    this.showList = true;
    this.submitBtn = true;
    this.rowData = [];
  }

  getAllData(){
    this.aggregatorChargesService.getAll().subscribe({
      next:(data: AggregatorCharges[])=>{
        this.dataSource = new MatTableDataSource(data);
        this.dataSource.paginator = this.dataSourcePaginator;
        this.dataSource.sort = this.sort;
        this.dataSource.connect().subscribe(d => this.renderedData = d);
        this.aggregatorChargesData = data;
      }
    });
  }
  

  tbl_FilterDatatable(value:string) {
    this.dataSource.filter = value.trim().toLocaleLowerCase();
  }

  showSwalmessage(message, text, icon, confirmButton): void {
    if (! confirmButton) {
        Swal.fire({ title: message, text: text, icon: icon, timer: 2000, showConfirmButton: false });
    } else {
        Swal.fire({ title: message, text: text, icon: icon, timer: 2000, showConfirmButton: true, confirmButtonText: "Yes", cancelButtonText: "No" });
    }
  }

  onSave(formValue){  
    this.showLoader = true;
    this.aggregatorChargesService.create(formValue).subscribe({
      next: (data:any) => {
        if (data['status'] == 1 && formValue.revision_status == 'Future' && this.editFlag == 'versionCreate' ) {
          this.showSwalmessage('Version Created Successfully!','','success',false)
        }
        else if (data['status'] == 2) {
          this.showSwalmessage('Your record has been added successfully!','','success',false)
        }
        else if (data['status'] == 1) {
          this.showSwalmessage('Version has been updated successfully!','','success',false)
        }
        this.getAllData();
        this.showList = true;
        this.listDiv = false;
        this.showLoader = false;  
      },
      error:(error) => {
        this.showLoader = false;  
        this.showSwalmessage(this.errorCodes.getErrorMessage(error.status), error.error, 'error', true)
      }
    })
  }

  export_to_excel() {
    let XlsMasterData = this.dataSource.data.map(({ biller_name,bbpoubank_name,settlementtype_name,billingfrequency_name,start_date,end_date,revision_status}) => ({biller_name,bbpoubank_name,settlementtype_name,billingfrequency_name,start_date,end_date,revision_status}));
    const workSheet = XLSX.utils.json_to_sheet(XlsMasterData);
    const workBook: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(workBook, workSheet, 'SheetName');
    XLSX.writeFile(workBook, 'aggregatorCharges.xlsx');
  }



}
