import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AggregatorChargesService } from 'src/app/shared/services/aggregator-charges.service';
import { AllmasterService } from 'src/app/shared/services/allmaster.service';
import { DatePipe } from '@angular/common';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-aggregator-charges-details',
  templateUrl: './aggregator-charges-details.component.html',
  styleUrls: ['./aggregator-charges-details.component.sass'],
  providers: [DatePipe],

})
export class AggregatorChargesDetailsComponent implements OnInit {
  @Input() rowData: [];
  @Input() editFlag:string;
  @Input() submitBtn:boolean;
  @Input() showLoader:boolean;
  @Input() aggregatorChargesData: any;
  @Output() onSave = new EventEmitter<any>();
  @Output() onCancel = new EventEmitter<any>();

  
  aggregatorChargesForm: FormGroup;
  btnVal = 'Submit';
  cancelFlag=true 
  COMPANY_ID: string;
  COMPANY_TYPE:string;
  companydata: any = [];
  service_model: any = [];
  billing_mode: any = [];
  onboarding_mode: [];
  settlement_mode: [];
  service_mode: [];
  biller_cat: [];
  bbpou_bank_name:any=[];
  settlement_frequency: any = [];
  settlement_type: any = [];
  billing_frequency:any=[];
  transactionmode:any=[];
  transaction_type = [];
  company_name= [];
  billerdata =[];
  taxdata=[];
  companies=[];
  billerCategory_data=[];
  rate_percent_row = {};
  minv_maxv_dict = {};
  submitted = false;
  AggregatorData = []
  BBPOUBankData = []

  todayDate = new Date().toJSON().split('T')[0];
  todayDateMin = new Date().toJSON().split('T')[0];
  todayDateMax = new Date().toJSON().split('T')[0];

  constructor( private formBuilder: FormBuilder, 
               public datepipe: DatePipe, 
               private allmasterService: AllmasterService, 
               private aggregatorChrgsService: AggregatorChargesService) { }

  ngOnInit(): void {
    this.COMPANY_ID = localStorage.getItem('COMPANY_ID');
    this.COMPANY_TYPE  = localStorage.getItem('COMPANY_TYPE');

    this.aggregatorChargesForm = this.formBuilder.group({
      id: [''],      
      version_number: ['1'],
      biller_company_ref_id:['',Validators.required],
      aggregator_company_ref_id:['',Validators.required],
      bbpou_company_ref_id:['',Validators.required],
      start_date: ['', Validators.required],
      end_date: ['2099-12-31'],
      revision_status: ['', Validators.required],
      initialItemRow: this.formBuilder.array([this.initialItemRow()])
    });

    if(!Array.isArray(this.rowData)){
      this.editViewRecord(this.rowData,this.editFlag);
     }


    this.allmasterService.getMasterDataByType('Transaction Mode').subscribe({
      next:data =>{
        this.transactionmode = data;  
      } 
    });

    this.allmasterService.getMasterDataByType('Settlement Type').subscribe({
      next:data =>{
        this.settlement_type = data;  
      } 
    });

    this.allmasterService.getMasterDataByType('Settlement Frequency').subscribe({
      next:data =>{
        this.settlement_frequency = data;  
      } 
    });

    this.allmasterService.getMasterDataByType('Billing Frequency').subscribe({
      next:data =>{
        this.billing_frequency = data;  
      } 
    });

    this.allmasterService.getMasterDataByType('Transaction Type').subscribe({
      next:data =>{
        this.transaction_type = data;  
      } 
    });

    this.aggregatorChrgsService.getAllBillersData(this.COMPANY_TYPE).subscribe({
      next:(data: any) => {
        this.companydata = data;
      }
    });

    this.aggregatorChrgsService.getAggregatorData(this.COMPANY_TYPE).subscribe({
      next:data => {
        this.AggregatorData = data;
      }
    });

    this.aggregatorChrgsService.getBBPOUCompanyData(this.COMPANY_TYPE).subscribe({
      next:data => {
        this.BBPOUBankData = data;
      }
    });

    this.aggregatorChrgsService.getBillerCategory().subscribe({
      next:data =>{
        this.billerCategory_data = data;
      }
    });

    this.aggregatorChrgsService.gettaxdata('GST').subscribe({
      next:(data:[])=> {
        this.taxdata=data
      }
    })

    this. aggregatorChargesForm.get("start_date").valueChanges.subscribe({
      next:val => {
        if (val != null) {
          let todaydate1 = new Date().toJSON().split('T')[0];      
          if (val === todaydate1) {
            this. aggregatorChargesForm.get("revision_status").setValue("Effective");
          }
          else if(val > todaydate1){
            this. aggregatorChargesForm.get("revision_status").setValue("Future");
          }
        }
      }
    });

    this.aggregatorChargesForm.get("biller_company_ref_id").valueChanges.subscribe({
      next:val => {
        if(val){
          this.aggregatorChrgsService.getAllBillersData(this.COMPANY_TYPE).subscribe({
            next:(data:any)=> {
              this.companies = data
              let filtercompanydata = this.companies.filter(function(data: any){return data.id == val; });
              this.billing_mode = filtercompanydata[0]["billing_mode"];
              this.settlement_mode = filtercompanydata[0]["settlement_mode"];
              this.biller_cat = filtercompanydata[0]["biller_category"];
              this.onboarding_mode = filtercompanydata[0]["onboarding_type"];
              this.service_mode = filtercompanydata[0]["service_model"];
              this.company_name = filtercompanydata[0]["company_name"];
              this.bbpou_bank_name = filtercompanydata[0]["bbpou_bank"];
            }
          });
        }
      }
    });
  }  

  initialItemRow() {
    return this.formBuilder.group({
      id: [''],
      rate_flag: ['', Validators.required],
      rate: ['', Validators.required],
      percent: ['', Validators.required],
      min_commission: ['0', Validators.required],
      max_commission: ['0', Validators.required],
      tax_rate: ['', Validators.required],
      min_value: ['', Validators.required],
      max_value: ['99999999', Validators.required],
      min_transaction: ['0', Validators.required],
      max_transaction: ['0', Validators.required],
      transaction_mode_ref_id: ['', Validators.required],
    });
  }

  get formArray() {
    return this.aggregatorChargesForm.get('initialItemRow') as FormArray;
  }


  addNewRow() {   
    this.formArray.push(this.initialItemRow());
  }

  deleteRow(index) {
    if (this.formArray.length == 1) {
      return false;
    } else {
      this.formArray.removeAt(index);
      return true;
    }
  }

  transaction_all_modes(){
    let flag,i_len;
    for(let i=0; i<this.transactionmode.length; i++) {
      flag = 0;
      for(let j=0; j<(<FormArray>(this.aggregatorChargesForm.get('initialItemRow'))).length; j++){
        let gridRow = (<FormArray>(this.aggregatorChargesForm.get('initialItemRow'))).at(j);
        if(this.transactionmode[i]['id'] == gridRow.get('transaction_mode_ref_id').value){
          flag = 1
        }
      }
      if(flag!=1) {
        return false;
      }
      i_len = i
    }
    if((i_len+1)==this.transactionmode.length) {
      return true;
    }
    else {
      return false;
    } 
  }

  checkRadioButton(gridRowID,val){
    let gridRow = (<FormArray>(this.aggregatorChargesForm.get('initialItemRow'))).at(gridRowID);
    if(gridRow.get('rate_flag').value == val){
      return true;
    }
    else {
      return false;
    }
  }

  ratePercentFlag(event, index){
    let gridRow = (<FormArray>(this. aggregatorChargesForm.get('initialItemRow'))).at(index);
    if (event=='N'){
      gridRow.get("rate_flag").setValue(false);
      gridRow.get("rate").disable({ onlySelf: true});
      gridRow.get("rate").setValue("0");
      gridRow.get("percent").enable({ onlySelf: true});
      gridRow.get("percent").setValue("");
    }else{
      gridRow.get("rate_flag").setValue(true);
      gridRow.get("percent").disable({ onlySelf: true});
      gridRow.get("percent").setValue("0");
      gridRow.get("rate").enable({ onlySelf: true});
      gridRow.get("rate").setValue("");
    }
  }

  min_maxValue(rowIndex, mode){
    let currentGridRow = (<FormArray>(this.aggregatorChargesForm.get('initialItemRow'))).at(rowIndex);
    if(rowIndex == 0) {
      currentGridRow.get('min_value').setValue(1);
    }
    if(mode === 'MAX') {
      let nextGridRow = (<FormArray>(this.aggregatorChargesForm.get('initialItemRow'))).at(rowIndex);
      if(nextGridRow && null != nextGridRow) {
        let currentMaxValue = currentGridRow.get('max_value').value;
        this.minv_maxv_dict[nextGridRow.get('transaction_mode_ref_id').value] = {"maxv": currentMaxValue + 1};
      }
    }
  }

  changeon_feetype_fields(gridRowID){
    let gridRow = (<FormArray>(this.aggregatorChargesForm.get('initialItemRow'))).at(gridRowID);
    try {
      gridRow.get('min_value').setValue(this.minv_maxv_dict[gridRow.get('transaction_mode_ref_id').value]["maxv"])
    } catch (error) {
      gridRow.get('min_value').setValue(1);
    }
  }
  
  passed_applicable_rate_control(gridRowID){
    let gridRow = (<FormArray>(this.aggregatorChargesForm.get('initialItemRow'))).at(gridRowID);
    if(gridRow.get('rate').value<0){
      gridRow.get('rate').setValue(0);
    }
  }

  passed_applicable_percent_control(gridRowID){
    let gridRow = (<FormArray>(this.aggregatorChargesForm.get('initialItemRow'))).at(gridRowID);
    if(gridRow.get('percent').value<0){
      gridRow.get('percent').setValue(0);
    }
  }
  showSwalmessage(message, text, icon, confirmButton): void {
    if (! confirmButton) {
        Swal.fire({ title: message, text: text, icon: icon, timer: 2000, showConfirmButton: false });
    } else {
        Swal.fire({ title: message, text: text, icon: icon, timer: 2000, showConfirmButton: true, confirmButtonText: "Yes", cancelButtonText: "No" });
    }
  }
    
  onCancelForm(){
    this.cancelFlag=false
    this.aggregatorChargesForm.reset();
    this.onCancel.emit(false);
  }


  format_date(date){
    let d = new Date(date),
    month = '' + (d.getMonth() + 1),
    day = '' + d.getDate(),
    year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;
    return [year, month,day ].join('-');
  }

  editViewRecord(data,editFlag) {
    this.aggregatorChrgsService.getAggregatorChargesById(data.id).subscribe({
      next:(row: any) => {
        this.todayDateMax = "2099-12-31"
        if(editFlag == 'view'){
          this.submitBtn = false;
          let a = row.start_date
          this.aggregatorChargesForm.get("start_date").setValue(a)
        }
        let version_number = row.version_number
        if(editFlag== 'versionCreate'){
          this.btnVal = "Create Version";
          let currtDate = new Date().toJSON().split("T")[0];
          if(row.start_date == currtDate) {
          this.showSwalmessage("Cannot create version on the same day!",'',"warning",false);
          this.onCancelForm();
          }
          let futureCheckData = [];
          futureCheckData = this.aggregatorChargesData.find((x)=>{
           if(x.revision_status === 'Future' && x.source_name === row.source_name ){
             return x
           }
         })
  
         if(futureCheckData != undefined){
           this.showSwalmessage('Cannot create more than one future record for same Source!','','warning',false)
           this.onCancelForm();
         }
         version_number = Number(this.aggregatorChargesForm.get('version_number').value) + 1
         let newdate = new Date(this.todayDate);
         let new_to_date=newdate.setDate(newdate.getDate()+1);
         let new_to_date_1 =this.format_date(new_to_date)
         this.aggregatorChargesForm.get("start_date").setValue(new_to_date_1);  
        }
  
        if(editFlag == 'veredit'){
          this.btnVal = 'Update Version';
          this.aggregatorChargesForm.get("start_date").setValue(data.start_date)
        }
  
        this.aggregatorChargesForm.patchValue({
          id: row.id,
          biller_company_ref_id: row.biller_company_ref_id,
          bbpou_company_ref_id: row.bbpou_company_ref_id,
          aggregator_company_ref_id: row.aggregator_company_ref_id,
          end_date: row.end_date,
          version_number:version_number
        });
  
        this.aggregatorChargesForm.get('biller_company_ref_id').disable({ onlySelf: true});
        this.aggregatorChargesForm.setControl('initialItemRow', this.setExistingArray(row.initialItemRow));
        for(let i=0;i<this.formArray.length; i++){
          let gridRow = (<FormArray>(this.aggregatorChargesForm.get('initialItemRow'))).at(i);
          let flg = gridRow.get('rate_flag').value
          if(flg === false){
            gridRow.get('rate_flag').setValue('N')
          }
          else if(flg === true){
            gridRow.get('rate_flag').setValue('Y')
          }
        }
      }
    });
    this.showLoader = false;
    
  }
  setExistingArray(initialArray = []): FormArray {
    const formArray = new FormArray([]);
    initialArray.forEach(agg_fee => {
      formArray.push(this.formBuilder.group({
        transaction_mode_ref_id: agg_fee.transaction_mode_ref_id,
        rate_flag: agg_fee.rate_flag,
        rate: agg_fee.rate,
        percent: agg_fee.percent,
        min_commission: agg_fee.min_commission,
        max_commission: agg_fee.max_commission,
        tax_rate: agg_fee.tax_rate,
        min_value: agg_fee.min_value,
        max_value: agg_fee.max_value,
        min_transaction: agg_fee.min_transaction,
        max_transaction: agg_fee.max_transaction
      }));
    });
    return formArray;
  }

  checkTranctionTypeSeleced(check1, check2){
    if(check1===false && check2===false){
      this.showSwalmessage("Please select either BOTH or On-Us and Off-Us!",'',"warning",false);
      return;
    }

    let len = (<FormArray>(this.aggregatorChargesForm.get('initialItemRow'))).length

    if(check1===true ){
      if(len%2!=0){
        this.showSwalmessage("please select both on-us and off-us!",'',"warning",false);
        return;
      }
    }
  }

  onSubmit(){
    if (!this.aggregatorChargesForm.invalid){
      this.submitted = true;   
      this.aggregatorChargesForm.get('biller_company_ref_id').enable();
      let arr1 = []
      let arr2 = []
      let on_off_us_arr = []
      let both_arr = []

      this.transactionmode.forEach(element => {
      arr1.push(element["id"]);
      });

      for(let i=0; i<this.formArray.length; i++) { 
        let gridRow = (<FormArray>(this.aggregatorChargesForm.get('initialItemRow'))).at(i);
        const transaction_mode_ref_id = gridRow.get('transaction_mode_ref_id').value.toString(); 
        if(transaction_mode_ref_id!=''){
          arr2.push(Number.parseInt(gridRow.get('transaction_mode_ref_id').value));
        }     
      }

      this.transactionmode.forEach(element => {
        element["master_key"] == 'BOTH' ? both_arr.push(element["id"]) : on_off_us_arr.push(element["id"])
      });

      let difference = arr1.filter(x => !arr2.includes(x));

      let check1 = both_arr.every(function (element) {
        return difference.includes(element);
      });
      
      let check2 = on_off_us_arr.every(function (element) {
        return difference.includes(element);
      });

      this.checkTranctionTypeSeleced(check1, check2)

      if(difference.length === 1 || difference.length == 2){ 
        this.aggregatorChargesForm.value.start_date = this.datepipe.transform(this.aggregatorChargesForm.value.start_date, "yyyy-MM-dd")
        this.onSave.emit(this.aggregatorChargesForm.value);
      } 
    }
  }
}
