import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AggregatorChargesDetailsComponent } from './aggregator-charges-details.component';

describe('AggregatorChargesDetailsComponent', () => {
  let component: AggregatorChargesDetailsComponent;
  let fixture: ComponentFixture<AggregatorChargesDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AggregatorChargesDetailsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AggregatorChargesDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
