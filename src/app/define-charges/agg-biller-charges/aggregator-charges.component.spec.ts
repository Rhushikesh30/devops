import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AggregatorChargesComponent } from './aggregator-charges.component';

describe('AggregatorChargesComponent', () => {
  let component: AggregatorChargesComponent;
  let fixture: ComponentFixture<AggregatorChargesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AggregatorChargesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AggregatorChargesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
