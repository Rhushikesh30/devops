import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BillerBankChargesComponent } from './biller-bank-charges.component';

describe('BillerBankChargesComponent', () => {
  let component: BillerBankChargesComponent;
  let fixture: ComponentFixture<BillerBankChargesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BillerBankChargesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BillerBankChargesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
