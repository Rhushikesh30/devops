import { Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import Swal from "sweetalert2";
import { BillerbankChargesService } from 'src/app/shared/services/billerbank-charges.service';
import { AllmasterService } from 'src/app/shared/services/allmaster.service';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-biller-bank-charges-details',
  templateUrl: './biller-bank-charges-details.component.html',
  styleUrls: ['./biller-bank-charges-details.component.sass'],
  providers: [DatePipe],

})
export class BillerBankChargesDetailsComponent implements OnInit {
  @Input() rowData: [];
  @Input() editFlag:string;
  @Input() submitBtn:boolean;
  @Input() showLoader:boolean;
  @Input() billerBankChargesData : any;
  @Output() onSave = new EventEmitter<any>();
  @Output() onCancel = new EventEmitter<any>();

  cancelFlag=true
  billerbankchargesform: FormGroup;
  btnVal = 'Submit';
  companydata: any = [];
  submitted = false;
  billerbankchargesdata: any = [];
  biller_data: any = [];
  bbpoubank_data: any = [];
  onboarding_mode: [];
  service_mode: [];
  billing_mode: [];
  billerid;
  biller_cat: [];
  company_name: [];
  settlement_mode: [];
  billerCategory_data=[];
  onboarding_type_name:any=[];
  service_model: any = [];
  settlement_frequency: any = [];
  settlement_type: any = [];
  billing_frequency: any = [];
  transaction_type: any = [];
  tbl_FilteredData = [];
  disabled_val;
  taxdata: any = [];
  versionCreationFlag: any = [];
  disable_enable_status: any;
  btnValue = 'Submit';
  todayDate = new Date().toJSON().split('T')[0];
  todayDateMin = new Date().toJSON().split('T')[0];
  todayDateMax = new Date().toJSON().split('T')[0];
  transactionmode:any=[];
  rate_percent_row = {};
  login_id = parseInt(localStorage.getItem('COMPANY_ID'));
  admin_login = [];
  entitydata = [];
  fee_type: any = [];
  admin_login_data = [];
  minv_maxv_dict = {};
  newPayload = [];
  companies=[];
  companytype:any
  sidebardata: any;
  onboarding_type:any=[];
  service_model_name:any=[];
  billing_mode_name:any=[];
  settlement_type_name:any=[];
  biller_category:any=[];
  biller_category_name:any=[];
  bbpou_bank:any=[];
  bbpou_bank_name:any=[];
  billerStandardCharges:any =[];
  COMPANY_ID: string;
  COMPANY_TYPE: any;
  mainScope;
  editAppData: any=[];
  BillerNameValue:string

  constructor(private formBuilder: FormBuilder, 
    public datepipe: DatePipe ,
    private allmasterService: AllmasterService, 
    private billerbankChargesService: BillerbankChargesService ) { }

  ngOnInit(): void {
    this.COMPANY_ID = localStorage.getItem('COMPANY_ID');
    this.COMPANY_TYPE = localStorage.getItem('COMPANY_TYPE');

    this.billerbankchargesform = this.formBuilder.group({
      id: [''],
      bbpou_company_ref_id:[this.COMPANY_ID],
      version_number: ['1'],
      biller_company_ref_id:['',Validators.required],
      bbpou_bank_ref_id: ['', Validators.required],
      start_date: ['', Validators.required],
      end_date: ['2099-12-31'],
      revision_status: ['', Validators.required],
      initialItemRow: this.formBuilder.array([this.initialItemRow()])
    });

    if(!Array.isArray(this.rowData)){
      this.editViewRecord(this.rowData,this.editFlag);
    }     
    
    this.billerbankChargesService.gettaxdata('GST').subscribe({
      next:(data:[]) =>{
        this.taxdata=data;
      }
    })

    this.billerbankchargesform.get("biller_company_ref_id").valueChanges.subscribe({
      next:val => {
        if(val){
          this.billerSelectChangeFunction(val)
        }    
      }     
    });

    this.allmasterService.getMasterDataByType('Transaction Mode').subscribe({
      next:data =>{
        this.transactionmode = data;  
      } 
    });

    this.allmasterService.getMasterDataByType('Settlement Type').subscribe({
      next:data=>{
        this.settlement_type=data;
      }
    });

    this.allmasterService.getMasterDataByType('Settlement Frequency').subscribe({
      next:data =>{
        this.settlement_frequency = data;
      } 
    });

    this.allmasterService.getMasterDataByType('Billing Frequency').subscribe({
      next:data =>{
        this.billing_frequency = data;
      } 
    });

    this.allmasterService.getMasterDataByType('Transaction Type').subscribe({
      next:data =>{
        this.transaction_type = data;
      } 
    });

    this.billerbankChargesService.getAllBillersData(this.COMPANY_TYPE,'Biller').subscribe({
      next:(data: any)=>{
        this.companydata = data;
      }
    });

    this.billerbankChargesService.getBillerCategory().subscribe({
      next:data =>{
        this.billerCategory_data = data;
      }
    });

    this.billerbankchargesform.get("start_date").valueChanges.subscribe({
      next:val => {
        this.revisionStatusChangesFunction(val)
      }
    });

  }

  billerSelectChangeFunction(val){
    this.billerbankChargesService.getAllBillersData(this.COMPANY_TYPE,'Biller').subscribe({
      next:(data: any) => {
        this.companies = data
        let filtercompanydata = this.companies.filter(function(data: any) { return data.id == val; });
        this.billerbankchargesform.get('bbpou_bank_ref_id').setValue(filtercompanydata[0]["bbpou_bank_ref_id"])
        this.billing_mode = filtercompanydata[0]["billing_mode"];
        this.settlement_mode = filtercompanydata[0]["settlement_mode"];
        this.biller_cat = filtercompanydata[0]["biller_category"];
        this.onboarding_mode = filtercompanydata[0]["onboarding_type"];
        this.service_mode = filtercompanydata[0]["service_model"];
        this.company_name = filtercompanydata[0]["company_name"];
        this.bbpou_bank_name = filtercompanydata[0]["bbpou_bank"];
        this.companytype = filtercompanydata[0]['company_type'];
       
        if (this.btnVal == 'Submit' && this.submitBtn === true){
          this.billerbankChargesService.getBillerStandardChargesByID(filtercompanydata[0]['biller_category_id']).subscribe({
            next:data =>{
              this.billerStandardCharges = data[0];
              let gridRow = (<FormArray>(this.billerbankchargesform.get('initialItemRow'))).at(0)
              if(this.billerStandardCharges['is_rate'] === false){
                gridRow.get('rate_flag').setValue('N')
              }
              else if(this.billerStandardCharges['is_rate'] === true){
                gridRow.get('rate_flag').setValue('Y')
              }
              while (this.formArray.length) {
                this.formArray.removeAt(-1)
              }
              for(let myData in data){ 
                this.addDynamicNewRow2_process(data[myData]);
              }  
            }
          });
        }  
      }
    });
  }

  revisionStatusChangesFunction(val){
    let curr_date = new Date().toJSON().split('T')[0];
    curr_date = this.datepipe.transform(curr_date, 'dd/MM/yyyy');
    val = this.datepipe.transform(val, 'dd/MM/yyyy');
    if (val != null) {
      if (val=== curr_date) {
        this.billerbankchargesform.get("revision_status").setValue("Effective");
      }
      else if(val > curr_date){
        this.billerbankchargesform.get("revision_status").setValue("Future");
      }
    }
  }


  addDynamicNewRow2_process(data) {
    this.formArray.push(this.addInitialitemRow_process(data));
  }

  addInitialitemRow_process(data) {
    return this.formBuilder.group({
      rate_flag:  [data['is_rate'] === true?'Y':'N'],
      rate: [data['rate']],
      percent: [data['percent']],
      min_transaction: ['0'],
      max_transaction: ['0'],
      min_value: [data['min_transaction_value']],
      max_value: [data['max_transaction_value']],
      transaction_mode_ref_id: [93],
      min_commission: ['0'],
      max_commission: ['0'],
      biller_category_ref_id:[data['biller_category_ref_id']],
      tax_rate: [1],
    });
    
  }

  get formArray() {
    return this.billerbankchargesform.get('initialItemRow') as FormArray;
  }


  addNewRow() {   
      this.formArray.push(this.initialItemRow());
  }

  deleteRow(index) {
    if (this.formArray.length == 1) {
      return false;
    } else {
      this.formArray.removeAt(index);
      return true;
    }
  }

  transaction_all_modes(){
    let flag,i_len;
    for(let i=0; i<this.transactionmode.length; i++) {
      flag = 0;
      for(let j=0; j<(<FormArray>(this.billerbankchargesform.get('initialItemRow'))).length; j++){
        let gridRow = (<FormArray>(this.billerbankchargesform.get('initialItemRow'))).at(j);
        if(this.transactionmode[i]['id'] == gridRow.get('transaction_mode_ref_id').value){
          flag = 1
        }
      }
      if(flag!=1) {
        return false;
      }
      i_len = i
    }
    if((i_len+1)==this.transactionmode.length) {
      return true;
    }
    else {
      return false;
    } 
  }
  
  rate_percent_flag(event,i){
    let gridRow = (<FormArray>(this. billerbankchargesform.get('initialItemRow'))).at(i);
    if(gridRow.get("transaction_mode_ref_id").value in this.rate_percent_row && this.rate_percent_row[gridRow.get("transaction_mode_ref_id").value][0]!=i) {
      if( this.rate_percent_row[(<FormArray>this.billerbankchargesform.get("initialItemRow")).controls[i].get("transaction_mode_ref_id").value][1]== 'Y') {
       gridRow.get("rate_flag").setValue(true);
       gridRow.get("percent").setValue("0");
       gridRow.get("percent").disable({ onlySelf: true});
       gridRow.get("rate").enable({ onlySelf: true});
      }
      else if( this.rate_percent_row[(<FormArray>this.billerbankchargesform.get("initialItemRow")).controls[i].get("transaction_mode_ref_id").value][1]== 'N') {
       gridRow.get("rate_flag").setValue(false);
       gridRow.get("rate").setValue("0");
       gridRow.get("rate").disable({ onlySelf: true});
       gridRow.get("percent").enable({ onlySelf: true});
      }

    } else {
      if(event=='Y') {
     gridRow.get("rate_flag").setValue(true);
       gridRow.get("percent").setValue("0");
       gridRow.get("percent").disable({ onlySelf: true});
       gridRow.get("rate").enable({ onlySelf: true});
      }
      else if(event=='N') {
       gridRow.get("rate_flag").setValue(false);
       gridRow.get("rate").setValue("0");
       gridRow.get("rate").disable({ onlySelf: true});
       gridRow.get("percent").enable({ onlySelf: true});
    }
    this.rate_percent_row[(<FormArray>this.billerbankchargesform.get("initialItemRow")).controls[i].get("transaction_mode_ref_id").value] = [i,event];
    }
  }

  min_maxValue(rowIndex, mode) {
    let currentGridRow = (<FormArray>(this.billerbankchargesform.get('initialItemRow'))).at(rowIndex);
    if(rowIndex == 0) {
      currentGridRow.get('min_value').setValue(1);
    }
    if(mode === 'MAX') {
      let nextGridRow = (<FormArray>(this.billerbankchargesform.get('initialItemRow'))).at(rowIndex);
      if(nextGridRow && null != nextGridRow) {
        let currentMaxValue = currentGridRow.get('max_value').value;
        this.minv_maxv_dict[nextGridRow.get('transaction_mode_ref_id').value] = {"maxv": currentMaxValue + 1};
      }
    }
  }

  changeon_feetype_fields(gridRowID){
    let gridRow = (<FormArray>(this.billerbankchargesform.get('initialItemRow'))).at(gridRowID);
    try {
      gridRow.get('min_value').setValue(this.minv_maxv_dict[gridRow.get('transaction_mode_ref_id').value]["maxv"])
    } catch (error) {
      gridRow.get('min_value').setValue(1);
    }
  }

  checkRadioButton(gridRowID,val){
    let gridRow = (<FormArray>(this.billerbankchargesform.get('initialItemRow'))).at(gridRowID);
    let i = gridRowID;
    if (gridRow.get('transaction_mode_ref_id').value in this.rate_percent_row) {
      if( this.rate_percent_row[(<FormArray>this.billerbankchargesform.get("initialItemRow")).controls[i].get("transaction_mode_ref_id").value][1]== 'Y') {
       gridRow.get("rate_flag").setValue('Y');

       gridRow.get("percent").setValue("0");
       gridRow.get("percent").disable({ onlySelf: true});
       gridRow.get("rate").enable({ onlySelf: true});
      }
      else if( this.rate_percent_row[(<FormArray>this.billerbankchargesform.get("initialItemRow")).controls[i].get("transaction_mode_ref_id").value][1]== 'N') {
       gridRow.get("rate_flag").setValue('N');

       gridRow.get("rate").setValue("0");
       gridRow.get("rate").disable({ onlySelf: true});
       gridRow.get("percent").enable({ onlySelf: true});
      }
      if(this.rate_percent_row[gridRow.get('transaction_mode_ref_id').value][1] == val) {
        return true;
      }
      else {
        return false;
      }
    }
    else {
      if(gridRow.get('rate_flag').value == val) {
        return true;
      }
      else {
        return false;
      }
    }
  }

  checkIfExists(gridRowID) {
    let gridRow = (<FormArray>(this.billerbankchargesform.get('initialItemRow'))).at(gridRowID);
    if (gridRow.get('transaction_mode_ref_id').value in this.rate_percent_row) {
      return true;
    } else {
      return false;
    }
  }

  passed_applicable_rate_control(gridRowID){
    let gridRow = (<FormArray>(this.billerbankchargesform.get('initialItemRow'))).at(gridRowID);
    if(gridRow.get('rate').value<0){
      gridRow.get('rate').setValue(0);
    }
  }

  passed_applicable_percent_control(gridRowID){
    let gridRow = (<FormArray>(this.billerbankchargesform.get('initialItemRow'))).at(gridRowID);
    if(gridRow.get('percent').value<0){
      gridRow.get('percent').setValue(0);
    }
  }
  showSwalmessage(message, text, icon, confirmButton): void {
    if (! confirmButton) {
        Swal.fire({ title: message, text: text, icon: icon, timer: 2000, showConfirmButton: false });
    } else {
        Swal.fire({ title: message, text: text, icon: icon, timer: 2000, showConfirmButton: true, confirmButtonText: "Yes", cancelButtonText: "No" });
    }
  }

  onCancelForm(){
    this.cancelFlag=false
    this.billerbankchargesform.reset();
    this.onCancel.emit(false);
  }



  initialItemRow() {
    return this.formBuilder.group({
      id: [''],
      rate_flag: ['', Validators.required],
      rate: ['', Validators.required],
      percent: ['', Validators.required],
      min_commission: ['0', Validators.required],
      max_commission: ['0', Validators.required],
      tax_rate: ['', Validators.required],
      min_value: ['', Validators.required],
      max_value: ['99999999', Validators.required],
      min_transaction: ['0', Validators.required],
      max_transaction: ['0', Validators.required],
      transaction_mode_ref_id: ['', Validators.required],
      biller_category_ref_id:['',Validators.required]
    });
  } //end of initial Item Row

  format_date(date){
    let d = new Date(date),
    month = '' + (d.getMonth() + 1),
    day = '' + d.getDate(),
    year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;
    return [year, month,day ].join('-');
  }// end of format date

  setExistingArray(initialArray = []): FormArray {
    const formArray = new FormArray([]);
    initialArray.forEach(int_fee => {
      formArray.push(this.formBuilder.group({
        transaction_mode_ref_id: int_fee.transaction_mode_ref_id,
        rate_flag: int_fee.rate_flag,
        rate: int_fee.rate,
        percent: int_fee.percent,
        min_commission: int_fee.min_commission,
        max_commission: int_fee.max_commission,
        tax_rate: int_fee.tax_rate,
        min_value: int_fee.min_value,
        max_value: int_fee.max_value,
        min_transaction: int_fee.min_transaction,
        max_transaction: int_fee.max_transaction,
        biller_category_ref_id:int_fee.biller_category_ref_id
      }));
    });
    return formArray;
  }// end of set existing array

  editViewRecord(data,editFlag) {
    this.billerbankChargesService.getbillerBankChargesById(data.id).subscribe({
        next:(row: any) => {
          this.todayDateMax = "2099-12-31"
          if(editFlag == 'view'){
            this.submitBtn = false;
            let a = row.start_date
            this.billerbankchargesform.get("start_date").setValue(a)
          }
          let version_number = row.version_number
    
          if(editFlag== 'versionCreate'){
            this.btnVal = "Create Version";
            let currtDate = new Date().toJSON().split("T")[0];
    
              if(row.start_date == currtDate) {
              this.showSwalmessage("Cannot create version on the same day!",'',"warning",false);
              this.onCancelForm();
              }
              let futureCheckData = [];
               futureCheckData = this.billerBankChargesData.find((x)=>{
                if(x.revision_status === 'Future' && x.biller_name === row.biller_name ){
                  return x
                }
              })
    
              if(futureCheckData != undefined){
                this.showSwalmessage('Cannot create more than one future record for same Source!','','warning',false)
                this.onCancelForm();
              }
              version_number = Number(this.billerbankchargesform.get('version_number').value) + 1
              let newdate = new Date(this.todayDate);
              let new_to_date=newdate.setDate(newdate.getDate()+1);
              let new_to_date_1 =this.format_date(new_to_date)
              this.billerbankchargesform.get("start_date").setValue(new_to_date_1);          
          }//endofcreateVersion
    
          if(editFlag == 'veredit'){
            this.btnVal = 'Update Version';
            this.billerbankchargesform.get("start_date").setValue(data.start_date)
          }
    
          this.billerbankchargesform.patchValue({
            id: row.id,
            biller_company_ref_id: row.biller_company_ref_id,
            bbpou_bank_ref_id: row.bbpou_bank_ref_id,
            end_date: row.end_date,
            version_number:version_number
          });
    
          this.billerbankchargesform.get('biller_company_ref_id').disable({ onlySelf: true});
          this.billerbankchargesform.setControl('initialItemRow', this.setExistingArray(row.initialItemRow));
          for(let i=0;i<this.formArray.length; i++){
            let gridRow = (<FormArray>(this.billerbankchargesform.get('initialItemRow'))).at(i);
            let flg = gridRow.get('rate_flag').value
            if(flg === false){
              gridRow.get('rate_flag').setValue('N')
            }
            else if(flg === true){
              gridRow.get('rate_flag').setValue('Y')
            }
          }
        }
    });
    this.showLoader = false;    
  }


  tranctionsTypeCheck(check1, check2, difference){
    if(check1===false && check2===false){
      this.showSwalmessage("Please select either BOTH or On-Us and Off-Us!",'',"warning",false);
      return;
    }
    let len = (<FormArray>(this.billerbankchargesform.get('initialItemRow'))).length
    if(check1===true && difference.length === 2){
      if(len%2!=0){
        this.showSwalmessage("please select both on-us and off-us!",'',"warning",false);
        return;
      }   
    }
  }

  onSubmit() {
    if (!this.billerbankchargesform.invalid){
      this.submitted = true;   
      this.billerbankchargesform.get('biller_company_ref_id').enable();
      let arr1 = []
      let arr2 = []
      let on_off_us_arr = []
      let both_arr = []

      this.transactionmode.forEach(element => {
        arr1.push(element["id"]);
      });

      for(let i=0; i<this.formArray.length; i++) { 
        let gridRow = (<FormArray>(this.billerbankchargesform.get('initialItemRow'))).at(i);
        const transaction_mode_ref_id = gridRow.get('transaction_mode_ref_id').value.toString(); 
        if(transaction_mode_ref_id!=''){      
          arr2.push(Number.parseInt(gridRow.get('transaction_mode_ref_id').value));
        }     
      }
   
    this.transactionmode.forEach(element => {
      if(element["master_key"] == 'BOTH')
        both_arr.push(element["id"]);
      else
        on_off_us_arr.push(element["id"]);
      });

      let difference = arr1.filter(x => !arr2.includes(x));
      let check1 = both_arr.every(function (element) {
      return difference.includes(element);
    });

    let check2 = on_off_us_arr.every(function (element) {
      return difference.includes(element);
    });

    this.tranctionsTypeCheck(check1, check2, difference)

    if(difference.length === 1 || difference.length == 2){ 
      this.billerbankchargesform.value.start_date = this.datepipe.transform(this.billerbankchargesform.value.start_date, "yyyy-MM-dd")
      this.onSave.emit(this.billerbankchargesform.value);
    }
  }
}

}
