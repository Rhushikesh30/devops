import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BillerBankChargesDetailsComponent } from './biller-bank-charges-details.component';

describe('BillerBankChargesDetailsComponent', () => {
  let component: BillerBankChargesDetailsComponent;
  let fixture: ComponentFixture<BillerBankChargesDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BillerBankChargesDetailsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BillerBankChargesDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
