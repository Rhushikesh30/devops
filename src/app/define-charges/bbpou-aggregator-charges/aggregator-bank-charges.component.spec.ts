import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AggregatorBankChargesComponent } from './aggregator-bank-charges.component';

describe('AggregatorBankChargesComponent', () => {
  let component: AggregatorBankChargesComponent;
  let fixture: ComponentFixture<AggregatorBankChargesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AggregatorBankChargesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AggregatorBankChargesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
