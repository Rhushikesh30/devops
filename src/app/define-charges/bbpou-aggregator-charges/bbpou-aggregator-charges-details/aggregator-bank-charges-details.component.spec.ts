import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AggregatorBankChargesDetailsComponent } from './aggregator-bank-charges-details.component';

describe('AggregatorBankChargesDetailsComponent', () => {
  let component: AggregatorBankChargesDetailsComponent;
  let fixture: ComponentFixture<AggregatorBankChargesDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AggregatorBankChargesDetailsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AggregatorBankChargesDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
