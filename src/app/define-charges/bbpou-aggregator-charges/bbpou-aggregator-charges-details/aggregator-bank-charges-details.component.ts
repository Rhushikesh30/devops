import { Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import Swal from "sweetalert2";
import { BillerbankChargesService } from 'src/app/shared/services/billerbank-charges.service';
import { AllmasterService } from 'src/app/shared/services/allmaster.service';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-aggregator-bank-charges-details',
  templateUrl: './aggregator-bank-charges-details.component.html',
  styleUrls: ['./aggregator-bank-charges-details.component.sass'],
  providers: [DatePipe],

})
export class AggregatorBankChargesDetailsComponent implements OnInit {
  @Input() rowData: [];
  @Input() editFlag:string;
  @Input() submitBtn:boolean;
  @Input() showLoader:boolean;
  @Input() billerBankChargesData : any;
  @Output() onSave = new EventEmitter<any>();
  @Output() onCancel = new EventEmitter<any>();

  aggregatorbankchargesform: FormGroup;
  COMPANY_ID: string;
  COMPANY_TYPE: any;
  btnVal = 'Submit';
  cancelFlag=true
  submitted = false;
  taxdata: any = [];
  service_mode = [];
  billing_mode = [];
  settlement_mode = [];
  billerData = [];
  aggregatorData = [];
  company_data = [];
  companytype:any
  bbpou_bank_name:any=[];
  billerCategories=[];
  settlement_type: any = [];
  transaction_type: any = [];
  transactionmode:any=[];
  aggregatorStandardCharges = [];
  rate_percent_row = {};
  minv_maxv_dict = {};
  todayDate = new Date().toJSON().split('T')[0];
  todayDateMin = new Date().toJSON().split('T')[0];
  todayDateMax = new Date().toJSON().split('T')[0];
  BBPOUBankData: any = [];

  ratePercentRow:boolean = true

  constructor(private formBuilder: FormBuilder, 
              public datepipe: DatePipe ,
              private allmasterService: AllmasterService, 
              private billerbankChargesService: BillerbankChargesService) { }

  ngOnInit(): void {
    this.COMPANY_TYPE = localStorage.getItem('COMPANY_TYPE');

    this.aggregatorbankchargesform = this.formBuilder.group({
      id: [''],
      aggregator_company_ref_id:['', Validators.required],
      version_number: ['1'],
      biller_company_ref_id:[''],
      bbpou_company_ref_id:['', Validators.required],
      start_date: ['', Validators.required],
      end_date: ['2099-12-31'],
      revision_status: ['', Validators.required],
      initialItemRow: this.formBuilder.array([this.initialItemRow()])
    });

    if(!Array.isArray(this.rowData)){
      this.editViewRecord(this.rowData,this.editFlag);
     }

    this.billerbankChargesService.gettaxdata('GST').subscribe({
      next:(data:[]) => {
        this.taxdata=data;
      }
    });

    this.billerbankChargesService.getBBPOUCompanyData(this.COMPANY_TYPE).subscribe({
      next:data => {
        this.BBPOUBankData = data;
      }
    });

    this.allmasterService.getMasterDataByType('Transaction Mode').subscribe({
      next:data =>{
        this.transactionmode = data;  
      } 
    });

    this.allmasterService.getMasterDataByType('Settlement Type').subscribe({
      next:data=>{
        this.settlement_type=data;
      }
    });

    this.allmasterService.getMasterDataByType('Transaction Type').subscribe({
      next:data =>{
        this.transaction_type = data;  
      } 
    });

    this.billerbankChargesService.getAllAggregatorsData('Aggregator', this.COMPANY_TYPE).subscribe({
      next:(data: any) => {
        this.aggregatorData = data;
      }
    });

    this.billerbankChargesService.getBillerCategory().subscribe({
      next:data =>{
        this.billerCategories = data;
      }
    });

    this.aggregatorbankchargesform.get("start_date").valueChanges.subscribe({
      next:val => {
        let curr_date = new Date().toJSON().split('T')[0];
        curr_date = this.datepipe.transform(curr_date, 'dd/MM/yyyy');
        val = this.datepipe.transform(val, 'dd/MM/yyyy');  
         if (val != null) {        
          if (val=== curr_date  || val < curr_date){      
            this.aggregatorbankchargesform.get("revision_status").setValue("Effective");
          }
          else if(val > curr_date){
            this.aggregatorbankchargesform.get("revision_status").setValue("Future");
          }
        }
      }
    });
  }

  aggregatorChange(){
    while (this.formArray.length) {
      this.formArray.removeAt(-1)
    }
    this.billerbankChargesService.getAllBillersDataBYID(this.COMPANY_TYPE).subscribe({
      next:(data: any) => {
        this.billerData = data;
        this.billerbankChargesService.getAggregatorStandardCharges().subscribe({
          next:data =>{
            this.aggregatorStandardCharges = data[0];            
            for(let myData in data){ 
              this.addDynamicNewRow2_process(data[myData]);
            }
          }
        });
      }
    });
  }
  
  billerchange(){
    while (this.formArray.length) {
      this.formArray.removeAt(-1)
    }
    this.formArray.push(this.initialItemRow());
  }

  addDynamicNewRow2_process(data) {
    this.formArray.push(this.addInitialitemRow_process(data));
  }

  addInitialitemRow_process(data) {
    return this.formBuilder.group({
      rate_flag:  [data['is_rate']=== true?'Y':'N'],
      rate: [data['rate']],
      percent: [data['percent']],
      min_transaction: ['0'],
      max_transaction: ['0'],
      min_value: [data['min_transaction_value']],
      max_value: [data['max_transaction_value']],
      transaction_mode_ref_id: [data['transaction_mode_ref_id']],
      min_commission: ['0'],
      max_commission: ['0'],
      biller_category_ref_id:[data['biller_category_ref_id']],
      tax_rate: [data['tax_rate']]
    });
  }

  get formArray() {
    return this.aggregatorbankchargesform.get('initialItemRow') as FormArray;
  }

  addNewRow(){   
      this.formArray.push(this.initialItemRow());
  }

  deleteRow(index) {
    if (this.formArray.length == 1) {
      return false;
    } else {
      this.formArray.removeAt(index);
      return true;
    }
  }

  ratePercentFlag(event, index){
    let gridRow = (<FormArray>(this. aggregatorbankchargesform.get('initialItemRow'))).at(index);
    if (event=='N'){
      gridRow.get("rate_flag").setValue(false);
      gridRow.get("rate").disable({ onlySelf: true});
      gridRow.get("rate").setValue("0");
      gridRow.get("percent").enable({ onlySelf: true});
      gridRow.get("percent").setValue("");
    }else{
      gridRow.get("rate_flag").setValue(true);
      gridRow.get("percent").disable({ onlySelf: true});
      gridRow.get("percent").setValue("0");
      gridRow.get("rate").enable({ onlySelf: true});
      gridRow.get("rate").setValue("");
    }    
  }

  changeCategoriesFields(gridRowID){
    let gridRow = (<FormArray>(this.aggregatorbankchargesform.get('initialItemRow'))).at(gridRowID);
    let currentBillerCategories = gridRow.get('biller_category_ref_id').value
    let currentTransactionMode = gridRow.get('transaction_mode_ref_id').value

    if (currentTransactionMode==''){
      this.showSwalmessage("Select Transaction Mode!",'Before Selection Biller Category must selected Tr Mode',"warning",false);
      gridRow.get("biller_category_ref_id").setValue('');
    }else{
      let initialItemRow = this.aggregatorbankchargesform.get('initialItemRow').value
      initialItemRow.pop(gridRowID)
      let maxVAlue
      for (let indexValues of initialItemRow){
        if (indexValues.transaction_mode_ref_id == currentTransactionMode && indexValues.biller_category_ref_id == currentBillerCategories){
          maxVAlue = indexValues.max_value
          if(maxVAlue >= 99999999){
            this.showSwalmessage("Slabs are Over!",'if u want to define this slab please refaccor previous',"warning",false);
            gridRow.get("biller_category_ref_id").setValue('');
            gridRow.get("transaction_mode_ref_id").setValue('');
            gridRow.get("min_value").setValue(1);
          }else{
            gridRow.get("min_value").setValue(maxVAlue+1);
          }
        }
      }
    }
  }

  checkRadioButton(gridRowID, val){
    let gridRow = (<FormArray>(this.aggregatorbankchargesform.get('initialItemRow'))).at(gridRowID);
    if(gridRow.get('rate_flag').value == val) {
      return true;
    }
    else {
      return false;
    }
  }

  passedApplicableRateControl(gridRowID){
    let gridRow = (<FormArray>(this.aggregatorbankchargesform.get('initialItemRow'))).at(gridRowID);
    if(gridRow.get('rate').value<0){
      gridRow.get('rate').setValue(0);
    }
  }

  passedApplicablePercentControl(gridRowID){
    let gridRow = (<FormArray>(this.aggregatorbankchargesform.get('initialItemRow'))).at(gridRowID);
    if(gridRow.get('percent').value<0){
      gridRow.get('percent').setValue(0);
    }
  }

  showSwalmessage(message, text, icon, confirmButton): void {
    if (! confirmButton) {
        Swal.fire({ title: message, text: text, icon: icon, timer: 2000, showConfirmButton: false });
    } else {
        Swal.fire({ title: message, text: text, icon: icon, timer: 2000, showConfirmButton: true, confirmButtonText: "Yes", cancelButtonText: "No" });
    }
  }

  onCancelForm(){
    this.cancelFlag=false
    this.aggregatorbankchargesform.reset();
    this.onCancel.emit(false);
  }

  initialItemRow() {
    return this.formBuilder.group({
      id: [''],
      transaction_mode_ref_id: ['', Validators.required],
      biller_category_ref_id:['',Validators.required],
      rate_flag: ['', Validators.required],
      rate: ['0', Validators.required],
      percent: ['0', Validators.required],
      min_commission: ['0', Validators.required],
      max_commission: ['0', Validators.required],
      tax_rate: ['', Validators.required],
      min_value: ['1', Validators.required],
      max_value: ['99999999', Validators.required],
      min_transaction: ['0', Validators.required],
      max_transaction: ['0', Validators.required],
    });
  }

  format_date(date){
    let d = new Date(date),
    month = '' + (d.getMonth() + 1),
    day = '' + d.getDate(),
    year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;
    return [year, month,day ].join('-');
  }// end of format date

  setExistingArray(initialArray = []): FormArray {
    const formArray = new FormArray([]);
    initialArray.forEach(int_fee => {
      formArray.push(this.formBuilder.group({
        transaction_mode_ref_id: int_fee.transaction_mode_ref_id,
        rate_flag: int_fee.rate_flag,
        rate: int_fee.rate,
        percent: int_fee.percent,
        min_commission: int_fee.min_commission,
        max_commission: int_fee.max_commission,
        tax_rate: int_fee.tax_rate,
        min_value: int_fee.min_value,
        max_value: int_fee.max_value,
        min_transaction: int_fee.min_transaction,
        max_transaction: int_fee.max_transaction,
        biller_category_ref_id:int_fee.biller_category_ref_id      
      }));
    });
    return formArray;
  }// end of set existing array

  versionCreateFunction(row){
    this.btnVal = "Create Version";
    let currtDate = new Date().toJSON().split("T")[0];

      if(row.start_date == currtDate) {
        this.showSwalmessage("Cannot create version on the same day!",'',"warning",false);
        this.onCancelForm();
      }

      let futureCheckData = []; 
      if(row.biller_company_ref_id != null){
        futureCheckData = this.billerBankChargesData.find((x)=>{
          if(x.aggregator_name === row.aggregator_name && x.revision_status === 'Future' && x.biller_id === row.biller_company_ref_id){
            return x
          }
        })
      }else{
        futureCheckData = this.billerBankChargesData.find((x)=>{
          if(x.aggregator_name === row.aggregator_name && x.revision_status === 'Future'){
            return x
          }
        })            
      }

      if(futureCheckData != undefined){
        this.showSwalmessage('Cannot create more than one future record for same Source!','','warning',false)
        this.onCancelForm();
      }

      let newdate = new Date(this.todayDate);
      let new_to_date=newdate.setDate(newdate.getDate()+1);
      let new_to_date_1 =this.format_date(new_to_date)
      this.aggregatorbankchargesform.get("start_date").setValue(new_to_date_1);    
  }

  patchValueFunction(row, version_number){
    this.aggregatorbankchargesform.patchValue({
      id: row.id,
      aggregator_company_ref_id:row.aggregator_company_ref_id,
      biller_company_ref_id: row.biller_company_ref_id,
      bbpou_company_ref_id: row.bbpou_company_ref_id,
      end_date: row.end_date,
      version_number:version_number
    });
    
    this.aggregatorbankchargesform.setControl('initialItemRow', this.setExistingArray(row.initialItemRow));

    for(let i=0; i<this.formArray.length; i++){
      let gridRow = (<FormArray>(this.aggregatorbankchargesform.get('initialItemRow'))).at(i);
      let flg = gridRow.get('rate_flag').value
      flg === false ? gridRow.get('rate_flag').setValue('N') : gridRow.get('rate_flag').setValue('Y')
    }      
  }


  editViewRecord(data, editFlag) {
    this.billerbankChargesService.getbillerBankChargesById(data.id).subscribe({
      next:(row: any) => {
        this.todayDateMax = "2099-12-31"
        let version_number = row.version_number
  
        if(editFlag == 'view'){
          this.submitBtn = false;
          this.aggregatorbankchargesform.get('start_date').setValue(row.start_date)        
        }
  
        if(editFlag== 'versionCreate'){
          this.versionCreateFunction(row)
          version_number = Number(this.aggregatorbankchargesform.get('version_number').value) + 1        
        }
  
        if(editFlag == 'veredit'){
          this.btnVal = 'Update Version';
          this.aggregatorbankchargesform.get("start_date").setValue(data.start_date)
        }
        this.patchValueFunction(row, version_number)
      }
    });
    this.showLoader = false;
  }

  tranctionsTypeCheck(check1, check2, difference){
    if(this.aggregatorbankchargesform.get('biller_company_ref_id').value){
      if(check1===false && check2===false){
        this.showSwalmessage("Please select either BOTH or On-Us and Off-Us!",'',"warning",false);
        return;
      }
      if(check1===true && difference.length === 2){
        let len = (<FormArray>(this.aggregatorbankchargesform.get('initialItemRow'))).length
        if(len%2!=0){
          this.showSwalmessage("please select both on-us and off-us!",'',"warning",false);
          return;
        } 
      }
    }
  }

  onSubmit() {
    if (!this.aggregatorbankchargesform.invalid){
      this.submitted = true;   
      let arr1 = []
      let arr2 = []
      let on_off_us_arr = []
      let both_arr = []

      this.transactionmode.forEach(element => {
        arr1.push(element["id"]);
      });
      for(let i=0; i<this.formArray.length; i++) { 
        let gridRow = (<FormArray>(this.aggregatorbankchargesform.get('initialItemRow'))).at(i);
        const transaction_mode_ref_id = gridRow.get('transaction_mode_ref_id').value.toString(); 
        if(transaction_mode_ref_id!=''){
          arr2.push(Number.parseInt(gridRow.get('transaction_mode_ref_id').value));
        }     
      }
      
      this.transactionmode.forEach(element => {
        element["master_key"] == 'BOTH'? both_arr.push(element["id"]) : on_off_us_arr.push(element["id"])
      });
      
      let difference = arr1.filter(x => !arr2.includes(x));
      
      let check1 = both_arr.every(function (element) {
        return difference.includes(element);
      });

      let check2 = on_off_us_arr.every(function (element) {        
        return difference.includes(element);
      });

      this.tranctionsTypeCheck(check1, check2, difference)

      if(difference.length === 1 || difference.length == 2){ 
        this.aggregatorbankchargesform.value.start_date = this.datepipe.transform(this.aggregatorbankchargesform.value.start_date, "yyyy-MM-dd")
        this.onSave.emit(this.aggregatorbankchargesform.value);
      }
    }
  }
}




