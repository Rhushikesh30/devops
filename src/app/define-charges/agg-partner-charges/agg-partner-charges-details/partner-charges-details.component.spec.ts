import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PartnerChargesDetailsComponent } from './partner-charges-details.component';

describe('PartnerChargesDetailsComponent', () => {
  let component: PartnerChargesDetailsComponent;
  let fixture: ComponentFixture<PartnerChargesDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PartnerChargesDetailsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PartnerChargesDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
