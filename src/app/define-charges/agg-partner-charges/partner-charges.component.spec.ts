import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PartnerChargesComponent } from './partner-charges.component';

describe('PartnerChargesComponent', () => {
  let component: PartnerChargesComponent;
  let fixture: ComponentFixture<PartnerChargesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PartnerChargesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PartnerChargesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
