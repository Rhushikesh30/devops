import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AllmasterService } from 'src/app/shared/services/allmaster.service';
import { DatePipe } from '@angular/common';
import { NpciChargesServiceService } from 'src/app/shared/services/npci-charges-service.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-npci-charges-details',
  templateUrl: './npci-charges-details.component.html',
  styleUrls: ['./npci-charges-details.component.sass'],
  providers: [ DatePipe],
})
export class NpciChargesDetailsComponent implements OnInit {
  @Input() rowData: [];
  @Input() editFlag:string;
  @Input() submitBtn:boolean;
  @Input() showLoader:boolean;
  @Input() npciChargesData: any;
  @Output() onSave = new EventEmitter<any>();
  @Output() onCancel = new EventEmitter<any>();

  npciChargesForm: FormGroup;
  btnVal = 'Submit';
  cancelFlag=true 
  COMPANY_ID: string;
  COMPANY_TYPE:string;
  COMPANY_NAME:string;
  companydata: any = [];
  service_model: any = [];
  billing_mode: any = [];
  onboarding_mode: [];
  settlement_mode: [];
  service_mode: [];
  biller_mode: [];
  bbpou_bank_name:any=[];
  settlement_frequency: any = [];
  settlement_type: any = [];
  billing_frequency:any=[];
  transaction_mode:any=[];
  transaction_type = [];
  company_name= [];
  mti_data = [];
  billerdata =[];
  taxdata=[];
  feeTypeData=[];
  feecode_data=[];
  companies=[];
  payment_mode_data = [];
  payment_channel_data=[];
  billerCategoryData =[];
  rate_percent_row = {};
  minv_maxv_dict = {};
  submitted = false;
  todayDate = new Date().toJSON().split('T')[0];
  todayDateMin = new Date().toJSON().split('T')[0];
  todayDateMax = new Date().toJSON().split('T')[0];
  settlementtype:any
  BBPOUBankData =[]

    constructor(private formBuilder: FormBuilder, 
                public datepipe: DatePipe,
                private allmasterService: AllmasterService, 
                private npciChrgsService: NpciChargesServiceService) { }

  ngOnInit(): void {
    this.COMPANY_ID = localStorage.getItem('COMPANY_ID');
    this.COMPANY_TYPE  = localStorage.getItem('COMPANY_TYPE');
    this.COMPANY_NAME  = localStorage.getItem('COMPANY_NAME');

    this.npciChargesForm = this.formBuilder.group({
      id: [''],
      bbpou_company_ref_id: ['', Validators.required],
      version_number: ['1'],
      settlement_type_ref_id: [''],
      settlement_type :[''],
      start_date: ['', Validators.required],
      end_date: ['2099-12-31'],
      revision_status: ['', Validators.required],
      initialItemRow: this.formBuilder.array([this.initialItemRow()])
    });
    if(!Array.isArray(this.rowData)){
      this.editViewRecord(this.rowData,this.editFlag);
    }

    this.allmasterService.getMasterDataByType('Transaction Mode').subscribe({
      next:data =>{
        this.transaction_mode = data;  
      } 
    });

    this.npciChrgsService.getBBPOUCompanyData(this.COMPANY_TYPE).subscribe({
      next:data => {
        this.BBPOUBankData = data;
      }
    });

    this.allmasterService.getMasterDataByType('Settlement Type').subscribe({
      next:data=>{
        this.settlement_type=data;
        let newArray = data.filter(function(item){
            return item.master_key=='Net';
        });
        this.npciChargesForm.get('settlement_type_ref_id').setValue(newArray[0].id)
        this.npciChargesForm.get('settlement_type').setValue(newArray[0].master_key)
      }
    });

    this.allmasterService.getMasterDataByType('Settlement Frequency').subscribe({
      next:data =>{
        this.settlement_frequency = data;
      } 
    });

    this.allmasterService.getMasterDataByType('Billing Frequency').subscribe({
      next:data =>{
        this.billing_frequency = data;  
      } 
    });

    this.allmasterService.getMasterDataByType('Transaction Type').subscribe({
      next:data =>{
        this.transaction_type = data;  
      } 
    });

    this.allmasterService.getMasterDataByType('Fee Type').subscribe({
      next:data =>{
        this.feeTypeData = data;
      }
    });

    this.allmasterService.getMasterDataByType('MTI').subscribe({
      next:data =>{
        this.mti_data = data;
      }
    });

    this.allmasterService.getMasterDataByType('Payment Mode').subscribe({
      next:data =>{
        this.payment_mode_data = data;
      }
    });

    this.allmasterService.getMasterDataByType('Payment Channel').subscribe({
      next:data =>{
        this.payment_channel_data = data;
      }
    });
    
    this.npciChrgsService.getAllBillersData(this.COMPANY_TYPE).subscribe({
      next:(data: any) => {
        this.companydata = data;
      }
    });

    this.npciChrgsService.getBillerCategory().subscribe({
      next:data =>{
        this.billerCategoryData = data;
      }
    })

    this.npciChrgsService.getFeesCode().subscribe({
      next:data =>{
        this.feecode_data = data;
      }
    });

    this.npciChrgsService.gettaxdata('GST').subscribe({
      next:(data : []) => {
        this.taxdata=data;
      }
    })

    this.npciChargesForm.get("start_date").valueChanges.subscribe({
      next:val => {
        let curr_date = new Date().toJSON().split('T')[0];
        curr_date = this.datepipe.transform(curr_date, 'dd/MM/yyyy');
        val = this.datepipe.transform(val, 'dd/MM/yyyy');  
        if(val != null){
          if (val=== curr_date) {
            this.npciChargesForm.get("revision_status").setValue("Effective");
          }
          else if(val > curr_date){
            this.npciChargesForm.get("revision_status").setValue("Future");
          }
        }
      }
    });

    this.npciChrgsService.getAllBillersData(this.COMPANY_TYPE).subscribe({
      next:(data: any) => {
        this.companies = data;
      }
    });
  }//end of oninit

  initialItemRow() {
    return this.formBuilder.group({
      id: [''],
      transaction_mode_ref_id: ['', Validators.required],
      fee_type_flag_ref_id: ['', Validators.required],
      rate_flag: ['', Validators.required],
      rate: ['', Validators.required],
      percent: ['', Validators.required],
      min_commission: ['0', Validators.required],
      max_commission: ['0', Validators.required],
      tax_rate: ['', Validators.required],
      min_value: ['1', Validators.required],
      max_value: ['99999999', Validators.required],
      min_transaction: ['0', Validators.required],
      max_transaction: ['0', Validators.required],
      mti_ref_id:['', Validators.required],
      payment_mode_ref_id:['', Validators.required],
      payment_channel_ref_id:['', Validators.required],
      default_flag: [false],
      fees_code:['', Validators.required],
      biller_category_ref_id:['',Validators.required]

    });
  }
  get formArray() {
    return this.npciChargesForm.get('initialItemRow') as FormArray;
  }

  addNewRow() {   
    this.formArray.push(this.initialItemRow());   
  }

  deleteRow(index) {
  if (this.formArray.length == 1) {
    return false;
  } else {
    this.formArray.removeAt(index);
    return true;
  }
  }

  setExistingArray(initialArray = []): FormArray {
    const formArray = new FormArray([]);
    initialArray.forEach(int_fee => {
      formArray.push(this.formBuilder.group({
        transaction_mode_ref_id: int_fee.transaction_mode_ref_id,
        fee_type_flag_ref_id: int_fee.fee_type_flag_ref_id,
        rate_flag: int_fee.rate_flag,
        rate: int_fee.rate,
        percent: int_fee.percent,
        min_commission: int_fee.min_commission,
        max_commission: int_fee.max_commission,
        tax_rate: int_fee.tax_rate,
        min_value: int_fee.min_value,
        max_value: int_fee.max_value,
        min_transaction: int_fee.min_transaction,
        max_transaction: int_fee.max_transaction,
        mti_ref_id: int_fee.mti_ref_id,
        payment_mode_ref_id: int_fee.payment_mode_ref_id,
        payment_channel_ref_id: int_fee.payment_channel_ref_id,
        default_flag: int_fee.default_flag,
        fees_code: int_fee.fees_code,
        biller_category_ref_id:int_fee.biller_category_ref_id,
      }));
    });
    return formArray;
  }

  format_date(date) {
    let d = new Date(date),
      month = "" + (d.getMonth() + 1),
      day = "" + d.getDate(),
      year = d.getFullYear();
    if (month.length < 2) month = "0" + month;
    if (day.length < 2) day = "0" + day;
    return [year, month, day].join("-");
  }

  editViewRecord(data,editFlag) {
    this.npciChrgsService.getNPCIChargesById(data.id).subscribe({
      next:(row: any) => {
        this.todayDateMax = "2099-12-31"
        if(editFlag == 'view'){
          this.submitBtn = false;
          let newdate = row.start_date
          this.npciChargesForm.get("start_date").setValue(newdate)
        }
        let version_number = row.version_number
  
        if(editFlag== 'versionCreate'){
          this.btnVal = "Create Version";
          let currtDate = new Date().toJSON().split("T")[0];
  
            if(row.start_date == currtDate) {
            this.showSwalmessage("Cannot create version on the same day!",'',"warning",false);
            this.onCancelForm();
            }
  
            let futureCheckData = [];
             futureCheckData = this.npciChargesData.find((x)=>{
              if(x.revision_status === 'Future' && x.source_name === row.source_name ){
                return x
              }
            })
            if(futureCheckData != undefined){
              this.showSwalmessage('Cannot create more than one future record for same Source!','','warning',false)
              this.onCancelForm();
            }
  
          version_number = Number(this.npciChargesForm.get('version_number').value) + 1
          let newdate = new Date(this.todayDate);
          let new_to_date=newdate.setDate(newdate.getDate()+1);
          let new_to_date_1 =this.format_date(new_to_date)
          this.npciChargesForm.get("start_date").setValue(new_to_date_1);  
          this.npciChargesForm.get("revision_status").setValue('Future')
        }//endofcreateVersion
  
        if(editFlag == 'veredit'){
          this.btnVal = 'Update Version';
          this.npciChargesForm.get("start_date").setValue(data.start_date)
        }
  
        this.npciChargesForm.patchValue({
          id: row.id,
          bbpou_company_ref_id: row.bbpou_company_ref_id,
          settlement_type_ref_id: row.settlement_type_ref_id,
          end_date: row.end_date,
          version_number:version_number
        });
  
        this.npciChargesForm.setControl('initialItemRow', this.setExistingArray(row.initialItemRow));
        for(let i=0;i<this.formArray.length; i++){
          let gridRow = (<FormArray>(this.npciChargesForm.get('initialItemRow'))).at(i);
          let flg = gridRow.get('rate_flag').value
          if(flg === false){
            gridRow.get('rate_flag').setValue('N')
          }
          else if(flg === true){
            gridRow.get('rate_flag').setValue('Y')
          }
        }  
      }      
    });
    this.showLoader = false;
  }

  ratePercentFlag(event, index){
    let gridRow = (<FormArray>(this. npciChargesForm.get('initialItemRow'))).at(index);
    if (event=='N'){
      gridRow.get("rate_flag").setValue(false);
      gridRow.get("rate").disable({ onlySelf: true});
      gridRow.get("rate").setValue("0");
      gridRow.get("percent").enable({ onlySelf: true});
      gridRow.get("percent").setValue("");
    }else{
      gridRow.get("rate_flag").setValue(true);
      gridRow.get("percent").disable({ onlySelf: true});
      gridRow.get("percent").setValue("0");
      gridRow.get("rate").enable({ onlySelf: true});
      gridRow.get("rate").setValue("");
    }    
  }

  changeCategoriesFields(gridRowID){
    let gridRow = (<FormArray>(this.npciChargesForm.get('initialItemRow'))).at(gridRowID);
    let currentBillerCategories = gridRow.get('biller_category_ref_id').value
    let currentTransactionMode = gridRow.get('transaction_mode_ref_id').value
    let feeTypeFlagReference = gridRow.get('fee_type_flag_ref_id').value

    if (currentTransactionMode=='' || feeTypeFlagReference==''){
      this.showSwalmessage("Select Transaction Mode And Fee Type!",'Before Selection Biller Category must selected Tr Mode and Fee Type',"warning",false);
      gridRow.get("biller_category_ref_id").setValue('');
    }else{
      let initialItemRow = this.npciChargesForm.get('initialItemRow').value
      initialItemRow.pop(gridRowID)
      let maxVAlue
      for (let indexValues of initialItemRow){
        if (indexValues.transaction_mode_ref_id == currentTransactionMode && indexValues.biller_category_ref_id == currentBillerCategories && indexValues.fee_type_flag_ref_id==feeTypeFlagReference){
          maxVAlue = indexValues.max_value
          if(maxVAlue >= 99999999){
            this.showSwalmessage("Slabs are Over!",'if u want to define this slab please refaccor previous',"warning",false);
            gridRow.get("biller_category_ref_id").setValue('');
            gridRow.get("transaction_mode_ref_id").setValue('');
            gridRow.get("fee_type_flag_ref_id").setValue('');
            gridRow.get("min_value").setValue(1);
          }else{
            gridRow.get("min_value").setValue(maxVAlue+1);
          }
        }
        else{
          gridRow.get("max_value").setValue(99999999);
          gridRow.get("min_value").setValue(1);
        }
      }
    }
  }

  checkRadioButton(gridRowID, val) {
    let gridRow = (<FormArray>(this.npciChargesForm.get('initialItemRow'))).at(gridRowID);
      if(gridRow.get('rate_flag').value == val) {
        return true;
      }
      else {
        return false;
      }
  }

  default_flag_value(gridRowID){
    let gridRow = (<FormArray>(this.npciChargesForm.get('initialItemRow'))).at(gridRowID);
    if(gridRow.get('default_flag').value === true) {
      gridRow.get('default_flag').setValue(true);
    }
    else{
      gridRow.get('default_flag').setValue(false);
    } 
  }

  showSwalmessage(message, text, icon, confirmButton): void {
    if (! confirmButton) {
        Swal.fire({ title: message, text: text, icon: icon, timer: 2000, showConfirmButton: false });
    } else {
        Swal.fire({ title: message, text: text, icon: icon, timer: 2000, showConfirmButton: true, confirmButtonText: "Yes", cancelButtonText: "No" });
    }
  }
      
  onCancelForm(){
    this.cancelFlag=false
    this.npciChargesForm.reset();
    this.onCancel.emit(false);
  }

  checkFeesTypeSelected(arr2, fee_arr2){
    for(let i=0; i<this.formArray.length; i++) { 
      let gridRow = (<FormArray>(this.npciChargesForm.get('initialItemRow'))).at(i);
      const transaction_mode_ref_id = gridRow.get('transaction_mode_ref_id').value.toString(); 
      const fee_type_flag_ref_id = gridRow.get('fee_type_flag_ref_id').value.toString();

      if(transaction_mode_ref_id!=''){
        arr2.push(Number.parseInt(gridRow.get('transaction_mode_ref_id').value));
        arr2.push(Number.parseInt(gridRow.get('fee_type_flag_ref_id').value));
        arr2.push(Number.parseInt(gridRow.get('biller_category_ref_id').value));
        fee_arr2.push(Number.parseInt(gridRow.get('fee_type_flag_ref_id').value));
      }  

      if(fee_type_flag_ref_id == ''){
        this.showSwalmessage("Please Select Fee Type!",'',"warning",false);
        return;
      }
    }
  }

  checkSameCategoryFunction(len, number, slen, intlen){
    if(len%2!=0){
      this.showSwalmessage("Please select both for all fee type !",'',"warning",false);
      return false;
      }

    if(slen != intlen){        
      this.showSwalmessage("Please Select different feetype!",'',"warning",false);
      return false;
    }
  }

  checkTranctionTypeSelected(check1, check2, difference, slen, intlen ){
    if(check1===false && check2===false ){
      this.showSwalmessage("Please select either BOTH or On-Us and Off-Us'!",'',"warning",false);
      return false;
    }

    if(check1===true && difference.length === 2){
      this.showSwalmessage("Please select both on-us and off-us!",'',"warning",false);
      return false;
    }

    let len = (<FormArray>(this.npciChargesForm.get('initialItemRow'))).length
    if(check1 === false && check2 === true  && difference.length === 2){
      this.checkSameCategoryFunction(len, 2, slen, intlen)
      return false;
    }   
    
    else if(check1 === true && check2 === false && difference.length === 1){
      this.checkSameCategoryFunction(len, 4, slen, intlen)
      return false;
    }
  }

  onSubmit(){
    if (!this.npciChargesForm.invalid) {
      this.submitted = true;
      let arr2 = []
      let arr1 = []
      let on_off_us_arr = []
      let both_arr = []
      let fee_arr2=[];

      this.checkFeesTypeSelected(arr2, fee_arr2) 
  
      this.transaction_mode.forEach(element => {
        arr1.push(element["id"]);
      });
    
      this.transaction_mode.forEach(element => {
        element["master_key"] == 'BOTH' ? both_arr.push(element["id"]) : on_off_us_arr.push(element["id"])
      });
     
      let difference = arr1.filter(x => !arr2.includes(x));
      
      let check1 = both_arr.every(function (element) {
        return difference.includes(element);
      });

      let check2 = on_off_us_arr.every(function (element) {
        return difference.includes(element);
      });

      let switch_arr=[];
      let interchange_arr=[];
  
      this.feeTypeData.forEach(element => {
        element["master_key"] == 'Switch Fee' ? switch_arr.push(element["id"]) : interchange_arr.push(element["id"])
      });

      let sf=[]; let intf=[];
      sf = fee_arr2.filter(x=> switch_arr.includes(x))
      intf = fee_arr2.filter(x=> interchange_arr.includes(x))
      let slen = sf.length
      let intlen = intf.length

      this.checkTranctionTypeSelected(check1, check2, difference, slen, intlen)

      if(difference.length === 1 || difference.length === 2){
        this.npciChargesForm.value.start_date = this.datepipe.transform(this.npciChargesForm.value.start_date, "yyyy-MM-dd")
        this.onSave.emit(this.npciChargesForm.value);
      }
    }
  }
}
