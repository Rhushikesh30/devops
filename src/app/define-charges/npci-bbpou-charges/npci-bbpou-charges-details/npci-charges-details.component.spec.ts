import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NpciChargesDetailsComponent } from './npci-charges-details.component';

describe('NpciChargesDetailsComponent', () => {
  let component: NpciChargesDetailsComponent;
  let fixture: ComponentFixture<NpciChargesDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NpciChargesDetailsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NpciChargesDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
