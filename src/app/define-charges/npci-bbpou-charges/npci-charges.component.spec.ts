import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NpciChargesComponent } from './npci-charges.component';

describe('NpciChargesComponent', () => {
  let component: NpciChargesComponent;
  let fixture: ComponentFixture<NpciChargesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NpciChargesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NpciChargesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
