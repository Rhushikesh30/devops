import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RoleMasterDetailsComponent } from './role-master-details.component';

describe('RoleMasterDetailsComponent', () => {
  let component: RoleMasterDetailsComponent;
  let fixture: ComponentFixture<RoleMasterDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RoleMasterDetailsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RoleMasterDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
