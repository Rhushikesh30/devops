import { Component, OnInit, Input, Output, EventEmitter, } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-role-master-details',
  templateUrl: './role-master-details.component.html',
  styleUrls: ['./role-master-details.component.sass']
})
export class RoleMasterDetailsComponent implements OnInit {
  @Input() rowData: [];
  @Input() submitBtn: boolean;
  @Input() showLoader: boolean;
  @Input() roleAllData: any;
  @Output() onSave = new EventEmitter<any>();
  @Output() onCancel = new EventEmitter<any>();

  ROLLMasterForm: FormGroup;
  btnVal = 'Submit';

  constructor( public formbuilder: FormBuilder ) { }

  ngOnInit(): void {
    this.ROLLMasterForm = this.formbuilder.group({
      ID: [''],
      role_name: ['', [Validators.required,Validators.maxLength(20),Validators.pattern("^[a-zA-Z0-9_ ]*$")]],

    });

    if (!Array.isArray(this.rowData)) {
      this.viewRecord(this.rowData);
    }
  }

  viewRecord(row) {
    this.ROLLMasterForm.patchValue({
      ID: row.id,
      role_name: row.role_name,

  });

  if (this.submitBtn === true) this.btnVal = 'Update';
  this.showLoader = false;
}

onCancelForm() {
  this.ROLLMasterForm.reset();
  this.onCancel.emit(false);
}

onSubmit() {
  if (this.ROLLMasterForm.invalid) {
    return;
  }
  else {
    this.onSave.emit(this.ROLLMasterForm.value);
  }
}
}
