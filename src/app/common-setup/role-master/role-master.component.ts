import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { RolemasterService } from 'src/app/shared/services/rolemaster.service'
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { ROLLMasterElement } from 'src/app/shared/models/role-master';
import { ErrorCodes } from 'src/app/shared/error-codes';
import Swal from 'sweetalert2';
import * as XLSX from 'xlsx';
import { ManageSecurityService } from 'src/app/core/service/manage-security.service';

@Component({
  selector: 'app-role-master',
  templateUrl: './role-master.component.html',
  styleUrls: ['./role-master.component.sass'],
  providers: [ErrorCodes]

})
export class RoleMasterComponent implements OnInit {
  @ViewChild(DatatableComponent, { static: false }) table: DatatableComponent;
  @ViewChild('ALLMasterPaginator',{read: MatPaginator}) ALLMasterPaginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  dataSource :MatTableDataSource<ROLLMasterElement>;
  displayColumns = [
    'actions',
    'role_name',
  ];
  renderedData: ROLLMasterElement[];
  screenName = 'Roles';
  submitBtn = true;
  showLoader = false;
  rowData = [];
  listDiv:boolean = false;
  showList:boolean = true;
  sidebardata: any;
  userId: string;
  
  constructor(private roleMasterService: RolemasterService, 
              private manageSecurityService:ManageSecurityService,
              private errorCodes: ErrorCodes) { }

  ngOnInit(): void {
    this.getAllData();
    let userId =localStorage.getItem('USER_ID')
    this.manageSecurityService.getAccessLeftPanel(userId,this.screenName).subscribe({
      next:data =>{
        this.sidebardata =data;
      }
    });
  }

  editViewRecord(row,flag){
    this.rowData = row;
    this.showList = false;
    this.submitBtn = flag;
    this.listDiv = true;
    this.showLoader = false;
  }

  deleteRecord(rowId) {
    Swal.fire({ title: "Are you sure you want to delete?", text: "", icon: "warning", showCancelButton: true, confirmButtonText: "Yes", cancelButtonText: "No" }).then((result) => {
        if (result.value) {
          this.roleMasterService.delete(rowId).subscribe({
            next: (data:any) => {
              this.showSwalmessage("Your record has been deleted successfully!", "", "success", false);
              this.getAllData();
              this.rowData = []; 
            },
            error:(error) => {
              this.showSwalmessage(this.errorCodes.getErrorMessage(error.status), error.error, 'error', true)
            }
          })
        }
    });
  }

  showFormList(item: boolean) {
    if(item ===false){
      this.listDiv = true;
      this.showList = false;
    }
    else{
      this.listDiv = false;
      this.showList = true;
    }
  }

  onCancel(item: boolean){
    this.listDiv = item;
    this.showList = true;
    this.submitBtn = true;
    this.rowData = [];
  }

  showSwalmessage(message, text, icon, confirmButton): void {
    if (! confirmButton) {
        Swal.fire({ title: message, text: text, icon: icon, timer: 2000, showConfirmButton: false });
    } else {
        Swal.fire({ title: message, text: text, icon: icon, timer: 2000, showConfirmButton: true, confirmButtonText: "Yes", cancelButtonText: "No" });
    }
  }

  onSave(formValue){
    if(this.dataSource.filteredData.length > 0){
      for(let res in this.dataSource.filteredData){
        if(formValue.id && this.dataSource.filteredData[res].id != formValue.id && this.dataSource.filteredData[res].role_name == formValue.role_name ){
          this.showSwalmessage('Record Already Exist!','Name should be unique','warning',false)
          return;
        }
        if(!formValue.id && this.dataSource.filteredData[res].role_name == formValue.role_name  ){
          this.showSwalmessage('Record Already Exist!','Name should be unique','warning',false)
          return;
        }
      }
    }
    this.showLoader = true
    this.roleMasterService.create(formValue).subscribe({
      next: (data:any) => {
        if (data['status'] == 1) {
          this.showSwalmessage('Your record has been updated successfully!','','success',false)
        }
        else if(data['status'] == 2) {
          this.showSwalmessage('Your record has been added successfully!','','success',false)
        }
        this.getAllData();
        this.showList = true;
        this.listDiv = false;
        this.showLoader = false; 
      },
      error:(error) => {
        this.showLoader = false; 
        this.showSwalmessage(this.errorCodes.getErrorMessage(error.status), error.error, 'error', true)
      }
    })
  }

    getAllData(){
    this.roleMasterService.getAll().subscribe({
      next:(data: ROLLMasterElement[])=>{
        this.dataSource = new MatTableDataSource(data);
        this.dataSource.paginator = this.ALLMasterPaginator;
        this.dataSource.sort = this.sort;
        this.dataSource.connect().subscribe(d => this.renderedData = d);
      }
    });   
  }
  
  tbl_FilterDatatable(value:string) {
    this.dataSource.filter = value.trim().toLocaleLowerCase();
  }

  export_to_excel() {
    let XlsMasterData = this.dataSource.data.map(({role_name}) => ({role_name}));
    const workSheet = XLSX.utils.json_to_sheet(XlsMasterData);
    const workBook: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(workBook, workSheet, 'SheetName');
    XLSX.writeFile(workBook, 'Rolemaster_data.xlsx');
  }

}


 