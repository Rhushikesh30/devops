import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AllMasterDetailsComponent } from './all-master-details.component';

describe('AllMasterDetailsComponent', () => {
  let component: AllMasterDetailsComponent;
  let fixture: ComponentFixture<AllMasterDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AllMasterDetailsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AllMasterDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
