import { Component, OnInit,Input, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-all-master-details',
  templateUrl: './all-master-details.component.html',
  styleUrls: ['./all-master-details.component.sass']
})
export class AllMasterDetailsComponent implements OnInit {
  @Input() rowData: [];
  @Input() submitBtn:boolean;
  @Input() showLoader:boolean;
  @Output() onSave = new EventEmitter<any>();
  @Output() onCancel = new EventEmitter<any>();

  allmasterForm: FormGroup;
  btnVal = 'Submit';
  cancelFlag=true 

  constructor(private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.allmasterForm = this.formBuilder.group({
      master_type: ['',[Validators.required, Validators.maxLength(50), Validators.pattern("^[a-zA-Z0-9_ ]*$")]],
      master_value: ['',[Validators.required, Validators.maxLength(50), Validators.pattern("^[a-zA-Z0-9_ ]*$")]],
      master_key:['',[Validators.required, Validators.maxLength(50), Validators.pattern("^[a-zA-Z0-9_ ]*$")]],
      id: ['']
   });
   
   
   if(!Array.isArray(this.rowData)){
    this.viewRecord(this.rowData);
   }
  }
  ngOnDestroy(): void
  {
    if (this.cancelFlag)
    {
      this.allmasterForm.reset();
      this.onCancel.emit(false);
    }
  }
  viewRecord(row) {
    this.allmasterForm.patchValue({
      master_type:row.master_type,
      master_value:row.master_value,
      master_key:row.master_key,
      id: row.id
    });

    if(this.submitBtn === true) this.btnVal = 'Update';
  }

  onCancelForm(){
    this.cancelFlag=false
    this.allmasterForm.reset();
    this.onCancel.emit(false);
  }

  onSubmit() {
    if (this.allmasterForm.invalid) {
      return;
    }
    else
    {
      this.onSave.emit(this.allmasterForm.value);
    }
  }
}
