import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AllMasterComponent } from './all-master.component';

describe('AllMasterComponent', () => {
  let component: AllMasterComponent;
  let fixture: ComponentFixture<AllMasterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AllMasterComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AllMasterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
