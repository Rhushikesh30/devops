import { Component, OnInit, ViewChild } from '@angular/core';
import Swal from 'sweetalert2';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import * as XLSX from 'xlsx';
import { AllmasterService } from 'src/app/shared/services/allmaster.service'
import { ManageSecurityService } from 'src/app/core/service/manage-security.service';
import { Allmaster } from 'src/app/shared/models/allmaster';
import { ErrorCodes } from 'src/app/shared/error-codes';

@Component({
  selector: 'app-all-master',
  templateUrl: './all-master.component.html',
  styleUrls: ['./all-master.component.sass'],
  providers: [ErrorCodes]
})
export class AllMasterComponent implements OnInit {
  @ViewChild('dataSourcePaginator',{read: MatPaginator}) dataSourcePaginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  dataSource :MatTableDataSource<Allmaster>;
  resultData: Allmaster[];
  displayColumns = [
    'actions',
    'master_type',
    'master_value',
    'master_key'
  ];
  renderedData: Allmaster[];
  screenName = 'All Master';
  submitBtn = true;
  showLoader = false;
  rowData = [];
  listDiv:boolean = false;
  showList:boolean = true;
  sidebarData: any;
  

  constructor(private manageSecurityService :ManageSecurityService,
              private allmasterService:AllmasterService,
              private errorCodes: ErrorCodes) { }

  ngOnInit(): void {
    this.getAllData();    
    let userId = localStorage.getItem('USER_ID');
    this.manageSecurityService.getAccessLeftPanel(userId,this.screenName).subscribe({
      next:data =>{
        this.sidebarData =data;
      }
    });
  }//end of ngoninit

  editViewRecord(row,flag){
    this.rowData = row;
    this.showList = false;
    this.submitBtn = flag;
    this.listDiv = true;
    this.showLoader = false;
  }

  showFormList(item: boolean) {
    if(item ===false){
      this.listDiv = true;
      this.showList = false;
    }
    else{
      this.listDiv = false;
      this.showList = true;
    }
  }

  onCancel(item: boolean){
    this.listDiv = item;
    this.showList = true;
    this.submitBtn = true;
    this.rowData = [];
  }

  showSwalmessage(message, text, icon, confirmButton): void {
    if (! confirmButton) {
        Swal.fire({ title: message, text: text, icon: icon, timer: 2000, showConfirmButton: false });
    } else {
        Swal.fire({ title: message, text: text, icon: icon, timer: 2000, showConfirmButton: true, confirmButtonText: "Yes", cancelButtonText: "No" });
    }
  }

  
  onSave(formValue){
    if(this.dataSource){
      for(let res in this.dataSource.filteredData){
        
        if(formValue.id && this.dataSource.filteredData[res].id != formValue.id && this.dataSource.filteredData[res].master_type == formValue.master_type && this.dataSource.filteredData[res].master_value == formValue.master_value && this.dataSource.filteredData[res].master_key == formValue.master_key){
          this.showSwalmessage('Record Already Exist!','Record should be unique','warning',false)
          return;
        }
        if(!formValue.id && this.dataSource.filteredData[res].master_type == formValue.master_type && this.dataSource.filteredData[res].master_value == formValue.master_value && this.dataSource.filteredData[res].master_key == formValue.master_key){
          this.showSwalmessage('Record Already Exist!','Record should be unique','warning',false)
          return;
        }
      }
    }
    this.showLoader = true
    this.allmasterService.create(formValue).subscribe({
      next: (data:any) => {
        if (data['status'] == 1) {
          this.showSwalmessage('Your record has been updated successfully!','','success',false)
        }
        else if(data['status'] == 2) {
          this.showSwalmessage('Your record has been added successfully!','','success',false)
        }
        this.getAllData();
        this.showList = true;
        this.listDiv = false;
        this.showLoader = false 
      },
      error:(error) => {
        this.showLoader = false 
        this.showSwalmessage(this.errorCodes.getErrorMessage(error.status), error.error, 'error', true)
      }
    })
    




  }

  deleteRecord(rowId) {
    Swal.fire({ title: "Are you sure you want to delete?", text: "", icon: "warning", showCancelButton: true, confirmButtonText: "Yes", cancelButtonText: "No" }).then((result) => {
      if (result.value) {
          this.allmasterService.delete(rowId).subscribe({
            next:(data: any) => {
              this.showSwalmessage("Your record has been deleted successfully!", "", "success", false);
              this.getAllData();
              this.rowData = [];
          },
          error:(error) => {
            this.showLoader = false; 
            this.showSwalmessage(this.errorCodes.getErrorMessage(error.status), error.error, 'error', true)
          }
        })
      }
    });
  }

  getAllData(){
    this.allmasterService.getAll().subscribe({
      next:(data: Allmaster[])=>{
        this.dataSource = new MatTableDataSource(data);
        this.dataSource.paginator = this.dataSourcePaginator;
        this.dataSource.sort = this.sort;
        this.dataSource.connect().subscribe(d => this.renderedData = d);
      }
    });
  }

  tbl_FilterDatatable(value:string) {
    this.dataSource.filter = value.trim().toLocaleLowerCase();
  }

  export_to_excel() {
    let XlsMasterData = this.dataSource.data.map(({ master_type,master_value,master_key }) => ({master_type,master_value,master_key}));
    const workSheet = XLSX.utils.json_to_sheet(XlsMasterData);
    const workBook: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(workBook, workSheet, 'SheetName');
    XLSX.writeFile(workBook, 'allmaster.xlsx');
  }
}





