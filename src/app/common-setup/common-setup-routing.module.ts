import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UomComponent } from './uom/uom.component';
import { CurrencyComponent } from './currency/currency.component';
import { AllMasterComponent } from './all-master/all-master.component';
import { RoleMasterComponent } from './role-master/role-master.component';

const routes: Routes = [
  {
    path: 'uom',
    component: UomComponent
  },
  {
    path: 'currency', 
    component: CurrencyComponent
  },
  {
    path: 'all-master',
    component: AllMasterComponent,
  },
  {
    path: 'role-master',
    component:RoleMasterComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CommonSetupRoutingModule { }
