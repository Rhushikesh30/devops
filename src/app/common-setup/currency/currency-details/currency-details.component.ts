import { Component, OnInit, Input, Output, EventEmitter,OnDestroy} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-currency-details',
  templateUrl: './currency-details.component.html',
  styleUrls: ['./currency-details.component.sass']
})

export class CurrencyDetailComponent implements OnInit,OnDestroy{
  @Input() rowData: [];
  @Input() submitBtn:boolean;
  @Input() showLoader:boolean;
  @Output() onSave = new EventEmitter<any>();
  @Output() onCancel = new EventEmitter<any>();
  cancelFlag=true
  currencyForm: FormGroup;
  btnVal = 'Submit';

  constructor(
    private formBuilder: FormBuilder
  ) { }

  ngOnInit(): void {
    this.currencyForm = this.formBuilder.group({
      name: ['',[Validators.required, Validators.maxLength(30), Validators.pattern("^[a-zA-Z0-9_ ]*$")]],
      iso_codes:['',[Validators.required, Validators.maxLength(10), Validators.pattern("^[a-zA-Z0-9_ ]*$")]],
      country_code:[''],
      currency_code: [''],
      currency_symbol: [''],
      is_deleted: false,
      id: [''],

   });
   if(!Array.isArray(this.rowData)){
    this.viewRecord(this.rowData);
   }
  }

  ngOnDestroy(): void
  {
    if (this.cancelFlag)
    {
      this.currencyForm.reset();
      this.onCancel.emit(false);
    }
  }
  viewRecord(row) {
    this.currencyForm.patchValue({
      name: row.name,
      iso_codes: row.iso_codes,
      country_code: row.country_code,
      currency_code: row.currency_code,
      currency_symbol: row.currency_symbol,
      id: row.id
    });

    if(this.submitBtn === true) this.btnVal = 'Update';
  }

  onCancelForm(){
    this.cancelFlag=false
    this.currencyForm.reset();
    this.onCancel.emit(false);
  }

  onSubmit() {
    if (this.currencyForm.invalid) {
      return;
    }
    else
    {
      this.onSave.emit(this.currencyForm.value);
    }
  }
}

