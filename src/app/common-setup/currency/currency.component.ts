import { Component, OnInit, ViewChild } from '@angular/core';
import { CurrencyService} from 'src/app/shared/services/currency.service';
import Swal from 'sweetalert2';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import * as XLSX from 'xlsx';
import { ManageSecurityService } from 'src/app/core/service/manage-security.service';
import { Currency } from 'src/app/shared/models/currency';
import { ErrorCodes } from 'src/app/shared/error-codes';
@Component({
  selector: 'app-currency',
  templateUrl: './currency.component.html',
  styleUrls: ['./currency.component.sass'],
  providers: [ErrorCodes]
})

export class CurrencyComponent implements OnInit {
  @ViewChild('dataSourcePaginator',{read: MatPaginator}) dataSourcePaginator: MatPaginator;
  @ViewChild('CurrencyMasterPaginator',{read: MatPaginator}) CurrencyMasterPaginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  dataSource :MatTableDataSource<Currency>;
  resultData: Currency[];
  displayColumns = [
    'actions',
    'name',
    'country_code',
    'currency_symbol'
  ];
  renderedData: Currency[];
  sidebarData: any;
  screenName = 'Country Currency';
  submitBtn = true;
  showLoader = false;
  rowData = [];
  listDiv:boolean = false;
  showList:boolean = true;

  constructor(
    private manageSecurityService :ManageSecurityService,
    private currencyService:CurrencyService,
    private errorCodes: ErrorCodes) { }

  ngOnInit(): void {
    this.getAllData();
    let userId = localStorage.getItem('USER_ID');
    this.manageSecurityService.getAccessLeftPanel(userId,this.screenName).subscribe({
      next:data =>{
        this.sidebarData =data;
      }
    })
  }

  editViewRecord(row,flag){
    this.rowData = row;
    this.showList = false;
    this.submitBtn = flag;
    this.listDiv = true;
    this.showLoader = false;
  }

  showFormList(item: boolean) {
    if(item ===false){
      this.listDiv = true;
      this.showList = false;
    }
    else{
      this.listDiv = false;
      this.showList = true;
    }
  }

  onCancel(item: boolean){
    this.listDiv = item;
    this.showList = true;
    this.submitBtn = true;
    this.rowData = [];
  }

  showSwalmessage(message, text, icon, confirmButton): void {
    if (! confirmButton) {
        Swal.fire({ title: message, text: text, icon: icon, timer: 2000, showConfirmButton: false });
    } else {
        Swal.fire({ title: message, text: text, icon: icon, timer: 2000, showConfirmButton: true, confirmButtonText: "Yes", cancelButtonText: "No" });
    }
  }

  onSave(formValue){
    this.showLoader = true
    this.currencyService.create(formValue).subscribe({
      next: (data:any) => {
        if (data['status'] == 1) {
          this.showSwalmessage('Your record has been updated successfully!','','success',false)
        }
        else if(data['status'] == 2) {
          this.showSwalmessage('Your record has been added successfully!','','success',false)
        }
        this.getAllData();
        this.showList = true;
        this.listDiv = false;
        this.showLoader = false 
      },
      error:(error) => {
        this.showLoader = false;
        this.showSwalmessage(this.errorCodes.getErrorMessage(error.status), error.error, 'error', true)
      }
    })
  }

  deleteRecord(rowId) {
    Swal.fire({ title: "Are you sure you want to delete?", text: "", icon: "warning", showCancelButton: true, confirmButtonText: "Yes", cancelButtonText: "No" }).then((result) => {
        if (result.value) {
          this.currencyService.delete(rowId).subscribe({
            next: (data:any) => {
              this.showSwalmessage("Your record has been deleted successfully!", "", "success", false);
              this.getAllData();
              this.rowData = [];
            },
            error:(error) => {
              this.showSwalmessage(this.errorCodes.getErrorMessage(error.status), error.error, 'error', true)
            }
          })
        }
    });
  }

  getAllData(){
    this.currencyService.getAll().subscribe({
      next:(data: Currency[])=>{
        this.dataSource = new MatTableDataSource(data);
        this.dataSource.paginator = this.dataSourcePaginator;
        this.dataSource.sort = this.sort;
        this.dataSource.connect().subscribe(d => this.renderedData = d);
      }
    });
  }

  tbl_FilterDatatable(value:string) {
    this.dataSource.filter = value.trim().toLocaleLowerCase();
  }

  export_to_excel() {
    let XlsMasterData = this.dataSource.data.map(({name,iso_codes,country_code,currency_code,currency_symbol}) => ({name,iso_codes,country_code,currency_code,currency_symbol}));
    const workSheet = XLSX.utils.json_to_sheet(XlsMasterData);
    const workBook: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(workBook, workSheet, 'SheetName');
    XLSX.writeFile(workBook, 'currency_data.xlsx');
  }

}


