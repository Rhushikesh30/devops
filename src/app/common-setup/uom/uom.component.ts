import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CommonSetupService } from 'src/app/core/service/common-setup.service';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { Router } from '@angular/router';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import * as XLSX from 'xlsx';
import { ManageSecurityService } from 'src/app/core/service/manage-security.service';
import * as $ from "jquery";

@Component({
  selector: 'app-uom',
  templateUrl: './uom.component.html',
  styleUrls: ['./uom.component.sass']
})
export class UomComponent implements OnInit {
  @ViewChild(DatatableComponent, { static: false }) table: DatatableComponent;
  @ViewChild('UOMMasterPaginator',{read: MatPaginator}) UOMMasterPaginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  UOMMMasterData :MatTableDataSource<UOMMasterElement>;
  displayColumns = [
    'actions',
    'uom_code',
    'uom_description',
  ];
  renderedData: UOMMasterElement[];

  uomForm: FormGroup;
  submitted = false;
  BTN_VAL='Submit';
  APPLICATION_ID: string;
  USERID: string;
  SUB_APPLICATION_ID: string;
  CREATED_BY: string;
  tbl_FilteredData = [];
  sidebardata: any;

  constructor(private formBuilder: FormBuilder,
              private commonSetupService: CommonSetupService,
              private router: Router, 
              private manageSecurity:ManageSecurityService) { }

  ngOnInit(): void {
    this.showList();
    this.CREATED_BY = localStorage.getItem('USER_ID');
    this.uomForm = this.formBuilder.group({
      ID: [''],
      uom_code: ['', [Validators.required,Validators.maxLength(20),Validators.pattern("^[a-zA-Z0-9_ ]*$")]],
      uom_description: ['', [Validators.required,Validators.maxLength(30),Validators.pattern("^[a-zA-Z0-9_ ]*$")]],
      sub_application_id: [localStorage.getItem('SUB_APPLICATION_ID')],      
      application_id:  [localStorage.getItem('APPLICATION_ID')],
      USERID: [localStorage.getItem('ID')],
      created_by: [localStorage.getItem('USER_ID')],
    });

    $( document ).ready(function() {
      $('#new_entry_form').hide();
      $('#new_entry_title').hide();
      $('#btn_list').hide();
    });

    this.commonSetupService.getUomData().subscribe({
      next:(data:[])=>{
        this.UOMMMasterData = new MatTableDataSource(data);
        this.UOMMMasterData.paginator = this.UOMMasterPaginator;
        this.UOMMMasterData.sort = this.sort;
        this.UOMMMasterData.connect().subscribe(d => this.renderedData = d);
        this.tbl_FilteredData =data;
      }
    });  

    this.manageSecurity.getAccessLeftPanel(this.CREATED_BY,'UOM').subscribe({
      next:data =>{
        this.sidebardata=data;
      }
    })
  }

  tbl_FilterDatatable(value:string) {
    this.UOMMMasterData.filter = value.trim().toLocaleLowerCase();
  }

  viewAllMasterData(uom){
    this.uomForm.patchValue({
      uom_code: uom.uom_code,
      uom_description: uom.uom_description,
      ID: uom.id
    });  

    let id=$("#ID").val();
		if(id!='')
		{
			$("#new_entry_form").show();
			$("#new_entry_title").show();
			$("#btn_list").hide();
			$("#btn_new_entry").hide();
			$("#list_form").hide();
			$("#list_title").hide();
      $("#submit_btn").hide();
		}
  }

  editUomData(uom){
    this.uomForm.patchValue({
      uom_code: uom.uom_code,
      uom_description: uom.uom_description,
      ID: uom.id
    });
    this.BTN_VAL='Update';
    
    let id=$("#ID").val();
		if(id!='')
		{
			$("#new_entry_form").show();
			$("#new_entry_title").show();
			$("#btn_list").hide();
			$("#btn_new_entry").hide();
			$("#list_form").hide();
			$("#list_title").hide();
		}
  }

  showNewEntry()
  {
    $("#list_form").hide();
    $("#list_title").hide();
    $("#btn_new_entry").hide();
    $("#btn_list").show();
    $("#new_entry_form").show();
    $("#new_entry_title").show();
  }

	showList()
  {
    $("#list_form").show();
    $("#list_title").show();
    $("#btn_new_entry").show();
    $("#btn_list").hide();
    $("#new_entry_form").hide();
    $("#new_entry_title").hide();
  }
  get f() { return this.uomForm.controls; }

  onSubmit() {
    this.submitted = true;
    if (this.uomForm.invalid) {
        return false
    }
  }

  export_isp_usrs() {
    let XlsMasterData = this.UOMMMasterData.data.map(({uom_code,uom_description}) => 
      ({uom_code,uom_description}));
    const workSheet = XLSX.utils.json_to_sheet(XlsMasterData);
    const workBook: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(workBook, workSheet, 'SheetName');
    XLSX.writeFile(workBook, 'leftpanel_data.xlsx');
  }
}

export interface UOMMasterElement {
  id:any;
  uom_code:any;
  uom_description:any;
}
