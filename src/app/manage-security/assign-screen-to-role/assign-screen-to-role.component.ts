import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder,FormControl } from '@angular/forms';
import { ThemePalette } from '@angular/material/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { ManageSecurityService } from 'src/app/core/service/manage-security.service';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import * as XLSX from 'xlsx';
import { DatePipe } from '@angular/common';
import { ErrorCodes } from 'src/app/shared/error-codes';

declare const $: any;
@Component({
  selector: 'app-assign-screen-to-role',
  templateUrl: './assign-screen-to-role.component.html',
  styleUrls: ['./assign-screen-to-role.component.sass'],
  providers: [DatePipe, ErrorCodes]
})

export class AssignScreenToRoleComponent implements OnInit {
  dataSource;
  @ViewChild(DatatableComponent, { static: false }) table: DatatableComponent;
  @ViewChild('AssignScreenMasterPaginator',{read: MatPaginator}) AssignScreenMasterPaginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  AssignScreenMasterData :MatTableDataSource<AssignScreenMasterElement>;
  displayColumns = [
    'actions',
    'company_name',
    'role_name'
  ];
  renderedData: AssignScreenMasterElement[];
  COMPANY_ID: string;
  USERID: string;
  ScreenToRole_Form: FormGroup;
  submitted = false;
  BTN_VAL = 'Submit';
  CREATED_BY: any;
  ScreenToRole_Data: any;
  screenToRole: any;
  companydata = [];
  roledata: any = [];
  designationdata: any = [];
  leftpaneldata = [];
  assign_screen_to_role: any = [];
  taken: any;
  color: ThemePalette = 'primary';
  newscreen_to_role: any;
  newScreenToRole_Data: any;
  newrolepayload = [];
  DATE=this.datepipe.transform(new Date(), 'YYYY-MM-ddThh:mm');
  sidebardata: any;
  FilterMasterData: any;
  filterdCompanyName:string;
  public getCompanytoSearch: FormControl = new FormControl();

  columns = [
    { name: 'form_name' },
    { name: 'read_access' },
    { name: 'write_access' },
    { name: 'delete_access' }
  ];

  displayColumn: string[] = [
    'check',
    'form_name',
    'read_access',
    'write_access',
    'delete_access'
  ];

  displayedColumns: string[] = [
    'action',
    'assigned_to_role'
  ]

  tbl_FilteredData = [];
  role: any;
  company: any;
  abc: string;
  role_by_id: [];
  company_by_id: [];
  shouldDisable: boolean = false;

  constructor(private formBuilder: FormBuilder, 
              private manageSecurityService: ManageSecurityService, 
              private router: Router, 
              public datepipe: DatePipe,
              private errorCodes: ErrorCodes) { }

  @Input() screen_to_role_table: any;

  ngOnInit() {
    this.showList();
    this.USERID = localStorage.getItem('ID');
    this.COMPANY_ID = localStorage.getItem('COMPANY_ID');
    this.CREATED_BY = localStorage.getItem('USER_ID');
    this.companydata = [{'company_name':localStorage.getItem('COMPANY_NAME'), id :this.COMPANY_ID}]

    $(document).ready(function () {
      $('#new_entry_form').hide();
      $('#new_entry_title').hide();
      $('#btn_list').hide();
    });

    this.ScreenToRole_Form = this.formBuilder.group({
      id: [''],
      assigned_to_role: [''],
      company_ref_id: [this.COMPANY_ID],
      assign_screen_to_role: [this.assign_screen_to_role]
    });

    this.manageSecurityService.getAccessLeftPanel(this.CREATED_BY,'Screens to Role Link').subscribe({
      next:data =>{
        this.sidebardata = data;
      }
    })

    this.manageSecurityService.get_screen_to_role().subscribe({
      next:(data: []) => {
        this.FilterMasterData = data;
        this.AssignScreenMasterData = new MatTableDataSource(data);
        this.AssignScreenMasterData.paginator = this.AssignScreenMasterPaginator;
        this.AssignScreenMasterData.sort = this.sort;
        this.AssignScreenMasterData.connect().subscribe(d => this.renderedData = d);
        this.tbl_FilteredData =data;
        this.ScreenToRole_Data = data;
      }
    });

    this.manageSecurityService.getRoleRef_By_id().subscribe({
      next:data => {
        this.designationdata = data;      
        let MstData = this.FilterMasterData.filter(data=>(data.company_ref_id_id==Number(this.COMPANY_ID)));
        let MstApiData= this.designationdata.filter(data=>(data.company_ref_id!=MstData));
        this.roledata = MstApiData.filter(({ id: id1 }) => !MstData.some(({ assigned_to_role: id2 }) => id2 === id1));
      }
    });

    this.ScreenToRole_Form.get("company_ref_id").valueChanges.subscribe({
      next:val => {
        this.roledata=[]
        if (val != ''){
          let MstData = this.FilterMasterData.filter(data=>(data.company_ref_id_id==val));
          let MstApiData= this.designationdata.filter(data=>(data.company_ref_id!=MstData));
          this.roledata = MstApiData.filter(({ id: id1 }) => !MstData.some(({ assigned_to_role: id2 }) => id2 === id1));
        }
      }
    });

    this.manageSecurityService.getLeftPanelData().subscribe({
      next:(data: any) => {
        this.leftpaneldata = data;      
      }
    });

    this.getCompanytoSearch.valueChanges.subscribe({
      next:val=>{
        this.filterdCompanyName=val;
      }
    })
  }

  showNewEntry() {
    $("#list_form").hide();
    $("#list_title").hide();
    $("#btn_new_entry").hide();
    $("#btn_list").show();
    $("#new_entry_form").show();
    $("#new_entry_title").show();
  }

  showList() {
    $("#list_form").show();
    $("#list_title").show();
    $("#btn_new_entry").show();
    $("#btn_list").hide();
    $("#new_entry_form").hide();
    $("#new_entry_title").hide();
  }

  get f() { return this.ScreenToRole_Form.controls; }

  tbl_FilterDatatable(value:string) {
    this.AssignScreenMasterData.filter = value.trim().toLocaleLowerCase();
  }

  onCheckboxChangeisParentY(fid, leftpanel, values, assigned_to_role, company_ref_id){
    for(let indexValues of fid){
      if(leftpanel.parent_code==indexValues.parent_code && leftpanel.is_parent=="Y"){
        values.push(indexValues.id);            
        this.assign_screen_to_role.push({
          id: '',
          assigned_to_role: assigned_to_role,
          form_ref_id: indexValues.id,
          company_ref_id:company_ref_id,
          parent_code: indexValues.parent_code,
          child_code: indexValues.child_code,
          sub_child_code: indexValues.sub_child_code,
          read_access: 'N',
          write_access: 'N',
          delete_access: 'N',
        });
        $('#list [value="'+values.join('"],[value="')+'"]').prop('checked',true);
      }
      else{
        $('#list [value="'+leftpanel.id+'"]').prop('checked',true);
      }
    }
  }

  onCheckboxChangeisParentN(assigned_to_role, leftpanel, company_ref_id){
    this.assign_screen_to_role.push({
      id: '',
      assigned_to_role: assigned_to_role,
      form_ref_id: leftpanel.id,
      company_ref_id:company_ref_id,
      parent_code: leftpanel.parent_code,
      child_code: leftpanel.child_code,
      sub_child_code: leftpanel.sub_child_code,
      read_access: 'N',
      write_access: 'N',
      delete_access: 'N'
    });
  }

  onCheckboxChangeCheckedisParentY(fid, values){
    for(let indexValues of fid){
      values.push(indexValues.id);
      $('#list [value="'+values.join('"],[value="')+'"]').prop('checked',false);
      for(let indexValues2 of this.assign_screen_to_role){
        if(indexValues2.form_ref_id==indexValues.id){
          let index=this.assign_screen_to_role.indexOf(indexValues2)
          if (index > -1) {
            this.assign_screen_to_role.splice(index, 1);
          }
        }
      }
    }
  }

  onCheckboxChangeCheckedisParentN(leftpanel){
    for(let [index, indexValues] of this.assign_screen_to_role.entries()){
      if(indexValues.form_ref_id==leftpanel.id){
        this.assign_screen_to_role.splice(index, 1);
      }
    }
  }

  onCheckboxChange(leftpanel, event) {    
    let values=[];
    let fid=this.leftpaneldata.filter(x=>x.parent_code==leftpanel.parent_code);
    const assigned_to_role=this.ScreenToRole_Form.value.assigned_to_role;
    const company_ref_id=this.ScreenToRole_Form.value.company_ref_id;
    if(event.target.checked===true && assigned_to_role==''){
      Swal.fire("Please select role first");
      event.target.checked=false;
    }
    if(event.target.checked===true && assigned_to_role!=''){
      if(leftpanel.is_parent=="Y"){
        this.onCheckboxChangeisParentY(fid, leftpanel, values, assigned_to_role, company_ref_id)
      }
      else{
        this.onCheckboxChangeisParentN(assigned_to_role, leftpanel, company_ref_id)
      }
    }

    if(event.target.checked===false && leftpanel.is_parent=="Y"){ 
      this.onCheckboxChangeCheckedisParentY(fid, values)
    }

    if(event.target.checked===false && leftpanel.is_parent=="N"){
      this.onCheckboxChangeCheckedisParentN(leftpanel)
    }
  }

  readCheckedisParentY(fid, values){
    for (let indexValues of fid) {
      values.push(indexValues.id);
      for (let indexValues2 of this.assign_screen_to_role) {
        if (indexValues2.form_ref_id == indexValues.id) {
          indexValues2.read_access = "Y";
        }
      }
      $(".switchRead input[name=read" + values.join("],[name=read") + "]").prop('checked', true);
    }    
  }

  readCheckedisParentN(leftpanel){
    for(let indexValues of this.assign_screen_to_role){
      if(indexValues.form_ref_id==leftpanel.id){
        indexValues.read_access = "Y";
      }
    }
    $(".switchRead input[name=read"+leftpanel.id+"]").prop('checked',true);
  }

  readCheckedFalseisParentY(fid, values){
    for(let indexValues of fid){
      values.push(indexValues.id);
      for(let indexValues2 of this.assign_screen_to_role){
        if(indexValues2.form_ref_id==indexValues.id){
          indexValues2.read_access = "N";
        }
      }
      $(".switchRead input[name=read"+values.join("],[name=read")+"]").prop('checked',false);
    }
  }

  read(leftpanel, event) {
    let values = [];
    let fid = this.leftpaneldata.filter(x => x.parent_code == leftpanel.parent_code);

    if (event.target.checked === true && leftpanel.is_parent=='Y'){
      this.readCheckedisParentY(fid, values)
    }

    if(event.target.checked===true && leftpanel.is_parent=="N"){
      this.readCheckedisParentN(leftpanel)
    }

    if(event.target.checked===false && leftpanel.is_parent=="Y"){
      this.readCheckedFalseisParentY(fid, values)
    }

    if(event.target.checked===false && leftpanel.is_parent=="N"){
      for(let indexValues of this.assign_screen_to_role){
        if(indexValues.form_ref_id==leftpanel.id){
          indexValues.read_access = "N";
        }
      }
      $(".switchRead input[name=read"+leftpanel.id+"]").prop('checked',false);
    }
  }

  writeCheckedisParentY(fid, values){
    for(let indexValues of fid){
      values.push(indexValues.id);
      for(let indexValues2 of this.assign_screen_to_role){
        if(indexValues2.form_ref_id==indexValues.id){
          indexValues2.read_access = "Y";
          indexValues2.write_access = "Y";
        }
      }
      $(".switchRead input[name=read"+values.join("],[name=read")+"]").prop('checked',true);
      $(".switchWrite input[name=write"+values.join("],[name=write")+"]").prop('checked',true);
    }
  }

  writeCheckedisParentN(leftpanel){
    for(let valueChanges of this.assign_screen_to_role){
      if(valueChanges.form_ref_id==leftpanel.id){
        valueChanges.read_access = "Y";
        valueChanges.write_access = "Y";
      }
    }
    $(".switchRead input[name=read"+leftpanel.id+"]").prop('checked',true);
    $(".switchWrite input[name=write"+leftpanel.id+"]").prop('checked',true);
  }

  writeCheckedFalseisParentY(fid, values){
    for(let indexValues of fid){
      values.push(indexValues.id);
      for(let indexValues2 of this.assign_screen_to_role){
        if(indexValues2.form_ref_id==indexValues.id){
          indexValues2.read_access = "N";
          indexValues2.write_access = "N";
        }
      }
      $(".switchRead input[name=read"+values.join("],[name=read")+"]").prop('checked',false);
      $(".switchWrite input[name=write"+values.join("],[name=write")+"]").prop('checked',false);
    }
  }

  write(leftpanel, event) {
    let values=[];
    let fid=this.leftpaneldata.filter(x=>x.parent_code==leftpanel.parent_code);

    if(event.target.checked===true && leftpanel.is_parent=="Y"){
      this.writeCheckedisParentY(fid, values)
    }

    if(event.target.checked===true && leftpanel.is_parent=="N"){
      this.writeCheckedisParentN(leftpanel)
    }

    if(event.target.checked===false && leftpanel.is_parent=="Y"){
      this.writeCheckedFalseisParentY(fid, values)
    }
    if(event.target.checked===false && leftpanel.is_parent=="N"){
      for(let indexValues of this.assign_screen_to_role){
        if(indexValues.form_ref_id==leftpanel.id){
          indexValues.read_access = "N";
          indexValues.write_access = "N";
        }
      }
      $(".switchRead input[name=read"+leftpanel.id+"]").prop('checked',false);
      $(".switchWrite input[name=write"+leftpanel.id+"]").prop('checked',false);
    }
  }

  deleteCheckedisParentY(fid, values){
    for(let indexValues of fid){
      values.push(indexValues.id);
      for(let indexValues2 of this.assign_screen_to_role){
        if(indexValues2.form_ref_id==indexValues.id){
          indexValues2.read_access = "Y";
          indexValues2.write_access = "Y";
          indexValues2.delete_access = "Y";
        }
      }
      $(".switchRead input[name=read"+values.join("],[name=read")+"]").prop('checked',true);
      $(".switchWrite input[name=write"+values.join("],[name=write")+"]").prop('checked',true);
      $(".switchDelete input[name=delete"+values.join("],[name=delete")+"]").prop('checked',true);
    }
  }

  deleteCheckedisParentN(leftpanel){
    for(let indexValues of this.assign_screen_to_role){
      if(indexValues.form_ref_id==leftpanel.id){
        indexValues.read_access = "Y";
        indexValues.write_access = "Y";
        indexValues.delete_access = "Y";
      }
    }
    $(".switchRead input[name=read"+leftpanel.id+"]").prop('checked',true);
    $(".switchWrite input[name=write"+leftpanel.id+"]").prop('checked',true);
    $(".switchDelete input[name=delete"+leftpanel.id+"]").prop('checked',true);
  }

  deleteCheckedFalseisParentY(fid, values){
    for(let indexValues of fid){
      values.push(indexValues.id);
      for(let indexValues2 of this.assign_screen_to_role){
        if(indexValues2.form_ref_id==indexValues.id){
          indexValues2.read_access = "N";
          indexValues2.write_access = "N";
          indexValues2.delete_access = "N";
        }
      }
      $(".switchRead input[name=read"+values.join("],[name=read")+"]").prop('checked',false);
      $(".switchWrite input[name=write"+values.join("],[name=write")+"]").prop('checked',false);
      $(".switchDelete input[name=delete"+values.join("],[name=delete")+"]").prop('checked',false);
    }
  }

  delete(leftpanel, event) {
  let values=[];
  let fid=this.leftpaneldata.filter(x=>x.parent_code==leftpanel.parent_code);

  if(event.target.checked===true && leftpanel.is_parent=="Y"){
    this.deleteCheckedisParentY(fid, values)
  }

  if(event.target.checked===true && leftpanel.is_parent=="N"){
    this.deleteCheckedisParentN(leftpanel)
  }

  if(event.target.checked===false && leftpanel.is_parent=="Y"){
    this.deleteCheckedFalseisParentY(fid, values)
  }

  if(event.target.checked===false && leftpanel.is_parent=="N"){
    for(let indexValues of this.assign_screen_to_role){
      if(indexValues.form_ref_id==leftpanel.id){
        indexValues.read_access = "N";
        indexValues.write_access = "N";
        indexValues.delete_access = "N";
      }
    }
    $(".switchRead input[name=read"+leftpanel.id+"]").prop('checked',false);
    $(".switchWrite input[name=write"+leftpanel.id+"]").prop('checked',false);
    $(".switchDelete input[name=delete"+leftpanel.id+"]").prop('checked',false);
  }
}

  onSubmit() {
    this.submitted = true;    
    if (this.ScreenToRole_Form.invalid || this.ScreenToRole_Form.value.assign_screen_to_role.length == 0) {
      Swal.fire("Please select the checkbox!");
    }
    else {
      this.manageSecurityService.saveScreenToRoleData(this.ScreenToRole_Form.value.assign_screen_to_role).subscribe({
        next: (data:any) => {
          if (data['status'] == 1) {
            this.showList();
            Swal.fire({
              title: 'Your record has been updated successfully!',
              icon: 'success',
              timer: 2000,
              showConfirmButton: false
            });
          }
          if (data['status'] == 2) {
            this.showList();
            Swal.fire({
              title: 'Your record has been added successfully!',
              icon: 'success',
              timer: 2000,
              showConfirmButton: false
            });
          }
          this.router.navigate(['/manage-security/assign-screen-role']).then(() => {
            setTimeout(function() {window.location.reload();} , 2000);
          });
        },
        error:(error) => {
          Swal.fire({
            title: this.errorCodes.getErrorMessage(error.status),
            icon: 'error',
            text:error.error,
            timer: 2000,
            showConfirmButton: true
          });
        }
      })
    }
  }

  edit_ScreenToRole(assignscreen) {
    this.FilterMasterData=[]    
    this.shouldDisable = true;
    this.ScreenToRole_Form.patchValue({
      id: assignscreen.id,
      company_ref_id:assignscreen.company_ref_id_id,
      assigned_to_role: assignscreen.assigned_to_role,
      updated_by:this.CREATED_BY,
    });
    this.manageSecurityService.getAllData(assignscreen.assigned_to_role).subscribe({
      next:(data: []) => {
        this.screenToRole = data;      
        let values = [], read = [], write = [], deletes = [];
        for (let indexValues of this.screenToRole){
          values.push(indexValues.form_ref_id);
          this.assign_screen_to_role.push({
            id: indexValues.id,
            assigned_to_role: assignscreen.assigned_to_role,
            form_ref_id: indexValues.form_ref_id,
            company_ref_id: Number(assignscreen.company_ref_id),
            read_access: indexValues.read_access,
            write_access: indexValues.write_access,
            delete_access: indexValues.delete_access,
            parent_code: indexValues.parent_code,
            child_code: indexValues.child_code,
            sub_child_code: indexValues.sub_child_code
          });
          this.ScreenToRole_Form.patchValue({
            assign_screen_to_role: this.assign_screen_to_role
          });
          if (indexValues.read_access == "Y") {
            read.push(indexValues.form_ref_id);
            $(".switchRead input[name=read" + read.join("],[name=read") + "]").prop('checked', true);
          }
          if (indexValues.write_access == "Y") {
            write.push(indexValues.form_ref_id);
            $(".switchWrite input[name=write" + write.join("],[name=write") + "]").prop('checked', true);
          }
          if (indexValues.delete_access == "Y") {
            deletes.push(indexValues.form_ref_id);
            $(".switchDelete input[name=delete" + deletes.join("],[name=delete") + "]").prop('checked', true);
          }
        }
        $('#list [value="' + values.join('"],[value="') + '"]').prop('checked', true);
      }      
    });

    this.BTN_VAL = 'Update';
    let id = $("#id").val();
    if (id != '') {
      $("#new_entry_form").show();
      $("#new_entry_title").show();
      $("#btn_list").hide(); 
      $("#btn_new_entry").hide();
      $("#list_form").hide();
      $("#list_title").hide();
    }
  }

  viewAllMasterData(assignscreen){
    this.FilterMasterData=[]
    this.shouldDisable = true;
    this.ScreenToRole_Form.patchValue({
      id: assignscreen.id,
      company_ref_id:assignscreen.company_ref_id_id,
      assigned_to_role: assignscreen.assigned_to_role,
      updated_by:this.CREATED_BY,
    });
    this.manageSecurityService.getAllData(assignscreen.assigned_to_role).subscribe({
      next:(data: []) => {
        this.screenToRole = data;      
        let values = [], read = [], write = [], deletes = [];
        for (let indexValues of this.screenToRole) {
          values.push(indexValues.form_ref_id);
          this.assign_screen_to_role.push({
            id: indexValues.id,
            assigned_to_role: assignscreen.assigned_to_role,
            form_ref_id: indexValues.form_ref_id,
            company_ref_id: Number(assignscreen.company_ref_id),
            read_access: indexValues.read_access,
            write_access: indexValues.write_access,
            delete_access: indexValues.delete_access,
            parent_code: indexValues.parent_code,
            child_code: indexValues.child_code,
            sub_child_code: indexValues.sub_child_code
          });
          this.ScreenToRole_Form.patchValue({
            assign_screen_to_role: this.assign_screen_to_role
          });
          if (indexValues.read_access == "Y") {
            read.push(indexValues.form_ref_id);
            $(".switchRead input[name=read" + read.join("],[name=read") + "]").prop('checked', true);
          }
          if (indexValues.write_access == "Y") {
            write.push(indexValues.form_ref_id);
            $(".switchWrite input[name=write" + write.join("],[name=write") + "]").prop('checked', true);
          }
          if (indexValues.delete_access == "Y") {
            deletes.push(indexValues.form_ref_id);
            $(".switchDelete input[name=delete" + deletes.join("],[name=delete") + "]").prop('checked', true);
          }
        }
        $('#list [value="' + values.join('"],[value="') + '"]').prop('checked', true);
      }      
    });
    let id = $("#id").val();
    if (id != '') {
      $("#new_entry_form").show();
      $("#new_entry_title").show();
      $("#btn_list").hide(); 
      $("#btn_new_entry").hide();
      $("#list_form").hide();
      $("#list_title").hide();
      $("#submit").hide();
    }    
  }

  delete_ScreenToRole(ID) {    
    Swal.fire({
      title: 'Are you sure you want to delete?',
      text: "",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes',
      cancelButtonText: 'No'
    }).then((result) => {
      if (result.value) {        
        this.manageSecurityService.deleteScreenToRoleData(ID).subscribe({
          next: (data:any) => {
            Swal.fire({
              title: 'Your record has been deleted successfully!',
              icon: 'success',
              timer: 2000,
              showConfirmButton: false
            });
            this.router.navigate(['/manage-security/assign-screen-role']).then(() => {
              setTimeout(function() {window.location.reload();} , 2000);
            }); 
          }
        })
      }
    });
  }
  export_isp_usrs() {
    let XlsMasterData = this.AssignScreenMasterData.data.map(({company_name,role_name}) => 
      ({company_name,role_name}));
    const workSheet = XLSX.utils.json_to_sheet(XlsMasterData);
    const workBook: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(workBook, workSheet, 'SheetName');
    XLSX.writeFile(workBook, 'assignscreen_data.xlsx');
  }
}

export interface AssignScreenMasterElement {
  id:any;
  company_name:string;
  role_name:string;
}