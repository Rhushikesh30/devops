import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AssignScreenToRoleComponent } from './assign-screen-to-role.component';

describe('AssignScreenToRoleComponent', () => {
  let component: AssignScreenToRoleComponent;
  let fixture: ComponentFixture<AssignScreenToRoleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AssignScreenToRoleComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AssignScreenToRoleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
