import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AssignScreenToRoleComponent } from './assign-screen-to-role/assign-screen-to-role.component';

const routes: Routes = [
  {
    path:'assign-screen-role',
    component:AssignScreenToRoleComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ManageSecurityRoutingModule { }
