import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'searchBiller'
})
export class SearchBillerPipe implements PipeTransform {

  transform(items: any,term: any): any {
    if(term ==undefined)
      return items;
    return items.filter(function(Product){
        return Product['company_name'].toLowerCase().includes(term.toLowerCase());
    })
  }

}
