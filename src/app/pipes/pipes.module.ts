import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SearchBillerPipe } from './Search-Filter/search-biller.pipe';



@NgModule({
  declarations: [
    SearchBillerPipe
  ],
  imports: [
    CommonModule
  ],
  exports:[
    SearchBillerPipe
  ]
})
export class PipesModule { }
