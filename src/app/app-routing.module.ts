import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { Page404Component } from './authentication/page404/page404.component';
import { AuthGuard } from './core/guard/auth.guard';
import { AuthLayoutComponent } from './layout/app-layout/auth-layout/auth-layout.component';
import { MainLayoutComponent } from './layout/app-layout/main-layout/main-layout.component';
const routes: Routes = [
  {
    path: '',
    component: MainLayoutComponent,
    canActivate: [AuthGuard],
    children: [
      { path: '', redirectTo: '/authentication/signin', pathMatch: 'full' },
      {
        path: 'dashboard',
        loadChildren: () =>
          import('./dashboard/dashboard.module').then((m) => m.DashboardModule)
      },
      {
        path: 'common-setup',
        loadChildren: () =>
          import('./common-setup/common-setup.module').then((m) => m.CommonSetupModule),
        canActivate: [AuthGuard]
      },
      {
        path: 'tax_setup',
        loadChildren: () =>
          import('./finance-tax/finance.module').then((m) => m.FinanceModule),
        canActivate: [AuthGuard]
      },
      {
        path:'define-bbpou',
        loadChildren: () =>          
        import('./define-bbpou/define-bbpou.module').then((m) => m.DefineBBPOUModule),
        canActivate: [AuthGuard]
      },
      {
        path: 'aggregator-setup',
        loadChildren: () =>         
        import('./onboard-aggregator-tsp/onboard-aggregator.module').then((m) => m.OnboardAggregatorModule),
        canActivate: [AuthGuard]
      },
      {
        path:'onboard-biller',
        loadChildren: () => 
          import('./onboard-biller/Onboard-Biller.module').then((m) => m.DefineOnboardBiller),
        canActivate: [AuthGuard]
      },
      {
        path: 'reports',
        loadChildren: () =>
          import('./reports/reports.module').then((m) => m.BillerReportsModule),
        canActivate: [AuthGuard]
      },
      {
        path:'manage-security',
        loadChildren: () => 
          import('./manage-security/manage-security.module').then((m) => m.ManageSecurityModule),
        canActivate: [AuthGuard]
      },
      {
        path:'api_hub',
        loadChildren: () => 
          import('./api-hub/api-hub.module').then((m) => m.ApiHubModule ),
        canActivate: [AuthGuard]
      }    
    ]
  },
  {
    path: 'authentication',
    component: AuthLayoutComponent,
    loadChildren: () =>
      import('./authentication/authentication.module').then(
        (m) => m.AuthenticationModule
      )
  },
  { path: '**', component: Page404Component }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { relativeLinkResolution: 'legacy' })],
  exports: [RouterModule]
})
export class AppRoutingModule {}
