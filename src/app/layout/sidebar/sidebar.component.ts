import { DOCUMENT } from '@angular/common';
import { Router, NavigationEnd } from '@angular/router';
import {Component, Inject, ElementRef, OnInit, Renderer2, HostListener, OnDestroy} from '@angular/core';
import { ROUTES } from './sidebar-items';
import { AuthService } from 'src/app/core/service/auth.service';
import { ManageSecurityService } from 'src/app/core/service/manage-security.service';
@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.sass']
})
export class SidebarComponent implements OnInit, OnDestroy {
  public sidebarItems: any[];
  level1Menu = '';
  level2Menu = '';
  level3Menu = '';
  public innerHeight: any;
  public bodyTag: any;
  listMaxHeight: string;
  listMaxWidth: string;
  headerHeight = 60;
  routerObj = null;
  screenToRole:any;
  leftpanelparentdata:any;
  leftpanelchilddata:any;
  leftpanelsubchilddata:any;
  role_ref_id: any;

  constructor(
    @Inject(DOCUMENT) private document: Document,
    private renderer: Renderer2,
    public elementRef: ElementRef,
    private authService: AuthService,
    private router: Router,
    private roleSecurityService: ManageSecurityService) {

    this.routerObj = this.router.events.subscribe({
      next:(event) => {
        if (event instanceof NavigationEnd) {
          const currenturl = event.url.split('?')[0];
          this.level1Menu = currenturl.split('/')[1];
          this.level2Menu = currenturl.split('/')[2];
          this.renderer.removeClass(this.document.body, 'overlay-open');
        }
      }
    });
    if (this.authService.currentUserValue) {
      this.currentUserExistFunction()
    }
  }

  currentUserExistFunction(){
    this.roleSecurityService.getLeftPanelAccess(localStorage.getItem('USER_ID'),localStorage.getItem('ROLE_ID')).subscribe({
      next:(lpdata: []) => {
        this.leftpanelparentdata = lpdata;
        for(let panelValue of this.leftpanelparentdata){
          ROUTES.push({
            path: '',
            title: panelValue.form_name,
            moduleName: panelValue.module_path,
            parent_code:panelValue.parent_code,
            child_code:panelValue.child_code,
            icon: panelValue.icon_class,
            class: 'menu-toggle',
            groupTitle: false,
            sequenceId: panelValue.sequence_id,
            submenu: []
          })
          this.leftpanelchilddata = panelValue.submenu_level1;
          this.leftPanelChilDataFunction()
        }
        this.routingLengthSequenceFunction()
      }
    });      
  }

  leftPanelChilDataFunction(){
    for(let childValue of this.leftpanelchilddata){
      ROUTES.forEach(x=>{
        if(x.parent_code==childValue.parent_code && childValue.form_link !=""){
          x.submenu.push({
            path: childValue.module_path+'/'+childValue.form_link,
            title: childValue.form_name,
            moduleName: childValue.form_link,
            parent_code:childValue.parent_code,
            child_code:childValue.child_code,
            icon: childValue.icon_class,
            class: 'ml-menu',
            groupTitle: false,
            sequenceId: childValue.sequence_id,
            submenu: []
          });
        }
        else if(x.parent_code==childValue.parent_code){
          x.submenu.push({
            path: childValue.module_path,
            title: childValue.form_name,
            moduleName: childValue.form_link,
            parent_code:childValue.parent_code,
            child_code:childValue.child_code,
            icon: childValue.icon_class,
            class: 'ml-sub-menu',
            groupTitle: false,
            sequenceId: childValue.sequence_id,
            submenu: []
          });
        }
      });
      this.leftpanelsubchilddata = childValue.submenu_level2;
      
      for(let subChildValue of this.leftpanelsubchilddata){
        ROUTES.forEach(x=>{
          x.submenu.forEach(y=>{
            if(y.child_code==subChildValue.child_code){
              y.submenu.push({
                path: subChildValue.module_path+'/'+subChildValue.form_link,
                title: subChildValue.form_name,
                moduleName: subChildValue.form_link,
                parent_code:subChildValue.parent_code,
                child_code:subChildValue.child_code,
                icon: subChildValue.icon_class,
                class: 'ml-menu',
                groupTitle: false,
                sequenceId: subChildValue.sequence_id,
                submenu: []
              });
            }
          });
        });
      }
    }
  }

  routingLengthSequenceFunction(){
    for(let indexValue of ROUTES){
      indexValue.submenu.sort((a, b) => {
        if (a['sequenceId'] > b['sequenceId']) {
          return 1;
        } else if (a['sequenceId'] === b['sequenceId']) {
          return 0;
        } else {
          return -1;
        }
      });
    }
    ROUTES.sort((a, b) => {
      if (a['sequenceId'] > b['sequenceId']) {
        return 1;
      } else if (a['sequenceId'] === b['sequenceId']) {
        return 0;
      } else {
        return -1;
        }
      });          
    this.sidebarItems=ROUTES;     
  }


  @HostListener('window:resize', ['$event'])
  windowResizecall(event) {
    this.setMenuHeight();
    this.checkStatuForResize(false);
  }

  @HostListener('document:mousedown', ['$event'])
  onGlobalClick(event): void {
    if (!this.elementRef.nativeElement.contains(event.target)) {
      this.renderer.removeClass(this.document.body, 'overlay-open');
    }
  }
  callLevel1Toggle(event: any, element: any) {
    if (element === this.level1Menu) {
      this.level1Menu = '0';
    } else {
      this.level1Menu = element;
    }
    const hasClass = event.target.classList.contains('toggled');
    if (hasClass) {
      this.renderer.removeClass(event.target, 'toggled');
    } else {
      this.renderer.addClass(event.target, 'toggled');
    }
  }
  callLevel2Toggle(event: any, element: any) {
    if (element === this.level2Menu) {
      this.level2Menu = '0';
    } else {
      this.level2Menu = element;
    }
    const hasClass = event.target.classList.contains('toggled');
    if (hasClass) {
      this.renderer.removeClass(event.target, 'toggled');
    } else {
      this.renderer.addClass(event.target, 'toggled');
    }
  }
  callLevel3Toggle(event: any, element: any) {
    if (element === this.level3Menu) {
      this.level3Menu = '0';
    } else {
      this.level3Menu = element;
    }
  }

  ngOnInit() {
    this.initLeftSidebar();
    this.bodyTag = this.document.body;
  }

  ngOnDestroy() {
    this.routerObj.unsubscribe();
  }

  initLeftSidebar() {
    const _this = this;
    _this.setMenuHeight();
    _this.checkStatuForResize(true);
  }

  setMenuHeight() {
    this.innerHeight = window.innerHeight; 
    const height = this.innerHeight - this.headerHeight;
    this.listMaxHeight = height + '';
    this.listMaxWidth = '500px';
  }

  isOpen() {
    return this.bodyTag.classList.contains('overlay-open');
  }

  checkStatuForResize(firstTime) {
    if (window.innerWidth < 1170) {
      this.renderer.addClass(this.document.body, 'ls-closed');
    }
     else {
      this.renderer.removeClass(this.document.body, 'ls-closed');
    }
  }

  mouseHover(e) {
    const body = this.elementRef.nativeElement.closest('body');    
    if (body.classList.contains('submenu-closed')) {
      this.renderer.addClass(this.document.body, 'side-closed-hover');
      this.renderer.removeClass(this.document.body, 'submenu-closed');
    }
  }

  mouseOut(e) {
    const body = this.elementRef.nativeElement.closest('body');
    if (body.classList.contains('side-closed-hover')) {
      this.renderer.removeClass(this.document.body, 'side-closed-hover');
      this.renderer.addClass(this.document.body, 'submenu-closed');
    }
  }  
}
