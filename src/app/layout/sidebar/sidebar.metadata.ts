export interface RouteInfo {
  path: string;
  title: string;
  moduleName: string;
  icon: string;
  class: string;
  groupTitle: boolean;
  sequenceId: any;
  parent_code:string;
  child_code:string;
  submenu: RouteInfo[];
}
