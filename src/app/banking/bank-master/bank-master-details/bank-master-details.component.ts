import { Component, OnInit, Input, Output, EventEmitter, } from '@angular/core';
import { FormArray , FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BankMasterService } from 'src/app/shared/services/bank-master.service'

@Component({
  selector: 'app-bank-master-details',
  templateUrl: './bank-master-details.component.html',
  styleUrls: ['./bank-master-details.component.sass'],
})
export class BankMasterDetailsComponent implements OnInit {
  @Input() rowData: [];
  @Input() editFlag:string;
  @Input() submitBtn: boolean;
  @Input() showLoader: boolean;
  @Input() bankMasterData: any;
  @Output() onSave = new EventEmitter<any>();
  @Output() onCancel = new EventEmitter<any>();  

  bankmasterForm: FormGroup;
  btnVal = 'Submit';
  cancelFlag = true;
  tbl_FilteredData = [];
  countrydata: any = [];
  citydata: any = [];
  statedata = [];
  accounttypeData: any = [];
  submitted = false;
  company_id:string;
  company_type: any;
  isFlag = false;

  constructor(
    public formBulider: FormBuilder,
    private bankMasterService: BankMasterService) { }

  ngOnInit(): void {
    this.company_type = localStorage.getItem('COMPANY_TYPE');
    this.bankmasterForm = this.formBulider.group({
      id: [''],
      bank_name: ['', [Validators.required,Validators.maxLength(30)]],
      branch_name: ['', Validators.required,],
      bank_code: ['', [Validators.required,Validators.pattern("^[0-9]*$")]],
      account_number: ['', [Validators.required, Validators.minLength(9), Validators.maxLength(26),Validators.pattern("^[0-9]*$")]],
      account_type: ['', Validators.required],
      swift_number: [''],
      ifsc_code: ['', [Validators.required, Validators.maxLength(11), Validators.pattern('[A-Z]{4}[0-9]{7}')]],
      iban_number: [''],
      cash_credit_limit: ['', Validators.required],
      address1: ['', [Validators.required, Validators.maxLength(100)]],
      address2: ['', [Validators.required, Validators.maxLength(100)]],
      country_ref_id: ['', Validators.required],
      state_ref_id: ['', Validators.required],
      city_ref_id: ['', Validators.required],
      pincode: ['', [Validators.required, Validators.maxLength(6), Validators.pattern('[0-9]{6}')]],
      nodal_acc_flag :[false],
      company_ref_id: [parseInt(localStorage.getItem('COMPANY_ID'))],
      initialItemRow: this.formBulider.array([this.initialItemRow()])
    });
    
    if(!Array.isArray(this.rowData)){
      this.editViewRecord(this.rowData,this.editFlag);
    }

    this.bankMasterService.getAccountTypeData('Account Type').subscribe({
      next: (data:any) => {
        this.accounttypeData =data;  
      }
    })

    this.bankMasterService.getAllCountryData().subscribe({
      next: (data:any) => {
        this.countrydata =data;  
      }
    }) 

    this.bankmasterForm.get('country_ref_id').valueChanges.subscribe({
      next: (val:any) => {
        if(val!= null){
          this.bankMasterService.getAllStateData(val).subscribe({
            next: (data:any) => {
              this.statedata =data;  
            }
          }) 
        }else{
          this.statedata = []
        }
      }
    })
    
    this.bankmasterForm.get('state_ref_id').valueChanges.subscribe({
      next: (val:any) => {
        if(val != null){
          this.bankMasterService.getAllCityData(val).subscribe({
            next: (data:any) => {
              this.citydata =data;  
            }
          }) 
        }else{
          this.citydata = []
        }
      }
    })
    
    if(this.company_type == 'Aggregator'){
      this.isFlag = true;
    }
  }

  initialItemRow() {
    return this.formBulider.group({
      contact_person_name: ['', Validators.required],
      designation: [''],
      contact_person_mobile_number: ['', [Validators.required, Validators.pattern('[0-9]{10}')]],
      email: ['', Validators.required],
    });
  }

  get formArray() {
    return this.bankmasterForm.get('initialItemRow') as FormArray;
  }

  addNewRow() {
    this.formArray.push(this.initialItemRow());
  }

  deleteRow(index) {
    if (this.formArray.length == 1) {
      return false;
    } else {
      this.formArray.removeAt(index);
      return true;
    }
  }

  get f() { return this.bankmasterForm.controls; }

  onCancelForm(){
    this.cancelFlag=false
    this.bankmasterForm.reset();
    this.onCancel.emit(false);
  }

  onSubmit() {
    if (this.bankmasterForm.invalid) {
      return;
    }
     else {
      this.submitted = true;
      this.onSave.emit(this.bankmasterForm.value);
  }}

  setExistingArray(initialArray = []): FormArray {
    const formArray = new FormArray([]);
    initialArray.forEach(element => {
      formArray.push(this.formBulider.group({
        id: element.id,
        contact_person_name: element.contact_person_name,
        nodal_acc_flag:element.nodal_acc_flag,
        designation: element.designation,
        contact_person_mobile_number: element.contact_person_mobile_number,
        email: element.email,
      }));
    });
    return formArray;
  }


  editViewRecord(data,editFlag){
  this.bankMasterService.getBankMasterById(data.id).subscribe({
    next: (row:any) => {
      if(editFlag == 'view'){
        this.submitBtn = false;
      }

      if(editFlag == 'edit'){
         this.btnVal = 'Update';
      }      

    this.bankmasterForm.patchValue({
      id: row.id,
      bank_name: row.bank_name,
      branch_name: row.branch_name,
      bank_code: row.bank_code,
      account_number: row.account_number,
      account_type: row.account_type,
      swift_number: row.swift_number,
      ifsc_code: row.ifsc_code,
      iban_number: row.iban_number,
      cash_credit_limit: row.cash_credit_limit,
      address1: row.address1,
      address2: row.address2,
      country_ref_id: row.country_ref_id,
      state_ref_id: row.state_ref_id,
      city_ref_id: row.city_ref_id,
      pincode: row.pincode,
      nodal_acc_flag: row.nodal_acc_flag,
      company_ref_id:row.company_ref_id
    });   

    this.bankmasterForm.setControl('initialItemRow', this.setExistingArray(row.initialItemRow));
    this.showLoader = false; 
    }
  })
  }
}

