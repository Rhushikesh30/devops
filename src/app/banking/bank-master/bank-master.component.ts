import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { BankMasterService } from 'src/app/shared/services/bank-master.service'
import { ManageSecurityService } from 'src/app/core/service/manage-security.service';
import { BankMasterElement } from 'src/app/shared/models/bank-master';
import { ThemePalette } from "@angular/material/core";
import Swal from 'sweetalert2';
import * as XLSX from 'xlsx';
import { ErrorCodes } from 'src/app/shared/error-codes';
@Component({
  selector: 'app-bank-master',
  templateUrl: './bank-master.component.html',
  styleUrls: ['./bank-master.component.sass'],
  providers: [ErrorCodes]
})
export class BankMasterComponent implements OnInit {
  @ViewChild('dataSourcePaginator',{read: MatPaginator}) dataSourcePaginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  dataSource :MatTableDataSource<BankMasterElement>;
  resultData: BankMasterElement[];
  displayColumns = [
    'actions',
    'bank_name',
    'branch_name',
    'bank_code',
    'account_number',
  ];
  renderedData: BankMasterElement[];

  
  screenName = 'Bank Account';
  submitBtn = true;
  showLoader = false;
  rowData = [];
  listDiv:boolean = false;
  showList:boolean = true;
  sidebarData: any;
  bankmasterData = [];
  editFlag = '';
  color: ThemePalette = "primary";

  constructor(
    private bankMasterService: BankMasterService,
    private manageSecurityService: ManageSecurityService,
    private errorCodes: ErrorCodes) { }

  ngOnInit(): void {
   this.getAllData();
    let userId =localStorage.getItem('USER_ID')
    this.manageSecurityService.getAccessLeftPanel(userId,this.screenName).subscribe({
      next: (data:any) => {
        this.sidebarData =data;  
      },
      error:(error) => {
        this.showSwalmessage(this.errorCodes.getErrorMessage(error.status), error.error, 'error', true)
      }
    })
  }

  editViewRecord(row,flag){
    this.rowData = row;
    this.showList = false;
    this.submitBtn = flag;
    this.listDiv = true;
    this.showLoader = false;
    this.editFlag = flag
  }

  deleteRecord(rowId) {
    Swal.fire({ title: "Are you sure you want to delete?", text: "", icon: "warning", showCancelButton: true, confirmButtonText: "Yes", cancelButtonText: "No" }).then((result) => {
        if (result.value) {
          this.bankMasterService.delete(rowId).subscribe({
            next: (data:any) => {
              this.showSwalmessage("Your record has been deleted successfully!", "", "success", false);
              this.getAllData();
              this.rowData = []; 
            },
            error:(error) => {
              this.showSwalmessage(this.errorCodes.getErrorMessage(error.status), error.error, 'error', true)
            }
          })
        }
    });
  }


  showFormList(item: boolean) {
    if(item ===false){
      this.listDiv = true;
      this.showList = false;
    }
    else{
      this.listDiv = false;
      this.showList = true;
    }
  }

  onCancel(item: boolean){
    this.listDiv = item;
    this.showList = true;
    this.submitBtn = true;
    this.rowData = [];
  }
  
  getAllData(){
    this.bankMasterService.getAll().subscribe({
      next: (data:any) => {
        this.dataSource = new MatTableDataSource(data);
        this.dataSource.paginator = this.dataSourcePaginator;
        this.dataSource.sort = this.sort;
        this.dataSource.connect().subscribe(d => this.renderedData = d);
        this.bankmasterData = data;
      },
      error:(error) => {
        this.showSwalmessage(this.errorCodes.getErrorMessage(error.status), error.error, 'error', true)
      }
    })
  }

  tbl_FilterDatatable(value:string) {
    this.dataSource.filter = value.trim().toLocaleLowerCase();
  }

  showSwalmessage(message, text, icon, confirmButton): void {
    if (!confirmButton) {
        Swal.fire({ title: message, text: text, icon: icon, timer: 2000, showConfirmButton: false });
    } else {
      Swal.fire({ title: message, text: text, icon: icon, timer: 2000, showConfirmButton: true, confirmButtonText: "Yes", cancelButtonText: "No" });
    }
  }

  onSave(formValue){
    this.showLoader = true
    this.bankMasterService.create(formValue).subscribe({
      next: (data:any) => {
        if (data['status'] == 1) {
          this.showSwalmessage('Your record has been updated successfully!','','success',false)
        }
        else if(data['status'] == 2) {
          this.showSwalmessage('Your record has been added successfully!','','success',false)
        }
       this.getAllData();
        this.showList = true;
        this.listDiv = false;
        this.showLoader = false 
      },
      error:(error) => {
        this.showLoader = false
        this.showSwalmessage(this.errorCodes.getErrorMessage(error.status), error.error, 'error', true)
      }
    })
  }
  
  export_to_excel() {
    let XlsMasterData = this.dataSource.data.map(({bank_name,branch_name,bank_code,account_number,account_type,ifsc_code, iban_number,cash_credit_limit,
      address1 , address2,country_ref_id , state_ref_id,city_ref_id,pincode,nodal_acc_flag}) => 
      ({bank_name,branch_name,bank_code,account_number,account_type,ifsc_code, iban_number, cash_credit_limit,
         address1, address2,country_ref_id , state_ref_id,city_ref_id,pincode,nodal_acc_flag}));
    const workSheet = XLSX.utils.json_to_sheet(XlsMasterData);
    const workBook: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(workBook, workSheet, 'SheetName');
    XLSX.writeFile(workBook, 'Bank_data.xlsx');
  }

}

