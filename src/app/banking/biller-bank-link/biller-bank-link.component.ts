import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { ManageSecurityService } from 'src/app/core/service/manage-security.service';
import{ BillerBankLinkService } from 'src/app/shared/services/biller-bank-link.service'
import { BillerBankLink } from 'src/app/shared/models/biller-bank-link'
import Swal from 'sweetalert2';
import * as XLSX from 'xlsx';
import { ErrorCodes } from 'src/app/shared/error-codes';

@Component({
  selector: 'app-biller-bank-link',
  templateUrl: './biller-bank-link.component.html',
  styleUrls: ['./biller-bank-link.component.sass'],
  providers: [ErrorCodes]
})
export class BillerBankLinkComponent implements OnInit {
  @ViewChild('dataSourcePaginator',{read: MatPaginator}) dataSourcePaginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  dataSource: MatTableDataSource<BillerBankLink>;
  resultData: BillerBankLink[]; 

  displayColumns = [
    'actions',
    'biller_name',   
    'start_date',
    'end_date',
    'revision_status'
  ];

  renderedData: BillerBankLink[]; 
  screenName = 'Biller Bank Link';
  submitBtn = true;
  showLoader = false;
  rowData = [];
  listDiv:boolean = false;
  showList:boolean = true;
  sidebarData: any;
  billerBanklinkData = [];
  editFlag = '';
  versionCreationFlag: any = [];
  editSourceData : any = [];

  constructor(
    private manageSecurityService : ManageSecurityService,
    private billerBankLinkService : BillerBankLinkService,
    private errorCodes: ErrorCodes) { }

  ngOnInit(): void {
    this.getAllData();
    this.manageSecurityService.getAccessLeftPanel(localStorage.getItem('USER_ID'), this.screenName).subscribe({
      next: (data:any) => {
        this.sidebarData =data;  
      }
    })
  }

  editViewRecord(row, flag){
    this.rowData = row;
    this.showList = false;
    this.submitBtn = flag;
    this.listDiv = true;
    this.showLoader = false;
    this.editFlag = flag
  }

  showFormList(item: boolean) {
    if(item ===false){
      this.listDiv = true;
      this.showList = false;
    }
    else{
      this.listDiv = false;
      this.showList = true;
    }
  }

  onCancel(item: boolean){
    this.listDiv = item;
    this.showList = true;
    this.submitBtn = true;
    this.rowData = [];
  }
  
  getAllData(){
    this.billerBankLinkService.getAll(localStorage.getItem('COMPANY_TYPE')).subscribe({
      next: (data:any) =>{
        this.billerBanklinkData = data;
        this.dataSource = new MatTableDataSource(data);
        this.dataSource.paginator = this.dataSourcePaginator;
        this.dataSource.sort = this.sort;
        this.dataSource.connect().subscribe(d => this.renderedData = d);
        for (let value of data){
          if (value.revision_status == 'Effective') {
            this.versionCreationFlag.push(value.id);
          }
          if (value.revision_status == "Future"){
            this.editSourceData.push(value.revision_status)
          }
        }
      }
    });
  }
  
  tbl_FilterDatatable(value:string) {
    this.dataSource.filter = value.trim().toLocaleLowerCase();
  }

  showSwalmessage(message, text, icon, confirmButton): void {
    if (! confirmButton) {
        Swal.fire({ title: message, text: text, icon: icon, timer: 2000, showConfirmButton:false});
    } else {
      Swal.fire({ title: message, text: text, icon: icon, timer: 2000, showConfirmButton: true, confirmButtonText: "Yes", cancelButtonText: "No" });
    }
  }

  onSave(formValue){    
    this.showLoader = true;
    this.billerBankLinkService.create(formValue).subscribe({
      next: (data:any) => {
        if (data['status'] == 1 && formValue.revision_status == 'Future' && this.editFlag == 'versionCreate'){
          this.showSwalmessage('Version Created Successfully!','','success',false)
        }
        else if (data['status'] == 2) {
          this.showSwalmessage('Your record has been added successfully!','','success',false)
        }
        else if (data['status'] == 1) {
          this.showSwalmessage('Version has been updated successfully!','','success',false)
        }
        this.getAllData();
        this.showList = true;
        this.listDiv = false;
        this.showLoader = false 
      },
      error:(error) => {
        this.showLoader = false;
        this.showSwalmessage(this.errorCodes.getErrorMessage(error.status), error.error, 'error', true)
      }
    })
  }
  
  export_to_excel(){
    let XlsMasterData = this.dataSource.data.map(({biller_name,start_date,end_date,revision_status}) => 
      ({biller_name,start_date,end_date,revision_status}));
    const workSheet = XLSX.utils.json_to_sheet(XlsMasterData);
    const workBook: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(workBook, workSheet, 'SheetName');
    XLSX.writeFile(workBook, 'Biller_Bank_data.xlsx');
  }
}



