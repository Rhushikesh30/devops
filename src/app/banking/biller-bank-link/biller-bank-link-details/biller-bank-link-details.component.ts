import { Component, OnInit, Input, Output, EventEmitter, } from '@angular/core';
import { FormArray , FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BillerBankLinkService } from 'src/app/shared/services/biller-bank-link.service';
import Swal from "sweetalert2";
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-biller-bank-link-details',
  templateUrl: './biller-bank-link-details.component.html',
  styleUrls: ['./biller-bank-link-details.component.sass'],
  providers:[DatePipe]
})
export class BillerBankLinkDetailsComponent implements OnInit {
  @Input() rowData: [];
  @Input() editFlag:string;
  @Input() submitBtn: boolean;
  @Input() showLoader: boolean;
  @Input() bbpoubanklinkData: any;
  @Output() onSave = new EventEmitter<any>();
  @Output() onCancel = new EventEmitter<any>();

  billerBankLinkForm: FormGroup;
  billerData = [];
  bankData :any = [];
  newPayload = [];
  bank_inserted = [];
  bbpou_banks = [];
  btnVal = "Submit";
  todayDate = new Date().toJSON().split('T')[0];
  submitted = false;
  cancelFlag = true;
  COMPANY_TYPE: string;
  COMPANY_ID: string;
  getBillerName: any = [];

  constructor(
    private formBuilder: FormBuilder,
    private billerBankLinkService: BillerBankLinkService,
    private datepipe : DatePipe
    ) { }

  ngOnInit(): void {
    this.COMPANY_ID = localStorage.getItem('COMPANY_ID');
    this.COMPANY_TYPE = localStorage.getItem('COMPANY_TYPE')
    
    this.billerBankLinkForm = this.formBuilder.group({
      id: [''],
      biller_ref_id: [''],     
      aggregator_ref_id: [''],     
      bbpou_comp_ref_id: [''],     
      biller_name: [localStorage.getItem('COMPANY_NAME')],
      start_date: ['', Validators.required],
      end_date: ['2099-12-31'],
      revision_status: [''],
      initialItemRow: this.formBuilder.array([this.initialItemRow()])
    });

    if(!Array.isArray(this.rowData)){
      this.editViewRecord(this.rowData, this.editFlag);
    }

    if(this.COMPANY_TYPE == 'Biller'){
      this.billerBankLinkService.getBillersAllBanks(this.COMPANY_ID).subscribe({
        next:(data: any) => {
          this.bankData = data;
        }
      });
    }

    this. billerBankLinkForm.get("start_date").valueChanges.subscribe({
      next: (val:any) =>{
        if (val!='') {
          let todaydate = new Date().toJSON().split('T')[0];
          let startdate = this.datepipe.transform(val,'yyyy-MM-dd');
          if (startdate === todaydate) {
            this. billerBankLinkForm.get("revision_status").setValue("Effective");
          }
          else if(startdate > todaydate){
            this. billerBankLinkForm.get("revision_status").setValue("Future");
          }
        }
      } 
    });
  }
  
  uniqueBanksContols(gridRowID) {
    let gridRow = (<FormArray>(this.billerBankLinkForm.get('initialItemRow'))).at(gridRowID);
    let id = gridRow.get('bank_ref_id').value;
    for(let i=0; i<(<FormArray>(this.billerBankLinkForm.get('initialItemRow'))).length; i++){
      if(i==gridRowID){
        continue;
      }
      if ((<FormArray>(this.billerBankLinkForm.get('initialItemRow'))).at(i).get("bank_ref_id").value==id){
        (<FormArray>(this.billerBankLinkForm.get('initialItemRow'))).at(gridRowID).get("bank_ref_id").setValue('');
        Swal.fire({
          title: 'Oops...you selected the same bank twice!',
          icon: 'error',
          showConfirmButton: false
        })
        break;
      }
    }
  }

  percentageShareTotal(gridRowID){
    let gridRow = (<FormArray>(this.billerBankLinkForm.get('initialItemRow'))).at(gridRowID);
    if(gridRow.get('percentage_share').value<0){
      gridRow.get('percentage_share').setValue(0);
    }
    else if(gridRow.get('percentage_share').value>100){
      gridRow.get('percentage_share').setValue(100);
    }
    let per_sum = 0;
    for(let i=0; i<(<FormArray>(this.billerBankLinkForm.get('initialItemRow'))).length; i++){
      per_sum = per_sum + (<FormArray>(this.billerBankLinkForm.get('initialItemRow'))).at(i).get("percentage_share").value
      if (per_sum>100){
        (<FormArray>(this.billerBankLinkForm.get('initialItemRow'))).at(gridRowID).get("percentage_share").setValue('');
        Swal.fire({
          title: 'Oops...you exceeded the percentage share limit!',
          icon: 'error',
          showConfirmButton: false
        })
        break;
      }
    }
  }

  initialItemRow() {
    return this.formBuilder.group({
      bank_ref_id: ['', Validators.required],
      percentage_share: ['', Validators.required]
    });
  }

  get formArr() {
    return this.billerBankLinkForm.get('initialItemRow') as FormArray;
  }

  addNewRow() {
    this.formArr.push(this.initialItemRow());
  }

  deleteRow(index) {
    if (this.formArr.length == 1) {
      return false;
    } else {
      this.formArr.removeAt(index);
      return true;
    }
  }

  get f() {
    return this.billerBankLinkForm.controls;
  }  

  setExistingArray(initialArray = []): FormArray {
    const formArray = new FormArray([]);
    initialArray.forEach(element => {
      formArray.push(this.formBuilder.group({
        id: element.id,
        bank_ref_id: element.bank_ref_id,
        percentage_share: element.percentage_share
      }));
    });
    return formArray;
  }

  format_date(date){
    let d = new Date(date),
    month = '' + (d.getMonth() + 1),
    day = '' + d.getDate(),
    year = d.getFullYear();
    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;
    return [year, month,day ].join('-');
  }

  showSwalmessage(message, text, icon, confirmButton): void {
    if (! confirmButton) {
        Swal.fire({ title: message, text: text, icon: icon, timer: 2000, showConfirmButton: false });
    } else {
        Swal.fire({ title: message, text: text, icon: icon, timer: 2000, showConfirmButton: true, confirmButtonText: "Yes", cancelButtonText: "No" });
    }
  }

  onCancelForm(){
    this.cancelFlag=false
    this.billerBankLinkForm.reset();
    this.onCancel.emit(false);
  }

  editViewRecord(data, editFlag) {    
    this.billerBankLinkService.getBillersAllBanks(data.biller_ref_id).subscribe({
      next:(data: any) => {
        this.bankData = data;
      }
    });

    if(editFlag == 'view'){
      this.submitBtn = false;
      let a = data.start_date
      this.billerBankLinkForm.get("start_date").setValue(a)
    }

    if(editFlag== 'versionCreate'){
      this.btnVal = "Create Version";
      let currtDate = new Date().toJSON().split("T")[0];
      if(data.start_date == currtDate) {
        this.showSwalmessage("Cannot create version on the same day!",'',"warning",false);
        this.onCancelForm();
      }
    }

    this.billerBankLinkService.getbillerbankbyDataId(data.id).subscribe({
      next:(row: any) => {
        this.billerBankLinkForm.patchValue({
          id: row.id,
          biller_name: row.biller_name,
          biller_ref_id: row.biller_ref_id,
          aggregator_ref_id: row.aggregator_ref_id,
          bbpou_comp_ref_id: row.bbpou_comp_ref_id,
          start_date: row.start_date,
          end_date: row.end_date
        });
        this.newPayload = row.initialItemRow
        this.billerBankLinkForm.setControl('initialItemRow', this.setExistingArray(this.newPayload));
      }
    });
    this.showLoader = false;
  }



  onSubmit(){ 
    if (this.billerBankLinkForm.invalid){
      return;
    }    
    let perChecker: number= 0; 
    for(let i=0; i<((<FormArray>(this.billerBankLinkForm.get('initialItemRow'))).length); i++){
      perChecker = perChecker + Number((<FormArray>(this.billerBankLinkForm.get('initialItemRow'))).at(i).get("percentage_share").value);
    }
    if (perChecker==100.0000){  
      (<FormArray>(this.billerBankLinkForm.get('initialItemRow'))).get("percentage_share");
    } 
    else if(perChecker < 100){
      this.showSwalmessage("Percentage share limit should be 100%!",'',"warning",false);
      return;
    }
    let startdate1  = this.billerBankLinkForm.get('start_date').value
    let startdate2 = this.datepipe.transform(startdate1,'yyyy-MM-dd');
    this.billerBankLinkForm.get('start_date').setValue(startdate2)
    this.onSave.emit(this.billerBankLinkForm.value);        
  }


}

