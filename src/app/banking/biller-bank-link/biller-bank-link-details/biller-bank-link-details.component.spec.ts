import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BillerBankLinkDetailsComponent } from './biller-bank-link-details.component';

describe('BillerBankLinkDetailsComponent', () => {
  let component: BillerBankLinkDetailsComponent;
  let fixture: ComponentFixture<BillerBankLinkDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BillerBankLinkDetailsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BillerBankLinkDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
