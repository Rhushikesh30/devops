import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BillerBankLinkComponent } from './biller-bank-link.component';

describe('BillerBankLinkComponent', () => {
  let component: BillerBankLinkComponent;
  let fixture: ComponentFixture<BillerBankLinkComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BillerBankLinkComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BillerBankLinkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
